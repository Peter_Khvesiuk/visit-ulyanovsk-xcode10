//
//  NotificationService.h
//  PushMedia
//
//  Created by Petr Khvesiuk on 03/06/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <UserNotifications/UserNotifications.h>

@interface NotificationService : UNNotificationServiceExtension

@end
