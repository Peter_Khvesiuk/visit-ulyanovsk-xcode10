//
//  NotificationService.m
//  PushMedia
//
//  Created by Petr Khvesiuk on 03/06/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "NotificationService.h"

@interface NotificationService ()

@property (nonatomic, strong) void (^contentHandler)(UNNotificationContent *contentToDeliver);
@property (nonatomic, strong) UNMutableNotificationContent *bestAttemptContent;

@end

@implementation NotificationService

- (void)didReceiveNotificationRequest:(UNNotificationRequest *)request withContentHandler:(void (^)(UNNotificationContent * _Nonnull))contentHandler {
    self.contentHandler = contentHandler;
    self.bestAttemptContent = [request.content mutableCopy];
    
    
    
    NSDictionary *userInfo = request.content.userInfo;
    if (userInfo == nil) {
        [self contentComplete];
        return;
    }
    
    //    self.bestAttemptContent.title = [NSString stringWithFormat:@"%@ [modified]", self.bestAttemptContent.title];
    
    //    self.contentHandler(self.bestAttemptContent);
    UNNotificationCategory *category;
    
    if ([userInfo[@"showContent"][@"identifier"] isEqualToString:@"btnShowContent"]) {
        NSString *Button1Title = userInfo[@"showContent"][@"btnTitle"];
        NSString *Button1Id = userInfo[@"showContent"][@"identifier"];//btnShowMeObject
        NSLog(@"Button1Id = %@",Button1Id);
        UNNotificationAction *ActionBtn = [UNNotificationAction actionWithIdentifier:Button1Id title:Button1Title options:UNNotificationActionOptionForeground];
//        UNNotificationAction *DismissBtn = [UNNotificationAction actionWithIdentifier:@"Dismiss" title:@"Dismiss" options:UNNotificationActionOptionDestructive];
        
        category = [UNNotificationCategory categoryWithIdentifier:@"pushMedia" actions:@[ActionBtn] intentIdentifiers:@[] options:UNNotificationCategoryOptionNone];

    } else {
        category = [UNNotificationCategory categoryWithIdentifier:@"pushMedia" actions:@[] intentIdentifiers:@[] options:UNNotificationCategoryOptionNone];
    }
    
    NSSet *categories = [NSSet setWithObject:category];
    [[UNUserNotificationCenter currentNotificationCenter] setNotificationCategories:categories];
    
    
    
    
    
    NSString *mediaUrl = userInfo[@"mediaUrl"];
    NSString *mediaType = userInfo[@"mediaType"];
    
    if (mediaUrl == nil || mediaType == nil) {
        [self contentComplete];
        return;
    }
    
    // load the attachment
    [self loadAttachmentForUrlString:mediaUrl
                            withType:mediaType
                   completionHandler:^(UNNotificationAttachment *attachment) {
                       if (attachment) {
                           self.bestAttemptContent.attachments = [NSArray arrayWithObject:attachment];
                       }
                       [self contentComplete];
                   }];
    
    // Modify the notification content here...
    
}

//- (void)serviceExtensionTimeWillExpire {
//    // Called just before the extension will be terminated by the system.
//    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
//    self.contentHandler(self.bestAttemptContent);
//}

- (void)serviceExtensionTimeWillExpire {
    // Called just before the extension will be terminated by the system.
    // Use this as an opportunity to deliver your "best attempt" at modified content, otherwise the original push payload will be used.
    [self contentComplete];
}

- (void)contentComplete {
    self.contentHandler(self.bestAttemptContent);
}

- (NSString *)fileExtensionForMediaType:(NSString *)type {
    NSString *ext = type;
    
    if ([type isEqualToString:@"image"]) {
        ext = @"jpg";
    }
    
    if ([type isEqualToString:@"video"]) {
        ext = @"mp4";
    }
    
    if ([type isEqualToString:@"audio"]) {
        ext = @"mp3";
    }
    NSLog(@"fileExtensionForMediaType type = %@", type);
    return [@"." stringByAppendingString:ext];
}

- (void)loadAttachmentForUrlString:(NSString *)urlString withType:(NSString *)type completionHandler:(void(^)(UNNotificationAttachment *))completionHandler  {
    NSLog(@"loadAttachmentForUrlString urlString = %@", urlString);
    __block UNNotificationAttachment *attachment = nil;
    NSURL *attachmentURL = [NSURL URLWithString:urlString];
    NSString *fileExt = [self fileExtensionForMediaType:type];
    
    NSLog(@"loadAttachmentForUrlString fileExt = %@", fileExt);
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session downloadTaskWithURL:attachmentURL
                completionHandler:^(NSURL *temporaryFileLocation, NSURLResponse *response, NSError *error) {
                    if (error != nil) {
                        NSLog(@"hhhhhhh2 = %@", error.localizedDescription);
                    } else {
                        NSFileManager *fileManager = [NSFileManager defaultManager];
                        NSURL *localURL = [NSURL fileURLWithPath:[temporaryFileLocation.path stringByAppendingString:fileExt]];
                        [fileManager moveItemAtURL:temporaryFileLocation toURL:localURL error:&error];
                        
                        NSError *attachmentError = nil;
                        attachment = [UNNotificationAttachment attachmentWithIdentifier:@"" URL:localURL options:nil error:&attachmentError];
                        if (attachmentError) {
                            NSLog(@"hhhhhhh = %@", attachmentError.localizedDescription);
                        }
                    }
                    completionHandler(attachment);
                }] resume];
}

@end
