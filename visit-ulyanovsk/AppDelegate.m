//
//  AppDelegate.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 19.12.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import "AppDelegate.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "PKDataManager.h"
#import "PKServerManager.h"
#import <Harpy/Harpy.h>

#import "PKMainViewController.h"

@interface AppDelegate ()<HarpyDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    if([[PKDataManager sharedManager] isFirstTimeRun]){
        // . NSLog(@"isFirstTimeRun!");
    }
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions]; //Facebook init
    
    
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    

    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    
    #warning isdebug mode!
    if(!isDebug){
        
        
    
        UIViewController *firstViewControllerToShow = nil;

        if (![[PKDataManager sharedManager] isSkipVideo]) {
            firstViewControllerToShow =
            [storyboard instantiateInitialViewController];
        } else {
            firstViewControllerToShow = [storyboard instantiateViewControllerWithIdentifier:@"StartSkipVideo"];
        }
        
        
        self.window.rootViewController = firstViewControllerToShow;
    } else {
        self.window.rootViewController = [storyboard instantiateInitialViewController];
    }
    
    
    
    [self.window makeKeyAndVisible];
    
    // Set the UIViewController that will present an instance of UIAlertController
    [[Harpy sharedInstance] setPresentingViewController:_window.rootViewController];
    
    // (Optional) Set the Delegate to track what a user clicked on, or to use a custom UI to present your message.
    [[Harpy sharedInstance] setDelegate:self];
    
    // (Optional) When this is set, the alert will only show up if the current version has already been released for X days.
    // By default, this value is set to 1 (day) to avoid an issue where Apple updates the JSON faster than the app binary propogates to the App Store.
        [[Harpy sharedInstance] setShowAlertAfterCurrentVersionHasBeenReleasedForDays:0];
    
    // (Optional) The tintColor for the alertController
    //    [[Harpy sharedInstance] setAlertControllerTintColor:[UIColor purpleColor]];
    
    // (Optional) Set the App Name for your app
    //    [[Harpy sharedInstance] setAppName:@"iTunes Connect Mobile"];
    
    /* (Optional) Set the Alert Type for your app
     By default, Harpy is configured to use HarpyAlertTypeOption */
    [[Harpy sharedInstance] setAlertType:HarpyAlertTypeOption];
    
    /* (Optional) If your application is not available in the U.S. App Store, you must specify the two-letter
     country code for the region in which your applicaiton is available. */
    //    [[Harpy sharedInstance] setCountryCode:@"en-US"];
    
    /* (Optional) Overrides system language to predefined language.
     Please use the HarpyLanguage constants defined in Harpy.h. */
    //    [[Harpy sharedInstance] setForceLanguageLocalization:HarpyLanguageRussian];
    
    // Turn on Debug statements
    [[Harpy sharedInstance] setDebugEnabled:false];
    
    // Perform check for new version of your app
    [[Harpy sharedInstance] checkVersion];
    
    
    
    
    return YES;
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.

    
    //    [self saveContext];
    
    [[PKDataManager sharedManager] saveContext];
//    // . NSLog(@"Freedom");
}




//#pragma mark - Core Data stack
//
//@synthesize persistentContainer = _persistentContainer;
//
//- (NSPersistentContainer *)persistentContainer {
//    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
//    @synchronized (self) {
//        if (_persistentContainer == nil) {
//            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"coredatamodel"];
//            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
//                if (error != nil) {
//                    // Replace this implementation with code to handle the error appropriately.
//                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                    
//                    /*
//                     Typical reasons for an error here include:
//                     * The parent directory does not exist, cannot be created, or disallows writing.
//                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                     * The device is out of space.
//                     * The store could not be migrated to the current model version.
//                     Check the error message to determine what the actual problem was.
//                    */
//                    // . NSLog(@"Unresolved error %@, %@", error, error.userInfo);
//                    abort();
//                }
//            }];
//        }
//    }
//    
//    return _persistentContainer;
//}
//
//#pragma mark - Core Data Saving support
//
//- (void)saveContext {
//    NSManagedObjectContext *context = self.persistentContainer.viewContext;
//    NSError *error = nil;
//    if ([context hasChanges] && ![context save:&error]) {
//        // Replace this implementation with code to handle the error appropriately.
//        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//        // . NSLog(@"Unresolved error %@, %@", error, error.userInfo);
//        abort();
//    }
//}


#pragma mark - Facebook
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}
//#pragma mark - HarpyDelegate
//- (void)harpyDidShowUpdateDialog
//{
//    NSLog(@"%s", __FUNCTION__);
//}
//
//- (void)harpyUserDidLaunchAppStore
//{
//    NSLog(@"%s", __FUNCTION__);
//}
//
//- (void)harpyUserDidSkipVersion
//{
//    NSLog(@"%s", __FUNCTION__);
//}
//
//- (void)harpyUserDidCancel
//{
//    NSLog(@"%s", __FUNCTION__);
//}
//
//- (void)harpyDidDetectNewVersionWithoutAlert:(NSString *)message
//{
//    NSLog(@"%@", message);
//}

#pragma mark - Push Notifications
- (void)registerUserNotification
{
//    NSLog(@"registerUserNotification");
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
        if( !error ) {
            // required to get the app to do anything at all about push notifications
            dispatch_async(dispatch_get_main_queue(), ^{
                [[UIApplication sharedApplication] registerForRemoteNotifications];
            });
            NSLog( @"Push registration success." );
        } else {
            NSLog( @"Push registration FAILED" );
            NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
            NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
        }
    }];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
//    NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken %@", [deviceToken base64EncodedDataWithOptions:0]);
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as per need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    NSLog(@"Device Token is \n%@",token);
    
    [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:@"PushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[PKServerManager sharedManager] handshakeWithServer];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"PushToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[PKServerManager sharedManager] handshakeWithServer];
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    // custom code to handle push while app is in the foreground
    NSLog(@"%@", notification.request.content.userInfo);
    
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)(void))completionHandler
{
    
//    Если несколько конопок
//    if ([response.actionIdentifier isEqualToString:@"btnShowContent"]) {
//        NSLog(@"response BTN 1 actionURL = %@, identifier == %@", response.notification.request.content.userInfo[@"showContent"][@"identifier"], response.notification.request.identifier);
//    } else {
//        NSLog(@"response BTN 2 actionURL = %@, identifier == %@",response.notification.request.content.userInfo[@"showContent"][@"identifier"], response.notification.request.identifier);
//    }

    
    if ([response.notification.request.content.userInfo[@"showContent"][@"identifier"] isEqualToString:@"btnShowContent"]) {
        
        int contentId =  (int)[response.notification.request.content.userInfo[@"showContent"][@"contentId"] integerValue];
        if(contentId > 0){
            
            NSString *contentType = response.notification.request.content.userInfo[@"showContent"][@"contentType"];
            
//            NSLog(@"contentId = %i, contentType == %@", contentId, contentType);
            
            if([contentType isEqualToString:@"mapObject"] || [contentType isEqualToString:@"mapGeoRoute"] || [contentType isEqualToString:@"calendar"] || [contentType isEqualToString:@"tip"]){
                
                UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                UINavigationController * navControl = (UINavigationController*)[storyboard instantiateViewControllerWithIdentifier:@"StartSkipVideo"];
                NSArray *viewContrlls=[navControl viewControllers];
                for( int i=0;i<[ viewContrlls count];i++){
                    id vcs=[viewContrlls objectAtIndex:i];
                    if([vcs isKindOfClass:[PKMainViewController class]]){
                        
                        PKMainViewController *vc = (PKMainViewController*)vcs;
                        [vc setInstanceView:YES];
                        vc.contentID = contentId;
                        vc.contentType = contentType;
                        vc.needToShowContentAfterPush = YES;
                        self.window.rootViewController = navControl;
                    }
                }
            }
        }
    }
    
//    NSLog( @"Handle push from background or closed" );
    // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
    NSLog(@"%@", response.notification.request.content.userInfo);
    completionHandler();
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
   openSettingsForNotification:(UNNotification *)notification{
    //Open notification settings screen in app
}
@end
