//
//  PKUserInteraction.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 28.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKUserInteraction.h"
#import "PKDataManager.h"
#import "PKServerManager.h"



@implementation PKUserInteraction


+(PKUserInteraction*) sharedManager{
    
    static PKUserInteraction* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PKUserInteraction alloc] init];
    });
    
    return manager;
}

- (void) switchObjectLike:(NSManagedObjectID*) objectID{
    
    
    
    
//    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
//    f.numberStyle = NSNumberFormatterDecimalStyle;
//    NSString* mapObjectIDString = [f stringFromNumber:[NSNumber numberWithInteger:mapObjectid]];
    // . NSLog(@"idobj = %@", [NSString stringWithFormat: @"%d", mapObjectIdServer]);
    // . NSLog(@"mapObjectid = %@, %@", [NSString stringWithFormat: @"%d", mapObjectIdServer], objectID);
    NSString* clientID = [[PKDataManager sharedManager] getClientID];
    if(![clientID isEqualToString:@"null"]){
        
        
        NSArray* objectToServer = [[PKDataManager sharedManager] switchObjectLike:objectID];
        NSInteger mapObjectIdServer = [[objectToServer objectAtIndex:0] integerValue];
        NSString* objectOrGeo = [objectToServer objectAtIndex:1];
    
        [[PKServerManager sharedManager] sendLiketoServerFor:(NSString *) objectOrGeo
                                                       andId:[NSString
                                            stringWithFormat: @"%ld", (long)mapObjectIdServer]
                                                andOnSuccess:^(NSArray *posts) {
                                                             
                                                         }
                                                    onFailure:^(NSError *error, NSInteger statusCode) {
                                                        NSArray* objectToServerFailed = [[PKDataManager sharedManager] switchObjectLike:objectID];
                                                        NSLog(@"objectToServerFailed = %@",objectToServerFailed);
                                                            }];
    } else {
        
        id navigationCont = [[[UIApplication sharedApplication] keyWindow] rootViewController];
//        NSLog(@"navigationController = %@",navigationCont);
        UINavigationController *navigationController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
        NSArray *viewContrlls=[navigationController viewControllers];
        for( int i=0;i<[ viewContrlls count];i++){
            id map=[viewContrlls objectAtIndex:i];
//            NSLog(@"viewContrlls = %@", [map class]);
            //if([map isKindOfClass:[PKMainMapViewController class]]){
                // A is your class where to popback
                //[[self navigationController] popToRootViewControllerAnimated:YES];
                //return;
            //}
        }
        
        
//        NSLog(@"navigationController = %@",navigationController);
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *vc= [storyBoard instantiateViewControllerWithIdentifier:@"ProfileLogin"]; //PKProfileLoginVC
        [navigationController pushViewController:vc animated:YES];
    }
    
    
}


@end
