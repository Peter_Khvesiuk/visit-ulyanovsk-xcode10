//
//  PKUserInteraction.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 28.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@class NSManagedObjectID;

@interface PKUserInteraction : NSObject


+ (PKUserInteraction*)sharedManager;


- (void)switchObjectLike:(NSManagedObjectID*) objectID;


@end
