//
//  PKGeoRouteDetailVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 07.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAKImageGalleryView.h"

@class PKGeoRoute;

@interface PKGeoRouteDetailVC : UIViewController
@property (strong, nonatomic) PKGeoRoute *geoRoute;
@property (nonatomic) int geoRouteId;
@property (weak, nonatomic) MAKImageGalleryView *imageGalleryView;
@end
