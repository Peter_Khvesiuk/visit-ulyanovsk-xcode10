//
//  PKGeoRouteDetailVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 07.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKGeoRouteDetailVC.h"

//#import "PKMapObjectInfoBlocksView.h"

#import "PKGeoRoute+CoreDataClass.h"
#import "PKMapObj+CoreDataClass.h"
#import "PKTypeObj+CoreDataClass.h"

#import "PKObjImages+CoreDataClass.h"

#import "PKUOStyleKit.h"

#import "PKInterfaceManager.h"
#import "PKTopNavButtonView.h"

//#import "PKObjectInfoBlockCVC.h"
//#import <AFNetworking/UIImageView+AFNetworking.h>
//#import "AFNetworking+ImageActivityIndicator.h"

//#import "PKObjectInfoBlockCV.h"

#import "PKMainMapViewController.h"

#import "PKUserInteraction.h"
#import "PKButtonLike.h"

#import <AFNetworking/AFImageDownloader.h>

#import "UILabel+Copyable.h"

#import "PKPageViewController.h"
#import "PKComercialOrganization+CoreDataClass.h"

#import "PKInfoRouteAdressViewController.h"
#import "PKInfoGeoPhoneViewController.h"
#import "PKInfoEmailViewController.h"
#import "PKInfoSiteViewController.h"

#import "PKDataManager.h"
//static CGFloat regularFZMargin = 14.0;
static CGFloat PKGeoRouteCellTextAreaHeight  = 44.0;

@interface PKGeoRouteDetailVC () <MAKImageGalleryViewDataSource,
//PKMapObjectInfoBlocksViewDataSource,
UIScrollViewDelegate>{
    PKPageViewController *vc;
}
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;

//, PKObjectInfoBlockCVDataSource, PKObjectInfoBlockViewCollectionDataSource>
//@property (weak, nonatomic) IBOutlet MAKImageGalleryView *imageGalleryView;
//@property (weak, nonatomic) PKMapObjectInfoBlocksView *infoBlocksView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subTitleLabel;
@property (strong, nonatomic) UITextView *descriptionLabel;
@property (strong, nonatomic) UILabel *categoryLabel;
@property (strong, nonatomic) UIButton* btnBack;
@property (retain, nonatomic) UIScrollView * scrollView;
@property (assign, nonatomic) BOOL scrollWasAnimated;
//@property (strong, nonatomic) IBOutlet PKObjectInfoBlockCV *infoBlocksView;

//@property (strong, nonatomic) PKObjectInfoBlockViewCollection *infoBlockCVC;

@property (strong, nonatomic) UIImageView *imagggview;
@property (strong, nonatomic) NSMutableArray *downloads;

@property (strong, nonatomic) UISelectionFeedbackGenerator *feedbackGenerator;

@end

@implementation PKGeoRouteDetailVC
@synthesize geoRoute;
@synthesize geoRouteId;
@synthesize imageGalleryView;

//- (instancetype)initWithMapObject:(PKMapObj *)mapObject {
//    self = [super init];
//    if (self) {
//        self.mapObject = mapObject;
////        self.title = mapObject.title;
//    }
//    return self;
//}
-(void)dealloc{
    // . NSLog(@"\ndealloc PKGeoRouteDetailVC");
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // . NSLog(@"viewDidAppear");
    
    self.topNavButtonView.open = YES;
//    if(self.btnBack != nil){
//        [self.view addSubview:self.btnBack];
//        CGRect frame = self.btnBack.frame;
//        frame.origin.x = 0;
//        [UIView animateWithDuration:0.2
//                              delay:0.0
//                            options: UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             self.btnBack.frame = frame;
//                         }
//                         completion:^(BOOL finished){
//                             // . NSLog(@"complete");
//                             self.scrollWasAnimated = YES;
//                         }];
//    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.tabBarController.tabBar setHidden:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.geoRoute == nil){
        self.geoRoute = [[PKDataManager sharedManager] oneGeoRouteWithId: geoRouteId];
    }
    
    
    self.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    
    
    
    self.scrollWasAnimated = NO;
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];//
    [self.view addSubview:_scrollView];
    self.scrollView.delegate = self;
    
    
    MAKImageGalleryView * imageGalleryView = [[MAKImageGalleryView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, zoomDetailImageHeight)];
    self.imageGalleryView = imageGalleryView;
    self.imageGalleryView.imageGalleryDataSource = self;
    self.imageGalleryView.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor];
    [_scrollView addSubview:self.imageGalleryView];
    
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(zoomDetailImageHeight+10.f, 0, 0, 0);
    
    CGRect sizeButtonLike = CGRectMake(_scrollView.frame.size.width - PKGeoRouteCellTextAreaHeight - regularFZMargin, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height + regularFZMargin, PKGeoRouteCellTextAreaHeight, PKGeoRouteCellTextAreaHeight);
    PKButtonLike * likesButton = [[PKButtonLike alloc] initWithFrame:sizeButtonLike];
    [likesButton setImageWithLikeEnabled:self.geoRoute.mylike withLikeCount:self.geoRoute.likes andIsDetail:YES];
    [likesButton addTarget:self
                    action:@selector(pushBtnLike:)
          forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:likesButton];
    
    
    
    
    
    
    
    
    
    
    
    
    self.titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:detailFZFontSizeTitle];
    _titleLabel.numberOfLines = 0;
    _titleLabel.text = self.geoRoute.title;
    [_scrollView addSubview:_titleLabel];
    CGSize titleLabelSize = [_titleLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - PKGeoRouteCellTextAreaHeight - regularFZMargin * 2.0), CGFLOAT_MAX)];
    _titleLabel.frame = CGRectMake(regularFZMargin, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height + regularFZMargin, titleLabelSize.width, titleLabelSize.height);
    
    //    // . NSLog(@"_titleLabel.frame = %@ %f %f %f %f",self.mapObject.title, _titleLabel.frame.origin.x, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    
    
    self.subTitleLabel = [[UILabel alloc] init];
    _subTitleLabel.font = [UIFont systemFontOfSize:detailFZFontSizeDescription];
    _subTitleLabel.numberOfLines = 0;
//    _subTitleLabel.text = self.mapObject.subtitle;
    [_scrollView addSubview:_subTitleLabel];
    CGSize subTitleLabelSize = [_subTitleLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - PKGeoRouteCellTextAreaHeight -regularFZMargin - regularFZMargin), CGFLOAT_MAX)];
    _subTitleLabel.frame = CGRectMake(regularFZMargin, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + regularFZMargin, subTitleLabelSize.width, subTitleLabelSize.height);
    
    
    //    // . NSLog(@"subTitleLabel = %f %f %f %f", _subTitleLabel.frame.origin.x, _subTitleLabel.frame.origin.y, _subTitleLabel.frame.size.width, _subTitleLabel.frame.size.height);
    //    // . NSLog(@"subTitleLabel = %f", _subTitleLabel.frame.origin.y);
    //    // . NSLog(@"subTitleLabel = %f", _subTitleLabel.frame.size.height);
    //    // . NSLog(@"subTitleLabel = %f", regularFZMargin);
    //    // . NSLog(@"subTitleLabel = %f", _subTitleLabel.frame.origin.y + _subTitleLabel.frame.size.height + regularFZMargin);
    
    self.categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(regularFZMargin, _subTitleLabel.frame.origin.y + _subTitleLabel.bounds.size.height + regularFZMargin, imageGalleryView.frame.size.width - regularFZMargin - regularFZMargin, 20.f)];
    _categoryLabel.font = [UIFont systemFontOfSize:16.0];
    _categoryLabel.numberOfLines = 1;
    self.categoryLabel.text = self.geoRoute.types.typeObjValue;
    
    
    // . NSLog(@"categoryLabel = %f %f %f %f", _categoryLabel.frame.origin.x, _categoryLabel.frame.origin.y, _categoryLabel.frame.size.width, _categoryLabel.frame.size.height);
    
    
    
    
    //    // . NSLog(@"gh imageGalleryView = %f  %f", self.imageGalleryView.frame.origin.y, self.imageGalleryView.frame.size.height);
    //    // . NSLog(@"gh subTitleLabel = %f  %f", self.subTitleLabel.frame.origin.y, self.subTitleLabel.frame.size.height);
    //    // . NSLog(@"gh categoryLabel = %f  %f", self.categoryLabel.frame.origin.y, self.categoryLabel.frame.size.height);
    __weak PKGeoRouteDetailVC *weakSelf = self;
    NSDictionary* blocksArray = [self blocksArray];
    // . NSLog(@"JKD self = %@", self);
    CGRect infoBlockFrame;
    
    if([blocksArray[@"classes"] count] != 0){
        // . NSLog(@"JKD self = %@", self);
        vc = [[PKPageViewController alloc]initWithViewControllerClasses:blocksArray[@"classes"] andIconImage:blocksArray[@"images"] andIconImageSelected:blocksArray[@"imagesSelected"] andObject: self.geoRoute andSelfVC: self];
        
        
        //Установите ширину каждого элемента
        vc.itemWidth = 60;
        //Выберите стиль подчеркивания
        vc.style = PKTopViewStyleNOLine;//PKTopViewStyleLine;
        //（Не подчеркнул）
        //        vc.style = ASTopViewStyleNOLine;
        //Установить по обе стороны пустого Ширина
        //        vc.sideBothWidth = 20;
        //Установить нормальный цвет шрифта
        //        vc.normalTitleColor = [UIColor orangeColor];
        //Настройки, выбранные при изменении шрифта, цвета
        //        vc.selectTitleColor = [UIColor blueColor];
        //Установить верхний элемент Цвет фона
        vc.topViewBackGroundColor = [UIColor clearColor];
        //Задать цвет подчеркивания
        //        vc.lineColor = [UIColor blueColor];
        //Набор всех предметов Ширина массива
        //    vc.itemWidthArray = @[@(150),@(80),@(70),@(90),@(150)];
        
        //#warning device size
        CGFloat marginLeftBlock = 50.f;//regularFZMargin;
        
        infoBlockFrame = CGRectMake(marginLeftBlock, _categoryLabel.frame.origin.y + _categoryLabel.frame.size.height + 20.f, self.view.bounds.size.width - marginLeftBlock, 160.f);
        //        CGSize cellSize = CGSizeMake(infoBlockFrame.size.width/2 - 10.f, infoBlockFrame.size.height/2 - 5.f);
        //        UIView* infoBlocks = [[UIView alloc] initWithFrame:infoBlockFrame];
        //        [infoBlocks setBackgroundColor: [UIColor redColor]];
        
        
        
        
        [self addChildViewController:vc];
        vc.view.frame = infoBlockFrame;
        [_scrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    } else {
        infoBlockFrame = CGRectMake(0, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + 20.f, 0, 0);
    }
        
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, _categoryLabel.frame.origin.y + _categoryLabel.frame.size.height - 50.f, self.view.bounds.size.width + 70.f, zoomDetailImageHeight)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    vc.view.clipsToBounds = YES;
    [_scrollView insertSubview:backgroundInfoBlockImage atIndex:0];
    //    [[self.imageGalleryView superview] sendSubviewToBack : backgroundInfoBlockImage];
    
    
//    self.infoBlocksView = infoBlocks;
//    [_scrollView addSubview:self.infoBlocksView];
//    self.infoBlocksView.infoBlocksDataSource = self;
    
    
    
    
    self.descriptionLabel = [[UITextView alloc] init];
    _descriptionLabel.font = [UIFont systemFontOfSize:16.0];
    //    _descriptionLabel.textAlignment = UITextAlignmentRight;
    _descriptionLabel.textAlignment = NSTextAlignmentJustified;
    _descriptionLabel.selectable = YES;
    _descriptionLabel.editable = NO;
    _descriptionLabel.scrollEnabled = NO;
    _descriptionLabel.backgroundColor = [UIColor clearColor];
    //    _descriptionLabel.lineBreakMode = NSLineBreakByCharWrapping;
    //    _descriptionLabel.numberOfLines = 0;
    //    _descriptionLabel.copyingEnabled = YES;
    _descriptionLabel.text = self.geoRoute.descriptions;
    [_scrollView addSubview:_descriptionLabel];
    //    CGSize lLabelSize = [self.mapObject.descr sizeWithFont: _descriptionLabel.font forWidth:(_scrollView.frame.size.width - regularFZMargin * 2.0) lineBreakMode:_descriptionLabel.lineBreakMode];
    CGSize descriptionSize = [_descriptionLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin * 2.0), CGFLOAT_MAX)];
    _descriptionLabel.frame = CGRectMake(regularFZMargin, infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin, descriptionSize.width, descriptionSize.height);
    
    CGFloat contentHeight = _descriptionLabel.frame.origin.y + _descriptionLabel.frame.size.height + regularFZMargin;
    if(contentHeight < self.view.bounds.size.height){
        contentHeight = self.view.bounds.size.height + 20.f;
    }
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, contentHeight);
    
    
    
    
    //+++++++++++++++++++++++++++++
    
//    UIButton* btnBack = [PKInterfaceManager slimNavbar:self];
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
//    self.btnBack = btnBack;
    
//    if(btnBack != nil){
//        [self.view addSubview:self.btnBack];
//    }
    
    //    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    //+++++++++++++++++++++++++++++
    
    
}
//    self.objectInfoCollectionViewHeight.constant = self.objectInfoCollectionView.contentSize.height;

//    // . NSLog(@"self.infoBlocksView %@", self.infoBlocksView);
//    self.infoBlocksView.blockViewDataSource = self;
//    // . NSLog(@"self.infoBlocksView.blockViewDataSource %@", self.infoBlocksView.blockViewDataSource);



//    // . NSLog(@"self.infoBlockCVC %@", self.infoBlockCVC);
//    self.infoBlockCVC.blockViewCollectionDataSource = self;
//    // . NSLog(@"self.infoBlockCVC.blockViewCollectionDataSource %@", self.infoBlockCVC.blockViewCollectionDataSource);

//    self.infoBlockCVC.blockViewDataSource = self;

//    self.titleLabel.text = self.mapObject.title;
//    // . NSLog(@"self.mapObject.title =%@", self.mapObject.title);

//    // . NSLog(@"self.mapObject.title =%@", self.mapObject.);



//- (void)startDisplayLinkIfNeeded
//{
//    if (!_displayLink) {
//        // do not change the method it calls as its part of the GLKView
//        _displayLink = [CADisplayLink displayLinkWithTarget:self.view selector:@selector(display)];
//        [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:UITrackingRunLoopMode];
//    }
//}
-(NSDictionary*)blocksArray{
    NSMutableArray* classMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* imageMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* imageSelectedMutableArray = [[NSMutableArray alloc] init];
//    PKComercialOrganization * org = self.geoRoute.geoOrganization;

    // . NSLog(@"self.geoRoute.geoOrganization %@", self.geoRoute.geoOrganization.nameOrg);
    
//    NSArray *workingTime = [self.mapObject.workingTime allObjects];
//    if(!([workingTime count] == 0)){
//        [classMutableArray addObject:[PKBlockTimeViewController class]];
//        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockTimeWithIconColor:[PKUOStyleKit uOTextColor]]];
//        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockTimeWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
//    }
//
    
    if(self.geoRoute.geoJSON != nil){
        [classMutableArray addObject:[PKInfoRouteAdressViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(self.geoRoute.geoOrganization != nil){
        if(![self.geoRoute.geoOrganization.phoneOrg isEqualToString:@"0"]){
            [classMutableArray addObject:[PKInfoGeoPhoneViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(![self.geoRoute.geoOrganization.emailOrg isEqualToString:@""]){
            [classMutableArray addObject:[PKInfoEmailViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(![self.geoRoute.geoOrganization.siteOrg isEqualToString:@""]){
            [classMutableArray addObject:[PKInfoSiteViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
    }
    
//
//    
//    
//    
//    
//    if(mapObject.wiFi){
//        [classMutableArray addObject:[PKBlockWiFiViewController class]];
//        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockWiFiWithIconColor:[PKUOStyleKit uOTextColor]]];
//        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockWiFiWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
//    }
//    if(mapObject.payCard){
//        [classMutableArray addObject:[PKInfoPayCardViewController class]];
//        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCardWithIconColor:[PKUOStyleKit uOTextColor]]];
//        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCardWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
//    }
    
    
    NSMutableDictionary* mutableDictionary = [[NSMutableDictionary alloc] init];
    [mutableDictionary setValue:classMutableArray forKey:@"classes"];
    [mutableDictionary setValue:imageMutableArray forKey:@"images"];
    [mutableDictionary setValue:imageSelectedMutableArray forKey:@"imagesSelected"];
    NSDictionary* dict = [[NSDictionary alloc] initWithDictionary:mutableDictionary];
    return dict;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.feedbackGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [self.feedbackGenerator prepare];
}

// Do any additional setup after loading the view.
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGFloat y = scrollView.contentOffset.y;
    // . NSLog(@"y = %.f", y);
    if(y< -64.f){
//        [self.topNavButtonView setHideFraction:( 1 - (-1* (y+ 64.f)) / ( 32.f) )];
        // Keep the generator in a prepared state.
        CGFloat fraction = ( 1 - (-1* (y+ 64.f)) / ( 32.f) );
        
        [self.topNavButtonView setHideFraction:fraction];
        if(fraction>0){
            
            //            NSLog(@"y = %f fraction = %f YES", y, fraction);
        } else {
            [self.feedbackGenerator selectionChanged];
            self.feedbackGenerator = nil;
            //            NSLog(@"y = %f fraction = %f", y, fraction);
        }
    }
//        if(self.btnBack != nil){
//            CGFloat x = (y+ 64.f) *4;
//            [self.btnBack setFrame:CGRectMake(x, self.btnBack.frame.origin.y, self.btnBack.frame.size.width, self.btnBack.frame.size.height)];
//        }
//    } else if(self.scrollWasAnimated){
//        [self.btnBack setFrame:CGRectMake(0, self.btnBack.frame.origin.y, self.btnBack.frame.size.width, self.btnBack.frame.size.height)];
//    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    //    NSLog(@"y2 = %f", scrollView.contentOffset.y);
    if(scrollView.contentOffset.y>= -64.f){
        
        [self.topNavButtonView setHideFraction:1.f];
    }
    self.feedbackGenerator = nil;
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    
    CGFloat y = scrollView.contentOffset.y;
    //    NSLog(@"y3 = %f", y );
    if(y< -(64.f + 32.f)){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    self.feedbackGenerator = nil;
}
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    // . NSLog(@"scrollView.contentOffset2 %.f", scrollView.contentOffset.y);
//
//    CGFloat y = scrollView.contentOffset.y;
//    if(y> 250.f){
//        if(y< 300.f){
//            CGFloat alpha = 0.8f * 50.f / (300.f - y);
//            self.navigationController.view.backgroundColor = [UIColor colorWithWhite:1.f alpha:alpha];
//            [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                          forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
//            self.navigationController.navigationBar.shadowImage = [UIImage new];
//        } else {
//            self.navigationController.view.backgroundColor = [UIColor colorWithWhite:1.f alpha:0.8f];
//        }
//
//
//    } else {
//            [self.navigationController.navigationBar setBackgroundImage: [UIImage new]//[PKUOStyleKit imageOfDetailNavBarTranslucentWithSize: CGSizeMake(self.navigationController.navigationBar.bounds.size.width, self.navigationController.navigationBar.bounds.size.height+20.f)] //[UIImage new]
//                                                              forBarMetrics:UIBarMetricsDefault];
//    }
//}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    // . NSLog(@"scrollView.contentOffset2 %.f", scrollView.contentOffset.y);
//}
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
//                  willDecelerate:(BOOL)decelerate{
//    // . NSLog(@"scrollView.contentOffset1 %.f", scrollView.contentOffset.y);
//}

-(void)popBack:(id)sender{
    // . NSLog(@"popBack");
    UIButton* btn = (UIButton*)sender;
    CGRect frame = btn.frame;
    frame.origin.x = - btn.frame.size.width;
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         btn.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [[self navigationController] popViewControllerAnimated:YES];
                     }];
    
}

-(void)showOnMap{
    [self performSegueWithIdentifier:@"toMapFromGeoRouteDetail" sender:self];
//    [self.tabBarController.tabBar setHidden:YES];
//    NSArray *viewContrlls=[[self navigationController] viewControllers];
//    for( int i=0;i<[ viewContrlls count];i++){
//        id map=[viewContrlls objectAtIndex:i];
//        // . NSLog(@"viewContrlls = %@", [map class]);
//        if([map isKindOfClass:[PKMainMapViewController class]]){
//            // A is your class where to popback
//            [[self navigationController] popToViewController:map animated:YES];
//            return;
//        }
//    }
//
//    NSArray *tabBarControllers = [self.tabBarController viewControllers];
//    for( int i=0;i<[ tabBarControllers count];i++){
//        id map2=[tabBarControllers objectAtIndex:i];
//        if([map2 isKindOfClass:[UINavigationController class]]){
//            UINavigationController * navControl = (UINavigationController*)map2;
//            NSArray *viewContrlls=[navControl viewControllers];
//            for( int i=0;i<[ viewContrlls count];i++){
//                id map=[viewContrlls objectAtIndex:i];
//                // . NSLog(@"viewContrlls = %@", [map class]);
//                if([map isKindOfClass:[PKMainMapViewController class]]){
//
//                    PKMainMapViewController *mapViewController = (PKMainMapViewController*)map;
//                    [mapViewController setShowGeoRouteDestanation:self.geoRoute];
//                    // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
//                    self.tabBarController.selectedIndex = i;//[self.tabBarController.viewControllers indexOfObject:map];
//
//                    // A is your class where to popback
//                    //                    [[self navigationController] popToViewController:map animated:YES];
//                    return;
//                }
//            }
//        }
//    }
    
}


-(void)pushBtnLike: (PKButtonLike*) sender{

    if(self.geoRoute.mylike){
        if(self.geoRoute.likes>0){
            [sender setImageWithLikeEnabled:NO withLikeCount:self.geoRoute.likes - 1 andIsDetail:YES];
        } else {
            [sender setImageWithLikeEnabled:NO withLikeCount:self.geoRoute.likes andIsDetail:YES];
        }
    } else {
        [sender setImageWithLikeEnabled:YES withLikeCount:self.geoRoute.likes + 1 andIsDetail:YES];
    }
    [sender setNeedsDisplay];
    [[PKUserInteraction sharedManager] switchObjectLike:self.geoRoute.objectID];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

//#pragma mark - PKObjectInfoBlockCVCDataSource
//- (NSInteger)numberOfBlockInObjectInfoBlockViewCollection:(PKObjectInfoBlockCVC *)infoBlockVC {
//    return 4;
//}
//- (PKMapObj*)mapObjectInObjectInfoBlockViewCollection:(PKObjectInfoBlockCVC *)infoBlockVC atIndex:(NSInteger)index{
//
//    return self.mapObject;
//}


//#pragma mark - PKObjectInfoBlockCVDataSource
//- (NSInteger)numberOfBlockInBlocksView:(MAKImageGalleryView *)galleryView {
//    return 4;
//}
//- (PKMapObj*)mapObjectInBlocksView:(MAKImageGalleryView *)galleryView atIndex:(NSInteger)index{
//
//    return self.mapObject;
//}




//#pragma mark - PKMapObjectInfoBlocksViewDataSource
//- (NSInteger)numberOfBlockInBlocksView:(MAKImageGalleryView *)blockView {
//    return 4;
//}
//- (PKGeoRoute*)blockInfoFromMapObject{
//
//    return self.geoRoute;
//}

//-(CGSize)blockSize{
//    //UICollectionViewCell
//    // . NSLog(@"infoBlocksView self.view.bounds.size.width = %f", self.view.bounds.size.width);
//    CGSize cellSize = CGSizeMake(self.view.bounds.size.width, 60);
//    return cellSize;
//}



#pragma mark - MAKImageGalleryViewDataSource
- (NSInteger)numberOfImagesInGallery:(MAKImageGalleryView *)galleryView {
    
    if([[self.geoRoute.images allObjects] count] == 0 && [self.geoRoute.mapObject allObjects] > 0){
        PKMapObj *mapObject = [[self.geoRoute.mapObject allObjects] firstObject];
        return [[mapObject.images allObjects] count] + 1;
    } else if(self.geoRoute.mapObject == nil){
        return 1;
    }
    return [[self.geoRoute.images allObjects] count] + 1;
    
}
- (void)loadImageInGallery:(MAKImageGalleryView *)galleryView atIndex:(NSInteger)index callback:(void(^)(UIImage *))callback{
    
    
    BOOL imageFromMapObject = NO;
    BOOL imagePreview = NO;
    NSString *imageName;
    if(index == 0){
        imagePreview = YES;
        if([self.geoRoute.prevImageName isEqualToString:@"no_image.jpg"]){
            PKMapObj *mapObject = [[self.geoRoute.mapObject allObjects] firstObject];
            if(![mapObject.prevImageName isEqualToString:@"no_image.jpg"]){
                imageName = mapObject.prevImageName;
                imageFromMapObject = YES;
            }
        } else {
            imageName = self.geoRoute.prevImageName;
        }
    } else {
        PKObjImages *image;
        if([[self.geoRoute.images allObjects] count] > 0){
            image = [[self.geoRoute.images allObjects] objectAtIndex:index - 1];
            imageName = image.imageName;
            
        } else if([self.geoRoute.mapObject allObjects] > 0){
            PKMapObj *mapObject = [[self.geoRoute.mapObject allObjects] firstObject];
            image = [[mapObject.images allObjects] objectAtIndex:index - 1];
            imageName = image.imageName;
            imageFromMapObject = YES;
        }
        
    }
    
    
    NSString* imageUrl;
    NSString* dataPath;
    
    
    if(imageName != nil && ![imageName isEqualToString:@""] && ![imageName isEqualToString:@"no_image.jpg"]){
        if(imagePreview){
            if(imageFromMapObject){
                dataPath = LOCAL_mapObjImagePathPreview(imageName);
                imageUrl = URL_mapObjImagePathPreview(imageName);
            } else {
                dataPath = LOCAL_geoRouteImagePathPreview(imageName);
                imageUrl = URL_geoRouteImagePathPreview(imageName);
            }
        } else {
            if(imageFromMapObject){
                dataPath = LOCAL_mapObjImagePathPhotogall(imageName);
                imageUrl = URL_mapObjImagePathPhotogall(imageName);
            } else {
                dataPath = LOCAL_geoRouteImagePathPhotogall(imageName);
                imageUrl = URL_geoRouteImagePathPhotogall(imageName);
            }
        }
        
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
            callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
        } else {
            AFImageDownloader* download = [[AFImageDownloader alloc] init];
            [self.downloads addObject:download]; //СОХРАНИТЬ ОБЪЕКТ В ПАМЯТИ
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: imageUrl] cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                 timeoutInterval:1];
            [download downloadImageForURLRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
                
                callback(image);
                
                
            } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
                // . NSLog(@"Error download image");
            }];
        }
    }
    
    
    
    
    
//    NSString* imageUrl;
//    BOOL imageWasSet = NO;
//    if(index == 0){
//        if(![self.geoRoute.prevImageName isEqualToString:@"no_image.jpg"]){
//            NSString* dataPath = LOCAL_geoRouteImagePathPreview(self.geoRoute.prevImageName);
//            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
//                // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
//                callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
//                imageWasSet = YES;
//            } else {
//                // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
//                imageUrl = URL_geoRouteImagePathPreview(self.geoRoute.prevImageName);
//            }
//        } else {
//            callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
//            imageWasSet = YES;
//        }
//    } else {
//        
//        PKObjImages *image = [[self.geoRoute.images allObjects] objectAtIndex:index - 1];
//        
//        if(![image.imageName isEqualToString:@"no_image.jpg"]){
//            
//            NSString* dataPath = LOCAL_geoRouteImagePathPhotogall(image.imageName);
//            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
//                // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
//                callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
//                imageWasSet = YES;
//            } else {
//                // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
//                imageUrl = URL_geoRouteImagePathPhotogall(image.imageName);
//            }
//        } else {
//            callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
//            imageWasSet = YES;
//        }
//    }
//    
//    
//    if(!imageWasSet){
//    
//    //    // . NSLog(@"imageUrl = %@", imageUrl);
//        AFImageDownloader* download = [[AFImageDownloader alloc] init];
//        
//        [self.downloads addObject:download]; //СОХРАНИТЬ ОБЪЕКТ В ПАМЯТИ
//        
//        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: imageUrl] cachePolicy:NSURLRequestReturnCacheDataElseLoad
//                                             timeoutInterval:1];
//        [download downloadImageForURLRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
//            [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
//            
//            callback(image);
//            
//            
//        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
//            [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
//            // . NSLog(@"Error download image");
//        }];
//    }
}
- (UIViewContentMode)imageGallery:(MAKImageGalleryView *)galleryView contentModeForImageAtIndex:(NSInteger)index {
    return UIViewContentModeScaleAspectFill;
}


#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toMapFromGeoRouteDetail"]){
        PKMainMapViewController *vc = (PKMainMapViewController*)[segue destinationViewController];
        [vc setShowGeoRouteDestanation:self.geoRoute];
    }
}


@end
