//
//  PKGeoRouteCollectionViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZOZolaZoomTransition.h"

@interface PKGeoRouteCollectionViewController : UICollectionViewController <ZOZolaZoomTransitionDelegate, UINavigationControllerDelegate>

@end

@class PKButtonLike;//ZOProduct,

@interface PKGeoRouteCell : UICollectionViewCell

//@property (strong, nonatomic) ZOProduct *product;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) PKButtonLike *likesButton;

@end
