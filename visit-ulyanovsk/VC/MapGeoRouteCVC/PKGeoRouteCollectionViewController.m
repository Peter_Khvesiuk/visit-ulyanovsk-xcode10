//
//  PKGeoRouteCollectionViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKGeoRouteCollectionViewController.h"
#import "PKGeoRouteDetailVC.h"


#import "PKServerManager.h"
#import "PKDataManager.h"
#import "PKUserInteraction.h"

#import "PKGeoRoute+CoreDataClass.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#import <STPopup/STPopup.h>
#import "PKMapBottomPopupVC.h"

#import "PKUOStyleKit.h"

#import "PKButtonLike.h"

#import "PKInterfaceManager.h"
#import "PKTopNavButtonView.h"

#import "PKMapObjectDetailVC.h"
#import "PKMainMapViewController.h"
//#import <QuartzCore/QuartzCore.h>


static NSString * PKGeoRouteCellId           = @"PKGeoRouteCell";
//static CGFloat zoomCellMargin          = 10.0;
//static CGFloat zoomCellSpacing         = 10.0;
//static CGFloat zoomCellTextAreaHeight  = 44.0;
//static CGFloat cellsInRow                     = 2.0;
//static CGFloat detailImageHeight              = 300.0;





@interface PKGeoRouteCollectionViewController ()<NSFetchedResultsControllerDelegate, STPopupControllerTransitioning, PKFilterBottomPopupVCDelegate>


@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;


//@property (nonatomic) CGFloat cellTextAreaHeight;
//@property (nonatomic) CGFloat cellMargin;
//@property (nonatomic) CGFloat cellSpacing;
@property (nonatomic) CGFloat detailImageHeight;
@property (nonatomic) CGFloat cellsInRow;


@property NSMutableArray *sectionChanges;
@property NSMutableArray *itemChanges;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (weak, nonatomic) STPopupController* popup;

@property (nonatomic, strong) NSArray *selectionsTypes;
@property (nonatomic, strong) NSArray *selectionsAdditional;


@property (strong, nonatomic) NSPredicate * predicate1;
//@property (strong, nonatomic) NSPredicate * predicate2;
//@property (strong, nonatomic) NSNumber * flagForPredicatSelected1;
//@property (strong, nonatomic) NSString * flagForPredicatSelected2;

@property (strong, nonatomic) UIRefreshControl *refreshControl;


//@property (strong, nonatomic) NSArray *products;
@property (weak, nonatomic) PKGeoRouteCell *selectedCell;

@end

@implementation PKGeoRouteCollectionViewController




#pragma mark - Constructors
//
//- (instancetype)init {
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//    //[flowLayout setSectionInset:UIEdgeInsetsMake(100.f, 100.f, 100.f, 100.f)];
//    self = [super initWithCollectionViewLayout:flowLayout];
//    if (self) {
//        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
//    }
//    return self;
//}

#pragma mark - View Lifecycle
-(void)dealloc{
    // . NSLog(@"\ndealloc PKGeoRouteCollectionViewController");
}
- (void)viewDidLoad {
    [super viewDidLoad];

    self.detailImageHeight = zoomDetailImageHeight;
    self.cellsInRow = zoomCellsInRow;
    
    
    
    
    
    //+++++++++++++++++++++++++++++
//    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE_UP(@"tours")  andTitleHide:NO];
    
    
    
    
    
    
    
    
    
    
    
    
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(54.f + 10.f, 0, 70.f, 0);
    
    
    
    
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refreshControl;
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    //+++++++++++++++++++++++++++++
    
    
    
    self.navigationController.delegate = self;
    
    // Load demo data
    //    NSMutableArray *products = [[NSMutableArray alloc] initWithCapacity:10];
    //    for (NSInteger i=0; i<10; i++) {
    //        ZOProduct *product = [[ZOProduct alloc] init];
    //        product.title = [NSString stringWithFormat:@"Product %ld", i];
    //        product.imageName = [NSString stringWithFormat:@"product_%ld.jpg", i];
    //        [products addObject:product];
    //    }
    //    self.products = products;
    //
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.94 alpha:1.0];
    [self.collectionView registerClass:[PKGeoRouteCell class] forCellWithReuseIdentifier:PKGeoRouteCellId];
}
#warning do I need viewWillAppear?
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
//    [self reloadwithPredicateDefault];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}

#pragma mark - Button controll

-(void)popBackToMainVC:(id)sender{
    [[self.tabBarController navigationController] popToRootViewControllerAnimated:YES];
//    [self performSegueWithIdentifier:@"toMainVC" sender:sender];
//    // . NSLog(@"popToRootViewControllerAnimated popBack");
//    [self.navigationController popToRootViewControllerAnimated:YES];
}



















































-(void)pushBtnLike: (PKButtonLike*) sender{
    //    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:(PKGeoRouteCell *)sender.superview.superview];
    PKGeoRoute* geoRoute = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [[PKUserInteraction sharedManager] switchObjectLike:geoRoute.objectID];
}


#pragma mark - UICollectionViewDelegate & Data Source Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    // . NSLog(@"numberOfRowsInSection = %lu", (unsigned long)[sectionInfo numberOfObjects]);
    //warning Incomplete implementation, return the number of items
    return [sectionInfo numberOfObjects];
    //    return [_products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PKGeoRouteCell *cell = (PKGeoRouteCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PKGeoRouteCellId forIndexPath:indexPath];
    if (!cell) {
        cell = [[PKGeoRouteCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}







































- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = (PKGeoRouteCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    // . NSLog(@"didSelectItemAtIndexPath = self.selectedCell KK = %@", self.selectedCell);
    [self performSegueWithIdentifier:@"toGeoRouteDetailVC" sender:self.selectedCell];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toGeoRouteDetailVC"]){
        [self.tabBarController.tabBar setHidden:YES];
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
        PKGeoRouteDetailVC*vc = [segue destinationViewController] ;
        [vc setGeoRoute:(PKGeoRoute *) [self.fetchedResultsController objectAtIndexPath:indexPath]];

    }

}

-(void)reloadMapIfNeeded{
    
        NSArray *tabBarControllers = [self.tabBarController viewControllers];
        for( int i=0;i<[ tabBarControllers count];i++){
            id map2=[tabBarControllers objectAtIndex:i];
            if([map2 isKindOfClass:[UINavigationController class]]){
                UINavigationController * navControl = (UINavigationController*)map2;
                NSArray *viewContrlls=[navControl viewControllers];
                NSMutableArray *newViewControllers = [[NSMutableArray alloc] init];
                for( int i=0;i<[ viewContrlls count];i++){
                    id vc=[viewContrlls objectAtIndex:i];
                    // . NSLog(@"viewContrlls = %@", [map class]);
                    if([vc isKindOfClass:[PKMainMapViewController class]]){
                        
                        PKMainMapViewController *mapViewController = (PKMainMapViewController*)vc;
                        
                        
                        [mapViewController reloadAnnotationWithArray:self.selectionsTypes andArray:self.selectionsAdditional];
                        [newViewControllers addObject:mapViewController];
                        // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
                        //                        return;
                        
                        
                    } else if([vc isKindOfClass:[PKMapObjectDetailVC class]] || [vc isKindOfClass:[PKGeoRouteDetailVC class]]){
                        //#warning kill PKMapObjectDetailVC if needed
                        
                    } else {
                        [newViewControllers addObject:vc];
                    }
                }
                [navControl setViewControllers:newViewControllers];
                
            }
        }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Cell width is half the screen width - edge margin - half the center margin
    
    
    CGFloat width = (self.collectionView.frame.size.width / self.cellsInRow) - zoomCellMargin - (zoomCellSpacing / self.cellsInRow);
    CGFloat height = ((self.detailImageHeight / self.collectionView.frame.size.width) * width) + zoomCellTextAreaHeight;
    return CGSizeMake(width, height);
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(zoomTopMargin, zoomCellMargin, 70.f, zoomCellMargin);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

#pragma mark - UINavigationControllerDelegate Methods

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    // Sanity
    if (fromVC != self && toVC != self) return nil;
    
    // Determine if we're presenting or dismissing
    ZOTransitionType type = (fromVC == self) ? ZOTransitionTypePresenting : ZOTransitionTypeDismissing;
    
    // Create a transition instance with the selected cell's imageView as the target view
    ZOZolaZoomTransition *zoomTransition = [ZOZolaZoomTransition transitionFromView:_selectedCell.imageView
                                                                               type:type
                                                                           duration:0.5
                                                                           delegate:self];
    zoomTransition.fadeColor = self.collectionView.backgroundColor;
    
    return zoomTransition;
}

#pragma mark - ZOZolaZoomTransitionDelegate Methods

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
        startingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The starting frame is taken from the selected cell's imageView.
//        // . NSLog(@"\n fg x = %f, y = %f, w = %f, h = %f", [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].origin.x, [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].origin.y, [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].size.width, [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].size.height);
        return [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView];
    } else if ([fromViewController isKindOfClass:[PKGeoRouteDetailVC class]]) {
        // We're popping back to this master controller. The starting frame is taken from the detailController's imageView.
        PKGeoRouteDetailVC *detailController = (PKGeoRouteDetailVC *)fromViewController;
//        // . NSLog(@"\n fg x = %f, y = %f, w = %f, h = %f", [detailController.imageGalleryView
//                                                        convertRect:detailController.imageGalleryView.bounds toView:relativeView].origin.x, [detailController.imageGalleryView                             convertRect:detailController.imageGalleryView.bounds toView:relativeView].origin.y, [detailController.imageGalleryView                                                                                                    convertRect:detailController.imageGalleryView.bounds toView:relativeView].size.width, [detailController.imageGalleryView                                                                                                                                                                                                     convertRect:detailController.imageGalleryView.bounds toView:relativeView].size.height);
        return [detailController.imageGalleryView
                convertRect:detailController.imageGalleryView.bounds toView:relativeView];
    }
    
    return CGRectZero;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
       finishingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // . NSLog(@"didSelectItemAtIndexPath = self.selectedCell KK1 = %@", _selectedCell);
        // We're pushing to the detail controller. The finishing frame is taken from the detailController's imageView.
        PKGeoRouteDetailVC *detailController = (PKGeoRouteDetailVC *)toViewController;
        
        return [detailController.imageGalleryView convertRect:CGRectMake(0, marginTopNavigationHeight, detailController.imageGalleryView.bounds.size.width, detailController.imageGalleryView.bounds.size.height) toView:relativeView];
#warning Navigator bar height epxected as 22? Okay?
    } else if ([fromViewController isKindOfClass:[PKGeoRouteDetailVC class]]) {
        // . NSLog(@"didSelectItemAtIndexPath = self.selectedCell KK2 = %@", _selectedCell);
        // . NSLog(@"\n fg x = %f, y = %f, w = %f, h = %f", [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].origin.x, [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].origin.y, [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].size.width, [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView].size.height);
        // We're popping back to this master controller. The finishing frame is taken from the selected cell's imageView.
        return [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView];
    }
    
    return CGRectZero;
}

- (NSArray *)supplementaryViewsForZolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition {
    // Here we're returning all UICollectionViewCells that are clipped by the edge
    // of the screen. These will be used as "supplementary views" so that the clipped
    // cells will be drawn in their entirety rather than appearing cut off during the
    // transition animation.
    
    NSMutableArray *clippedCells = [NSMutableArray arrayWithCapacity:[[self.collectionView visibleCells] count]];
    for (UICollectionViewCell *visibleCell in self.collectionView.visibleCells) {
        CGRect convertedRect = [visibleCell convertRect:visibleCell.bounds toView:self.view];
        if (!CGRectContainsRect(self.view.frame, convertedRect)) {
            [clippedCells addObject:visibleCell];
        }
    }
    return clippedCells;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
   frameForSupplementaryView:(UIView *)supplementaryView
              relativeToView:(UIView *)relativeView {
    
    return [supplementaryView convertRect:supplementaryView.bounds toView:relativeView];
}








#pragma mark - Collection view content controll

- (void)refershControlAction{
    [[PKServerManager sharedManager]
     getMainJSONDataFromServerOnSuccess:^(BOOL isComplete){
         if(isComplete){
            [self.refreshControl endRefreshing];
            //            [self.collectionView reloadData];
//            [self reloadwithPredicateDefault];
            [self reloadMapIfNeeded];
        }
         
     } inProgress:^(CGFloat progress) {
         // . NSLog(@"progress == %f", progress);
     } onFailure:^(NSError* error, NSInteger statusCode) {
         [self.refreshControl endRefreshing];
     }];
        
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    _sectionChanges = [[NSMutableArray alloc] init];
    _itemChanges = [[NSMutableArray alloc] init];
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKGeoRoute"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    
    
    
    NSSortDescriptor* titleAscending =
    [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    
//    NSSortDescriptor* nameOrgAscending =
//    [[NSSortDescriptor alloc] initWithKey:@"geoOrganization.nameOrg" ascending:YES];

    
    
//    self.selectionsTypes = [NSArray arrayWithObjects:@"пешеходный", nil]; //  [[PKDataManager sharedManager] getFilterTypeObjectsArray];
    _predicate1 = [NSPredicate predicateWithFormat:@"types.typeCategory == %i", 2];
    
    [fetchRequest setSortDescriptors:@[titleAscending]];
    [fetchRequest setPredicate:_predicate1];//

    
    
    
    
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil//@"ageTo"
                                                   cacheName:nil];//@"Master"
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        // . NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
    
    
    
    
    
    return _fetchedResultsController;
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    change[@(type)] = @(sectionIndex);
    [_sectionChanges addObject:change];
    
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath {
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_itemChanges addObject:change];
}
- (void)reloadwithPredicateDefault { ///delete?????????????????????????
    // Make sure to delete the cache
    // (using the name from when you created the fetched results controller)
    [NSFetchedResultsController deleteCacheWithName:nil];
    // Delete the old fetched results controller
    self.fetchedResultsController = nil;
    // TODO: Handle error!
    // This will cause the fetched results controller to be created
    // with the new predicate
    [self.fetchedResultsController performFetch:nil];
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource
- (NSManagedObjectContext*) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[PKDataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
}

//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
//    return [[self.fetchedResultsController sections] count];
//}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.collectionView performBatchUpdates:^{
        for (NSDictionary *change in self->_sectionChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeMove:
                        // . NSLog(@"NSFetchedResultsChangeMove empty!!!!!!!!!");
                        break;
                    case NSFetchedResultsChangeUpdate:
                        // . NSLog(@"NSFetchedResultsChangeUpdate empty!!!!!!!!!");
                        break;
                }
            }];
        }
        for (NSDictionary *change in self->_itemChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeUpdate:
                        [self.collectionView reloadItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeMove:
                        [self.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                        break;
                }
            }];
        }
    } completion:^(BOOL finished) {
        self->_sectionChanges = nil;
        self->_itemChanges = nil;
    }];
}





#pragma mark - ConfigureCell



-(void)configureCell:(PKGeoRouteCell*) cell atIndexPath:(NSIndexPath*)indexPath{
    PKGeoRoute *geoRoute = [self.fetchedResultsController objectAtIndexPath:indexPath];
   
//    // . NSLog(@"cell height = %.f %.f", cell.bounds.size.height, cell.bounds.size.width);
    // . NSLog(@"geoRoute.title %@ %hd", geoRoute.title,  geoRoute.idgeo);
    cell.imageView.image = nil;//[UIImage imageNamed:@"icon2.png"];// //
    cell.imageView.backgroundColor = [UIColor lightGrayColor];
    cell.titleLabel.text = geoRoute.title;
    
    
    //******
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    //******
    //    cell.layer.shadowOffset = CGSizeMake(1, 0);
    //    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    //    cell.layer.shadowRadius = 5;
    //    cell.layer.shadowOpacity = .25;
    //    cell.contentView.layer.cornerRadius = 10.0;
    //    cell.contentView.layer.borderColor = [[UIColor clearColor] CGColor];
    //    cell.contentView.layer.masksToBounds = YES;
    //******
    
    //    cell.contentView.layer.borderWidth = 1.0;
    //    cell.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    
    [cell.likesButton setImageWithLikeEnabled:geoRoute.mylike withLikeCount:geoRoute.likes andIsDetail:NO];
    [cell.likesButton addTarget:self
                         action:@selector(pushBtnLike:)
               forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.imageView.image = [PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(cell.bounds.size.width, cell.bounds.size.height)];
    
    if(![geoRoute.prevImageName isEqualToString:@"no_image.jpg"]){
        
        NSString* dataPath = LOCAL_geoRouteImagePathPreview(geoRoute.prevImageName);
        if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
            // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
            [cell.imageView setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
        } else {
            // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
            NSString* imageUrl = URL_geoRouteImagePathPreview(geoRoute.prevImageName);
            NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
            __weak PKGeoRouteCell* weakCellImage = cell;
            //CellWherePhoto.trenerPhotoDetail.image = nil;
            [cell.imageView setImageWithURLRequest:requestwhereImage
                                  placeholderImage:nil
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                               
                                               //         // . NSLog(@"requestwhereImage success");
                                               
                                               
                                               weakCellImage.imageView.image = image;
                                               
                                               //                                           weakCellImage.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
                                               [weakCellImage layoutSubviews];
                                               
                                           }
                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                               // . NSLog(@"failure imageUrl = %@",imageUrl);
                                           }];
        }
        
    }
}

#pragma mark - Popup control
-(void)segueFilter:(id)sender{
    PKMapBottomPopupVC* mapBottomPopupController =  [self.storyboard instantiateViewControllerWithIdentifier:@"PKMapBottomPopupVC"];//[[PKMapBottomPopupVC alloc] init];
    //    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PKMapBottomPopupVC"]];
    mapBottomPopupController.delegate = self;
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:mapBottomPopupController];
    
    
    popupController.style = STPopupStyleBottomSheet;
    
    popupController.containerView.layer.cornerRadius = 10;
    
//#warning check Background tap
//    [popupController.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundViewDidTap)]];
    
    // . NSLog(@"popupController1 %@", popupController);
    self.popup = popupController;
    [popupController presentInViewController:self];
}
-(void)backgroundViewDidTap{
//#warning check this tap
    //    STPopupController* popupController = [[STPopupController alloc] initWithRootViewController:self.popup];
    // . NSLog(@"popupController2 %@", self.popup);
    
    // . NSLog(@"PKMapObjectCollectionViewController selections selectionsTypes = %@", self.selectionsTypes);
    
    // . NSLog(@"PKMapObjectCollectionViewController selections selectionsAdditional = %@", self.selectionsAdditional);
    //    [self.popup dismiss];
    [self.popup popViewControllerAnimated:YES];
    
}

@end

#pragma mark - PKGeoRouteCell Implementation

@interface PKGeoRouteCell ()

@property (strong, nonatomic, readwrite) UIImageView *imageView;
//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;



@end

@implementation PKGeoRouteCell

#pragma mark - Constructors

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_imageView];
        
        self.titleLabel = [[UILabel alloc] init];
        if(self.contentView.frame.size.width > 300){
            _titleLabel.numberOfLines = 1;
        } else {
            _titleLabel.numberOfLines = 2;
        }
        _titleLabel.font = [UIFont systemFontOfSize:zoomTextFontTitle];
        [self.contentView addSubview:_titleLabel];
        
        self.likesButton = [[PKButtonLike alloc] init];
        
        [self.contentView addSubview:_likesButton];
        
        self.layer.borderWidth = 0.0;
        self.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        
        
    }
    return self;
}

#pragma mark - Setters

//- (void)setProduct:(ZOProduct *)product {
//    _product = product;
//    _titleLabel.text = product.title;
//    _imageView.image = [UIImage imageNamed:product.imageName];
//
//    [self setNeedsLayout];
//}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    
    //    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height - zoomCellTextAreaHeight);
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    CGFloat labelWidth = self.contentView.frame.size.width - 5.0 - zoomCellTextAreaHeight;
    CGFloat labelHeight = [_titleLabel sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].height;
    _titleLabel.frame = CGRectMake(5.0, _imageView.frame.origin.y + _imageView.frame.size.height + 5.0, labelWidth, labelHeight);
    
    _likesButton.frame = CGRectMake(self.contentView.frame.size.width - zoomCellTextAreaHeight, self.contentView.frame.size.height - zoomCellTextAreaHeight, zoomCellTextAreaHeight, zoomCellTextAreaHeight);
}


@end
