//
//  PKFilterTotalTableViewCell.h
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 26.08.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKFilterTotalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *totalTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCountLabel;
@end
