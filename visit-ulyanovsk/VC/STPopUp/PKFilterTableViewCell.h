//
//  PKFilterTableViewCell.h
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 23.08.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "PKMapBottomPopupVC.h"
//@protocol PKSendDataButtonsProtocol <NSObject>
//- (void)buttonsTag:(NSInteger) tag;
//- (BOOL)isAdditionalFromButtonsTitle:(NSString *) title;
//-(UIImage*)additionalFilterImageWithItem:(NSString*)item andPressed:(BOOL)pressed;
//
//@end
@interface PKFilterTableViewCell : UITableViewCell

@property (nonatomic,assign) id delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollCell;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (nonatomic, strong) NSArray *selections;

@end
