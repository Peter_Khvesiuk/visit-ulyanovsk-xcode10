//
//  PKFilterPlacesTableViewCell.h
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 24.08.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKMapBottomPopupVC.h"

@interface PKFilterPlacesTableViewCell : UITableViewCell


@property (nonatomic,assign) id delegate;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollCell;
@property (weak, nonatomic) IBOutlet UILabel *title;

@property (nonatomic, strong) NSArray *selections;

@end
