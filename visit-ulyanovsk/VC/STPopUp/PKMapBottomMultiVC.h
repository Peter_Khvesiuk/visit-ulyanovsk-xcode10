//
//  PKMapBottomMultiVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 11.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PKMapBottomMultiVC;
@protocol PKMapBottomMultiVCDelegate <NSObject>

- (void)multiSelectionViewController:(PKMapBottomMultiVC *)vc didFinishWithSelections:(NSArray *)selections;
-(UIImage*)additionalFilterImageWithItem:(NSString*)item andPressed:(BOOL)pressed;
@end

@interface PKMapBottomMultiVC : UITableViewController

@property (nonatomic) BOOL isAdditionalFilter;
@property (nonatomic, weak) id<PKMapBottomMultiVCDelegate> delegate;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSArray *defaultSelections;

@end
