//
//  PKFilterPlacesTableViewCell.m
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 24.08.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKFilterPlacesTableViewCell.h"
#import "PKDataManager.h"
#import "PKUOStyleKit.h"

@implementation PKFilterPlacesTableViewCell
{
    NSArray *_buttons;
}
@synthesize delegate;
- (void)setSelections:(NSArray *)selections
{
    if(selections == nil || selections.count == 0){
        selections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1]];
    }
    
    selections = [selections sortedArrayUsingSelector:@selector(localizedCompare:)];
    _selections = selections;
    [_buttons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
//    self.placeholderLabel.hidden = selections.count > 0;
    
    CGFloat buttonX = 15;
    NSMutableArray *buttons = [NSMutableArray new];
    
    
    
    for (NSString *selection in selections) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.layer.cornerRadius = 10;
        
        if([self sendButtonActionsIsAdditionalFromButtonsTitle:selection]){
            
            button.backgroundColor = [UIColor whiteColor];//[PKUOStyleKit bgColorGradientBottom];//button.tintColor;
            button.tag = 1;
            button.contentEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
            button.titleLabel.font = [UIFont boldSystemFontOfSize:16];
            [button setTitle:selection forState:UIControlStateNormal];
            [button setTitleColor:[PKUOStyleKit bgColorGradientBottom] forState:UIControlStateNormal];
            [button sizeToFit];
            //            button.frame = CGRectMake(buttonX, (self.scrollView.frame.size.height - button.frame.size.height) / 2, button.frame.size.width, button.frame.size.height);
            button.frame = CGRectMake(buttonX, (self.scrollCell.frame.size.height - button.frame.size.height) / 4, button.frame.size.width, button.frame.size.height);
            
            [button
             addTarget:self
             action:@selector(sendButtonActions:) forControlEvents:UIControlEventTouchUpInside];
            
            [buttons addObject:button];
            [self.scrollCell addSubview:button];
            
            buttonX += button.frame.size.width + 10;
        } else {
            
            
        }
        //        button.backgroundColor = [PKUOStyleKit bgColorGradientBottom];
        
    }
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(sendUIViewActions:)];
    [self.scrollCell addGestureRecognizer:singleFingerTap];
    
    
    
    self.scrollCell.contentSize = CGSizeMake(buttonX, self.scrollCell.frame.size.height-1);
    
    
    
    
    _buttons = [NSArray arrayWithArray:buttons];
}
-(void)sendButtonActions:(id)sender{
    UIButton *btn = (UIButton*)sender;
    //    [delegate buttonsTitle:btn.titleLabel.text];
    [delegate buttonsTag:btn.tag];
}
-(void)sendUIViewActions:(id)sender{
    [delegate buttonsTag:1];
}
- (BOOL)sendButtonActionsIsAdditionalFromButtonsTitle:(NSString *)title{
    NSLog(@"sendButtonActionsIsAdditionalFromButtonsTitle = %@", title);
    return [delegate isAdditionalFromButtonsTitle:title];
}
-(UIButton *)configButton:(NSString *)selection andPressed:(BOOL)pressed{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    UIImage *img = [delegate additionalFilterImageWithItem:(NSString*)selection andPressed:pressed];
    UIImage *imgHighlighted = [delegate additionalFilterImageWithItem:(NSString*)selection andPressed:!pressed];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
    //    button.layer.cornerRadius = 10;
    button.backgroundColor = [UIColor clearColor];//[PKUOStyleKit bgColorGradientBottom];
    button.tag = 2;
    return button;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
@end
