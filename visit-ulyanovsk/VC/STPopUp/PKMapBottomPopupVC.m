//
//  PKMapBottomPopupVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 11.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapBottomPopupVC.h"
#import "PKMapBottomMultiVC.h"

#import <STPopup/STPopup.h>

#import "PKDataManager.h"
#import "PKTypeObj+CoreDataClass.h"
#import "PKUOStyleKit.h"

#import "PKFilterTableViewCell.h"
#import "PKFilterPlacesTableViewCell.h"
#import "PKFilterTotalTableViewCell.h"





@interface PKMapBottomPopupVC() <PKMapBottomMultiVCDelegate, PKSendDataButtonsProtocol>

@property (strong, nonatomic) NSString *bottomSheetTypeOfPlaces;
@property (strong, nonatomic) NSString *bottomSheetAdditionals;


@end



@implementation PKMapBottomPopupVC
{
    NSArray *_typeOfPlacesSelections;
    NSArray *_additionalSelections;
}

@synthesize countFilteredObject;

-(void)viewDidLoad{
    [super viewDidLoad];
    self.bottomSheetTypeOfPlaces = TRANSLATE_UP(@"type_of_places");
    self.bottomSheetAdditionals = TRANSLATE_UP(@"additional");
    
    self.title =  TRANSLATE_UP(@"filters");
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-50.f, -30.f, self.tableView.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    UIImageView *tempImageView = [[UIImageView alloc] init];
    [tempImageView setFrame:self.tableView.frame];
    [tempImageView addSubview:backgroundInfoBlockImage];
    self.tableView.backgroundView = tempImageView;
//    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}
- (void)longPressedButtonsTag:(NSInteger) tag{
    NSLog(@"longPressedButtonsTag Z");
    NSArray *itemsArray = PKFilterAdditionalArray;
    NSString *item = [itemsArray objectAtIndex:tag];
    NSLog(@"longPressedButtonsTag Z item = %@", item);
    if([_additionalSelections count] == 0 || _additionalSelections == nil){
        _additionalSelections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:2]];
    }
    
    NSMutableArray *newAdditionalSelections = [[NSMutableArray alloc] initWithArray:_additionalSelections];
    NSLog(@"longPressedButtonsTag Z newAdditionalSelections = %@", newAdditionalSelections);
    NSInteger anIndexA=[newAdditionalSelections indexOfObject:item];

    if(NSNotFound == anIndexA) {
        [newAdditionalSelections addObject:item];
        [self saveSelections:newAdditionalSelections forCategory:2];
        NSLog(@"not found");
    } else if([item isEqualToString: [newAdditionalSelections objectAtIndex: anIndexA]]){
        [newAdditionalSelections removeObjectAtIndex:anIndexA];
        [self saveSelections:newAdditionalSelections forCategory:2];
    }
    
    if ([self.delegate respondsToSelector:@selector(didFinishWithArray:andArray:)]) {
        self.countFilteredObject = [self.delegate didFinishWithArray:(NSArray *)_typeOfPlacesSelections andArray:(NSArray *)_additionalSelections];
        NSLog(@"count = %ld", (long)self.countFilteredObject);
        
//        NSLog(@"ff = %@", [STPopupNavigationBar appearance]);
//
//        [STPopupNavigationBar appearance].tintColor = [UIColor redColor];
        
    }
    [self.tableView reloadData];
}


- (void)buttonsTag:(NSInteger) tag{
    NSLog(@"title = %ld %lu", (long)tag, (unsigned long)[_typeOfPlacesSelections count]);
    NSString *segueTo = nil;
    
    if(tag == 1){
        NSLog(@"tag1 === %ld %lu", (long)tag, (unsigned long)[_typeOfPlacesSelections count]);
        segueTo = @"BottomTypeOfPlacesSegue";
    } else {
        NSLog(@"tag2 === %ld %lu", (long)tag, (unsigned long)[_typeOfPlacesSelections count]);
        segueTo = @"BottomAdditionalSegue";
    }
//    NSString *segueTo = [self isAdditionalFromButtonsTitle:title]? @"BottomTypeOfPlacesSegue" : @"BottomAdditionalSegue";
    [self performSegueWithIdentifier:segueTo sender:self];
    
}
-(UIImage*)additionalFilterImageWithItem:(NSString*)item andPressed:(BOOL)pressed {
    
    UIColor *color = [PKUOStyleKit bgColorGradientBottom];
    if(!pressed){
        color = [PKUOStyleKit uOTextColor50];
    }
    NSArray *itemsArray = PKFilterAdditionalArray;
    NSInteger anIndex= [itemsArray indexOfObject:item];
    
    UIImage *img = [[UIImage alloc] init];
    switch (anIndex) {
        case 0:
            img = [PKUOStyleKit imageOfIconFilterTimeWithIconColor:color];
            break;
        case 1:
            img = [PKUOStyleKit imageOfIconFilterDisablesWithIconColor:color];
            break;
        case 2:
            img = [PKUOStyleKit imageOfIconBlockWiFiWithIconColor:color];
            break;
        case 3:
            img = [PKUOStyleKit imageOfIconBlockCardWithIconColor:color];
            break;
        case 4:
            img = [PKUOStyleKit imageOfIconKidsFriendlyWithIconColor:color];
            break;
        default:
            img = [PKUOStyleKit imageOfIconBlockCardWithIconColor:color];
            break;
    }
    
    
    //    UIImage *img = [[UIImage alloc] init];
    //    if([item isEqualToString:TRANSLATE_UP(@"now_open")]){
    //        img = [PKUOStyleKit imageOfIconFilterTimeWithIconColor:color];
    //    } else if([item isEqualToString:TRANSLATE_UP(@"available_for_disabled")]){
    //        img = [PKUOStyleKit imageOfIconFilterDisablesWithIconColor:color];
    //    } else if([item isEqualToString:TRANSLATE(@"Free_WiFi")]){
    //        img = [PKUOStyleKit imageOfIconBlockWiFiWithIconColor:color];
    //    } else if([item isEqualToString:TRANSLATE(@"Cards_accepted")]){
    //        img = [PKUOStyleKit imageOfIconBlockCardWithIconColor:color];
    //    }
    
    return img;
}
//- (BOOL)isTypeOfPlacesFromButtonsTitle:(NSString *) title{
//    if([_typeOfPlacesSelections count] == 0 || _typeOfPlacesSelections == nil){
//        _typeOfPlacesSelections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1]];
//    }
//
//    NSLog(@"_typeOfPlacesSelections = %@", [_typeOfPlacesSelections containsObject:title]? @"YES" : @"NO");
//    return [_typeOfPlacesSelections containsObject:title]? YES : NO;
//}
- (BOOL)isAdditionalFromButtonsTitle:(NSString *) title{
    if([_typeOfPlacesSelections count] == 0 || _typeOfPlacesSelections == nil){
        _typeOfPlacesSelections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1]];
    }
    
    return [_typeOfPlacesSelections containsObject:title]? YES : NO;
}
- (void)multiSelectionViewController:(PKMapBottomMultiVC *)vc didFinishWithSelections:(NSArray *)selections
{
    if ([vc.title isEqualToString:self.bottomSheetTypeOfPlaces]) {
        [self saveSelections:selections forCategory:1];
    }
    else if ([vc.title isEqualToString:self.bottomSheetAdditionals]) {
        [self saveSelections:selections forCategory:2];
    }
    if ([self.delegate respondsToSelector:@selector(didFinishWithArray:andArray:)]) {
        self.countFilteredObject = [self.delegate didFinishWithArray:(NSArray *)_typeOfPlacesSelections andArray:(NSArray *)_additionalSelections];
        
        
    }
    [self.tableView reloadData];
    
}
-(void)saveSelections:(NSArray*)selections forCategory:(int)category{
    if(category == 1){
        _typeOfPlacesSelections = selections;
    } else if(category == 2){
        
        _additionalSelections = selections;
    }
    [[PKDataManager sharedManager] setFilterTypeObjectsArray:selections andFilterCategory:category];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0:
            
            
            if ([cell isKindOfClass:[PKFilterTotalTableViewCell class]]) {
                PKFilterTotalTableViewCell *cellFilter = (PKFilterTotalTableViewCell*) cell;
                cellFilter.totalTextLabel.text = PK_st_TRANSLATE_UP(@"filter_results", @":");
                cellFilter.totalCountLabel.text = [NSString stringWithFormat:@"%ld", (long)self.countFilteredObject];
                cellFilter.selectionStyle = UITableViewCellSelectionStyleNone;
                cell = cellFilter;
                
                
            }
            break;
        case 1:
            if ([cell isKindOfClass:[PKFilterPlacesTableViewCell class]]) {
//                cell.textLabel.text = TRANSLATE_UP(@"type_of_places");
//                cell.tintColor = [PKUOStyleKit bgColorGradientBottom];
//                cell.imageView.image = [PKUOStyleKit imageOfIconBlockPlacesWithIconColor:[PKUOStyleKit uOTextColor50]];

                
                
                PKFilterTableViewCell *cellFilter = (PKFilterTableViewCell*) cell;
                cellFilter.title.text = TRANSLATE_UP(@"type_of_places");
                cellFilter.title.textColor = [PKUOStyleKit uOTextColor];
                cellFilter.delegate = self;
                cellFilter.selections = _typeOfPlacesSelections;
                cell = cellFilter;
                
                
            }
            break;
        case 2:
            if ([cell isKindOfClass:[PKFilterTableViewCell class]]) {
                PKFilterTableViewCell *cellFilter = (PKFilterTableViewCell*) cell;
                cellFilter.title.text = TRANSLATE_UP(@"additional");
                cellFilter.title.textColor = [PKUOStyleKit uOTextColor];
                cellFilter.delegate = self;
                cellFilter.selections = _additionalSelections;
                cell = cellFilter;
//                cell.textLabel.text = TRANSLATE_UP(@"additional");
//                cell.tintColor = [PKUOStyleKit bgColorGradientBottom];
//                cell.imageView.image = [PKUOStyleKit imageOfIconBlockAdditionalWithIconColor:[PKUOStyleKit uOTextColor50]];
            }
           
            break;
            
        default:
            break;
    }
    cell.backgroundColor = [UIColor clearColor];
    /*
    if ([cell isKindOfClass:[PKMapBottomPopupVCell class]]) {
        PKMapBottomPopupVCell *selectionCell = (PKMapBottomPopupVCell *)cell;
        selectionCell.delegate = self;
//        selectionCell.selections = [[NSArray arrayWithArray:_typeOfPlacesSelections] arrayByAddingObjectsFromArray:_additionalSelections];
        selectionCell.selections = _typeOfPlacesSelections;
    }
     */
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    
    PKMapBottomMultiVC *destinationViewController = (PKMapBottomMultiVC *)segue.destinationViewController;
    destinationViewController.delegate = self;
    if ([segue.identifier isEqualToString:@"BottomTypeOfPlacesSegue"]) {
        destinationViewController.title = self.bottomSheetTypeOfPlaces;
//        [[PKDataManager sharedManager] getTypesOfObjectsArray];
        destinationViewController.items = [[PKDataManager sharedManager] getTypesOfObjectsArrayGrouped];
        if([_typeOfPlacesSelections count] == 0 || _typeOfPlacesSelections == nil){
            _typeOfPlacesSelections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1]];
        }
        destinationViewController.defaultSelections = _typeOfPlacesSelections;
    } else if ([segue.identifier isEqualToString:@"BottomAdditionalSegue"]) {
        destinationViewController.title = self.bottomSheetAdditionals;
        destinationViewController.isAdditionalFilter = YES;
        
        NSArray *itemsArray = PKFilterAdditionalArray;
        NSDictionary *itemsDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:itemsArray,self.bottomSheetAdditionals, nil];
        NSArray *additionalArray = [[NSArray alloc] initWithObjects:itemsDictionary, nil];
//        NSMutableDictionary *itemsDictionaryMutable = [[NSMutableDictionary alloc] init];
//        [itemsDictionaryMutable setObject:itemsArray forKey:self.bottomSheetAdditionals];
        destinationViewController.items =  additionalArray;//itemsDictionaryMutable;
//        destinationViewController.items = @[ TRANSLATE_UP(@"now_open"), TRANSLATE_UP(@"available_for_disabled"), TRANSLATE(@"Free_WiFi"), TRANSLATE(@"Cards_accepted") ];
        if([_additionalSelections count] == 0 || _additionalSelections == nil){
            _additionalSelections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:2]];
        }
        destinationViewController.defaultSelections = _additionalSelections;
    }
}

@end
