//
//  PKFilterTableViewCell.m
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 23.08.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKFilterTableViewCell.h"
#import "PKDataManager.h"
#import "PKUOStyleKit.h"

@implementation PKFilterTableViewCell
{
    NSArray *_buttons;
}
@synthesize delegate;
- (void)setSelections:(NSArray *)selections
{
    if(selections == nil || selections.count == 0){
        selections = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:2]];
    }

    selections = [selections sortedArrayUsingSelector:@selector(localizedCompare:)];
    _selections = selections;
    [_buttons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat buttonX = 0.f;
    NSMutableArray *buttons = [NSMutableArray new];
    
    NSArray *itemsArray = PKFilterAdditionalArray;
    
    buttonX = 15;
    NSInteger index = 0;
    for (NSString *itemA in itemsArray) {
        BOOL pressed = NO;
        
        NSInteger anIndexA=[selections indexOfObject:(NSString*) itemA];
        if(NSNotFound == anIndexA) {
            NSLog(@"not found");
        } else if([itemA isEqualToString: [selections objectAtIndex: anIndexA]]){
            pressed = YES;
        }
        UIButton *btn = [self configButton:itemA andPressed:pressed];
        btn.frame = CGRectMake(11.f, 7.f, 32.f, 32.f);
        btn.tag = index;
        [btn
         addTarget:self
         action:@selector(sendButtonActions:) forControlEvents:UIControlEventTouchUpInside];
        
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressActions:)];
        [btn addGestureRecognizer:longPress];
        
        UIView *btnBackGround = [[UIView alloc] init];
        btnBackGround.frame = CGRectMake(buttonX, (self.scrollCell.frame.size.height - 44.f) / 2, 52.f, 44.f);
        btnBackGround.backgroundColor = [UIColor whiteColor];
        btnBackGround.clipsToBounds = YES;
        btnBackGround.layer.cornerRadius = 10.f;
        
        
        
        [btnBackGround addSubview:btn];
        
        
        [buttons addObject:btn];
        [self.scrollCell addSubview:btnBackGround];
        
        buttonX += btnBackGround.frame.size.width + 10;
        
        index++;
    }
    
    buttonX = self.scrollCell.frame.size.width;
    self.scrollCell.contentSize = CGSizeMake(buttonX, self.scrollCell.frame.size.height-1);
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(sendUIViewActions:)];
    [self.scrollCell addGestureRecognizer:singleFingerTap];
    
    
    _buttons = [NSArray arrayWithArray:buttons];
}
-(void)longPressActions:(UILongPressGestureRecognizer*)sender{
    
    if (sender.state == UIGestureRecognizerStateEnded) {
        
        //Do Whatever You want on End of Gesture
        UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
        
        [delegate longPressedButtonsTag:[tapRecognizer.view tag]];
    }
}
-(void)sendButtonActions:(id)sender{
    [delegate buttonsTag:2];
}
-(void)sendUIViewActions:(id)sender{
    [delegate buttonsTag:2];
}
- (BOOL)sendButtonActionsIsAdditionalFromButtonsTitle:(NSString *)title{
    
    return [delegate isAdditionalFromButtonsTitle:title];
}
-(UIButton *)configButton:(NSString *)selection andPressed:(BOOL)pressed{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    UIImage *img = [delegate additionalFilterImageWithItem:(NSString*)selection andPressed:pressed];
    UIImage *imgHighlighted = [delegate additionalFilterImageWithItem:(NSString*)selection andPressed:!pressed];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    [button setBackgroundImage:imgHighlighted forState:UIControlStateHighlighted];
//    button.layer.cornerRadius = 10;
    button.backgroundColor = [UIColor clearColor];//[PKUOStyleKit bgColorGradientBottom];

    return button;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
