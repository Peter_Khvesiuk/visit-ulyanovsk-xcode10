//
//  PKMapBottomMultiVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 11.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapBottomMultiVC.h"
#import <STPopup/STPopup.h>
#import "PKUOStyleKit.h"
#import "PKDataManager.h"


typedef enum {
    stateNoneSelected = 0,
    stateSelected = 1,
    stateSelectedMix = 2,
    stateDontUse = 3,
} CheckboxState;

@implementation PKMapBottomMultiVC
{
    NSMutableSet *_mutableSelections;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    
    if(self.isAdditionalFilter){
        return @"";
    }
    NSDictionary *item = [self.items objectAtIndex:section];
    return [NSString stringWithFormat:@"%@", [[item allKeys] firstObject]];
//    return [NSString stringWithFormat:@"%@", [self.items allKeys] [section]];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(self.isAdditionalFilter){
        return 0;
    }
    return 56;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(self.isAdditionalFilter){
        return [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 56.0)];
    }
    // create the parent view that will hold header Label
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(10.0, 0.0, 300.0, 56.0)];
    
    // create the button object
    UILabel * headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];//clearColor
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor blackColor];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:18];
    headerLabel.frame = CGRectMake(64.f, 5.f, 300.f, 44.f);
    
    NSDictionary *item = [self.items objectAtIndex:section];
    NSArray* obj = (NSArray*) [item objectForKey:[[item allKeys] firstObject]];
    
    
    UIImageView *checkbox = [[UIImageView alloc] initWithFrame:CGRectMake(16.f, 11.f, 32.f, 32.f)];
    [checkbox setImage: [self checkboxWithArray:obj]];
    [customView addSubview:checkbox];
    
    headerLabel.text = [NSString stringWithFormat:@"%@",[[item allKeys] firstObject]];
    
    customView.tag = section;
    [customView addSubview:headerLabel];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(groupPressed:)];
    [customView addGestureRecognizer:singleFingerTap];
    
    return customView;
}
- (UIImage*)checkboxWithArray:(NSArray*)obj {
    BOOL pressed = NO;
    BOOL many = NO;
    UIColor *color = [PKUOStyleKit uOTextColor];
    if([self checkedOrNot:(NSArray*)obj] == stateSelectedMix){
        pressed = YES;
        many = YES;
//        color = [PKUOStyleKit uOTextColor];
    } else if([self checkedOrNot:(NSArray*)obj] == stateSelected){
        pressed = YES;
        color = [PKUOStyleKit bgColorGradientBottom];
    }
    return [PKUOStyleKit imageOfIconBlockCheckBoxWithIconColor:color pressed:pressed manyCondition:many];
}
- (CheckboxState)checkedOrNot:(NSArray*)obj {
    
    BOOL mutableSelectionsYES = NO;
    BOOL mutableSelectionsNO = NO;
    for (NSString *itemString in obj) {
        
        if([_mutableSelections containsObject:itemString]){
            mutableSelectionsYES = YES;
        } else {
            mutableSelectionsNO = YES;
        }
        
    }
  

    
    if([obj count] == 1){
        if(mutableSelectionsYES){
            return stateSelected;
        } else {
            return stateNoneSelected;
        }
    } else {
        if(mutableSelectionsYES && !mutableSelectionsNO){
            return stateSelected;
        } else if(!mutableSelectionsYES && mutableSelectionsNO){
            return stateNoneSelected;
        }
    }
    
    return stateSelectedMix;
}
- (void)groupPressed:(UITapGestureRecognizer *)recognizer
{
    
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    
    
//    NSArray* obj = (NSArray*) [self.items objectForKey:[self.items allKeys] [recognizer.view.tag]];
    NSDictionary *item = [self.items objectAtIndex:[recognizer.view tag]];
    NSArray* obj = (NSArray*) [item objectForKey:[[item allKeys] firstObject]];
    

    BOOL needDelete = NO;
    if([self checkedOrNot:obj] == stateSelected){
        needDelete = YES;
    }
    
    for (NSString *itemString in obj) {
        NSString *item = itemString;
        if(needDelete){
            if ([_mutableSelections containsObject:item]) {
                [_mutableSelections removeObject:item];
            }
        } else {
            if (![_mutableSelections containsObject:item]) {
                [_mutableSelections addObject:item];
            }
        }
        
    }
    
    
    

    
    
    [self.tableView reloadData];

}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{


    return  [self.items count];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.tableView.backgroundColor = [PKUOStyleKit lightNotWhite];
}

- (IBAction)done:(id)sender {
    if ([self.delegate respondsToSelector:@selector(multiSelectionViewController:didFinishWithSelections:)]) {
        [self.delegate multiSelectionViewController:self didFinishWithSelections:_mutableSelections.allObjects];
    }
    [self.popupController popViewControllerAnimated:YES];
}


- (void)setDefaultSelections:(NSArray *)defaultSelections
{
    _defaultSelections = defaultSelections;
    _mutableSelections = [NSMutableSet setWithArray:defaultSelections];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    

    NSDictionary *item = [self.items objectAtIndex:section];
    return [[item objectForKey:[[item allKeys] firstObject]] count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellId = @"Multi-Selection Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];

    }

    
    NSDictionary *itemz = [self.items objectAtIndex:indexPath.section];
    NSArray* obj = [itemz objectForKey:[[itemz allKeys] firstObject]];
    
    
    NSString *itemString = [NSString stringWithFormat: @"%@",[obj objectAtIndex:indexPath.row]];
    

    NSString *item = itemString;
    cell.textLabel.text = item;

    BOOL pressed = [_mutableSelections containsObject:item] ? YES : NO;
    UIColor *color = [PKUOStyleKit uOTextColor50];
    if(!pressed){
        color = [PKUOStyleKit uOTextColor50];
    }
    
    if(self.isAdditionalFilter){
        cell.imageView.image = [_delegate additionalFilterImageWithItem:item andPressed:pressed];
    } else {
        cell.imageView.image = [PKUOStyleKit imageOfIconBlockCheckBoxWithIconColor:color pressed:pressed manyCondition:NO];
    }
    cell.selectionStyle = UITableViewCellSeparatorStyleSingleLine;
    UIView *bgColorView = [[UIView alloc] init];
    [bgColorView setBackgroundColor: [PKUOStyleKit lightNotWhite]];
    [cell setSelectedBackgroundView:bgColorView];

//
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!_mutableSelections) {
        _mutableSelections = [NSMutableSet new];
    }
    
    NSDictionary *itemz = [self.items objectAtIndex:indexPath.section];
    NSArray* obj = [itemz objectForKey:[[itemz allKeys] firstObject]];
    
    
    NSString *itemString = [NSString stringWithFormat: @"%@",[obj objectAtIndex:indexPath.row]];
    
    
    NSString *item = itemString;

    if (![_mutableSelections containsObject:item]) {
        [_mutableSelections addObject:item];
    }
    else {
        [_mutableSelections removeObject:item];
    }
    [tableView reloadData];
}
@end
