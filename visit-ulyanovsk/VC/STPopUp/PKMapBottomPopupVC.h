//
//  PKMapBottomPopupVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 11.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKMapBottomPopupVC;
@protocol PKFilterBottomPopupVCDelegate <NSObject>
- (NSInteger)didFinishWithArray:(NSArray *)selectionsTypes andArray:(NSArray *)selectionsAdditional;

@end
@protocol PKSendDataButtonsProtocol <NSObject>
- (void)buttonsTag:(NSInteger) tag;
- (void)longPressedButtonsTag:(NSInteger) tag;
- (BOOL)isAdditionalFromButtonsTitle:(NSString *) title;
-(UIImage*)additionalFilterImageWithItem:(NSString*)item andPressed:(BOOL)pressed;

@end
@interface PKMapBottomPopupVC : UITableViewController
@property (nonatomic, weak) id<PKFilterBottomPopupVCDelegate, PKSendDataButtonsProtocol> delegate;
@property (nonatomic) NSInteger countFilteredObject;
@end
//@interface PKMapBottomPopupVCell
//
//
//@end

