//
//  PKMainMapViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 22.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMainMapViewController.h"

#import <CoreData/CoreData.h>
#import "PKDataManager.h"
#import "PKMapObj+CoreDataClass.h"
#import "PKGeoRoute+CoreDataClass.h"
#import "PKTypeObj+CoreDataClass.h"
#import "PKTypeObj+CoreDataClass.h"

#import <STPopup/STPopup.h>
#import "PKMapBottomPopupVC.h"

#import <Mapbox/Mapbox.h>
#import "PKMapAnnotation.h"
#import "PKMapCallout.h"
#import "PKMapCalloutFreeStuff.h"

#import "PKUOStyleKit.h"

#import "PKDrawPolylineManager.h"
//#import "PKDrawGeoRoute.h"

#import "PKGeoRouteDetailVC.h"
#import "PKMapObjectCollectionViewController.h"
#import "PKMapObjectDetailVC.h"

#import "PKInterfaceManager.h"
#import "PKTopNavButtonView.h"

#import "PKMainViewController.h"

//+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
//#import <AVFoundation/AVFoundation.h>
//#import <MapboxCoreNavigation/MapboxCoreNavigation.h>
//#import <MapboxDirections/MapboxDirections.h>
//#import <MapboxNavigation/MapboxNavigation.h>

//@import AVFoundation;
//@import MapboxCoreNavigation;
//@import MapboxDirections;
//@import MapboxNavigation;
//+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++



@interface PKMainMapViewController ()<MGLMapViewDelegate,STPopupControllerTransitioning,PKFilterBottomPopupVCDelegate, PKSendDataButtonsProtocol>{
    int _currentIndex;
    NSTimer *_timer;
}
@property (strong, nonatomic) IBOutlet UILongPressGestureRecognizer *longPress;

@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;

//@property (weak, nonatomic) IBOutlet UISegmentedControl *mapStyleSegmentControl;
//@property (strong, nonatomic) IBOutlet UIBarButtonItem *filterBtn;

@property (nonatomic, strong) MGLMapView *mapView;
@property (nonatomic) MGLCoordinateBounds ulyanovskArea;

@property (nonatomic) id <MGLAnnotation> annotationArea;

@property (weak, nonatomic) STPopupController* popup;
@property (nonatomic, strong) NSArray *selectionsTypes;
@property (nonatomic, strong) NSArray *selectionsAdditional;


@property (nonatomic) CLLocationCoordinate2D savedCenterCoordinate;
@property (nonatomic) CLLocationDirection savedHeading;
@property (nonatomic) CGFloat savedPitch;
@property (nonatomic) CLLocationDistance savedAltitude;

@property (nonatomic) UIProgressView *progressView;

@property (nonatomic, strong) PKMapObj *mapObject;
@property (nonatomic, strong) PKGeoRoute *geoRoute;
@property (nonatomic, strong) NSSet <PKMapObj*> *mapObjects;

@property (nonatomic) MGLShapeSource *polylineSource;
@property (nonatomic) MGLStyleLayer *geoRouteLayer;
@property (nonatomic) NSArray<CLLocation *> *geoRoutePoints;

//@property (nonatomic) MGLStyleLayer *citiNameLayer;
//@property (nonatomic) MGLStyleLayer *arrowsToCitiesLayer;

//@property (nonatomic) NSArray<CLLocation *> *locations;

//+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
//@property (weak, nonatomic) UIButton *toggleNavigationButton;
//@property (weak, nonatomic) UILabel *howToBeginLabel;
//@property (nonatomic, assign) CLLocationCoordinate2D destination;
//@property (nonatomic) MBDirections *directions;
//@property (nonatomic) MBRoute *route;
//@property (nonatomic) MBRouteController *navigation;
//@property (nonatomic) NSLengthFormatter *lengthFormatter;
//@property (nonatomic) AVSpeechSynthesizer *speechSynth;
//+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++

@end

@implementation PKMainMapViewController


//@synthesize moveToDestanation;

- (void)viewDidLoad {
    [super viewDidLoad];


    [self initMapController];
    
    //+++++++++++++++++++++++++++++
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    //+++++++++++++++++++++++++++++
    
   
    // Do any additional setup after loading the view.
    
    
    //+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
    //Выполните любые дополнительные настройки после загрузки вида, обычно из кончика.
//    self.mapView.userTrackingMode = MGLUserTrackingModeFollow;
    
//    self.lengthFormatter = [[NSLengthFormatter alloc] init];
//    self.lengthFormatter.unitStyle = NSFormattingUnitStyleShort;
//
//    self.speechSynth = [[AVSpeechSynthesizer alloc] init];
//    self.speechSynth.delegate = self;
//    [self resumeNotifications];
    //+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
}
-(void)initMapController{
    // . NSLog(@"initMapController");
    [self setTitle:TRANSLATE_UP(@"map")];
    /*Границы карты*/
    self.ulyanovskArea = MGLCoordinateBoundsMake(sw_MAP_BORDER, ne_MAP_BORDER);
    /*Границы карты*/
    
    
    NSURL *styleURL = [NSURL URLWithString:mapStyleDefault];
    self.mapView = [[MGLMapView alloc] initWithFrame:self.view.bounds styleURL:styleURL];
    //    self.mapStyleSegmentControl.hidden = YES;
    //    [self setControlButtonsHidden:YES];
    [self.view insertSubview:self.mapView atIndex:0];
    self.mapView.delegate = self;
    self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    /*Navigation*/
//    [self.mapView addGestureRecognizer:self.longPress];
    /*Navigation*/
    
    if(self.fifa2018){
        self.mapView.zoomEnabled = NO;
        self.mapView.pitchEnabled = NO;
        self.mapView.showsUserLocation = NO;
        [self setAnatation];
    } else if(self.moveToMapObject || self.showManyDestanations){
        self.mapView.showsUserLocation = YES;
        // . NSLog(@"\nmap scenario moveToDestanation == YES");
    } else {
        self.mapView.showsUserLocation = YES;
        [self deafultCameraAndCoordinateWithAnimated:NO];
        
        
        MGLMapCamera *cameraInitial = [MGLMapCamera cameraLookingAtCenterCoordinate:self.mapView.centerCoordinate fromDistance:200 pitch:60 heading:263];
        [self.mapView setCamera:cameraInitial animated:NO];
        
        // Задайте свойство делегата представления карты.
        
        
        
        //            [self.view insertSubview:self.mapStyleSegmentControl aboveSubview:self.mapView];
        //            [self.mapStyleSegmentControl addTarget:self action:@selector(changeStyle:) forControlEvents:UIControlEventValueChanged];
        if([self.selectionsTypes count] == 0 || self.selectionsTypes == nil){
            self.selectionsTypes = [[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1];
        }
        if([self.selectionsAdditional count] == 0 || self.selectionsAdditional == nil){
            self.selectionsAdditional = [[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:2];
        }
        
        self.savedCenterCoordinate = self.mapView.centerCoordinate;
        self.savedHeading = self.mapView.camera.heading;
        self.savedPitch = self.mapView.camera.pitch;
        self.savedAltitude = self.mapView.camera.altitude;
    }
    
    
    
}
- (void)mapView:(MGLMapView *)mapView didFinishLoadingStyle:(MGLStyle *)style {
    // . NSLog(@"\nmap scenario didFinishLoadingStyle");
    if(self.fifa2018){
        [self hidePlaceCityNameLayerFifa2018AndStyle:style];
    } else {
        [self map3DbuildingsWith:mapView andStyle:style];
    }
    
    
}
-(void)mapViewDidFinishLoadingMap:(MGLMapView *)mapView {
    // . NSLog(@"\nmap scenario mapViewDidFinishLoadingMap");
    if(self.fifa2018){
//        [self hidePlaceCityNameLayerFifa2018];
    } else
    if(self.moveToMapObject || self.showManyDestanations){
//        [self setAnatation];
        if(self.moveToMapObject && self.mapObject != nil){
            self.mapView.selectedAnnotations = self.mapView.annotations;
        } else if(self.showManyDestanations && self.geoRoute != nil){
            [self addLayerGeoRoute];
            [self animatePolyline];
        }
    } else {
        [self mapMemCenterAnimation:mapView];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
    // . NSLog(@"viewDidAppear");
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // . NSLog(@"viewWillAppear");
    if(self.moveToMapObject && self.showManyDestanations && [self.mapObjects count]>0){
        if([self.mapObjects count]==1){
            self.mapObject = [[self.mapObjects allObjects] firstObject];
            [self moveDestanationMapObject];
        } else {
            [self showMapObjects];
        }
    } else if(self.moveToMapObject && self.mapObject != nil){
        [self moveDestanationMapObject];
    } else if(self.showManyDestanations && self.geoRoute != nil){
        
        if([self.geoRoute.geoJSON isEqualToString:@""]){
            if([self.geoRoute.mapObject count] == 1){
                PKMapObj *mapObject = [[self.geoRoute.mapObject allObjects] firstObject];
                self.moveToMapObject = YES;
                self.mapObject = mapObject;
                [self moveDestanationMapObject];
            } else {
                for (PKMapObj *mapObject in [self.geoRoute.mapObject allObjects]) {
                    [self setAnontationFromObject:mapObject];
                }
            }
        } else {
            [self showGeoRouteDestanation];
        }
        
        
        
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.moveToMapObject = NO;
    self.showManyDestanations = NO;
    self.topNavButtonView.open = NO;
    
    //+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
    /*[self suspendNotifications];
    [self.navigation suspendLocationUpdates];*/
    //+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
}
- (void)dealloc {
    // Remove offline pack observers.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MAP Navigation
////+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
//
//- (IBAction)didLongPress:(UILongPressGestureRecognizer *)sender {
//    // . NSLog(@"long press");
////    if (sender.state != UIGestureRecognizerStateBegan) {
////        return;
////    }
////
////    CGPoint point = [sender locationInView:self.mapView];
////    self.destination = [self.mapView convertPoint:point toCoordinateFromView:self.mapView];
////    [self getRoute];
//}
//- (void)resumeNotifications {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didPassSpokenInstructionPoint:) name:MBRouteControllerDidPassSpokenInstructionPointNotification object:_navigation];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(progressDidChange:) name:MBRouteControllerProgressDidChangeNotification object:_navigation];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willReroute:) name:MBRouteControllerWillRerouteNotification object:_navigation];
//}
//
//- (void)suspendNotifications {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:MBRouteControllerDidPassSpokenInstructionPointNotification object:_navigation];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:MBRouteControllerProgressDidChangeNotification object:_navigation];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:MBRouteControllerWillRerouteNotification object:_navigation];
//}
//
//- (void)didPassSpokenInstructionPoint:(NSNotification *)notification {
//    MBRouteProgress *routeProgress = (MBRouteProgress *)notification.userInfo[MBRouteControllerRouteProgressKey];
//    NSString *text = routeProgress.currentLegProgress.currentStepProgress.currentSpokenInstruction.text;
//
//    [self.speechSynth speakUtterance:[AVSpeechUtterance speechUtteranceWithString:text]];
//}
//
//- (void)progressDidChange:(NSNotification *)notification {
//    // Если Вы используете MapboxCoreNavigation,
//    // это было бы хорошее время, чтобы обновить элементы пользовательского интерфейса.
//    // Вы можете захватить текущий routeProgress как:
//    // let routeProgress = уведомление.userInfo![RouteControllerRouteProgressKey] как! RouteProgress
//}
//
//- (void)willReroute:(NSNotification *)notification {
//    [self getRoute];
//}
//
//- (void)getRoute {
//    NSArray<MBWaypoint *> *waypoints = @[[[MBWaypoint alloc] initWithCoordinate:self.mapView.userLocation.coordinate coordinateAccuracy:-1 name:nil],
//                                         [[MBWaypoint alloc] initWithCoordinate:self.destination coordinateAccuracy:-1 name:nil]];
//
//    MBNavigationRouteOptions *options = [[MBNavigationRouteOptions alloc] initWithWaypoints:waypoints profileIdentifier:MBDirectionsProfileIdentifierAutomobileAvoidingTraffic];
//    options.includesSteps = YES;
//    options.routeShapeResolution = MBRouteShapeResolutionFull;
//
//    NSURLSessionDataTask *task = [[MBDirections sharedDirections] calculateDirectionsWithOptions:options completionHandler:^(NSArray<MBWaypoint *> * _Nullable waypoints, NSArray<MBRoute *> * _Nullable routes, NSError * _Nullable error) {
//
//        if (!routes.firstObject) {
//            return;
//        }
//
////        if (self.mapView.annotations) {
////            [self.mapView removeAnnotations:self.mapView.annotations];
////        }
//
//        MBRoute *route = routes.firstObject;
//        CLLocationCoordinate2D *routeCoordinates = malloc(route.coordinateCount * sizeof(CLLocationCoordinate2D));
//        [route getCoordinates:routeCoordinates];
//
//        MGLPolyline *polyline = [MGLPolyline polylineWithCoordinates:routeCoordinates count:route.coordinateCount];
//
//        [self.mapView addAnnotation:polyline];
//        [self.mapView setVisibleCoordinates:routeCoordinates count:route.coordinateCount edgePadding:UIEdgeInsetsZero animated:YES];
//
//        free(routeCoordinates);
//
//        self.route = route;
//
//        [self startNavigation:route];
//    }];
//
//    [task resume];
//}
//- (void)startNavigation:(MBRoute *)route {
//    /*MBSimulatedLocationManager *locationManager = [[MBSimulatedLocationManager alloc] initWithRoute:route];
//    MBNavigationViewController *controller = [[MBNavigationViewController alloc] initWithRoute:route
//                                                                                    directions:[MBDirections sharedDirections]
//                                                                                         style:nil
//                                                                               locationManager:locationManager];
//    [self presentViewController:controller animated:YES completion:nil];
//
//    // Приостановить уведомления и пусть 'MBNavigationViewController' обрабатывать все прогресс и голосовые обновления.
//    [self suspendNotifications];*/
//}
//+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++



#pragma Map Uses Scenario


-(void)setShowMapObjects:(NSSet <PKMapObj*>*) mapObjects{
    // . NSLog(@"\nmap z scenario setShowMapObjects NSSet");
    self.mapObjects = mapObjects;
    self.moveToMapObject = YES;
    self.showManyDestanations = YES;
}
-(void)setMoveToDestanationMapObject:(PKMapObj*)mapObject{
    // . NSLog(@"\nmap z scenario setMoveToDestanationMapObject");
    self.mapObject = mapObject;
    self.moveToMapObject = YES;
}
-(void)setShowGeoRouteDestanation:(PKGeoRoute*)geoRoute{
    // . NSLog(@"\nmap z scenario setShowGeoRouteDestanation");
    self.geoRoute = geoRoute;
    self.showManyDestanations = YES;
}

-(void)moveDestanationMapObject{
    // . NSLog(@"\nmap z scenario moveDestanationMapObject");
        // . NSLog(@"self.moveToDestanation == YES");
//        [self.mapView removeAnnotations:self.mapView.annotations];
    
        [self setAnontationFromObject:self.mapObject];
        [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake([self.mapObject.latitude doubleValue], [self.mapObject.longitude doubleValue])
                                zoomLevel:16
                                 animated:NO];
    
}
-(void)deafultCameraAndCoordinateWithAnimated:(BOOL)animated{
    // . NSLog(@"\nmap z deafultCameraAndCoordinateWithAnimated");
    [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(54.319227, 48.407721) //Ленинский мемориал
                            zoomLevel:16 direction:0 animated:animated];
    MGLMapCamera *cameraInitial = [MGLMapCamera cameraLookingAtCenterCoordinate:self.mapView.centerCoordinate fromDistance:200 pitch:0 heading:0];
    [self.mapView setCamera:cameraInitial animated:animated];
}
-(void)showMapObjects{
    // . NSLog(@"\nmap z showMapObjects");
    NSMutableArray *coordinateMapObjects = [[NSMutableArray alloc] init];
    for (PKMapObj *mapObject in [self.mapObjects allObjects]) {
        NSMutableArray *coordinate = [[NSMutableArray alloc] init];
        [coordinate addObject:mapObject.longitude];
        [coordinate addObject:mapObject.latitude];
        [coordinateMapObjects addObject:coordinate];
        [self setAnontationFromObject:mapObject];
    }
    
    MGLCoordinateBounds bounds = [self calculateBoundsWithArray: coordinateMapObjects];
    CGFloat inset = 25;
    if(self.fifa2018){
        inset = 35;
    }
    [self.mapView setVisibleCoordinateBounds:bounds edgePadding:UIEdgeInsetsMake(inset, inset, inset ,inset) animated:NO];
    
}
-(void)showGeoRouteDestanation{
    // . NSLog(@"\nmap z showGeoRouteDestanation");
//    // . NSLog(@"self.showGeoRouteDestanations == YES self.geoRoute.title = %@", self.geoRoute.title);
//    [self.mapView removeAnnotations:self.mapView.annotations];
//    [self deafultCameraAndCoordinateWithAnimated:NO];
    NSData* data = [self.geoRoute.geoJSON dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    MGLCoordinateBounds bounds = [self calculateBoundsWithArray: [[[[values objectForKey:@"features"] firstObject] objectForKey:@"geometry"] objectForKey:@"coordinates"]];
    [self.mapView setVisibleCoordinateBounds:bounds edgePadding:UIEdgeInsetsMake(25, 25, 25 ,25) animated:NO];
    
    //    координаты
    NSArray *coordinates = [[[[values objectForKey:@"features"] firstObject] objectForKey:@"geometry"] objectForKey:@"coordinates"];
    NSMutableArray<CLLocation *> *locations = [NSMutableArray array];
    for (NSArray<NSNumber *> *c in coordinates) {
        [locations addObject:[[CLLocation alloc] initWithLatitude:[c[1] doubleValue] longitude:[c[0] doubleValue]]];
    }
    self.geoRoutePoints = locations;
    //    координаты
}




#pragma mark - Map Annotations and CalloutViews

- (MGLAnnotationImage *)mapView:(MGLMapView *)mapView imageForAnnotation:(id <MGLAnnotation>)annotation {
    
    MGLAnnotationImage *annotationImage;
    NSString *reuseIdentifier = @"id";
    if ([annotation isKindOfClass:[PKMapAnnotation class]]) {
        PKMapAnnotation *castAnnotation = (PKMapAnnotation *)annotation;
        reuseIdentifier = castAnnotation.reuseIdentifier;
        // . NSLog(@"2 reuseIdentifier = %@", reuseIdentifier);
        // . NSLog(@"!annotationImage = %@", annotation.title);
        // . NSLog(@"!annotationImage = %i", castAnnotation.mapObject.types.typeFavorites);
        UIImage *image;
        
        

//        NSArray *iconForMarkersAtraction = [[NSArray alloc] initWithArray:[iconForMarkers objectForKey:@"attraction"]];
//        NSArray *iconForMarkersPicturesque = [[NSArray alloc] initWithArray:[iconForMarkers objectForKey:@"picturesque"]];
//        NSArray *iconForMarkersTheatre = [[NSArray alloc] initWithArray:[iconForMarkers objectForKey:@"theatre"]];
//        NSArray *iconForMarkersPark = [[NSArray alloc] initWithArray:[iconForMarkers objectForKey:@"park"]];
//        NSArray *iconForMarkersMonument = [[NSArray alloc] initWithArray:[iconForMarkers objectForKey:@"monument"]];
//        NSArray *iconForMarkersInformation = [[NSArray alloc] initWithArray:[iconForMarkers objectForKey:@"information"]];
//
        
        // . NSLog(@"castAnnotation.mapObject.recomended = %hd", castAnnotation.mapObject.recomended);
        switch (castAnnotation.mapObject.types.typeIcon) {
            case 1:
                image = [PKUOStyleKit imageOfMapMarkerFoodWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"food%i", castAnnotation.mapObject.recomended];
                break;
            case 2:
                image = [PKUOStyleKit imageOfMapMarkerHotelWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"hotel%i", castAnnotation.mapObject.recomended];
                break;
            case 3:
                image = [PKUOStyleKit imageOfMapMarkerMuseumWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"museum%i", castAnnotation.mapObject.recomended];
                break;
            case 4:
                image = [PKUOStyleKit imageOfIconMarkerTheatreWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"attraction%i", castAnnotation.mapObject.recomended];
                break;
            case 5:
                image = [PKUOStyleKit imageOfIconMarkerPicturesqueWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"picturesque%i", castAnnotation.mapObject.recomended];
                break;
            case 6:
                image = [PKUOStyleKit imageOfIconMarkerTheatreWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"theatre%i", castAnnotation.mapObject.recomended];
                break;
            case 7:
                image = [PKUOStyleKit imageOfIconMarkerParkWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"park%i", castAnnotation.mapObject.recomended];
                break;
            case 8:
                image = [PKUOStyleKit imageOfIconMarkerMonumentWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"monument%i", castAnnotation.mapObject.recomended];
                break;
            case 9:
                
                if(self.fifa2018){
                    image = [PKUOStyleKit imageOfIconMarkerFifaUlskWithStadiumName: TRANSLATE_UP(@"Ulyanovsk")];
                    reuseIdentifier = @"Ulyanovsk";
                } else {
                    image = [PKUOStyleKit imageOfIconMarkerInformationWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                    reuseIdentifier = [NSString stringWithFormat:@"information%i", castAnnotation.mapObject.recomended];
                }
                break;
            case 10:
                image = [PKUOStyleKit imageOfIconMarkerFifa2018WithStadiumName: castAnnotation.mapObject.title];
                reuseIdentifier = castAnnotation.mapObject.title;
                break;
            default:
                image = [PKUOStyleKit imageOfMapMarkerDeafaultWithMarkerFavorite:castAnnotation.mapObject.recomended turIndust:NO];
                reuseIdentifier = [NSString stringWithFormat:@"default%i", castAnnotation.mapObject.recomended];
                break;
        }
        annotationImage = [mapView dequeueReusableAnnotationImageWithIdentifier:reuseIdentifier];
        if (!annotationImage) {
            annotationImage = [MGLAnnotationImage annotationImageWithImage:image reuseIdentifier:reuseIdentifier];
        }
    }
   
    
    // . NSLog(@"2 annotation_n = %@", annotation.title);
    return annotationImage;
}
- (void)mapView:(MGLMapView *)mapView didSelectAnnotation:(id <MGLAnnotation>)annot{
    // . NSLog(@"didSelectAnnotation");
    
    if(self.fifa2018){
        [self performSegueWithIdentifier:@"toMapObjectDetailVC" sender:annot];
    } else {
        [_timer invalidate];
        [self.mapView removeAnnotation:self.annotationArea];
        if ([annot isKindOfClass:[PKMapAnnotation class]]) {
            PKMapAnnotation *annotation = (PKMapAnnotation*) annot;
            //        NSLog(@"annotation.mapObject.geoJSON = %@", annotation.mapObject.geoJSON);
            if(![annotation.mapObject.geoJSON isEqualToString:@""]){
                
                NSData* data = [annotation.mapObject.geoJSON dataUsingEncoding:NSUTF8StringEncoding];
                NSDictionary *values = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                
                NSArray *coordinates = [[[[values objectForKey:@"features"] firstObject] objectForKey:@"geometry"] objectForKey:@"coordinates"][0];
                
                
                //            NSLog(@"cpp = %@", coordinates);
                
                //            double zoom = 16;
                //            if(self.mapView.zoomLevel >= zoom){
                //                zoom = mapView.zoomLevel;
                //
                //            }
                
                
                MGLCoordinateBounds bounds = [self calculateBoundsWithArray: coordinates];
                MGLMapCamera *cameraInitial = [MGLMapCamera cameraLookingAtCenterCoordinate:CLLocationCoordinate2DMake([annotation.mapObject.latitude doubleValue], [annotation.mapObject.longitude doubleValue]) fromDistance:mapView.camera.altitude pitch:0 heading:0];
                //            [self.mapView setCamera:cameraInitial animated:YES];
                __weak MGLMapView* weakMapView = mapView;
                [mapView setCamera:cameraInitial withDuration:1.0f animationTimingFunction:nil completionHandler:^{
                    [weakMapView setVisibleCoordinateBounds:bounds edgePadding:UIEdgeInsetsMake(15, 15, 15 ,15) animated:YES];
                    [self drawShape: coordinates];
                }];
                
                
                //    координаты
                //            NSArray *coordinates = [[[[values objectForKey:@"features"] firstObject] objectForKey:@"geometry"] objectForKey:@"coordinates"];
                //            NSMutableArray<CLLocation *> *locations = [NSMutableArray array];
                //            for (NSArray<NSNumber *> *c in coordinates) {
                //                [locations addObject:[[CLLocation alloc] initWithLatitude:[c[1] doubleValue] longitude:[c[0] doubleValue]]];
                //            }
                //            self.geoRoutePoints = locations;
                
                
                
                //            [[PKDrawPolylineManager sharedManager] drawPolyline:self.mapView withMapObject:(NSString*) annotation.mapObject.geoJSON];
            }
            
        }
    
    
    }
    
}
-(void)setAnatation{
    // . NSLog(@"\nmap z setAnatation");
    if(self.fifa2018){
        self.mapObjects = [[PKDataManager sharedManager] getMapObjectsFifa2018];
        [self showMapObjects];
    } else if(!self.moveToMapObject){
        NSArray* array = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getMapObjectsWithTypesLike:self.selectionsTypes andAdditional:self.selectionsAdditional orNameLike:nil]];
        for(PKMapObj* mapObject in array){
            // . NSLog(@"\nffg title %@ %@", mapObject.title, mapObject.recomended? @"YES": @"NO");
            [self setAnontationFromObject:mapObject];
        }
    }
    
}
-(void) setAnontationFromObject:(PKMapObj*)obj{
    // . NSLog(@"\nmap z setAnontationFromObject");
    PKMapAnnotation *annotation = [[PKMapAnnotation alloc] init];
    annotation.coordinate = CLLocationCoordinate2DMake([obj.latitude doubleValue], [obj.longitude doubleValue]);
    annotation.mapObject = obj;
    annotation.title = obj.title;
    annotation.reuseIdentifier = obj.types.typeObjValue;
    // . NSLog(@"1 setAnontationFromObject = %@ %@ %f %f", obj.types.typeObjValue, obj.title, [obj.latitude doubleValue], [obj.longitude doubleValue]);
    [self.mapView addAnnotation:annotation];
}

- (BOOL)mapView:(MGLMapView *)mapView annotationCanShowCallout:(id <MGLAnnotation>)annotation {
    // . NSLog(@"\nmap z scenario annotationCanShowCallout");
    // . NSLog(@"annotationCanShowCallout");
    // Всегда разрешайте выноски появляться при нажатии аннотаций.
    return !self.fifa2018;
}
- (UIView<MGLCalloutView> *)mapView:(MGLMapView *)mapView calloutViewForAnnotation:(id<MGLAnnotation>)annotation
{
    // . NSLog(@"\nmap z scenario calloutViewForAnnotation");
    // . NSLog(@"calloutViewForAnnotation");
    PKMapAnnotation* myannotation = (PKMapAnnotation*)annotation;
    //    [self.mapView.zoomLevel];
    
    //    CLLocationDistance dist = [self.mapView.centerCoordinate distanceFromLocation:self.mapView.centerCoordinate];
    // . NSLog(@"\nj1 regionDidChangeAnimated \nj1 pitch = %f, \nj1 heading = %f, \nj1 direction = %f, \nj1 zoomLevel = %f, \nj1 zooaltitudemLevel = %f",self.mapView.camera.pitch, self.mapView.camera.heading,self.mapView.direction, self.mapView.zoomLevel, self.mapView.camera.altitude);
    
    if([myannotation.mapObject.geoJSON isEqualToString:@""] || myannotation.mapObject.geoJSON == nil){
        double zoom = 16;
        if(self.mapView.zoomLevel >= zoom){
            zoom = self.mapView.zoomLevel;
        }
        
        
        [mapView setCenterCoordinate:myannotation.coordinate zoomLevel:zoom direction:self.mapView.direction animated:YES completionHandler:^{
            MGLMapCamera *cameraInitial = [MGLMapCamera cameraLookingAtCenterCoordinate:self.mapView.centerCoordinate fromDistance:200 pitch:60 heading:263];
            // . NSLog(@"\nj1 regionDidChangeAnimated < 16 \nj1 pitch = %f, \nj1 heading = %f, \nj1 direction = %f, \nj1 zoomLevel = %f, \nj1 zooaltitudemLevel = %f",self.mapView.camera.pitch, self.mapView.camera.heading,self.mapView.direction, self.mapView.zoomLevel, self.mapView.camera.altitude);
            [self.mapView setCamera:cameraInitial animated:YES];
        }];
    }
    
    
    
    
    
    // Instantiate and return our custom callout view.
    
    
    if(myannotation.mapObject.freeContent == 0){
        PKMapCalloutFreeStuff*calloutView = [[PKMapCalloutFreeStuff alloc] init];
        calloutView.representedObject = annotation;
        return calloutView;
    }
    
    PKMapCallout *calloutView = [[PKMapCallout alloc] init];
    calloutView.representedObject = annotation;
    return calloutView;
}
- (void)mapView:(MGLMapView *)mapView tapOnCalloutForAnnotation:(id<MGLAnnotation>)annotation
{
    // . NSLog(@"\nmap z scenario tapOnCalloutForAnnotation");
    // Optionally handle taps on the callout.
    // . NSLog(@"Tapped the callout for: %@", annotation);
    
    // Hide the callout.
    
    [mapView deselectAnnotation:annotation animated:YES];
    
    
    [self performSegueWithIdentifier:@"toMapObjectDetailVC" sender:annotation];
}
#pragma mark - MapBox's Custom Method
//-(void)setMapCameraFlat:(BOOL) flat{
//
//    if(flat){
//        // . NSLog(@"map camera set to flat");
//    } else {
//        // . NSLog(@"map camera set to 3d");
//    }
//}




-(void)mapMemCenterAnimation:(MGLMapView *)mapView {
    // . NSLog(@"\nmap z mapMemCenterAnimation");
    // . NSLog(@"mapMemCenterAnimation");
    // Дождитесь загрузки карты перед началом первого движения камеры.
    // Создайте камеру, вращающуюся вокруг одной и той же центральной точки на 180°.
    // `fromDistance:` это метров выше среднего уровня моря, что глаз должен быть для того, чтобы увидеть, что карта показывает.
    MGLMapCamera *camera = [MGLMapCamera cameraLookingAtCenterCoordinate:mapView.centerCoordinate fromDistance:5000 pitch:0 heading:0];
    // Анимация движения камеры в течение 5 секунд.
    __weak MGLMapView* weakMapView = mapView;
    [mapView setCamera:camera withDuration:5 animationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn] completionHandler:^(void){
        // . NSLog(@"finish animation");
        MGLMapCamera *cameraInitial = [MGLMapCamera cameraLookingAtCenterCoordinate:self.mapView.centerCoordinate fromDistance:7000 pitch:0 heading:0];
        [weakMapView setCamera:cameraInitial withDuration:2 animationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut] completionHandler:^(void){
            [self.mapView removeAnnotations:self.mapView.annotations];
            [self setAnatation];
        }];
    }];
}
-(void)moveToDestination{
    // . NSLog(@"\nmap z scenario moveToDestination");
    MGLMapCamera *camera = [MGLMapCamera cameraLookingAtCenterCoordinate:self.mapView.centerCoordinate fromDistance:4500 pitch:15 heading:45];
    [self.mapView setCamera:camera withDuration:5 animationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
}

- (void)changeMapStyle:(UISegmentedControl *)sender {
    if([self.mapView.styleURL isEqual:[NSURL URLWithString:mapStyleDefault]]){
        self.mapView.styleURL = [MGLStyle satelliteStyleURL];
//        [self setMapCameraFlat:NO];
    } else {
        self.mapView.styleURL = [NSURL URLWithString:mapStyleDefault];
//        [self setMapCameraFlat:YES];
    }
    // . NSLog(@"self.mapView.styleURL == %@", self.mapView.styleURL);
}
- (void)hidePlaceCityNameLayerFifa2018AndStyle:(MGLStyle *)style {
    if([self.mapView.styleURL isEqual:[NSURL URLWithString:mapStyleDefault] ]){
        MGLStyleLayer *placecitymds = [style layerWithIdentifier:@"place-city-md-s"];
        if (placecitymds != nil) {
            [placecitymds setVisible:NO];
        }
        MGLStyleLayer *placecitysm = [style layerWithIdentifier:@"place-city-sm"];
        if (placecitysm != nil) {
            [placecitysm setVisible:NO];
        }
        MGLStyleLayer *placetown = [style layerWithIdentifier:@"place-town"];
        if (placetown != nil) {
            [placetown setVisible:NO];
        }
        
        
        
        MGLStyleLayer *fifa2018arrow = [style layerWithIdentifier:@"fifa2018-3-0d4xz7"];
        if (fifa2018arrow != nil) {
            
            [fifa2018arrow setVisible:YES];
        }
        
    }
}

- (void)map3DbuildingsWith:(MGLMapView *)mapView andStyle:(MGLStyle *)style {
    
    //    3D дома ==============================================================
    if([self.mapView.styleURL isEqual:[NSURL URLWithString:mapStyleDefault] ]){
        MGLStyleLayer *fifa2018arrow = [self.mapView.style layerWithIdentifier:@"fifa2018-3-0d4xz7"];
        if (fifa2018arrow != nil) {
            
            [fifa2018arrow setVisible:NO];
        }
        
        
        MGLSource *source = [style sourceWithIdentifier:@"composite"];
        //    Доступ к Mapbox улиц источник и использовать его, чтобы создать `MGLFillExtrusionStyleLayer`. Идентификатор источника - "composite". Используйте свойство` sources ' для стиля для проверки идентификаторов источника.
        MGLFillExtrusionStyleLayer *layer = [[MGLFillExtrusionStyleLayer alloc] initWithIdentifier:@"buildings" source:source];
        layer.sourceLayerIdentifier = @"building";
        layer.predicate = [NSPredicate predicateWithFormat:@"extrude == 'true'"];
        layer.fillExtrusionHeight = [NSExpression expressionForKeyPath:@"height"];
        layer.fillExtrusionBase = [NSExpression expressionForKeyPath:@"min_height"];
        layer.fillExtrusionOpacity = [NSExpression expressionForConstantValue:@0.75];
        layer.fillExtrusionColor = [NSExpression expressionForConstantValue:[UIColor lightGrayColor]];
        // Вставьте слой вытягивания заливки под слоем метки POI. Если Вы не уверены, как называется слой, вы можете просмотреть стиль в Mapbox Studio или выполнить итерацию по свойству layers стиля, распечатав идентификатор каждого слоя.
        MGLStyleLayer *scalerank = [style layerWithIdentifier:@"poi-scalerank4-l15"];
        [style insertLayer:layer belowLayer:scalerank];
        
        
        
        /*

        MGLFillExtrusionStyleLayer *layer = [[MGLFillExtrusionStyleLayer alloc] initWithIdentifier:@"buildings" source:source];
        layer.sourceLayerIdentifier = @"building";
        
        // Filter out buildings that should not extrude.
        layer.predicate = [NSPredicate predicateWithFormat:@"extrude == 'true' AND height >= 0"];
        
        // Set the fill extrusion height to the value for the building height attribute.
        layer.fillExtrusionHeight = [MGLStyleValue valueWithInterpolationMode:MGLInterpolationModeIdentity sourceStops:nil attributeName:@"height" options:nil];
        layer.fillExtrusionBase =[MGLStyleValue valueWithInterpolationMode:MGLInterpolationModeIdentity sourceStops:nil attributeName:@"min_height" options:nil];
        layer.fillExtrusionOpacity = [MGLStyleValue valueWithRawValue:@0.75];
        layer.fillExtrusionColor = [MGLStyleValue valueWithRawValue:[UIColor lightGrayColor]];
        
        
        MGLStyleLayer *symbolLayer = [style layerWithIdentifier:@"poi-scalerank4-l15"];//по умолчанию poi-scalerank3
        [style insertLayer:layer belowLayer:symbolLayer];
          */
    }
    //    3D дома ==============================================================
    
}



#pragma mark - MapBox's Default Method






- (void)mapView:(MGLMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    // . NSLog(@"\nmap scenario regionDidChangeAnimated");
//      NSLog(@"regionDidChangeAnimated1 \n pitch = %f, \n heading = %f, \n direction = %f, \n zoomLevel = %f, \n zooaltitudemLevel = %f \n latitude = %f \n longitude = %f",self.mapView.camera.pitch, self.mapView.camera.heading,self.mapView.direction, self.mapView.zoomLevel, self.mapView.camera.altitude, self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude);
    
}











#pragma mark - Others stuff

-(MGLCoordinateBounds)calculateBoundsWithArray:(NSArray*) coordinates{
    //    NSMutableArray<CLLocation *> *locations = [NSMutableArray array];
    
    CGFloat e, w, n, s;
    e = 0;
    w = 0;
    n = 0;
    s = 0;
    for (NSArray<NSNumber *> *c in coordinates) {
//        NSLog(@"c = %@", c);
//        NSLog(@"c[0] = %@", c[0]);
//        NSLog(@"c[1] = %@", c[1]);
        if(n == 0){
            n = [c[0] doubleValue];
        } else if(n <  [c[0] doubleValue]) {
            n = [c[0] doubleValue];
        }
        if(s == 0){
            s = [c[0] doubleValue];
        } else if(s >  [c[0] doubleValue]) {
            s = [c[0] doubleValue];
        }
        if(e == 0){
            e = [c[1] doubleValue];
        } else if(e >  [c[1] doubleValue]) {
            e = [c[1] doubleValue];
        }
        if(w == 0){
            w = [c[1] doubleValue];
        } else if(w <  [c[1] doubleValue]) {
            w = [c[1] doubleValue];
        }
    }
    CLLocationCoordinate2D sw = CLLocationCoordinate2DMake(w, s);
    CLLocationCoordinate2D ne = CLLocationCoordinate2DMake(e, n);
    MGLCoordinateBounds bounds = MGLCoordinateBoundsMake(sw, ne);
    
    return bounds;
}


#pragma mark - Stroke and Fill shapes


- (CGFloat)mapView:(MGLMapView *)mapView alphaForShapeAnnotation:(MGLShape *)annotation {
    // Set the alpha for all shape annotations to 1 (full opacity)
    return 1.0f;
}

- (CGFloat)mapView:(MGLMapView *)mapView lineWidthForPolylineAnnotation:(MGLPolyline *)annotation {
    // Set the line width for polyline annotations
    return 2.0f;
}

- (UIColor *)mapView:(MGLMapView *)mapView strokeColorForShapeAnnotation:(MGLShape *)annotation {
    // . NSLog(@"\nmap z strokeColorForShapeAnnotation");
    // Set the stroke color for shape annotations
    // ... but give our polyline a unique color by checking for its `title` property
    // . NSLog(@"annotation.title = %@",annotation.title);
//    if ([annotation isKindOfClass:[PKMapAnnotation class]]) {
//        PKMapAnnotation *myannotation = (PKMapAnnotation*) annotation;
//
//        if(![myannotation.mapObject.geoJSON isEqualToString:@""] && myannotation.mapObject.geoJSON != nil){
//            NSLog(@"astrokeColorForShapeAnnotation");
//            return [UIColor colorWithRed:35.0f/255.0f green:62.0f/255.0f blue:149.0f/255.0f alpha:0.5f];
//        }
//    }
    //    if ([annotation.title isEqualToString:@"lenin"]) {
    //        // Mapbox cyan
    //        return [UIColor colorWithRed:123.0f/255.0f green:174.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
    //    } else if ([annotation.title isEqualToString:@"parking"]) {
    //        // Mapbox cyan
    //        return [UIColor colorWithRed:35.0f/255.0f green:62.0f/255.0f blue:149.0f/255.0f alpha:1.0f];
    //    } else if ([annotation.title isEqualToString:@"museum"]) {
    //        // Mapbox cyan
    //        return [UIColor colorWithRed:234.0f/255.0f green:228.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
    return [UIColor colorWithRed:35.0f/255.0f green:62.0f/255.0f blue:149.0f/255.0f alpha:0.5f];
//    return [UIColor redColor];
}
- (UIColor *)mapView:(MGLMapView *)mapView fillColorForPolygonAnnotation:(MGLPolygon *)annotation {
    // . NSLog(@"\nmap z fillColorForPolygonAnnotation");
    // Mapbox cyan fill color
    //    return [UIColor colorWithRed:59.0f/255.0f green:178.0f/255.0f blue:208.0f/255.0f alpha:0.5f];
    // . NSLog(@"annotation.title = %@",annotation.title);
//    if ([annotation.title isEqualToString:@"lenin"]) {
//        // Mapbox cyan
//        return [UIColor colorWithRed:123.0f/255.0f green:174.0f/255.0f blue:65.0f/255.0f alpha:1.0f];
//    } else if ([annotation.title isEqualToString:@"parking"]) {
//        // Mapbox cyan
//        return [UIColor colorWithRed:35.0f/255.0f green:62.0f/255.0f blue:149.0f/255.0f alpha:0.5f];
//    } else if ([annotation.title isEqualToString:@"museum"]) {
//        // Mapbox cyan
//        return [UIColor colorWithRed:234.0f/255.0f green:228.0f/255.0f blue:225.0f/255.0f alpha:1.0f];
//    } else{
//        return [UIColor redColor];
//    }
    return [UIColor colorWithRed:35.0f/255.0f green:62.0f/255.0f blue:149.0f/255.0f alpha:0.5f];
}
#pragma mark - Ulyanovsk Area

- (BOOL)mapView:(MGLMapView *)mapView shouldChangeFromCamera:(MGLMapCamera *)oldCamera toCamera:(MGLMapCamera *)newCamera {
    // . NSLog(@"\nmap z shouldChangeFromCamera");
    // Get the current camera to restore it after.
    MGLMapCamera *currentCamera = mapView.camera;
    
    // From the new camera obtain the center to test if it’s inside the boundaries.
    CLLocationCoordinate2D newCameraCenter = newCamera.centerCoordinate;
    
    // Set the map’s visible bounds to newCamera.
    mapView.camera = newCamera;
    MGLCoordinateBounds newVisibleCoordinates = mapView.visibleCoordinateBounds;
    
    // Revert the camera.
    mapView.camera = currentCamera;
    
    // Test if the newCameraCenter and newVisibleCoordinates are inside self.colorado.
    BOOL inside = MGLCoordinateInCoordinateBounds(newCameraCenter, self.ulyanovskArea);
    BOOL intersects = MGLCoordinateInCoordinateBounds(newVisibleCoordinates.ne, self.ulyanovskArea) && MGLCoordinateInCoordinateBounds(newVisibleCoordinates.sw, self.ulyanovskArea);
    
    return inside && intersects;
}




#pragma mark - Add layer with animation
- (void)addLayerGeoRoute {
    // . NSLog(@"\nmap z addLayerGeoRoute");
    // Add an empty MGLShapeSource, we’ll keep a reference to this and add points to this later.
    MGLShapeSource *source = [[MGLShapeSource alloc] initWithIdentifier:@"polyline" features:@[] options:nil];
    [self.mapView.style addSource:source];
    self.polylineSource = source;
    
    // Add a layer to style our polyline.
    MGLLineStyleLayer *layer = [[MGLLineStyleLayer alloc] initWithIdentifier:@"polyline" source:source];
    layer.lineJoin = [NSExpression expressionForConstantValue:@"round"];
    layer.lineCap = layer.lineJoin = [NSExpression expressionForConstantValue:@"round"];
    layer.lineColor = [NSExpression expressionForConstantValue:[UIColor redColor]];
    
    // The line width should gradually increase based on the zoom level.
    layer.lineWidth = [NSExpression expressionWithFormat:@"mgl_interpolate:withCurveType:parameters:stops:($zoomLevel, 'linear', nil, %@)",
  @{@14: @5, @18: @20}];
   // [self.mapView.style addLayer:layer];
    self.geoRouteLayer = layer;
    MGLStyleLayer *symbolLayer = [self.mapView.style layerWithIdentifier:@"waterway-label"];//по умолчанию poi-scalerank3
    [self.mapView.style insertLayer:self.geoRouteLayer aboveLayer:symbolLayer];
    
    /*
    MGLShapeSource *source = [[MGLShapeSource alloc] initWithIdentifier:@"polyline" features:@[] options:nil];
    if(self.geoRouteLayer != nil){
        // . NSLog(@"self.geoRouteLayer != nil");
        [self.mapView.style removeLayer:self.geoRouteLayer];
        [self.mapView.style removeSource:self.polylineSource];
    }
    [self.mapView.style addSource:source];
    
   
    self.polylineSource = source;
    
    // Add a layer to style our polyline.
    MGLLineStyleLayer *layer = [[MGLLineStyleLayer alloc] initWithIdentifier:@"polyline" source:source];
    layer.lineJoin = [MGLStyleValue valueWithRawValue:[NSValue valueWithMGLLineJoin:MGLLineJoinRound]];
    layer.lineCap = [MGLStyleValue valueWithRawValue:[NSValue valueWithMGLLineCap:MGLLineCapRound]];
    layer.lineColor = [MGLStyleValue valueWithRawValue:[UIColor redColor]];
    layer.lineWidth = [MGLStyleValue valueWithInterpolationMode:MGLInterpolationModeExponential
                                                    cameraStops: @{
                                                                   @14: [MGLStyleValue valueWithRawValue:@5],
                                                                   @18: [MGLStyleValue valueWithRawValue:@20]
                                                                   }
                                                        options:@{MGLStyleFunctionOptionDefaultValue:@1.75}];
    self.geoRouteLayer = layer;
    MGLStyleLayer *symbolLayer = [self.mapView.style layerWithIdentifier:@"waterway-label"];//по умолчанию poi-scalerank3
    [self.mapView.style insertLayer:self.geoRouteLayer aboveLayer:symbolLayer];
    
    
//    [self.mapView.style addLayer:self.geoRouteLayer];
     */
}

- (void)animatePolyline {
    // . NSLog(@"\nmap z animatePolyline");
    _currentIndex = 1;
    
    // Запустите Таймер, который будет имитировать добавление точек к нашей полилинии. Это также может представлять координаты, добавляемые к нашей полилинии из другого источника, например CLLocationManagerDelegate.
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(tick) userInfo:nil repeats:YES];
}

- (void)tick {
    // . NSLog(@"\nmap z tick");
    if (_currentIndex > self.geoRoutePoints.count) {
        [_timer invalidate];
        _timer = nil;
        if([self.mapView.annotations count] < [[self.geoRoute.mapObject allObjects] count]){
            // . NSLog(@"\nmap z tick end removeAnnotations");
            [self.mapView removeAnnotations:self.mapView.annotations];
            for(PKMapObj* obj in [self.geoRoute.mapObject allObjects]){
                [self setAnontationFromObject:obj];
            }
        }
        return;
    }
    
    // Создайте подмассив местоположений вплоть до текущего индекса.
    NSArray *currentLocations = [self.geoRoutePoints subarrayWithRange:NSMakeRange(0, _currentIndex)];
    
    CLLocation *currentCoordinate = (CLLocation*)[currentLocations lastObject];
//    CLLocationCoordinate2D las = CLLocationCoordinate2DMake(currentCoordinate.coordinate.latitude,  currentCoordinate.coordinate.longitude);
    // . NSLog(@"tick tick = %f %f", las.latitude, las.longitude);
    
    CLLocationCoordinate2D lasSW = CLLocationCoordinate2DMake(currentCoordinate.coordinate.latitude-0.001f,  currentCoordinate.coordinate.longitude-0.001f);
    CLLocationCoordinate2D lasNE = CLLocationCoordinate2DMake(currentCoordinate.coordinate.latitude+0.001f,  currentCoordinate.coordinate.longitude+0.001f);
    MGLCoordinateBounds lasBounds = MGLCoordinateBoundsMake(lasSW, lasNE);
    MGLCoordinateSpan lasSpan = MGLCoordinateSpanMake(0.001f, 0.001f);
    MGLCoordinateBounds lasOfset = MGLCoordinateBoundsOffset(lasBounds, lasSpan); //MGLCoordinateBoundsOffset(lasBounds, lasSpan);
    
    for(PKMapObj* obj in [self.geoRoute.mapObject allObjects]){
        
        CLLocationCoordinate2D objCoord = CLLocationCoordinate2DMake([obj.latitude doubleValue], [obj.longitude doubleValue]);
        
        if(MGLCoordinateInCoordinateBounds(objCoord, lasOfset)){
            // . NSLog(@"coors 2 = %f %f", las.latitude, las.longitude);
            [self setAnontationFromObject:obj];
        } else {
            // . NSLog(@"coors 2 != %f %f %f %f", lasOfset.ne.latitude, lasOfset.ne.longitude, lasOfset.sw.latitude, lasOfset.sw.longitude);
        }
    }
    [self updatePolylineWithLocations:currentLocations];
    
    _currentIndex++;
}

- (void)updatePolylineWithLocations:(NSArray<CLLocation *> *)locations {
    // . NSLog(@"\nmap z updatePolylineWithLocations");
    CLLocationCoordinate2D coordinates[locations.count];
    
    for (NSUInteger i = 0; i < locations.count; i++) {
        coordinates[i] = locations[i].coordinate;
    }
    
    MGLPolylineFeature *polyline = [MGLPolylineFeature polylineWithCoordinates:coordinates count:locations.count];
    
    // При обновлении формы MGLShapeSource карта будет перерисована нашей полилинией с текущими координатами.
    self.polylineSource.shape = polyline;
}




#pragma mark - Filter Popup Protocol
-(NSInteger)reloadAnnotationWithArray:(NSArray *)selectionsTypes andArray:(NSArray *)selectionsAdditional{
    [self.mapView removeAnnotations:self.mapView.annotations];
    if([selectionsTypes count] == 0 || selectionsTypes == nil){
        selectionsTypes = [[PKDataManager sharedManager] getTypesOfObjectsArrayGrouped]; //тут нужен функционал
    }
    self.selectionsTypes = selectionsTypes;
    self.selectionsAdditional = selectionsAdditional;
    //NSLog(@"self.selectionsTypes = %@ %@", self.selectionsTypes, self.selectionsAdditional);
    [self setAnatation];
    
    return [self.mapView.annotations count];
}
-(void)reloadCVCIfNeeded{

        NSArray *tabBarControllers = [self.tabBarController viewControllers];
        for( int i=0;i<[ tabBarControllers count];i++){
            id map2=[tabBarControllers objectAtIndex:i];
            if([map2 isKindOfClass:[UINavigationController class]]){
                UINavigationController * navControl = (UINavigationController*)map2;
                NSArray *viewContrlls=[navControl viewControllers];
                NSMutableArray *newViewControllers = [[NSMutableArray alloc] init];
                for( int i=0;i<[ viewContrlls count];i++){
                    id vc=[viewContrlls objectAtIndex:i];
                    // . NSLog(@"viewContrlls = %@", [map class]);
                    if([vc isKindOfClass:[PKMainMapViewController class]]){
                        PKMapObjectCollectionViewController *mapObjectViewController = (PKMapObjectCollectionViewController*)vc;
                        
                        
                        [mapObjectViewController reloadAnnotationWithArray:self.selectionsTypes andArray:self.selectionsAdditional];
                        [newViewControllers addObject:mapObjectViewController];
                        // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
                        //                        return;
                        
                        
                    } else if([vc isKindOfClass:[PKMapObjectDetailVC class]] || [vc isKindOfClass:[PKGeoRouteDetailVC class]]){
                        //#warning kill PKMapObjectDetailVC if needed
                        
                    } else {
                        [newViewControllers addObject:vc];
                    }
                }
                [navControl setViewControllers:newViewControllers];
                
            }
        }

}

- (NSInteger)didFinishWithArray:(NSArray *)selectionsTypes andArray:(NSArray *)selectionsAdditional{
    // . NSLog(@"multiSelectionViewController didFinishWithSelections %@",selectionsTypes);
    [self reloadCVCIfNeeded];
    return  [self reloadAnnotationWithArray:selectionsTypes andArray:selectionsAdditional];
}

#pragma mark - Filter Popup control
-(void)segueFilter:(id)sender{
    
    
    
    PKMapBottomPopupVC* mapBottomPopupController =  [self.storyboard instantiateViewControllerWithIdentifier:@"PKMapBottomPopupVC"];
    mapBottomPopupController.delegate = self;
    
    NSInteger objectCount = [self.mapView.annotations count];
    if(objectCount == 0){
        if([self.selectionsTypes count] == 0 || self.selectionsTypes == nil){
            self.selectionsTypes = [[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1];
        }
        if([self.selectionsAdditional count] == 0 || self.selectionsAdditional == nil){
            self.selectionsAdditional = [[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:2];
        }
        NSArray* array = [[NSArray alloc] initWithArray:[[PKDataManager sharedManager] getMapObjectsWithTypesLike:self.selectionsTypes andAdditional:self.selectionsAdditional orNameLike:nil]];
        objectCount = [array count];
    }
    
    
    mapBottomPopupController.countFilteredObject = objectCount;
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:mapBottomPopupController];
    
    
    popupController.style = STPopupStyleBottomSheet;
    
    popupController.containerView.layer.cornerRadius = 10;
    
    
    [popupController.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundViewDidTap)]];
    
    // . NSLog(@"popupController1 %@", popupController);
    self.popup = popupController;
    [popupController presentInViewController:self];
}
-(void)backgroundViewDidTap{
    
    //    STPopupController* popupController = [[STPopupController alloc] initWithRootViewController:self.popup];
    // . NSLog(@"popupController2 %@", self.popup);
    
    // . NSLog(@"PKMainMapViewController selections selectionsTypes = %@", self.selectionsTypes);
    
    // . NSLog(@"PKMainMapViewController selections selectionsAdditional = %@", self.selectionsAdditional);
    //    [self.popup dismiss];
    [self.popup popViewControllerAnimated:YES];
}


#pragma mark - Filter Popup Transition

- (void)popupControllerAnimateTransition:(nonnull STPopupControllerTransitioningContext *)context completion:(nonnull void (^)())completion {
    UIView *containerView = context.containerView;
    if (context.action == STPopupControllerTransitioningActionPresent) {
        containerView.transform = CGAffineTransformMakeTranslation(containerView.superview.bounds.size.width - containerView.frame.origin.x, 0);
        
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 usingSpringWithDamping:1 initialSpringVelocity:1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            context.containerView.transform = CGAffineTransformIdentity;
        } completion:^(BOOL finished) {
            completion();
        }];
    }
    else {
        [UIView animateWithDuration:[self popupControllerTransitionDuration:context] delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            containerView.transform = CGAffineTransformMakeTranslation(- 2 * (containerView.superview.bounds.size.width - containerView.frame.origin.x), 0);
        } completion:^(BOOL finished) {
            containerView.transform = CGAffineTransformIdentity;
            completion();
        }];
    }
}

- (NSTimeInterval)popupControllerTransitionDuration:(nonnull STPopupControllerTransitioningContext *)context {
    return context.action == STPopupControllerTransitioningActionPresent ? 0.5 : 0.35;
}




#pragma mark - Navigations back

-(void)popBackToMainVC:(id)sender{
    // . NSLog(@"popToRootViewControllerAnimated popBack");
    if(self.tabBarController != nil){
        [[self.tabBarController navigationController] popToRootViewControllerAnimated:YES];
    } else {
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }
    //    [self performSegueWithIdentifier:@"toMainVC" sender:sender];
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    
}

-(void)popBack:(id)sender{
    [[self navigationController] popViewControllerAnimated:YES];
}

#pragma mark - Navigations

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toMapObjectDetailVC"]){
        PKMapAnnotation* annotation = (PKMapAnnotation*)sender;
        PKMapObjectDetailVC*vc = [segue destinationViewController];
        PKMapObj* mapObject = [[PKDataManager sharedManager] oneMapObject: annotation.mapObject.objectID];
        [vc setMapObject:mapObject];
        // . NSLog(@"\ndealloc navigationController on stack = %lu", [[self.navigationController viewControllers] count]);
        
    } else if ([segue.identifier isEqualToString:@"StartNavigation"]) {
        //+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
        /*MBNavigationViewController *controller = (MBNavigationViewController *)[segue destinationViewController];
        
        controller.directions = [MBDirections sharedDirections];
        controller.route = self.route;
        
        controller.routeController.locationManager = [[MBSimulatedLocationManager alloc] initWithRoute:self.route];*/
        //+++++++++++++++++++++++++++++Navigation+++++++++++++++++++++++++++++
    } else if([segue.identifier isEqualToString:@"toMainVC"]){
        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
        NSArray *viewContrlls=[navControl viewControllers];
        for( int i=0;i<[ viewContrlls count];i++){
            id vcs=[viewContrlls objectAtIndex:i];
            if([vcs isKindOfClass:[PKMainViewController class]]){
                
                PKMainViewController *vc = (PKMainViewController*)vcs;
                [vc setInstanceView:YES];
                return;
            }
        }
    }
    
}


- (void)drawShape:(NSArray*)  coordinates {
    
    // <CLLocation *>
//    NSString *thePath = [[NSBundle mainBundle]  pathForResource:@"Roots" ofType:@"plist"];
//    NSDictionary *pointsDic = [[NSDictionary alloc] initWithContentsOfFile:thePath];
//
//    NSArray *pointsArray = [NSArray arrayWithArray:[pointsDic objectForKey:@"roade1"]];
    

//    NSInteger c = [pointsArray count];
//    CLLocationCoordinate2D pointsToUse[c];
    NSInteger c = [coordinates count];
    CLLocationCoordinate2D pointsToUse[c];
    
    int index = 0;
    for (NSArray<NSNumber *> *coord in coordinates) {
        
        pointsToUse[index] = CLLocationCoordinate2DMake([coord[1] doubleValue],[coord[0] doubleValue]);
        index++;
    }
    
    
    
//    for(int i = 0; i < c; i++) {
//        NSLog(@"CGPoint = %@", [coordinates objectAtIndex:i]);
//        CGPoint p = CGPointFromString([coordinates objectAtIndex:i]);
//        pointsToUse[i] = CLLocationCoordinate2DMake([[coordinates objectAtIndex:i] objectAtIndex:0],p.y);
////        pointsToUse[i] = CLLocationCoordinate2DMake(p.x,p.y);
//        NSLog(@"coord %f",pointsToUse [i].longitude);
//        NSLog(@"coord %f",pointsToUse [i].latitude);
//
//    }
    
    
    MGLPolygon *myPolyline = [MGLPolygon polygonWithCoordinates:pointsToUse count:c];
    self.annotationArea = myPolyline;
    
//    [self.mapView addAnnotation:self.annotationArea];
    [self animatePolylineArea];
//    [[self mv] addOverlay:myPolyline];
    
    
    
    // Create a coordinates array to hold all of the coordinates for our shape.
//        CLLocationCoordinate2D coordinates[] = {
    //        CLLocationCoordinate2DMake(45.522585, -122.685699),
    //        CLLocationCoordinate2DMake(45.534611, -122.708873),
    //        CLLocationCoordinate2DMake(45.530883, -122.678833),
    //        CLLocationCoordinate2DMake(45.547115, -122.667503),
    //        CLLocationCoordinate2DMake(45.530643, -122.660121),
    //        CLLocationCoordinate2DMake(45.533529, -122.636260),
    //        CLLocationCoordinate2DMake(45.521743, -122.659091),
    //        CLLocationCoordinate2DMake(45.510677, -122.648792),
    //        CLLocationCoordinate2DMake(45.515008, -122.664070),
    //        CLLocationCoordinate2DMake(45.502496, -122.669048),
    //        CLLocationCoordinate2DMake(45.515369, -122.678489),
    //        CLLocationCoordinate2DMake(45.506346, -122.702007),
    //        CLLocationCoordinate2DMake(45.522585, -122.685699),
    //    };
//    NSUInteger numberOfCoordinates = sizeof(coordinates) / sizeof(CLLocationCoordinate2D);
//
//    // Create our shape with the formatted coordinates array
//    MGLPolygon *shape = [MGLPolygon polygonWithCoordinates:coordinates count:numberOfCoordinates];
//
//    // Add the shape to the map
//    [self.mapView addAnnotation:shape];
}
- (void)animatePolylineArea {
    _currentIndex = 1;
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.4 target:self selector:@selector(tickArea) userInfo:nil repeats:YES];
}
- (void)tickArea {
    if (_currentIndex > 5) {
        [_timer invalidate];
        _timer = nil;
        return;
    }
    if (_currentIndex % 2){
        [self.mapView addAnnotation:self.annotationArea];
    } else {
        [self.mapView removeAnnotation:self.annotationArea];
    }
    
    
    _currentIndex++;
}

@end






