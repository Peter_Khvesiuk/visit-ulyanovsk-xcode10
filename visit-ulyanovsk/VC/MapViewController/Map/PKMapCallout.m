//
//  PKMapCallout.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 24.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapCallout.h"
#import "PKMapAnnotation.h"
#import "PKUOStyleKit.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#import "PKUOStyleKit.h"
//#import "PKButtonLike.h"
#import "PKMapObj+CoreDataClass.h"
//#import "PKDataManager.h"

// Set defaults for custom tip drawing
static CGFloat const tipHeight = 10.0;
static CGFloat const tipWidth = 20.0;

@interface PKMapCallout ()
@property (strong, nonatomic) UIButton *mainButton;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UIImageView *arrowToRight;
@property (strong, nonatomic) UIView *mainBody;
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *subTitle;
//@property (strong, nonatomic) PKButtonLike *buttonLike;


@end

@implementation PKMapCallout {
    id <MGLAnnotation> _representedObject;
    __unused UIView *_leftAccessoryView;/* unused */
    __unused UIView *_rightAccessoryView;/* unused */
    __weak id <MGLCalloutViewDelegate> _delegate;
    BOOL _dismissesAutomatically;
    BOOL _anchoredToAnnotation;
}

@synthesize representedObject = _representedObject;
@synthesize leftAccessoryView = _leftAccessoryView;/* unused */
@synthesize rightAccessoryView = _rightAccessoryView;/* unused */
@synthesize delegate = _delegate;
@synthesize anchoredToAnnotation = _anchoredToAnnotation;
@synthesize dismissesAutomatically = _dismissesAutomatically;

- (instancetype)initWithFrame:(CGRect)frame
{
    // . NSLog(@"PKMapCallout initWithFrame");
    self = [super initWithFrame:frame];
    if (self)
    {
        // . NSLog(@"PKMapCallout initWithFrame if (self)");
        self.backgroundColor = [UIColor clearColor];
        
        // Create and add a subview to hold the callout’s text
//        UIButton *mainBody = [UIButton buttonWithType:UIButtonTypeSystem];
//        mainBody.backgroundColor = [UIColor clearColor];//[self backgroundColorForCallout];
//        mainBody.tintColor = [UIColor whiteColor];
//        mainBody.contentEdgeInsets = UIEdgeInsetsMake(110.0, 110.0, 10.0, 10.0);
//        mainBody.layer.cornerRadius = 4.0;
        CGRect size = CGRectMake(0.f, 0.f, 300.f, 100.f);
        UIView *mainBody = [[UIView alloc] initWithFrame:size];
        mainBody.backgroundColor = [self backgroundColorForCallout];
        mainBody.layer.cornerRadius = 4.0;
        self.mainBody = mainBody;
        
        [self addSubview:self.mainBody];
        
        UIButton * mainButton = [[UIButton alloc] initWithFrame:self.mainBody.frame];
        mainButton.backgroundColor = [UIColor clearColor];
//        mainButton.tintColor = [UIColor blackColor];
        mainButton.contentEdgeInsets = UIEdgeInsetsMake(10.0, 110.0, 10.0, 10.0);
        self.mainButton = mainButton;
        [self.mainBody addSubview:self.mainButton];
        
        
        UIImageView *arrowToRight = [[UIImageView alloc] initWithFrame:CGRectMake(size.size.width - 17.f, size.size.height/2 - 20.f/2, 13.f, 20.f)];
        self.arrowToRight = arrowToRight;
        [self.mainBody addSubview:self.arrowToRight];
        
        
        CGRect sizeImage = CGRectMake(0.f, 0.f, 100.f, 100.f);
        UIImageView * imageView = [[UIImageView alloc] initWithFrame:sizeImage];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.layer.masksToBounds = YES;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
//        imageView.layer.cornerRadius = 4.0;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:imageView.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(4.0, 4.0)];
        CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
        maskLayer.frame = imageView.bounds;
        maskLayer.path  = maskPath.CGPath;
        imageView.layer.mask = maskLayer;
//        imageView.image.size = sizeImage;
        self.imageView = imageView;
        [self.mainBody addSubview:self.imageView];
        
        
        UILabel*title = [[UILabel alloc] initWithFrame:CGRectMake(sizeImage.size.width+ 5.f, 5.f, size.size.width - sizeImage.size.width - 20.f, 70.f)];
        title.font = [UIFont systemFontOfSize:15];
        title.numberOfLines = 0;
        self.title = title;
        [self.mainBody addSubview:self.title];
        
        
        UILabel*subTitle = [[UILabel alloc] initWithFrame:CGRectMake(sizeImage.size.width+ 5.f, size.size.height - 25.f, size.size.width - sizeImage.size.width - 25.f, 20.f)];
        subTitle.numberOfLines = 0;
        subTitle.font = [UIFont systemFontOfSize:13];
        [subTitle setTextColor:[UIColor grayColor]];
        [subTitle setTextAlignment:NSTextAlignmentRight];
        self.subTitle = subTitle;
        [self.mainBody addSubview:self.subTitle];
        
    }
    
    return self;
}
//-(void)pushBtnLike: (PKButtonLike*) sender{
//    // . NSLog(@"pushBtnLike");
//    PKMapAnnotation* annotation = (PKMapAnnotation*)self.representedObject;
//    if(annotation.myLike){
//        if(annotation.likesCount>0){
//            [sender setImageWithLikeEnabled:NO withLikeCount:annotation.likesCount - 1];
//        } else {
//            [sender setImageWithLikeEnabled:NO withLikeCount:annotation.likesCount];
//        }
//    } else {
//        [sender setImageWithLikeEnabled:YES withLikeCount:annotation.likesCount + 1];
//    }
//    [self.buttonLike setImageWithLikeEnabled:annotation.myLike withLikeCount:annotation.likesCount];
//
//    [sender setNeedsDisplay];
//    [[PKDataManager sharedManager] switchObjectLike:annotation.mapObjectID];
//
//}

#pragma mark - MGLCalloutView API
- (void)presentCalloutFromRect:(CGRect)rect inView:(nonnull UIView *)view constrainedToRect:(CGRect)constrainedRect animated:(BOOL)animated
//- (void)presentCalloutFromRect:(CGRect)rect inView:(UIView *)view constrainedToView:(UIView *)constrainedView animated:(BOOL)animated
{
    NSLog(@"PKMapCallout presentCalloutFromRect");
    if([self.representedObject isKindOfClass:[PKMapAnnotation class]]){
        PKMapAnnotation* annotation = (PKMapAnnotation*)self.representedObject;
        
        // Do not show a callout if there is no title set for the annotation
        if (![annotation respondsToSelector:@selector(title)])
        {
            return;
        }
        
        [view addSubview:self];
        
        // Prepare title label
        //    [self.mainButton setTitle:self.representedObject.title forState:UIControlStateNormal];
        
        self.arrowToRight.image = [PKUOStyleKit imageOfRowToRight];
        
//        [self.buttonLike setImageWithLikeEnabled:annotation.mapObject.mylike withLikeCount:annotation.mapObject.likes  andIsDetail:YES];
        
        self.title.text = annotation.mapObject.title;
        [self.title sizeToFit];
        self.subTitle.text = annotation.reuseIdentifier;
//        [self.subTitle sizeToFit];
        [self imageCalloutAccessoryViewForAnnotation:annotation];
        //    [self.mainBody sizeToFit];
        
        if ([self isCalloutTappable])
        {
            // Handle taps and eventually try to send them to the delegate (usually the map view)
            [self.mainButton addTarget:self action:@selector(calloutTapped) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            // Disable tapping and highlighting
            self.mainButton.userInteractionEnabled = NO;
        }
    }
    
    
    // Prepare our frame, adding extra space at the bottom for the tip
    // . NSLog(@"mainBody = %f", self.mainBody.bounds.size.width);
    CGFloat frameWidth = self.mainBody.bounds.size.width;//+100.f;
    CGFloat frameHeight = self.mainBody.bounds.size.height + tipHeight;// + 100.f;//self.mainBody.bounds.size.height
    CGFloat frameOriginX = rect.origin.x + (rect.size.width/2.0) - (frameWidth/2.0);
    CGFloat frameOriginY = rect.origin.y - frameHeight;
    self.frame = CGRectMake(frameOriginX, frameOriginY,
                            frameWidth, frameHeight);
    // . NSLog(@"\n frameOriginX = %.f,\n frameOriginY = %.f,\n frameWidth = %.f,\n frameHeight = %.f", frameOriginX, frameOriginY, frameWidth, frameHeight);
    if (animated)
    {
        self.alpha = 0.0;
        
        [UIView animateWithDuration:0.2 animations:^{
            self.alpha = 1.0;
        }];
    }
}

- (void)imageCalloutAccessoryViewForAnnotation:(id<MGLAnnotation>)annotation
{
    // . NSLog(@"PKMapCallout imageCalloutAccessoryViewForAnnotation");
    PKMapAnnotation* myannotation = (PKMapAnnotation*)annotation;
//    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 100.f, 100.f)];
    
    
    // . NSLog(@"myannotation.mapObject.prevImageName = %@", myannotation.mapObject.prevImageName);
   
//    self.imageView.image = nil;
    self.imageView.image = [PKUOStyleKit imageOfNoFotoWithSize:self.imageView.frame.size];
    if(![myannotation.mapObject.prevImageName isEqualToString:@"no_image.jpg"]){
        NSString* dataPath = LOCAL_mapObjImagePathPreview(myannotation.mapObject.prevImageName);
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
            // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
            [self.imageView setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
        } else {
            // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
            NSString* imageUrl = URL_mapObjImagePathPreview(myannotation.mapObject.prevImageName);
            NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
            __weak UIImageView* weakImage = self.imageView;
            //CellWherePhoto.trenerPhotoDetail.image = nil;
            [self.imageView setImageWithURLRequest:requestwhereImage
                                  placeholderImage:nil
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                               
                                               //         // . NSLog(@"requestwhereImage success");
                                               
                                               
                                               weakImage.image = image;
                                               
                                               //                                           weakCellImage.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
                                               [weakImage layoutSubviews];
                                               
                                           }
                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                               // . NSLog(@"failure imageUrl = %@",imageUrl);
                                           }];
        }
    }
    
}

- (void)dismissCalloutAnimated:(BOOL)animated
{
    // . NSLog(@"PKMapCallout dismissCalloutAnimated");
    if (self.superview)
    {
        if (animated)
        {
            [UIView animateWithDuration:0.2 animations:^{
                self.alpha = 0.0;
            } completion:^(BOOL finished) {
                [self removeFromSuperview];
            }];
        }
        else
        {
            [self removeFromSuperview];
        }
    }
}

// Allow the callout to remain open during panning.
- (BOOL)dismissesAutomatically {
    // . NSLog(@"PKMapCallout dismissesAutomatically");
    return NO;
}

- (BOOL)isAnchoredToAnnotation {
    // . NSLog(@"PKMapCallout isAnchoredToAnnotation");
    return YES;
}

// https://github.com/mapbox/mapbox-gl-native/issues/9228
- (void)setCenter:(CGPoint)center {
    // . NSLog(@"PKMapCallout setCenter");
    center.y = center.y - CGRectGetMidY(self.bounds);
    [super setCenter:center];
}

#pragma mark - Callout interaction handlers

- (BOOL)isCalloutTappable
{
    // . NSLog(@"PKMapCallout isCalloutTappable");
    if ([self.delegate respondsToSelector:@selector(calloutViewShouldHighlight:)]) {
        return [self.delegate performSelector:@selector(calloutViewShouldHighlight:) withObject:self];
    }
    
    return NO;
}

- (void)calloutTapped
{
     // . NSLog(@"PKMapCallout calloutTapped");
    if ([self isCalloutTappable] && [self.delegate respondsToSelector:@selector(calloutViewTapped:)])
    {
        [self.delegate performSelector:@selector(calloutViewTapped:) withObject:self];
    }
}

#pragma mark - Custom view styling

- (UIColor *)backgroundColorForCallout
{
    return [UIColor whiteColor];
}

- (void)drawRect:(CGRect)rect
{
    // Draw the pointed tip at the bottom
    UIColor *fillColor = [self backgroundColorForCallout];
    // . NSLog(@"\n rect.origin.x = %.f,\n rect.origin.y = %.f,\n rect.size.width = %.f,\n rect.size.height = %.f", rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    CGFloat tipLeft = rect.origin.x + (rect.size.width / 2.0) - (tipWidth / 2.0);
    CGPoint tipBottom = CGPointMake(rect.origin.x + (rect.size.width / 2.0), rect.origin.y + rect.size.height);
    CGFloat heightWithoutTip = rect.size.height - tipHeight - 1;
    // . NSLog(@"\n tipLeft = %.0f,\n tipBottom.x = %.0f,\n tipBottom.y = %.0f,\n heightWithoutTip = %.0f", tipLeft, tipBottom.x, tipBottom.y, heightWithoutTip);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    CGMutablePathRef tipPath = CGPathCreateMutable();
    CGPathMoveToPoint(tipPath, NULL, tipLeft, heightWithoutTip);
    CGPathAddLineToPoint(tipPath, NULL, tipBottom.x, tipBottom.y);
    CGPathAddLineToPoint(tipPath, NULL, tipLeft + tipWidth, heightWithoutTip);
    CGPathCloseSubpath(tipPath);
    
    [fillColor setFill];
    CGContextAddPath(currentContext, tipPath);
    CGContextFillPath(currentContext);
    CGPathRelease(tipPath);
}

@end
