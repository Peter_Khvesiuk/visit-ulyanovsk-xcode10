//
//  PKMapCallout.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 24.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mapbox/Mapbox.h>

@interface PKMapCallout : UIView <MGLCalloutView>

@end
