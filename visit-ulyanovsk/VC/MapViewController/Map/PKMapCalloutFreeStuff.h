//
//  PKMapCalloutFreeStuff.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 12.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Mapbox/Mapbox.h>

@interface PKMapCalloutFreeStuff : UIView <MGLCalloutView>

@end
