//
//  PKMapAnnotation.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 24.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Mapbox/Mapbox.h>
//#import <CoreData/CoreData.h>

@class PKMapObj;

@interface PKMapAnnotation : NSObject <MGLAnnotation>
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain, nonnull) PKMapObj * mapObject;

@property (nonatomic, copy, nullable) NSString *title;
//@property (nonatomic, copy, nullable) NSString *subtitle;
//@property (nonatomic, copy, nullable) NSString *imageName;
//@property (nonatomic, assign) int likesCount;
//@property (nonatomic, assign) BOOL myLike;

// Custom properties that we will use to customize the annotation's image.
//@property (nonatomic, copy, nonnull) NSManagedObjectID *mapObjectID;
//@property (nonatomic, copy, nonnull) UIImage *image;
@property (nonatomic, copy, nonnull) NSString *reuseIdentifier;

@end
