//
//  PKMainMapViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 22.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PKMapObj, PKGeoRoute;
@interface PKMainMapViewController : UIViewController

@property (assign, nonatomic) BOOL showManyDestanations;
@property (assign, nonatomic) BOOL moveToMapObject;

@property (assign, nonatomic) BOOL fifa2018;

-(void)setShowMapObjects:(NSSet <PKMapObj*>*) mapObjects;
-(void)setMoveToDestanationMapObject:(PKMapObj*)mapObject;
-(void)setShowGeoRouteDestanation:(PKGeoRoute*)geoRoute;
-(NSInteger)reloadAnnotationWithArray:(NSArray *)selectionsTypes andArray:(NSArray *)selectionsAdditional;

@end
