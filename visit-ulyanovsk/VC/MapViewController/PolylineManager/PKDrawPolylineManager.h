//
//  PKDrawPolylineManager.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 09.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mapbox/Mapbox.h>

@class MGLMapView;
@interface PKDrawPolylineManager : NSObject



+ (PKDrawPolylineManager*) sharedManager;

- (void)drawPolyline:(MGLMapView*) map withMapObject:(NSString*) jsonPath;

@end
