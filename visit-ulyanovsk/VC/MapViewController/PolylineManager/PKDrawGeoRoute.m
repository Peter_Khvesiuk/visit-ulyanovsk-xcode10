//
//  PKDrawGeoRoute.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 07.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKDrawGeoRoute.h"
@interface PKDrawGeoRoute ()

@property (nonatomic, weak) MGLMapView *mapView;
@property (nonatomic, weak) NSString *json;

@end
@implementation PKDrawGeoRoute

@synthesize mapView;

+ (PKDrawGeoRoute*) sharedManager {
    
    static PKDrawGeoRoute* manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PKDrawGeoRoute alloc] init];
    });
    return manager;
}
-(void)drawPolyline:(MGLMapView*) map andRouteJSONString:(NSString*)json{
    self.mapView = map;
    self.json = json;
    // Perform GeoJSON parsing on a background thread
    dispatch_queue_t backgroundQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(backgroundQueue, ^(void) {
        // Get the path for example.geojson in the app's bundle
//        NSString *jsonPath = [[NSBundle mainBundle] pathForResource:@"map" ofType:@"geojson"];
        
        // Convert the file contents to a shape collection feature object
//        NSData *data = [[NSData alloc] initWithContentsOfFile:jsonPath];
//        NSData *data = [[NSData alloc] initWithData:self.json];
        
        const char *utfString = [json UTF8String];
        NSData *data = [NSData dataWithBytes: utfString length: strlen(utfString)];
        
        MGLShapeCollectionFeature *shapeCollectionFeature = (MGLShapeCollectionFeature *)[MGLShape shapeWithData:data encoding:NSUTF8StringEncoding error:NULL];
        
        
        for (MGLPolylineFeature *polyline in shapeCollectionFeature.shapes) {
            polyline.title = polyline.attributes[@"name"];
            // . NSLog(@"polyline.title = %@", polyline.title);
            //            polyline.stroke = polyline.attributes[@"stroke"];
            //            polyline.title = polyline.attributes[@"stroke-opacity"];// "Crema to Council Crest"
            // . NSLog(@"polyline = %@",polyline);
            // Add the polyline to the map, back on the main thread
            // Use weak reference to self to prevent retain cycle
            __weak typeof(self) weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [weakSelf.mapView addAnnotation:polyline];
            });
        }
        //        MGLPolylineFeature *polyline = (MGLPolylineFeature *)shapeCollectionFeature.shapes.firstObject;
        
        // Optionally set the title of the polyline, which can be used for:
        //  - Callout view
        //  - Object identification
        // In this case, set it to the name included in the GeoJSON
        //        polyline.title = polyline.attributes[@"name"]; // "Crema to Council Crest"
        //
        //        // Add the polyline to the map, back on the main thread
        //        // Use weak reference to self to prevent retain cycle
        //        __weak typeof(self) weakSelf = self;
        //        dispatch_async(dispatch_get_main_queue(), ^(void) {
        //            [weakSelf.mapView addAnnotation:polyline];
        //        });
    });
}


- (void)drawShape {
    // Create a coordinates array to hold all of the coordinates for our shape.
    CLLocationCoordinate2D coordinates[] = {
        CLLocationCoordinate2DMake(45.522585, -122.685699),
        CLLocationCoordinate2DMake(45.534611, -122.708873),
        CLLocationCoordinate2DMake(45.530883, -122.678833),
        CLLocationCoordinate2DMake(45.547115, -122.667503),
        CLLocationCoordinate2DMake(45.530643, -122.660121),
        CLLocationCoordinate2DMake(45.533529, -122.636260),
        CLLocationCoordinate2DMake(45.521743, -122.659091),
        CLLocationCoordinate2DMake(45.510677, -122.648792),
        CLLocationCoordinate2DMake(45.515008, -122.664070),
        CLLocationCoordinate2DMake(45.502496, -122.669048),
        CLLocationCoordinate2DMake(45.515369, -122.678489),
        CLLocationCoordinate2DMake(45.506346, -122.702007),
        CLLocationCoordinate2DMake(45.522585, -122.685699),
    };
    NSUInteger numberOfCoordinates = sizeof(coordinates) / sizeof(CLLocationCoordinate2D);
    
    // Create our shape with the formatted coordinates array
    MGLPolygon *shape = [MGLPolygon polygonWithCoordinates:coordinates count:numberOfCoordinates];
    
    // Add the shape to the map
    [self.mapView addAnnotation:shape];
}




@end
