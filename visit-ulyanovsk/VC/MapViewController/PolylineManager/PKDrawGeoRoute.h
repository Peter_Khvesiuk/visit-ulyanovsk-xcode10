//
//  PKDrawGeoRoute.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 07.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mapbox/Mapbox.h>

//@class MGLMapView;
@interface PKDrawGeoRoute : NSObject

+(PKDrawGeoRoute*)sharedManager;
-(void)drawPolyline:(MGLMapView*)map andRouteJSONString:(NSString*)json;

@end
