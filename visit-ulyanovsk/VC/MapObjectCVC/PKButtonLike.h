//
//  PKButtonLike.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 26.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKButtonLike : UIButton

-(void)setImageWithLikeEnabled:(BOOL)enabled withLikeCount:(int) countLike andIsDetail:(BOOL) isDetail;

@end
