//
//  PKButtonLike.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 26.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKButtonLike.h"
#import "PKUOStyleKit.h"

@implementation PKButtonLike



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setImageWithLikeEnabled:(BOOL)enabled withLikeCount:(int) countLike andIsDetail:(BOOL) isDetail{
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSString* countLikeString = [f stringFromNumber:[NSNumber numberWithInteger:countLike]];
    
//    // . NSLog(@"liked frame = %f х %f", self.frame.size.height, self.frame.size.width);
    if(!isDetail){
        [self setImage:[PKUOStyleKit imageOfLikedIconWithFillHeart:!enabled likeCount:countLikeString]  forState:UIControlStateNormal];
        [self setImage:[PKUOStyleKit imageOfLikedIconWithFillHeart:enabled likeCount:countLikeString]  forState:UIControlStateHighlighted];
    } else {
        [self setImage:[PKUOStyleKit imageOfLikedIconBigWithFillHeart:!enabled likeCount:countLikeString] forState:UIControlStateNormal];
        [self setImage:[PKUOStyleKit imageOfLikedIconBigWithFillHeart:enabled likeCount:countLikeString]  forState:UIControlStateHighlighted];
    }
//    return self;
}

@end
