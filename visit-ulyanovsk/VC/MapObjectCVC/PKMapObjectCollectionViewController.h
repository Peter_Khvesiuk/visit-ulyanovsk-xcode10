//
//  PKMapObjectCollectionViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 31.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZOZolaZoomTransition.h"

@interface PKMapObjectCollectionViewController : UICollectionViewController <ZOZolaZoomTransitionDelegate, UINavigationControllerDelegate>

@property (nonatomic) BOOL contentRecomended;
@property (nonatomic, assign) int predicateRecomended;
-(void)reloadAnnotationWithArray:(NSArray*)selectionsTypes andArray:(NSArray*)selectionsAdditional;
@end

@class PKButtonLike, PKBrandLogo;//ZOProduct,

@interface PKMapObjectCell : UICollectionViewCell

//@property (strong, nonatomic) ZOProduct *product;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (strong, nonatomic, readwrite) PKBrandLogo *logoImageView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *distanceView;
@property (strong, nonatomic) PKButtonLike *likesButton;

+ (CGFloat) heightForText:(NSString*) text andWidth:(CGFloat)width;

@end
