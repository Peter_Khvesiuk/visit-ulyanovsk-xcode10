//
//  PKCollectionViewLayout.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 20.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKCollectionViewLayout : UICollectionViewLayout

@end
