//
//  PKMapObjectCollectionViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 31.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapObjectCollectionViewController.h"
#import "PKMapObjectDetailVC.h"


#import "PKServerManager.h"
#import "PKDataManager.h"
#import "PKUserInteraction.h"

#import "PKMapObj+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

#import <STPopup/STPopup.h>
#import "PKMapBottomPopupVC.h"

#import "PKUOStyleKit.h"

#import "PKButtonLike.h"

#import "PKInterfaceManager.h"
//#import "PKTopNavButtonView.h"
//#import "PKTopNavButtonLocationView.h"

#import "PKGeoRouteDetailVC.h"

#import "PKMainMapViewController.h"
#import "PKBrandLogo.h"

#import <CoreLocation/CoreLocation.h>
#import "PKPopUp.h"
#import <QuartzCore/QuartzCore.h>


static NSString * PKMapObjectCellId           = @"PKMapObjectCell";
//static CGFloat zoomCellMargin          = 10.0;
//static CGFloat zoomCellSpacing         = 10.0;
//static CGFloat zoomCellTextAreaHeight  = 44.0;
//static CGFloat cellsInRow                     = 2.0;
//static CGFloat detailImageHeight              = 300.0;

                   
                   
@interface PKMapObjectCollectionViewController ()<CLLocationManagerDelegate, NSFetchedResultsControllerDelegate, STPopupControllerTransitioning, PKFilterBottomPopupVCDelegate, PKSendDataButtonsProtocol>


@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@property (strong, nonatomic) PKTopNavButtonLocationView *topNavButtonLocationView;




@property (nonatomic) CGFloat detailImageHeight;
@property (nonatomic) CGFloat cellsInRow;


@property NSMutableArray *sectionChanges;
@property NSMutableArray *itemChanges;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;

@property (weak, nonatomic) STPopupController* popup;
@property (nonatomic, strong) NSArray *contentRecomendedTypes;
@property (nonatomic, strong) NSArray *selectionsTypes;
@property (nonatomic, strong) NSArray *selectionsAdditional;


@property (strong, nonatomic) NSPredicate *predicate1;
@property (strong, nonatomic) NSPredicate *predicate2;
@property (strong, nonatomic) NSSortDescriptor *distanceAscending;
@property (strong, nonatomic) NSArray<NSSortDescriptor *> *defaultSortDescriptor;
//@property (strong, nonatomic) NSNumber * flagForPredicatSelected1;
//@property (strong, nonatomic) NSString * flagForPredicatSelected2;

@property (strong, nonatomic) UIRefreshControl *refreshControl;


//@property (strong, nonatomic) NSArray *products;
@property (weak, nonatomic) PKMapObjectCell *selectedCell;

@property (nonatomic, strong) CLLocation *currentLoaction;
@property (nonatomic,strong) CLLocationManager *locationManager;

@property (nonatomic) BOOL sortByDistance;

@end

@implementation PKMapObjectCollectionViewController

@synthesize predicateRecomended;
@synthesize contentRecomended;

#pragma mark - Constructors

//- (instancetype)init{
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//    //[flowLayout setSectionInset:UIEdgeInsetsMake(50.f, 0.f, 0.f, 0.f)];
//    self = [super initWithCollectionViewLayout:flowLayout];
//    if (self) {
//        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
//    }
//    return self;
//}

#pragma mark - View Lifecycle
-(void)dealloc{
    // . NSLog(@"\ndealloc PKMapObjectCollectionViewController");
}
-(NSArray<NSSortDescriptor*>*)defaultSortDescriptor{
    if(_defaultSortDescriptor){
        return _defaultSortDescriptor;
    }
    NSSortDescriptor *fotoNeededAscending = [[NSSortDescriptor alloc] initWithKey:@"photoTrue" ascending:NO];
    NSSortDescriptor *recomendedAscending = [[NSSortDescriptor alloc] initWithKey:@"recomended" ascending:NO];
    NSSortDescriptor *titleAscending = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    return @[recomendedAscending, fotoNeededAscending, titleAscending];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.detailImageHeight = zoomDetailImageHeight;
    if(contentRecomended){
        self.cellsInRow = zoomCellsInRowRecomended;
    } else {
        self.cellsInRow = zoomCellsInRow;
    }
    
    //+++++++++++++++++++++++++++++
//    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    if(self.contentRecomended){
        switch (self.predicateRecomended) {
            case 1:
                self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE_UP(@"restaurants") andTitleHide:NO];
                break;
            case 2:
                self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE_UP(@"Hotels") andTitleHide:NO];
                break;
            case 3:
                self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE_UP(@"interesting") andTitleHide:NO];
                break;
            case 4:
                self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE_UP(@"my_list") andTitleHide:NO];
                break;
    
        }
    } else {
        self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE_UP(@"discover") andTitleHide:NO];
        self.topNavButtonLocationView = [[PKInterfaceManager sharedManager] loactionViewForCVC:self];
        
    }
    
    self.collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(54.f + 10.f, 0, 70.f, 0);
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refershControlAction) forControlEvents:UIControlEventValueChanged];
    
    self.refreshControl = refreshControl;
    [self.collectionView addSubview:self.refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    //+++++++++++++++++++++++++++++
    
    
    
    self.navigationController.delegate = self;
    
    // Load demo data
//    NSMutableArray *products = [[NSMutableArray alloc] initWithCapacity:10];
//    for (NSInteger i=0; i<10; i++) {
//        ZOProduct *product = [[ZOProduct alloc] init];
//        product.title = [NSString stringWithFormat:@"Product %ld", i];
//        product.imageName = [NSString stringWithFormat:@"product_%ld.jpg", i];
//        [products addObject:product];
//    }
//    self.products = products;
//
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.94 alpha:1.0];
    [self.collectionView registerClass:[PKMapObjectCell class] forCellWithReuseIdentifier:PKMapObjectCellId];
    
    
    /* ******      Location      ****** */
    
     
//    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 50.f, 0.f, 50.f, 56.f)];
////    btn.backgroundColor = [UIColor redColor];
//
//    [self.collectionView addSubview:btn];
//    [btn setTitle:@"cg" forState:UIControlStateNormal];
//    [btn addTarget:self action:@selector(calcDistance:) forControlEvents:UIControlEventTouchUpInside];
//
//    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width - 100.f, 60.f, 50.f, 40.f)];
//    btn2.backgroundColor = [UIColor greenColor];
//    [self.view addSubview:btn2];
//    [btn2 setTitle:@"cg" forState:UIControlStateNormal];
//    [btn2 addTarget:self action:@selector(cgBtn2) forControlEvents:UIControlEventTouchUpInside];
//
    
     /* ******      Location      ****** */
    
    
}
//-(void)cgBtn2{
//    NSLog(@"cgBtn2");
//    [self reloadwithPredicateDefault];
////    [self.collectionView reloadData];
//}
#pragma mark - Update location
-(CLLocationManager*)hereLocationManager{
    if(_locationManager){
        return _locationManager;
    }
    CLLocationManager* locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    return locationManager;
}
-(void)calcDistance:(id)sender{
    self.locationManager = [self hereLocationManager];
    
    if([CLLocationManager locationServicesEnabled]){
        if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied){
            [self showPopUPDisabledLocation];
        } else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined){
            [self showPopUPWithAskLocation];
        } else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways
                       || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse){
            
            self.topNavButtonLocationView.sortDistance = YES;
            self.topNavButtonLocationView.activeDistance = YES;
            [self.locationManager startUpdatingLocation];
        }
    } else {
        self.topNavButtonLocationView.sortDistance = NO;
        self.topNavButtonLocationView.activeDistance = NO;
        self.sortByDistance = NO;
    }
    
}
/*
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
            // Пользователь еще не сделал выбор в отношении этого приложения
            
            break;
        case kCLAuthorizationStatusRestricted:
            // Это приложение не авторизовано для использования услуг определения местоположения.
            // к активным ограничениям на услуги определения местоположения, пользователь не может изменить
            // этот статус, и, возможно, лично не отказано в авторизации
           
            break;
            
        case kCLAuthorizationStatusDenied:
            // Пользователь явно отказал в авторизации для этого приложения, или
            // услуги локации отключены в настройках.
            
            break;
            
        case kCLAuthorizationStatusAuthorizedAlways:
            // Пользователь предоставил разрешение на использование своего местоположения в любое время,
            // включая мониторинг регионов, посещений или значительных изменений местоположения.
            // Это значение следует использовать в iOS, tvOS и watchOS. Оно доступно на
            // MacOS, но kCLAuthorizationStatusAuthorized является синонимом и предпочтительнее.
            
            break;
            
        case kCLAuthorizationStatusAuthorizedWhenInUse:
            // Пользователь предоставил разрешение на использование своего местоположения только тогда, когда ваше приложение
            // виден для них (он будет виден им, если вы продолжите
            // получать обновления местоположения в фоновом режиме). Авторизация для использования
            // запуск API не был предоставлен.
            // Это значение недоступно в MacOS, его следует использовать в iOS, tvOS и
            // watchOS.
            break;
            
        default:
            break;
    }

}
*/
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    [self.locationManager stopUpdatingLocation];
    self.topNavButtonLocationView.activeDistance = NO;
    self.topNavButtonLocationView.sortDistance = NO;
    self.sortByDistance = NO;
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = [locations lastObject];
    self.currentLoaction = [[CLLocation alloc] initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];;
    //NSLog(@"latitude = %@", [NSString stringWithFormat:@"%f", location.coordinate.latitude]);
    //NSLog(@"longitude = %@", [NSString stringWithFormat:@"%f", location.coordinate.longitude]);
    [self.locationManager stopUpdatingLocation];
    self.topNavButtonLocationView.activeDistance = NO;
    self.topNavButtonLocationView.sortDistance = YES;
    
//    [[PKDataManager sharedManager] calculateDistanceForMapObjectsWith:_predicate1
//                                                         andPredicate:_predicate2
//                                                   andCurrentLoaction:_currentLoaction];
    self.sortByDistance = YES;
//    NSLog(@"CVC fetchedResultsController %@", [self.fetchedResultsController fetchRequest]);
    [[self.fetchedResultsController fetchRequest] setSortDescriptors:@[self.distanceAscending]];
//    NSLog(@"CVC fetchedResultsController %@", [self.fetchedResultsController fetchRequest]);
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        // Handle you error here
    }
    [[PKDataManager sharedManager] calculateDistanceForMapObjects:^(NSArray<PKEvent *> *mapObjects) {
        for (PKMapObj *mapObject in mapObjects) {
            CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:[mapObject.latitude doubleValue] longitude:[mapObject.longitude doubleValue]];
            CLLocationDistance distance = [self.currentLoaction distanceFromLocation:endLocation];
            mapObject.distance = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", distance]];
        }
    }];
}
-(void)showPopUPDisabledLocation{
    
    

    [[PKPopUp showMessage: TRANSLATE(@"to_display_nearby_objects,_you_need_to_change_the_geolocation_settings")
                withTitle:TRANSLATE_UP(@"your_location")]
              withConfirm:TRANSLATE_UP(@"change_settings")
                onConfirm:^{
                    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    if ([[UIApplication sharedApplication] canOpenURL:url]) {
                        [[UIApplication sharedApplication] openURL: url options:@{} completionHandler:^(BOOL success) {
                            
                        }];
                    }
                    
                } withCancel:TRANSLATE_UP(@"cancel") onCancel:^{
                    
        }];
}
-(void)showPopUPWithAskLocation{
    [[PKPopUp showMessage: TRANSLATE(@"to_display_objects_nearby,_your_location_will_be_requested") withTitle:TRANSLATE_UP(@"your_location")] withConfirm:TRANSLATE_UP(@"ok") onConfirm:^{
        [self.locationManager requestWhenInUseAuthorization];
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.tabBarController.tabBar setHidden:NO];
    
//    [self reloadwithPredicateDefault];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
    self.topNavButtonLocationView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
    self.topNavButtonLocationView.open = NO;
}

#pragma mark - Button controll

-(void)popBackToMainVC:(id)sender{
//    [self performSegueWithIdentifier:@"toMainVC" sender:sender];
    
//     [[self navigationController] popViewControllerAnimated:YES];
    
    [[self.tabBarController navigationController] popToRootViewControllerAnimated:YES];
    
//    // . NSLog(@"path = %@", self.path);
//    // . NSLog(@"view controllers on stack = %lu", [self.navigationController.viewControllers count]);
//    // . NSLog(@"index on stack %lu", [self.navigationController.viewControllers indexOfObject:self]);
//    // . NSLog(@"popToRootViewControllerAnimated popBack");
////    [self.navigationController popToRootViewControllexrAnimated:YES];
//    [[self.tabBarController navigationController] popToRootViewControllerAnimated:YES];
    
    
//    NSArray *viewContrlls=[[self navigationController] viewControllers];
//    for( int i=0;i<[ viewContrlls count];i++){
//        id map=[viewContrlls objectAtIndex:i];
//        // . NSLog(@"viewContrlls = %@", [map class]);
//        if([map isKindOfClass:[PKMainMapViewController class]]){
//            // A is your class where to popback
//            [[self navigationController] popToRootViewControllerAnimated:YES];
//            return;
//        }
//    }
    
    
    
//    NSArray *tabBarControllers = [self.tabBarController viewControllers];
//    for( int i=0;i<[ tabBarControllers count];i++){
//        id map2=[tabBarControllers objectAtIndex:i];
//        if([map2 isKindOfClass:[UINavigationController class]]){
//            UINavigationController * navControl = (UINavigationController*)map2;
//            NSArray *viewContrlls=[navControl viewControllers];
//            for( int i=0;i<[ viewContrlls count];i++){
//                id map=[viewContrlls objectAtIndex:i];
//                // . NSLog(@"viewContrlls = %@", [map class]);
//                if([map isKindOfClass:[PKMainMapViewController class]]){
//
//                    PKMainMapViewController *mapViewController = (PKMainMapViewController*)map;
//                    [mapViewController setMoveToDestanationMapObject:self.mapObject];
//                    // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
//                    self.tabBarController.selectedIndex = i;//[self.tabBarController.viewControllers indexOfObject:map];
//
//                    // A is your class where to popback
//                    //                    [[self navigationController] popToViewController:map animated:YES];
//                    return;
//                }
//            }
//        }
//    }
    
    
    
}

-(void)pushBtnLike: (PKButtonLike*) sender{
    //    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:(PKMapObjectCell *)sender.superview.superview];
    PKMapObj* mapObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [[PKUserInteraction sharedManager] switchObjectLike:mapObject.objectID];
}


#pragma mark - UICollectionViewDelegate & Data Source Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    //warning Incomplete implementation, return the number of items
    return [sectionInfo numberOfObjects];
//    return [_products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PKMapObjectCell *cell = (PKMapObjectCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PKMapObjectCellId forIndexPath:indexPath];
    if (!cell) {
        cell = [[PKMapObjectCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}
-(void)reloadMapIfNeeded{
    
    if(!self.predicateRecomended){
        NSArray *tabBarControllers = [self.tabBarController viewControllers];
        for( int i=0;i<[ tabBarControllers count];i++){
            id map2=[tabBarControllers objectAtIndex:i];
            if([map2 isKindOfClass:[UINavigationController class]]){
                UINavigationController * navControl = (UINavigationController*)map2;
                NSArray *viewContrlls=[navControl viewControllers];
                NSMutableArray *newViewControllers = [[NSMutableArray alloc] init];
                for( int i=0;i<[ viewContrlls count];i++){
                    id vc=[viewContrlls objectAtIndex:i];
                    // . NSLog(@"viewContrlls = %@", [map class]);
                    if([vc isKindOfClass:[PKMainMapViewController class]]){
                        
                        PKMainMapViewController *mapViewController = (PKMainMapViewController*)vc;
                        
                        
                        [mapViewController reloadAnnotationWithArray:self.selectionsTypes andArray:self.selectionsAdditional];
                        [newViewControllers addObject:mapViewController];
                        // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
//                        return;
                    
                        
                    } else if([vc isKindOfClass:[PKMapObjectDetailVC class]] || [vc isKindOfClass:[PKGeoRouteDetailVC class]]){
                        //#warning kill PKMapObjectDetailVC if needed
                        
                    } else {
                        [newViewControllers addObject:vc];
                    }
                }
                [navControl setViewControllers:newViewControllers];
                
            }
        }
    }
    
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = (PKMapObjectCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
   
    [self performSegueWithIdentifier:@"toMapObjectDetailVC" sender:self.selectedCell];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toMapObjectDetailVC"]){
        [self.tabBarController.tabBar setHidden:YES];
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
        PKMapObjectDetailVC*vc = [segue destinationViewController] ;
        [vc setMapObject:(PKMapObj *) [self.fetchedResultsController objectAtIndexPath:indexPath]];
        // . NSLog(@"\ndealloc navigationController on stack = %lu", [[self.navigationController viewControllers] count]);
        
    }
//    else if([segue.identifier isEqualToString:@"toMainVC"]){
//        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
//        NSArray *viewContrlls=[navControl viewControllers];
//        for( int i=0;i<[ viewContrlls count];i++){
//            id vcs=[viewContrlls objectAtIndex:i];
//            if([vcs isKindOfClass:[PKMainViewController class]]){
//
//                PKMainViewController *vc = (PKMainViewController*)vcs;
//                [vc setAnimationAndReloadingDataDisabled:YES];
//                return;
//            }
//        }
//    }
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Cell width is half the screen width - edge margin - half the center margin
    

    CGFloat width = (self.collectionView.frame.size.width / self.cellsInRow) - zoomCellMargin - (zoomCellSpacing / self.cellsInRow);
    CGFloat height = ((self.detailImageHeight / self.collectionView.frame.size.width) * width) + zoomCellTextAreaHeight;
    return CGSizeMake(width, height);
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(zoomTopMargin, zoomCellMargin, 70.f, zoomCellMargin);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

#pragma mark - UINavigationControllerDelegate Methods

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    // Sanity
    if (fromVC != self && toVC != self) return nil;
    
    // Determine if we're presenting or dismissing
    ZOTransitionType type = (fromVC == self) ? ZOTransitionTypePresenting : ZOTransitionTypeDismissing;
    
    // Create a transition instance with the selected cell's imageView as the target view
    ZOZolaZoomTransition *zoomTransition = [ZOZolaZoomTransition transitionFromView:_selectedCell.imageView
                                                                               type:type
                                                                           duration:0.5
                                                                           delegate:self];
    zoomTransition.fadeColor = self.collectionView.backgroundColor;
    
    return zoomTransition;
}

#pragma mark - ZOZolaZoomTransitionDelegate Methods

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
        startingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The starting frame is taken from the selected cell's imageView.
        return [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView];
    } else if ([fromViewController isKindOfClass:[PKMapObjectDetailVC class]]) {
        // We're popping back to this master controller. The starting frame is taken from the detailController's imageView.
        PKMapObjectDetailVC *detailController = (PKMapObjectDetailVC *)fromViewController;
        return [detailController.imageGalleryView
            convertRect:detailController.imageGalleryView.bounds toView:relativeView];
    }
    
    return CGRectZero;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
       finishingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The finishing frame is taken from the detailController's imageView.
        PKMapObjectDetailVC *detailController = (PKMapObjectDetailVC *)toViewController;
        return [detailController.imageGalleryView convertRect:CGRectMake(0, marginTopNavigationHeight, detailController.imageGalleryView.bounds.size.width, detailController.imageGalleryView.bounds.size.height) toView:relativeView];
#warning Navigator bar height epxected as 22? Okay?
    } else if ([fromViewController isKindOfClass:[PKMapObjectDetailVC class]]) {
        // We're popping back to this master controller. The finishing frame is taken from the selected cell's imageView.
        return [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView];
    }
    
    return CGRectZero;
}

- (NSArray *)supplementaryViewsForZolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition {
    // Here we're returning all UICollectionViewCells that are clipped by the edge
    // of the screen. These will be used as "supplementary views" so that the clipped
    // cells will be drawn in their entirety rather than appearing cut off during the
    // transition animation.
    
    NSMutableArray *clippedCells = [NSMutableArray arrayWithCapacity:[[self.collectionView visibleCells] count]];
    for (UICollectionViewCell *visibleCell in self.collectionView.visibleCells) {
        CGRect convertedRect = [visibleCell convertRect:visibleCell.bounds toView:self.view];
        if (!CGRectContainsRect(self.view.frame, convertedRect)) {
            [clippedCells addObject:visibleCell];
        }
    }
    return clippedCells;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
   frameForSupplementaryView:(UIView *)supplementaryView
              relativeToView:(UIView *)relativeView {
    
    return [supplementaryView convertRect:supplementaryView.bounds toView:relativeView];
}








#pragma mark - Collection view content controll

- (void)refershControlAction{
    [[self.fetchedResultsController fetchRequest] setSortDescriptors:self.defaultSortDescriptor];
    self.sortByDistance = NO;
    self.topNavButtonLocationView.sortDistance = NO;
    [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(BOOL isComplete){
        if(isComplete){
            
            [self.refreshControl endRefreshing];
            
            
//            [self.collectionView reloadData];
//            [self reloadwithPredicateDefault];
//            NSLog(@"ZZD refershControlAction");
            [self reloadMapIfNeeded];
        }
        
    } inProgress:^(CGFloat progress) {
        // . NSLog(@"progress == %f", progress);
    } onFailure:^(NSError* error, NSInteger statusCode) {
        [self.refreshControl endRefreshing];
    }];
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    _sectionChanges = [[NSMutableArray alloc] init];
    _itemChanges = [[NSMutableArray alloc] init];
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKMapObj"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    
    
    
    NSSortDescriptor* titleAscending;
    
    _predicate2 = [NSPredicate predicateWithFormat:@"wiFi >= %i", 0]; //пустой предикат
    
    
    if(contentRecomended){
        NSSortDescriptor* myLikesAscending;
        if(self.predicateRecomended == 4){
            myLikesAscending = [[NSSortDescriptor alloc] initWithKey:@"likes" ascending:NO];
            titleAscending = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
            self.contentRecomendedTypes = [[PKDataManager sharedManager] getContentRecomendedArrayWithPredicate:predicateRecomended];
            _predicate1 = [NSPredicate predicateWithFormat:@"mylike == %i", 1];
            [fetchRequest setSortDescriptors:@[myLikesAscending,titleAscending]];
        } else {
            titleAscending = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
            self.contentRecomendedTypes = [[PKDataManager sharedManager] getContentRecomendedArrayWithPredicate:predicateRecomended];
            _predicate1 = [NSPredicate predicateWithFormat:@"types.typeObjValue IN %@ and recomended = %i", self.contentRecomendedTypes, 1];
            [fetchRequest setSortDescriptors:@[titleAscending]];
        }
        
    } else {
        NSSortDescriptor* fotoNeededAscending;
        NSSortDescriptor* recomendedAscending;
        recomendedAscending = [[NSSortDescriptor alloc] initWithKey:@"recomended" ascending:NO];
        fotoNeededAscending = [[NSSortDescriptor alloc] initWithKey:@"photoTrue" ascending:NO];
        titleAscending = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
        self.selectionsTypes = [[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:1];
        self.selectionsAdditional = [[PKDataManager sharedManager] getFilterTypeObjectsArrayForFilterCategory:2];
        if([self.selectionsAdditional count] > 0){
            NSMutableArray<NSPredicate *> *predicateArray2 = [[NSMutableArray alloc] init];
            
            
            for (NSString *selAdditional in self.selectionsAdditional) {
                
                NSArray *itemsArray = PKFilterAdditionalArray;
                NSInteger anIndexA=[itemsArray indexOfObject:(NSString*) selAdditional];
                if(NSNotFound == anIndexA) {
                    NSLog(@"not found");
                } else if([selAdditional isEqualToString: [itemsArray objectAtIndex: anIndexA]]){
                    defineDayOfWeekFormat
                    NSInteger todayInt = defineTodayInt;
                    NSInteger hoursAndMinutes = [self hoursAndMinutes];
                    switch (anIndexA) {
                        case 0:
                            [predicateArray2 addObject: [NSPredicate predicateWithFormat: @"SUBQUERY(workingTime, $w, $w.day = %i AND (($w.begin <= %i AND $w.end > %i) OR ($w.begin > $w.end AND $w.end > %i) OR ($w.begin > $w.end AND $w.begin < %i))).@count > 0 OR workingTime.@count == 0", todayInt, hoursAndMinutes, hoursAndMinutes, hoursAndMinutes, hoursAndMinutes]];
                            break;
                        case 1:
                            [predicateArray2 addObject: [NSPredicate predicateWithFormat: @"avalibleForDisables = %i", 1]];
                            break;
                        case 2:
                            [predicateArray2 addObject: [NSPredicate predicateWithFormat: @"wiFi = %i", 1]];
                            break;
                        case 3:
                            [predicateArray2 addObject: [NSPredicate predicateWithFormat: @"payCard = %i", 1]];
                            break;
                        case 4:
                            [predicateArray2 addObject: [NSPredicate predicateWithFormat: @"kidsfriendly = %i", 1]];
                            break;
                            
                        default:
                            break;
                    }
                    
                    
                    
                }
                
            }
            
            _predicate2 = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray2];
        }
        
        self.distanceAscending = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
        
        NSMutableArray<NSPredicate *> *predicateArray1 = [[NSMutableArray alloc] init];
        
        if(!self.sortByDistance){
            [predicateArray1 addObject: [NSPredicate predicateWithFormat: @"brand == nil OR self == brand.firstMapObject"]];
        }
        
        
        if(self.sortByDistance){
            [fetchRequest setSortDescriptors:@[self.distanceAscending]];
        } else {
            [fetchRequest setSortDescriptors:self.defaultSortDescriptor]; //recomendedAscending,fotoNeededAscending,titleAscending];
        }
       
        [predicateArray1 addObject: [NSPredicate predicateWithFormat:@"types.typeObjValue IN %@", self.selectionsTypes]];
        
        _predicate1 = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray1];;
//
    }
    
    //    [fetchRequest setSortDescriptors:@[titleAscending,nameDescription]];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[_predicate1, _predicate2]];
    [fetchRequest setPredicate:predicate];
    
    
    
    
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil//@"ageTo"
                                                   cacheName:nil];//@"Master"
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
    
    
    
    
    
    return _fetchedResultsController;
}
-(NSString*)distanceStringFrom:(float)ditance{
    NSString* string = [[NSString alloc] init];
    if(ditance>250000){
        string = [NSString stringWithFormat:@"> 250 %@", TRANSLATE(@"km")];
    } else if(ditance>5000){
        string = [NSString stringWithFormat:@"%.0f %@", (ditance / 1000), TRANSLATE(@"km")];
    } else if(ditance>1500){
        string = [NSString stringWithFormat:@"%.1f %@", (ditance / 1000), TRANSLATE(@"km")];
    } else if(ditance>1000){
        string = [NSString stringWithFormat:@"%.2f %@", (ditance / 1000), TRANSLATE(@"km")];
    } else {
        string = [NSString stringWithFormat:@"%.0f %@", ditance, TRANSLATE(@"m")];
    }
    return string;
}
-(NSInteger)timeIntervalFromString:(NSString*)time{
    NSArray* hoursAndMinutes = [time componentsSeparatedByString:@":"];
    NSInteger hours = [hoursAndMinutes[0] integerValue];
    NSInteger minutes = [hoursAndMinutes[1] integerValue];
    return hours*3600+minutes*60;
}
-(NSInteger)hoursAndMinutes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *currDate = [NSDate date];
    NSString *dateString = [formatter stringFromDate:currDate];
    NSInteger hoursAndMinutes = [self timeIntervalFromString:(NSString*)dateString];
    return hoursAndMinutes;
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    change[@(type)] = @(sectionIndex);
    [_sectionChanges addObject:change];
    
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath {
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_itemChanges addObject:change];
}
-(void)reloadAnnotationWithArray:(NSArray*)selectionsTypes andArray:(NSArray*)selectionsAdditional{
    self.selectionsTypes = selectionsTypes;
    self.selectionsAdditional = selectionsAdditional;
    [self reloadwithPredicateDefault];
    
}
- (void)reloadwithPredicateDefault {

    // Make sure to delete the cache
    // (using the name from when you created the fetched results controller)
    [NSFetchedResultsController deleteCacheWithName:nil];
    // Delete the old fetched results controller
    self.fetchedResultsController = nil;
    // TODO: Handle error!
    // This will cause the fetched results controller to be created
    // with the new predicate
    [self.fetchedResultsController performFetch:nil];
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource
- (NSManagedObjectContext*) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[PKDataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
}

//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
//    return [[self.fetchedResultsController sections] count];
//}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.collectionView performBatchUpdates:^{
        for (NSDictionary *change in self->_sectionChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeMove:
                        [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        NSLog(@"ZZD NSFetchedResultsChangeMove empty!!!!!!!!!");
                        break;
                    case NSFetchedResultsChangeUpdate:
                        NSLog(@"ZZD NSFetchedResultsChangeUpdate empty!!!!!!!!!");
                        break;
                }
            }];
        }
        if(self->_itemChanges.count > 1){
//            NSLog(@"ZZD self->_itemChanges.count > 1");
            [self.collectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
            [self.collectionView setContentOffset:CGPointMake(0.f, -20.f) animated:YES];
        } else {
            for (NSDictionary *change in self->_itemChanges) {
                [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                    NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                    switch(type) {
                        case NSFetchedResultsChangeInsert:
//                            NSLog(@"ZZD NSFetchedResultsChangeInsert");
                            [self.collectionView insertItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeDelete:
//                            NSLog(@"ZZD NSFetchedResultsChangeDelete");
                            [self.collectionView deleteItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeUpdate:
//                            NSLog(@"ZZD NSFetchedResultsChangeUpdate");
                            [self.collectionView reloadItemsAtIndexPaths:@[obj]];
                            break;
                        case NSFetchedResultsChangeMove:
//                            NSLog(@"ZZD NSFetchedResultsChangeMove");
                            [self.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                            break;
                    }
                }];
            }
        }
    } completion:^(BOOL finished) {
        self->_sectionChanges = nil;
        self->_itemChanges = nil;
        
    }];
}



//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
//    // . NSLog(@"numberOfRowsInSection = %lu", (unsigned long)[sectionInfo numberOfObjects]);
//    //warning Incomplete implementation, return the number of items
//    return [sectionInfo numberOfObjects];
//}

#pragma mark - ConfigureCell

//- (PKMapObjectCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    PKMapObjectCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
//
//    if (!cell) {
//        cell = [[PKMapObjectCell alloc] init];
//        //    initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
//    }
//    //    PKMapObj *mapObj = [self.fetchedResultsController objectAtIndexPath:indexPath];
//
//
//    [self configureCell:cell atIndexPath:indexPath];
//
//
//    return cell;
//}

-(void)configureCell:(PKMapObjectCell*) cell atIndexPath:(NSIndexPath*)indexPath{
    PKMapObj *mapObj = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // . NSLog(@"cell height = %.f %.f", cell.bounds.size.height, cell.bounds.size.width);
    //    [cell setBackgroundColor: [UIColor redColor]];
    //    cell.mapObject = mapObj;
    //    [self configureCell:cell atIndexPath:indexPath];
    cell.imageView.image = nil;//[UIImage imageNamed:@"icon2.png"];// //
    cell.imageView.backgroundColor = [UIColor lightGrayColor];
    
//    CLLocation *startLocation = [[CLLocation alloc] initWithLatitude:self.currentLoaction longitude:startLongitude];
//    CLLocation *endLocation = [[CLLocation alloc] initWithLatitude:[mapObj.latitude doubleValue] longitude:[mapObj.longitude doubleValue]];
//    CLLocationDistance distance = [self.currentLoaction distanceFromLocation:endLocation];
    cell.titleLabel.text = mapObj.title;
    
    if(self.sortByDistance && [mapObj.distance doubleValue] != 0.0f){

        cell.distanceView.text = [NSString stringWithFormat:@"%@", [self distanceStringFrom: [mapObj.distance doubleValue]]];

    } else {
        NSUInteger count = [mapObj.brand.mapObjects count];
        if(count > 1){
            cell.distanceView.text = [NSString stringWithFormat:@"x%lu", (unsigned long)count];
        } else {
            cell.distanceView.text = @"";
        }
        
    }
    
    
    
    cell.logoImageView.image = nil;
    if(mapObj.brand.logoImage && ![mapObj.brand.logoImage isEqualToString:@"no_image.jpg"]){
        [cell.logoImageView setImageWithName:mapObj.brand.logoImage andContentModeTop:YES];
    }
    //******
//        cell.layer.masksToBounds = YES;
//        cell.layer.cornerRadius = 10;
    //******
//    cell.layer.shadowOffset = CGSizeMake(1, 0);
//    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
//    cell.layer.shadowRadius = 5;
//    cell.layer.shadowOpacity = .25;
//    cell.contentView.layer.cornerRadius = 10.0;
//    cell.contentView.layer.borderColor = [[UIColor clearColor] CGColor];
//    cell.contentView.layer.masksToBounds = YES;
    //******
    
//    cell.contentView.layer.borderWidth = 1.0;
//    cell.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    
    [cell.likesButton setImageWithLikeEnabled:mapObj.mylike withLikeCount:mapObj.likes andIsDetail:NO];
    [cell.likesButton addTarget:self
                         action:@selector(pushBtnLike:)
               forControlEvents:UIControlEventTouchUpInside];
    
    // . NSLog(@"cell.imageView.frame.size = %f %f", cell.imageView.frame.size.width, cell.imageView.frame.size.height - zoomCellTextAreaHeight);
    cell.imageView.image = [PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(cell.bounds.size.width, cell.bounds.size.height)];
    if(![mapObj.prevImageName isEqualToString:@"no_image.jpg"]){
        
        NSString* dataPath = LOCAL_mapObjImagePathPreview(mapObj.prevImageName);
        if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
            // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
            [cell.imageView setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
        } else {
            // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
            NSString* imageUrl = URL_mapObjImagePathPreview(mapObj.prevImageName);
            NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
            __weak PKMapObjectCell* weakCellImage = cell;
            //CellWherePhoto.trenerPhotoDetail.image = nil;
            [cell.imageView setImageWithURLRequest:requestwhereImage
                                  placeholderImage:nil
                                           success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                               
                                               //         // . NSLog(@"requestwhereImage success");
                                               
                                               
                                               weakCellImage.imageView.image = image;
                                               
                                               //                                           weakCellImage.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
                                               [weakCellImage layoutSubviews];
                                               
                                           }
                                           failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                               // . NSLog(@"failure imageUrl = %@",imageUrl);
                                           }];
        }
        
    }
}

#pragma mark - Popup control
-(void)segueFilter:(id)sender{
    PKMapBottomPopupVC* mapBottomPopupController =  [self.storyboard instantiateViewControllerWithIdentifier:@"PKMapBottomPopupVC"];//[[PKMapBottomPopupVC alloc] init];
    //    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:[[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PKMapBottomPopupVC"]];
    mapBottomPopupController.delegate = self;
    
    mapBottomPopupController.countFilteredObject = [self.collectionView numberOfItemsInSection:0];
    
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:mapBottomPopupController];
    
    
    popupController.style = STPopupStyleBottomSheet;
    
    popupController.containerView.layer.cornerRadius = 10;
    
    
    
    [popupController.backgroundView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backgroundViewDidTap)]];
    
    // . NSLog(@"popupController1 %@", popupController);
    self.popup = popupController;
    [popupController presentInViewController:self];
}
-(void)backgroundViewDidTap{
#warning check this tap
    //    STPopupController* popupController = [[STPopupController alloc] initWithRootViewController:self.popup];
    // . NSLog(@"popupController2 %@", self.popup);
    
    // . NSLog(@"PKMapObjectCollectionViewController selections selectionsTypes = %@", self.selectionsTypes);
    
    // . NSLog(@"PKMapObjectCollectionViewController selections selectionsAdditional = %@", self.selectionsAdditional);
    //    [self.popup dismiss];
    [self.popup popViewControllerAnimated:YES];
    
}
#pragma mark - Popup Protocol
- (NSInteger)didFinishWithArray:(NSArray *)selectionsTypes andArray:(NSArray *)selectionsAdditional{
    //    [self.mapView removeAnnotations:self.mapView.annotations];
    if([selectionsTypes count] == 0 || selectionsTypes == nil){
        selectionsTypes = [[PKDataManager sharedManager] getTypesOfObjectsArrayGrouped];
    }
    self.selectionsTypes = selectionsTypes;
    self.selectionsAdditional = selectionsAdditional;
    
    [self reloadwithPredicateDefault];
    [self reloadMapIfNeeded];
    //    [self setAnatation];
    return [self.collectionView numberOfItemsInSection:0];
}
@end

#pragma mark - PKMapObjectCell Implementation

@interface PKMapObjectCell ()

@property (strong, nonatomic, readwrite) UIImageView *imageView;
//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;



@end

@implementation PKMapObjectCell

#pragma mark - Constructors

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 10;
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_imageView];
        
        self.logoImageView = [[PKBrandLogo alloc] init];
        [self.contentView addSubview:_logoImageView];
        
        self.titleLabel = [[UILabel alloc] init];
        if(self.contentView.frame.size.width > 300){
            _titleLabel.font = [UIFont systemFontOfSize:zoomTextFontTitle300];
            _titleLabel.numberOfLines = 1;
        } else {
            _titleLabel.font = [UIFont systemFontOfSize:zoomTextFontTitle];
            _titleLabel.numberOfLines = 2;
        }
        
        
        [self.contentView addSubview:_titleLabel];
        
        self.distanceView = [[UILabel alloc] init];
        _distanceView.font = [UIFont systemFontOfSize:13.f weight:400];
        _distanceView.textColor = [UIColor whiteColor];
        _distanceView.layer.shadowColor = [[PKUOStyleKit uOTextColor] CGColor];
        _distanceView.layer.shadowOffset = CGSizeMake(0.0, 0.0);
        _distanceView.layer.shadowOpacity = 1;
        _distanceView.layer.shadowRadius = 2.f;
        _distanceView.layer.masksToBounds = NO;
        _distanceView.textAlignment = NSTextAlignmentRight;
        _distanceView.numberOfLines = 1;
        _distanceView.layer.shouldRasterize = YES;
        
        
        [self.contentView addSubview:_distanceView];
        
        self.likesButton = [[PKButtonLike alloc] init];
        
        [self.contentView addSubview:_likesButton];
        
        self.layer.borderWidth = 0.0;
        self.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        
        
    }
    return self;
}

#pragma mark - Setters

//- (void)setProduct:(ZOProduct *)product {
//    _product = product;
//    _titleLabel.text = product.title;
//    _imageView.image = [UIImage imageNamed:product.imageName];
//    
//    [self setNeedsLayout];
//}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.height - zoomCellTextAreaHeight);
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    
    _logoImageView.frame = CGRectMake(0.f, 0.f, 112.f, 70.f);
    _distanceView.frame = CGRectMake(self.contentView.frame.size.width - 83.f - 5.f, 0.f, 83.f, 32.f);
    
    CGFloat marginLeft;
    if(self.contentView.frame.size.width > 300){
        marginLeft = 11.f;
    } else {
        marginLeft = 5.f;
    }
        
    CGFloat labelWidth = self.contentView.frame.size.width - marginLeft - zoomCellTextAreaHeight;
    CGFloat labelHeight = [_titleLabel sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].height;
    _titleLabel.frame = CGRectMake(marginLeft, _imageView.frame.origin.y + _imageView.frame.size.height + marginLeft, labelWidth, labelHeight);
    _likesButton.frame = CGRectMake(self.contentView.frame.size.width - zoomCellTextAreaHeight, self.contentView.frame.size.height - zoomCellTextAreaHeight, zoomCellTextAreaHeight, zoomCellTextAreaHeight);
}
@end
