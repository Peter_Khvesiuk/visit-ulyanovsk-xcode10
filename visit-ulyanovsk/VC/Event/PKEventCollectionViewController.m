//
//  PKEventCollectionViewController.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 14/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKEventCollectionViewController.h"

#import "PKServerManager.h"
#import "PKDataManager.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "PKUOStyleKit.h"
//#import "PKInterfaceManager.h"
//#import "PKTopNavButtonView.h"
#import "PKEvent+CoreDataClass.h"

#import "PKInformTipsDetailVC.h"
#import "PKMapObj+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import "PKBrandLogo.h"

static NSString * PKEventsCellId           = @"PKEventsCell";

@interface PKEventCollectionViewController ()<NSFetchedResultsControllerDelegate>
//@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;


@property (nonatomic) CGFloat cellsInRow;
@property (nonatomic) CGFloat detailImageHeight;

@property NSMutableArray *sectionChanges;
@property NSMutableArray *itemChanges;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;


@property (strong, nonatomic) NSPredicate * predicate1;
@property (strong, nonatomic) NSPredicate * predicate2;
@property (strong, nonatomic) NSNumber * flagForPredicatSelected1;
@property (strong, nonatomic) NSString * flagForPredicatSelected2;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (strong, nonatomic) PKEventsCell *selectedCell;

@end

@implementation PKEventCollectionViewController
@synthesize predicate;
@synthesize delegate;
@synthesize ttitle;
@synthesize isTips;


-(void)dealloc{
    // . NSLog(@"\ndealloc PKEventCollectionViewController");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.detailImageHeight = zoomDetailImageHeight;
    self.cellsInRow = zoomCellsTips;
    //    #warning delegate????????
    //    self.navigationController.delegate = self;
    
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.94 alpha:1.0];//clearColor];//
    [self.collectionView registerClass:[PKEventsCell class] forCellWithReuseIdentifier:PKEventsCellId];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDelegate & Data Source Methods
//
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    // . NSLog(@"numberOfRowsInSection = %lu", (unsigned long)[sectionInfo numberOfObjects]);
    //warning Incomplete implementation, return the number of items
    return [sectionInfo numberOfObjects];
    //    return [_products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PKEventsCell *cell = (PKEventsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PKEventsCellId forIndexPath:indexPath];
    if (!cell) {
        cell = [[PKEventsCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = (PKEventsCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSArray* sendData = [[NSArray alloc] initWithObjects:self.selectedCell.imageView, self.collectionView, (PKEvent *) [self.fetchedResultsController objectAtIndexPath:indexPath], nil];
    
    [delegate sendDataToA:sendData];
    //    [self performSegueWithIdentifier:@"toInformTipsDetailVC" sender:self.selectedCell];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    //    if([segue.identifier isEqualToString:@"toInformTipsDetailVC"]){
    //
    //        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
    //        PKInformTipsDetailVC*vc = [segue destinationViewController] ;
    //        [vc setInformTip:(PKInformTips *) [self.fetchedResultsController objectAtIndexPath:indexPath]];
    //
    //    }
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Cell width is half the screen width - edge margin - half the center margin
    
    //    PKEventsCell *cell = (PKEventsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PKEventsCellId forIndexPath:indexPath];
    
    PKEvent *infoTip = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    CGFloat width = (self.collectionView.frame.size.width / self.cellsInRow) - zoomCellMargin - (zoomCellSpacing / self.cellsInRow);
    CGFloat newHeight = [PKEventsCell heightForText:infoTip.title andWidth:width];
    //    if(newHeight < cellTextAreaHeightTips){
    //        newHeight = cellTextAreaHeightTips;
    //    }
    
    
    
    
    CGFloat height = ((self.detailImageHeight / self.collectionView.frame.size.width) * width) + newHeight;
    return CGSizeMake(width, height);
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.f, zoomCellMargin, 70.f, zoomCellMargin);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}





#pragma mark - Collection view content controll

- (void)refershControlAction{
    [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(BOOL isComplete){
        if(isComplete){
            [self.refreshControl endRefreshing];
            //            [self.collectionView reloadData];
            [self reloadwithPredicateDefault];
        }
        
    } inProgress:^(CGFloat progress) {
        // . NSLog(@"progress == %f", progress);
    } onFailure:^(NSError* error, NSInteger statusCode) {
        [self.refreshControl endRefreshing];
    }];
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    _sectionChanges = [[NSMutableArray alloc] init];
    _itemChanges = [[NSMutableArray alloc] init];
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKEvent"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    
    NSSortDescriptor* sortAscending =
    [[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES];
    NSSortDescriptor* dateAscending =
    [[NSSortDescriptor alloc] initWithKey:@"dateStart" ascending:NO];
    [fetchRequest setSortDescriptors:@[dateAscending, sortAscending]];
    
//    _predicate1 = [NSPredicate predicateWithFormat:@"symbolCode == %@", predicate];

    _predicate1 = [NSPredicate predicateWithFormat:@"idevent IN %@", self.predicate];
//    NSLog(@"ZXLo here self.predicate = %@", self.predicate);

    [fetchRequest setPredicate:_predicate1];
    
    
    
    
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil//@"ageTo"
                                                   cacheName:nil];//@"Master"
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        // . NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
    
    
    
    
    
    return _fetchedResultsController;
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    change[@(type)] = @(sectionIndex);
    [_sectionChanges addObject:change];
    
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath {
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_itemChanges addObject:change];
}
- (void)reloadwithPredicateDefault {
    // Make sure to delete the cache
    // (using the name from when you created the fetched results controller)
    [NSFetchedResultsController deleteCacheWithName:nil];
    // Delete the old fetched results controller
    self.fetchedResultsController = nil;
    // TODO: Handle error!
    // This will cause the fetched results controller to be created
    // with the new predicate
    [self.fetchedResultsController performFetch:nil];
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource
- (NSManagedObjectContext*) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[PKDataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.collectionView performBatchUpdates:^{
        for (NSDictionary *change in self->_sectionChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeMove:
                        // . NSLog(@"NSFetchedResultsChangeMove empty!!!!!!!!!");
                        break;
                    case NSFetchedResultsChangeUpdate:
                        // . NSLog(@"NSFetchedResultsChangeUpdate empty!!!!!!!!!");
                        break;
                }
            }];
        }
        for (NSDictionary *change in self->_itemChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeUpdate:
                        [self.collectionView reloadItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeMove:
                        [self.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                        break;
                }
            }];
        }
    } completion:^(BOOL finished) {
        self->_sectionChanges = nil;
        self->_itemChanges = nil;
    }];
}
#pragma mark - ConfigureCell



-(void)configureCell:(PKEventsCell*) cell atIndexPath:(NSIndexPath*)indexPath{
    PKEvent *event = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.imageView.image = nil;
    cell.titleLabel.text = [NSString stringWithFormat:@"%@", event.title];//event.title;
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    cell.imageView.backgroundColor = [UIColor lightGrayColor];
    NSString* dataPath;
    NSString* imageUrl;

    cell.logoImageView.image = nil;
    if(event.brand.logoImage && ![event.brand.logoImage isEqualToString:@"no_image.jpg"]){
        [cell.logoImageView setImageWithName:event.brand.logoImage andContentModeTop:YES];
    }
    
    if([event.previewImgName isEqualToString:@"no_image.jpg"] && event.mapObjects != nil){
        PKMapObj *mapObject = [[event.mapObjects allObjects] firstObject];
        dataPath = LOCAL_mapObjImagePathPreview(mapObject.prevImageName);
        imageUrl = URL_mapObjImagePathPreview(mapObject.prevImageName);
    } else {
//        if(self.isTips){
//            dataPath = LOCAL_informImagePathPreview(event.previewImgName);
//            imageUrl = URL_informImagePathPreview(event.previewImgName);
//        } else {
            dataPath = LOCAL_eventsImagePathPreview(event.previewImgName);
            imageUrl = URL_eventsImagePathPreview(event.previewImgName);
//        }
    }
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        [cell.imageView setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
    } else {
        NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
        __weak PKEventsCell* weakCellImage = cell;
        //CellWherePhoto.trenerPhotoDetail.image = nil;
        [cell.imageView setImageWithURLRequest:requestwhereImage
                              placeholderImage:nil
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                           
                                           //         // . NSLog(@"requestwhereImage success");
                                           
                                           
                                           weakCellImage.imageView.image = image;
                                           
                                           //                                           weakCellImage.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
                                           [weakCellImage layoutSubviews];
                                           
                                       }
                                       failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                           // . NSLog(@"failure imageUrl = %@",imageUrl);
                                       }];
    }
}

#pragma mark - Popup control

@end

#pragma mark - PKEventsCell Implementation

@interface PKEventsCell ()

@property (strong, nonatomic, readwrite) UIImageView *imageView;

//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;



@end

@implementation PKEventsCell

#pragma mark - Constructors

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_imageView];
        
        self.logoImageView = [[PKBrandLogo alloc] init];
        [self.contentView addSubview:_logoImageView];
        
        self.titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:cellTextFontTitleTips];
        _titleLabel.numberOfLines = 0;
        [self.contentView addSubview:_titleLabel];
        self.layer.borderWidth = 0.0;
        self.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        
        
    }
    return self;
}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat labelWidth = self.contentView.frame.size.width - 15.f*2;
    CGFloat labelHeight = [_titleLabel sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].height;
    
    //    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
    _imageView.frame = CGRectMake(0.f, 0.f, self.contentView.frame.size.width, self.contentView.frame.size.height - (labelHeight + 30.f));//cellTextAreaHeightTips);//zoomCellTextAreaHeight
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    
    _logoImageView.frame = CGRectMake(0.f, self.contentView.frame.size.height - (labelHeight + 30.f + 70.f), 112.f, 70.f);
    
    _titleLabel.frame = CGRectMake(15.f, _imageView.frame.size.height +10.f, labelWidth, labelHeight);
    
}
+ (CGFloat) heightForText:(NSString*) text andWidth:(CGFloat)width {
    
    CGFloat offset = 15.0;
    
    UIFont* font = [UIFont systemFontOfSize:cellTextFontTitleTips];
    
    NSShadow* shadow = [[NSShadow alloc] init];
    NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraph setAlignment:NSTextAlignmentCenter];
    
    NSDictionary* attributes =
    [NSDictionary dictionaryWithObjectsAndKeys:
     font, NSFontAttributeName,
     paragraph, NSParagraphStyleAttributeName,
     shadow, NSShadowAttributeName, nil];
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width - 2 * offset, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];
    
    return CGRectGetHeight(rect) + 2 * offset;
}

@end
