//
//  PKEventCollectionViewController.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 14/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class PKBrandLogo;

@protocol senddataProtocol <NSObject>

-(void)sendDataToA:(NSArray *)array; //I am thinking my data is NSArray, you can use another object for store your information.

@end



@interface PKEventCollectionViewController : UICollectionViewController
//ZOZolaZoomTransitionDelegate, UINavigationControllerDelegate>

@property(nonatomic,assign)id delegate;
@property (strong, nonatomic) NSArray<NSNumber*>* predicate;
@property (strong, nonatomic) NSString* ttitle;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (nonatomic) BOOL isTips;
//-(instancetype)initWithPredicate:(NSString*)predicate andTips:(BOOL)isTips;
//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;


@end
@interface PKEventsCell : UICollectionViewCell

//@property (strong, nonatomic) ZOProduct *product;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (strong, nonatomic, readwrite) PKBrandLogo *logoImageView;
@property (strong, nonatomic) UILabel *titleLabel;
+ (CGFloat) heightForText:(NSString*) text andWidth:(CGFloat)width;
//@property (strong, nonatomic) UITextView *subTitleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;

@end

NS_ASSUME_NONNULL_END
