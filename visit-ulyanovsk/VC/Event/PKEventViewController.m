//
//  PKEventViewController.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 14/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKEventViewController.h"
#import "PKInterfaceManager.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"

#import "PKMainViewController.h"

#import "PKEventCollectionViewController.h"
#import "PKEventsPageViewController.h"
#import "PKEvent+CoreDataClass.h"

#import "PKEventDetailVC.h"



@interface PKEventViewController ()
{
    PKEventsPageViewController *vc;
}
@property (strong, nonatomic) PKEvent *eventTransiton;
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@end

@implementation PKEventViewController
@synthesize showTips;
@synthesize imageViewTransition;
@synthesize collectionViewTransition;
-(void)dealloc{
    // . NSLog(@"\ndealloc PKEventViewController");
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.delegate = self;
    
    NSString *title = [[NSString alloc] init];
    if(showTips){
        title = TRANSLATE(@"Tips");
    } else {
        title = TRANSLATE_UP(@"calendar");
    }
    
    
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:title andTitleHide:NO];
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, 50.f, self.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [self.view insertSubview:backgroundInfoBlockImage atIndex:0];
    self.view.clipsToBounds = YES;
    
    
    NSDictionary* blocksArray = [self blocksArray];
    
    if(blocksArray !=nil){
        vc = [[PKEventsPageViewController alloc] initWithViewControllerClasses:blocksArray[@"classes"] andPredicates:blocksArray[@"predicate"] andTitles:blocksArray[@"title"] andViewController:self];
        vc.itemWidth = 90;
        vc.style = PKTipsTopViewStyleNOLine;//PKTopViewStyleLine;
        vc.topViewBackGroundColor = [UIColor clearColor];
        vc.itemWidthArray = blocksArray[@"width"];
        
        
        CGRect infoBlockFrame = CGRectMake(0.f, marginTopNavigationHeight + 44.f + 20.f, self.view.bounds.size.width,  self.view.bounds.size.height - (marginTopNavigationHeight + 44.f + 20.f));
        
        [self addChildViewController:vc];
        vc.view.frame = infoBlockFrame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)sendDataToA:(NSArray *)array{
    imageViewTransition = [array firstObject];
    collectionViewTransition = [array objectAtIndex:1];
    self.eventTransiton = [array lastObject];
    // . NSLog(@"sendDataToA");// data will come here inside of ViewControllerA
    [self performSegueWithIdentifier:@"toEventDetailVC" sender:self];
}


-(void)popBackToMainVC:(id)sender{
    //    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    self.navigationController.delegate = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
-(NSArray <NSDictionary <NSString*, NSArray<NSNumber*>*> *> *)eventsFromDb{
//    return [[PKDataManager sharedManager] informTipsOrNewsFromBd:self.showTips];
    return [[PKDataManager sharedManager] eventsDateListFromBd];
}

-(NSDictionary*)blocksArray{
    NSMutableArray* classMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* titleMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* predicateMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* widthMutableArray = [[NSMutableArray alloc] init];
    //    NSMutableArray* predicateMutableArray = [[NSMutableArray alloc] init];
    
    
    NSArray *eventArray = [self eventsFromDb];
    if(eventArray == nil || [eventArray count] == 0){
        return nil;
    } else {
        
        for (NSDictionary *eventsDictionaryFromDb in eventArray) {
            NSString * key = [[eventsDictionaryFromDb allKeys] firstObject];
            NSArray<NSNumber*> *predicate = eventsDictionaryFromDb[key];
            [classMutableArray addObject:[PKEventCollectionViewController class]];
            [titleMutableArray addObject: key];
            [predicateMutableArray addObject:predicate];// @"Транспорт"];
            [widthMutableArray addObject:@([self lenghtForLabelFromString:TRANSLATE_UP(key)])];
        }
    }
    NSMutableDictionary* mutableDictionary = [[NSMutableDictionary alloc] init];
    [mutableDictionary setValue:classMutableArray forKey:@"classes"];
    [mutableDictionary setValue:predicateMutableArray forKey:@"predicate"];
    [mutableDictionary setValue:titleMutableArray forKey:@"title"];
    [mutableDictionary setValue:widthMutableArray forKey:@"width"];
    NSDictionary* dict = [[NSDictionary alloc] initWithDictionary:mutableDictionary];
    return dict;
}
-(CGFloat)lenghtForLabelFromString:(NSString*)string{
    // . NSLog(@"\nlenghtForLabel %@ = %lu %f", string, (unsigned long)[string length], [string length]*7.f);
    return ([string length]*10.f + 24.f);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toMainVC"]){
        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
        NSArray *viewContrlls=[navControl viewControllers];
        for( int i=0;i<[ viewContrlls count];i++){
            id vcs=[viewContrlls objectAtIndex:i];
            if([vcs isKindOfClass:[PKMainViewController class]]){
                
                PKMainViewController *vc = (PKMainViewController*)vcs;
                [vc setInstanceView:YES];
                return;
            }
        }
    } else if([segue.identifier isEqualToString:@"toEventDetailVC"]){
        PKEventDetailVC*vc = [segue destinationViewController] ;
        [vc setEvent:self.eventTransiton];
//        vc.isTips = self.showTips;
        
    }
}







#pragma mark - UINavigationControllerDelegate Methods

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    // Sanity
    if (fromVC != self && toVC != self) return nil;
    
    // Determine if we're presenting or dismissing
    ZOTransitionType type = (fromVC == self) ? ZOTransitionTypePresenting : ZOTransitionTypeDismissing;
    
    // Create a transition instance with the selected cell's imageView as the target view
    ZOZolaZoomTransition *zoomTransition = [ZOZolaZoomTransition transitionFromView: imageViewTransition
                                                                               type:type
                                                                           duration:0.5
                                                                           delegate:self];
    zoomTransition.fadeColor = [UIColor colorWithWhite:0.94 alpha:1.0];
    
    return zoomTransition;
}

#pragma mark - ZOZolaZoomTransitionDelegate Methods

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
        startingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The starting frame is taken from the selected cell's imageView.
        return [imageViewTransition convertRect:imageViewTransition.bounds toView:relativeView];
    } else if ([fromViewController isKindOfClass:[PKEventDetailVC class]]) {
        // We're popping back to this master controller. The starting frame is taken from the detailController's imageView.
        PKEventDetailVC *detailController = (PKEventDetailVC *)fromViewController;
        return [detailController.imageGalleryView
                convertRect:detailController.imageGalleryView.bounds toView:relativeView];
    }
    
    return CGRectZero;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
       finishingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The finishing frame is taken from the detailController's imageView.
        PKEventDetailVC *detailController = (PKEventDetailVC *)toViewController;
        
        return [detailController.imageGalleryView convertRect:CGRectMake(0, marginTopNavigationHeight, detailController.imageGalleryView.bounds.size.width, detailController.imageGalleryView.bounds.size.height) toView:relativeView];
        
#warning Navigator bar height epxected as 22? Okay?
    } else if ([fromViewController isKindOfClass:[PKEventDetailVC class]]) {
        // We're popping back to this master controller. The finishing frame is taken from the selected cell's imageView.
        return [imageViewTransition convertRect:imageViewTransition.bounds toView:relativeView];
    }
    
    return CGRectZero;
    
}

- (NSArray *)supplementaryViewsForZolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition {
    // Here we're returning all UICollectionViewCells that are clipped by the edge
    // of the screen. These will be used as "supplementary views" so that the clipped
    // cells will be drawn in their entirety rather than appearing cut off during the
    // transition animation.
    
    NSMutableArray *clippedCells = [NSMutableArray arrayWithCapacity:[[collectionViewTransition visibleCells] count]];
    for (UICollectionViewCell *visibleCell in collectionViewTransition.visibleCells) {
        CGRect convertedRect = [visibleCell convertRect:visibleCell.bounds toView:self.view];
        if (!CGRectContainsRect(self.view.frame, convertedRect)) {
            [clippedCells addObject:visibleCell];
        }
    }
    return clippedCells;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
   frameForSupplementaryView:(UIView *)supplementaryView
              relativeToView:(UIView *)relativeView {
    
    return [supplementaryView convertRect:supplementaryView.bounds toView:relativeView];
}


@end

