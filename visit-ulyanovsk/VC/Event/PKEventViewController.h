//
//  PKEventViewController.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 14/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZOZolaZoomTransition.h"

NS_ASSUME_NONNULL_BEGIN

@class UICollectionView;

@interface PKEventViewController : UIViewController<ZOZolaZoomTransitionDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIImageView *imageViewTransition;
@property (strong, nonatomic) UICollectionView *collectionViewTransition;
@property (nonatomic) BOOL showTips;
@end

NS_ASSUME_NONNULL_END
