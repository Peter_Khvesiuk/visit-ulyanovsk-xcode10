//
//  PKEventDetailVC.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 16/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAKImageGalleryView.h"

NS_ASSUME_NONNULL_BEGIN

@class PKEvent;

@interface PKEventDetailVC : UIViewController

@property (strong, nonatomic) PKEvent *event;
@property (weak, nonatomic) MAKImageGalleryView *imageGalleryView;
@property (nonatomic) int eventId;

@end
NS_ASSUME_NONNULL_END
