//
//  PKEventDetailVC.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 16/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKEventDetailVC.h"
#import "PKUOStyleKit.h"
#import "PKInterfaceManager.h"
#import "PKTopNavButtonView.h"

#import <AFNetworking/AFImageDownloader.h>

#import "UILabel+Copyable.h"

#import "PKPageViewController.h"
#import "PKComercialOrganization+CoreDataClass.h"

#import "PKInfoRouteAdressViewController.h"
#import "PKInfoGeoPhoneViewController.h"
#import "PKInfoEmailViewController.h"
#import "PKInfoSiteViewController.h"

#import "PKObjImages+CoreDataClass.h"
#import "PKEvent+CoreDataClass.h"
#import "PKInfoAdressViewController.h"

#import "PKMainMapViewController.h"

#import "PKBlockPhoneViewController.h"
#import "PKBlockWiFiViewController.h"
#import "PKKidsFrindlyBlockViewController.h"
#import "PKInfoPayCardViewController.h"
#import "PKInfoDisablesViewController.h"

#import "PKBlockAgeViewController.h"
#import "PKBlockIsFreeViewController.h"
#import "PKBlockCalendarViewController.h"
#import "PKBrand+CoreDataClass.h"
#import "PKBrandLogo.h"

#import "PKDataManager.h"

@class PKMapObj;

//#import "PKMainMapViewController.h"
//static CGFloat regularFZMargin = 14.0;
//static CGFloat PKGeoRouteCellTextAreaHeight  = 66.0;
//static CGFloat detailImageHeight              = 300.0;

@interface PKEventDetailVC () <MAKImageGalleryViewDataSource, UIScrollViewDelegate>{
    PKPageViewController *vc;
}
//@property (nonatomic) CGFloat cellTextAreaHeight;

@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subTitleLabel;
@property (strong, nonatomic) UITextView *descriptionLabel;
@property (strong, nonatomic) UILabel *categoryLabel;
@property (strong, nonatomic) UITextView *organizationsTextView;
@property (strong, nonatomic) UIButton* btnBack;
@property (retain, nonatomic) UIScrollView * scrollView;
@property (assign, nonatomic) BOOL scrollWasAnimated;
//@property (strong, nonatomic) UIImageView *imagggview;
@property (strong, nonatomic) NSMutableArray *downloads;
@property (weak, nonatomic)   NSSet <PKMapObj *> *mapObjects;

@property (strong, nonatomic) UISelectionFeedbackGenerator *feedbackGenerator;

@end

@implementation PKEventDetailVC
@synthesize event;
@synthesize imageGalleryView;
@synthesize eventId;
-(void)dealloc{
    // . NSLog(@"\ndealloc PKInformTipsDetailVC");
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // . NSLog(@"viewDidAppear");
    
    self.topNavButtonView.open = YES;
    //    if(self.btnBack != nil){
    //        [self.view addSubview:self.btnBack];
    //        CGRect frame = self.btnBack.frame;
    //        frame.origin.x = 0;
    //        [UIView animateWithDuration:0.2
    //                              delay:0.0
    //                            options: UIViewAnimationOptionCurveEaseOut
    //                         animations:^{
    //                             self.btnBack.frame = frame;
    //                         }
    //                         completion:^(BOOL finished){
    //                             // . NSLog(@"complete");
    //                             self.scrollWasAnimated = YES;
    //                         }];
    //    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    if(self.event == nil){
        self.event = [[PKDataManager sharedManager] oneEventWithId:eventId];
    }
    
    //    self.cellTextAreaHeight = cellTextAreaHeightTips;
    
    self.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    
    
    
    self.scrollWasAnimated = NO;
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];//
    //    self.scrollView.contentInset
    
    CGSize scrollViewContentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height+100);
    [self.scrollView setContentSize:scrollViewContentSize];
    
    
    [self.view addSubview:_scrollView];
    self.scrollView.delegate = self;
    
    
    MAKImageGalleryView * imageGalleryView = [[MAKImageGalleryView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, zoomDetailImageHeight)];
    self.imageGalleryView = imageGalleryView;
    self.imageGalleryView.imageGalleryDataSource = self;
    self.imageGalleryView.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor];
    [_scrollView addSubview:self.imageGalleryView];
    
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(zoomDetailImageHeight+10.f, 0, 0, 0);
    
    
    
    if(self.event.brand.logoImage && ![self.event.brand.logoImage isEqualToString:@"no_image.jpg"]){
        PKBrandLogo *logo = [[PKBrandLogo alloc] initWithFrame:CGRectMake(0, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height - (80.f + 0), 128.f, 80.f)];
        [_scrollView addSubview:logo];
        logo.clipsToBounds = YES;
        [logo setImageWithName:self.event.brand.logoImage andContentModeTop:NO];
    }
    
    
    
    self.titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:detailFZFontSizeTitle];
    _titleLabel.numberOfLines = 0;
    _titleLabel.text = self.event.title;
    [_scrollView addSubview:_titleLabel];
    CGSize titleLabelSize = [_titleLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - cellTextAreaHeightTips - regularFZMargin*2), CGFLOAT_MAX)];
    _titleLabel.frame = CGRectMake(regularFZMargin, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height + regularFZMargin, titleLabelSize.width, titleLabelSize.height);
    
    
    NSDictionary* blocksArray = [self blocksArray];
    
    CGFloat marginLeftBlock;//regularFZMargin;
    
    CGRect infoBlockFrame;
    
    __weak PKEventDetailVC *weakSelf = self;
    if([blocksArray[@"classes"] count] != 0){
        // . NSLog(@"JKD self = %@", self);
        vc = [[PKPageViewController alloc]initWithViewControllerClasses:blocksArray[@"classes"]  andIconImage:blocksArray[@"images"] andIconImageSelected:blocksArray[@"imagesSelected"] andObject: self.event andSelfVC: weakSelf];
        
        
        //Установите ширину каждого элемента
        vc.itemWidth = 60;
        //Выберите стиль подчеркивания
        vc.style = PKTopViewStyleNOLine;//PKTopViewStyleLine;
        //（Не подчеркнул）
        //        vc.style = ASTopViewStyleNOLine;
        //Установить по обе стороны пустого Ширина
        //        vc.sideBothWidth = 20;
        //Установить нормальный цвет шрифта
        //        vc.normalTitleColor = [UIColor orangeColor];
        //Настройки, выбранные при изменении шрифта, цвета
        //        vc.selectTitleColor = [UIColor blueColor];
        //Установить верхний элемент Цвет фона
        vc.topViewBackGroundColor = [UIColor clearColor];
        //Задать цвет подчеркивания
        //        vc.lineColor = [UIColor blueColor];
        //Набор всех предметов Ширина массива
        //    vc.itemWidthArray = @[@(150),@(80),@(70),@(90),@(150)];
        marginLeftBlock = 50.f;//regularFZMargin;
        
        infoBlockFrame = CGRectMake(marginLeftBlock, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + 20.f, self.view.bounds.size.width - marginLeftBlock, 160.f);
        
        
        //        CGSize cellSize = CGSizeMake(infoBlockFrame.size.width/2 - 10.f, infoBlockFrame.size.height/2 - 5.f);
        //        UIView* infoBlocks = [[UIView alloc] initWithFrame:infoBlockFrame];
        //        [infoBlocks setBackgroundColor: [UIColor redColor]];
        
        
        
        
        [self addChildViewController:vc];
        vc.view.frame = infoBlockFrame;
        [_scrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    } else {
        infoBlockFrame = CGRectMake(0, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + 20.f, 0, 0);
    }
    
    
    
    
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, _titleLabel.frame.origin.y + _titleLabel.frame.size.height - 20.f, self.view.bounds.size.width + 70.f, zoomDetailImageHeight)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    vc.view.clipsToBounds = YES;
    [_scrollView insertSubview:backgroundInfoBlockImage atIndex:0];
    //    [[self.imageGalleryView superview] sendSubviewToBack : backgroundInfoBlockImage];
    
    
    //    self.infoBlocksView = infoBlocks;
    //    [_scrollView addSubview:self.infoBlocksView];
    //    self.infoBlocksView.infoBlocksDataSource = self;
    
    
    CGFloat newsAdditionalInfoHeight = infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin;
    //    if(!self.informTip.isTips){
    //        UILabel *dateLabel = [[UILabel alloc] init];
    //        dateLabel.font = [UIFont italicSystemFontOfSize:detailFZFontSizeDescription];
    //        //    _descriptionLabel.textAlignment = UITextAlignmentRight;
    //        dateLabel.textAlignment = NSTextAlignmentRight;
    //        dateLabel.backgroundColor = [UIColor clearColor];
    //        dateLabel.text = self.informTip.dateString;
    //        CGSize dateLabelSize = [dateLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin * 2.0), CGFLOAT_MAX)];
    //        CGRect dateLabelFrame = CGRectMake(_scrollView.frame.size.width - regularFZMargin - dateLabelSize.width, infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin, dateLabelSize.width, dateLabelSize.height);
    //        dateLabel.frame = dateLabelFrame;
    //        [_scrollView addSubview:dateLabel];
    //
    //        UILabel *ageLabel = [[UILabel alloc] init];
    //        ageLabel.font = [UIFont boldSystemFontOfSize:(detailFZFontSizeDescription + 5.f)];
    //        //    _descriptionLabel.textAlignment = UITextAlignmentRight;
    //        ageLabel.textAlignment = NSTextAlignmentRight;
    //        ageLabel.backgroundColor = [UIColor clearColor];
    //        ageLabel.textColor = [PKUOStyleKit bgColorGradientBottom];
    //        ageLabel.text = self.informTip.ageString;
    //        CGSize ageLabelSize = [ageLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin * 2.0), CGFLOAT_MAX)];
    //        CGRect ageLabelFrame = CGRectMake(_scrollView.frame.size.width - regularFZMargin - ageLabelSize.width, dateLabelFrame.origin.y + dateLabelFrame.size.height + regularFZMargin, ageLabelSize.width, ageLabelSize.height);
    //        ageLabel.frame = ageLabelFrame;
    //        [_scrollView addSubview:ageLabel];
    //
    //        newsAdditionalInfoHeight = ageLabelFrame.origin.y + ageLabelFrame.size.height + regularFZMargin;
    //
    //    } else {
    //        newsAdditionalInfoHeight = infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin;
    //    }
    
    self.descriptionLabel = [[UITextView alloc] init];
    _descriptionLabel.font = [UIFont systemFontOfSize:detailFZFontSizeDescription];
    //    _descriptionLabel.textAlignment = UITextAlignmentRight;
    _descriptionLabel.textAlignment = NSTextAlignmentJustified;
    _descriptionLabel.selectable = YES;
    _descriptionLabel.editable = NO;
    _descriptionLabel.scrollEnabled = NO;
    _descriptionLabel.backgroundColor = [UIColor clearColor];
    //    _descriptionLabel.lineBreakMode = NSLineBreakByCharWrapping;
    //    _descriptionLabel.numberOfLines = 0;
    //    _descriptionLabel.copyingEnabled = YES;
    _descriptionLabel.text = self.event.descriptionss;
    [_scrollView addSubview:_descriptionLabel];
    //    CGSize lLabelSize = [self.mapObject.descr sizeWithFont: _descriptionLabel.font forWidth:(_scrollView.frame.size.width - regularFZMargin * 2.0) lineBreakMode:_descriptionLabel.lineBreakMode];
    CGSize descriptionSize = [_descriptionLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin * 2.0), CGFLOAT_MAX)];
    _descriptionLabel.frame = CGRectMake(regularFZMargin, newsAdditionalInfoHeight, descriptionSize.width, descriptionSize.height);
    
    
    CGFloat organizationsAdditionalInfoHeight = _descriptionLabel.frame.origin.y+ _descriptionLabel.frame.size.height + regularFZMargin*2;
    
    
    NSString *orgStrings = [[NSString alloc] init];
    orgStrings = @"";
    if([[self.event.organizations allObjects] count] > 0){
        NSString *orgss = [[NSString alloc] init];
        if([[self.event.organizations allObjects] count] == 1){
            orgss = @"Организатор:\n";
        } else {
            orgss = @"Организаторы:\n";
        }
        for (PKComercialOrganization *org in [self.event.organizations allObjects]) {
            if([orgStrings isEqualToString:@""]){
                orgStrings = [NSString stringWithFormat:@"%@%@", orgss, org.nameOrg];
            } else {
                orgStrings = [NSString stringWithFormat:@"%@,\n%@", orgStrings, org.nameOrg];
            }
            
        }
    }
    
    
    
    self.organizationsTextView = [[UITextView alloc] init];
    _organizationsTextView.font = [UIFont systemFontOfSize:detailFZFontSizeDescription];
    //    _descriptionLabel.textAlignment = UITextAlignmentRight;
    _organizationsTextView.textAlignment = NSTextAlignmentJustified;
    _organizationsTextView.selectable = YES;
    _organizationsTextView.editable = NO;
    _organizationsTextView.scrollEnabled = NO;
    _organizationsTextView.backgroundColor = [UIColor clearColor];
    //    _descriptionLabel.lineBreakMode = NSLineBreakByCharWrapping;
    //    _descriptionLabel.numberOfLines = 0;
    //    _descriptionLabel.copyingEnabled = YES;
    _organizationsTextView.text = orgStrings;
    [_scrollView addSubview:_organizationsTextView];
    //    CGSize lLabelSize = [self.mapObject.descr sizeWithFont: _descriptionLabel.font forWidth:(_scrollView.frame.size.width - regularFZMargin * 2.0) lineBreakMode:_descriptionLabel.lineBreakMode];
    CGSize organizationsSize = [_organizationsTextView sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin * 2.0), CGFLOAT_MAX)];
    _organizationsTextView.frame = CGRectMake(regularFZMargin, organizationsAdditionalInfoHeight, organizationsSize.width, organizationsSize.height);
    
    CGFloat contentHeight = _organizationsTextView.frame.origin.y + _organizationsTextView.frame.size.height + regularFZMargin;
    if(contentHeight < self.view.bounds.size.height){
        contentHeight = self.view.bounds.size.height + 20.f;
    }
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, contentHeight);
    
    //+++++++++++++++++++++++++++++
    //    [self.navigationController setNavigationBarHidden:YES];
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    
    
    
}


//-(void)logoImageToView:(UIImageView*)imageView withName:(NSString*)imageName{
//
//    NSString* dataPath;
//    NSString* imageUrl;
//    dataPath = LOCAL_LogosPath(imageName);
//    imageUrl = URL_LogosPath(imageName);
//
//    if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
//        [imageView setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
//    } else {
//        NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
//        __weak UIImageView* weakImageView = imageView;
//        imageView.image = nil;
//        [imageView setImageWithURLRequest:requestwhereImage
//                         placeholderImage:nil
//                                  success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
//
//                                      //         // . NSLog(@"requestwhereImage success");
//
//
//                                      weakImageView.image = image;
//
//                                      //                                           weakCellImage.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
//                                      [weakImageView layoutSubviews];
//
//                                  }
//                                  failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//                                      NSLog(@"failure imageUrl = %@",imageUrl);
//                                  }];
//    }
//
//
//}

-(NSDictionary*)blocksArray{
    NSMutableArray* classMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* imageMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* imageSelectedMutableArray = [[NSMutableArray alloc] init];
    //    PKComercialOrganization * org = self.geoRoute.geoOrganization;
    
    // . NSLog(@"self.geoRoute.geoOrganization %@", self.informTip.organization.nameOrg);
    
    //    if(self.geoRoute.geoJSON != nil){
    //        [classMutableArray addObject:[PKInfoRouteAdressViewController class]];
    //        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit uOTextColor]]];
    //        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    //    }
    NSLog(@"\n informTips.title %@ == %@",self.event.title, (PKMapObj*)[[self.event.mapObjects allObjects] firstObject].title);
    
    
    NSLog(@"\n mmm mapobjects = %lu", [[self.event.mapObjects allObjects] count]);
    
    
    
    if(self.event){
        
        [classMutableArray addObject:[PKBlockCalendarViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCalendarWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCalendarWithIconColor: [PKUOStyleKit bgColorGradientBottom]]];
        
        BOOL isFree = YES;
        if(self.event.price > 0){
            isFree = NO;
        }
        
        [classMutableArray addObject:[PKBlockIsFreeViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockPaymentWithIconColor:[PKUOStyleKit uOTextColor] pressed:isFree]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockPaymentWithIconColor: [PKUOStyleKit bgColorGradientBottom] pressed:isFree]];


        [classMutableArray addObject:[PKBlockAgeViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockAgeWithIconColor:[PKUOStyleKit uOTextColor] ageString: [NSString stringWithFormat:@"%i",self.event.ageFrom]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockAgeWithIconColor: [PKUOStyleKit bgColorGradientBottom] ageString: [NSString stringWithFormat:@"%i",self.event.ageFrom]]];

    }
    BOOL hasPhone = NO;
    BOOL hasEmail = NO;
    BOOL hasSite = NO;
    BOOL hasWifi = NO;
    BOOL iskidsfriendly = NO;
    BOOL isavalibleForDisables = NO;
    BOOL ispayCard = NO;
    
    if(self.event.organizations != nil){
        
        for (PKComercialOrganization *org in [self.event.organizations allObjects]) {
            if (![org.phoneOrg isEqualToString:@"0"]) {
                hasPhone = YES;
            }
            if (![org.emailOrg isEqualToString:@""]) {
                hasEmail = YES;
            }
            if (![org.siteOrg isEqualToString:@""]) {
                hasSite = YES;
            }
        }
        for (PKMapObj *mapObject in [self.event.mapObjects allObjects]) {
            if (![mapObject.phone isEqualToString:@"0"]) {
                hasPhone = YES;
            }
            if (![mapObject.email isEqualToString:@""]) {
                hasEmail = YES;
            }
            if (![mapObject.siteurl isEqualToString:@""]) {
                hasSite = YES;
            }
            if(mapObject.wiFi){
                hasWifi = YES;
            }
            if(mapObject.kidsfriendly){
                iskidsfriendly = YES;
            }
            if(mapObject.payCard){
                ispayCard = YES;
            }
            if(mapObject.avalibleForDisables){
                isavalibleForDisables = YES;
            }
        }
        if(hasPhone){
            [classMutableArray addObject:[PKBlockPhoneViewController class]];//PKInfoGeoPhoneViewController
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(hasEmail){
            [classMutableArray addObject:[PKInfoEmailViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(hasSite){
            [classMutableArray addObject:[PKInfoSiteViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(self.event.mapObjects != nil && [self.event.mapObjects count]>0){
            [classMutableArray addObject:[PKInfoAdressViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(hasWifi){
            [classMutableArray addObject:[PKBlockWiFiViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockWiFiWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockWiFiWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(iskidsfriendly){
            [classMutableArray addObject:[PKKidsFrindlyBlockViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconKidsFriendlyWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconKidsFriendlyWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(ispayCard){
            [classMutableArray addObject:[PKInfoPayCardViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCardWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCardWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
        if(isavalibleForDisables){
            [classMutableArray addObject:[PKInfoDisablesViewController class]];
            [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockDisablesWithIconColor:[PKUOStyleKit uOTextColor]]];
            [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockDisablesWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
        }
    }
    
    
    
    
    NSMutableDictionary* mutableDictionary = [[NSMutableDictionary alloc] init];
    [mutableDictionary setValue:classMutableArray forKey:@"classes"];
    [mutableDictionary setValue:imageMutableArray forKey:@"images"];
    [mutableDictionary setValue:imageSelectedMutableArray forKey:@"imagesSelected"];
    NSDictionary* dict = [[NSDictionary alloc] initWithDictionary:mutableDictionary];
    return dict;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.feedbackGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [self.feedbackGenerator prepare];
}

// Do any additional setup after loading the view.

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat y = scrollView.contentOffset.y;
    
    
    if(y< -64.f){
        
        // Keep the generator in a prepared state.
        CGFloat fraction = ( 1 - (-1* (y+ 64.f)) / ( 32.f) );
        
        [self.topNavButtonView setHideFraction:fraction];
        if(fraction>0){
            
            //            NSLog(@"y = %f fraction = %f YES", y, fraction);
        } else {
            [self.feedbackGenerator selectionChanged];
            self.feedbackGenerator = nil;
            //            NSLog(@"y = %f fraction = %f", y, fraction);
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    //    NSLog(@"y2 = %f", scrollView.contentOffset.y);
    if(scrollView.contentOffset.y>= -64.f){
        
        [self.topNavButtonView setHideFraction:1.f];
    }
    self.feedbackGenerator = nil;
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    
    CGFloat y = scrollView.contentOffset.y;
    //    NSLog(@"y3 = %f", y );
    if(y< -(64.f + 32.f)){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    self.feedbackGenerator = nil;
}


-(void)popBack:(id)sender{
    // . NSLog(@"popBack");
    UIButton* btn = (UIButton*)sender;
    CGRect frame = btn.frame;
    frame.origin.x = - btn.frame.size.width;
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         btn.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [[self navigationController] popViewControllerAnimated:YES];
                     }];
    
}

-(void)showOnMap{
    
    self.mapObjects = self.event.mapObjects;
    [self performSegueWithIdentifier:@"toMapFromEvent" sender:self];
}

-(void)showOnMapWithMapObjects:(NSSet<PKMapObj *> *)mapObjects{
    // . NSLog(@"showOnMapWithMapObjects jj");
    self.mapObjects = mapObjects;
    [self performSegueWithIdentifier:@"toMapFromEvent" sender:self];
    
    
    //    NSArray *viewContrlls=[[self navigationController] viewControllers];
    //    for( int i=0;i<[ viewContrlls count];i++){
    //        id map=[viewContrlls objectAtIndex:i];
    //        // . NSLog(@"viewContrlls = %@", [map class]);
    //        if([map isKindOfClass:[PKMainMapViewController class]]){
    //            // A is your class where to popback
    //            [[self navigationController] popToViewController:map animated:YES];
    //            return;
    //        }
    //    }
    //
    //    NSArray *tabBarControllers = [self.tabBarController viewControllers];
    //    for( int i=0;i<[ tabBarControllers count];i++){
    //        id map2=[tabBarControllers objectAtIndex:i];
    //        if([map2 isKindOfClass:[UINavigationController class]]){
    //            UINavigationController * navControl = (UINavigationController*)map2;
    //            NSArray *viewContrlls=[navControl viewControllers];
    //            for( int i=0;i<[ viewContrlls count];i++){
    //                id map=[viewContrlls objectAtIndex:i];
    //                // . NSLog(@"viewContrlls = %@", [map class]);
    //                if([map isKindOfClass:[PKMainMapViewController class]]){
    //
    //                    PKMainMapViewController *mapViewController = (PKMainMapViewController*)map;
    //                    [mapViewController setShowGeoRouteDestanation:self.geoRoute];
    //                    // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
    //                    self.tabBarController.selectedIndex = i;//[self.tabBarController.viewControllers indexOfObject:map];
    //
    //                    // A is your class where to popback
    //                    //                    [[self navigationController] popToViewController:map animated:YES];
    //                    return;
    //                }
    //            }
    //        }
    //    }
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





#pragma mark - MAKImageGalleryViewDataSource
- (NSInteger)numberOfImagesInGallery:(MAKImageGalleryView *)galleryView {
    if([[self.event.images allObjects] count] == 0 && [self.event.mapObjects allObjects] > 0){
        PKMapObj *mapObject = [[self.event.mapObjects allObjects] firstObject];
        return [[mapObject.images allObjects] count] + 1;
    } else if(self.event.mapObjects == nil){
        return 1;
    }
    return [[self.event.images allObjects] count] + 1;
}
- (void)loadImageInGallery:(MAKImageGalleryView *)galleryView atIndex:(NSInteger)index callback:(void(^)(UIImage *))callback{
    
    BOOL imageFromMapObject = NO;
    BOOL imagePreview = NO;
    NSString *imageName;
    if(index == 0){
        imagePreview = YES;
        if([self.event.previewImgName isEqualToString:@"no_image.jpg"]){
            PKMapObj *mapObject = [[self.event.mapObjects allObjects] firstObject];
            if(![mapObject.prevImageName isEqualToString:@"no_image.jpg"]){
                imageName = mapObject.prevImageName;
                imageFromMapObject = YES;
            }
        } else {
            imageName = self.event.previewImgName;
        }
    } else {
        PKObjImages *image;
        if([[self.event.images allObjects] count] > 0){
            image = [[self.event.images allObjects] objectAtIndex:index - 1];
            imageName = image.imageName;
            
        } else if([self.event.mapObjects allObjects] > 0){
            PKMapObj *mapObject = [[self.event.mapObjects allObjects] firstObject];
            image = [[mapObject.images allObjects] objectAtIndex:index - 1];
            imageName = image.imageName;
            imageFromMapObject = YES;
        }
        
    }
    
    
    NSString* imageUrl;
    NSString* dataPath;
    
    
    if(imageName != nil && ![imageName isEqualToString:@""] && ![imageName isEqualToString:@"no_image.jpg"]){
        if(imagePreview){
            if(imageFromMapObject){
                dataPath = LOCAL_mapObjImagePathPreview(imageName);
                imageUrl = URL_mapObjImagePathPreview(imageName);
            } else {
//                if(self.isTips){
//                    dataPath = LOCAL_informImagePathPreview(imageName);
//                    imageUrl = URL_informImagePathPreview(imageName);
//                } else {
                    dataPath = LOCAL_eventsImagePathPreview(imageName);
                    imageUrl = URL_eventsImagePathPreview(imageName);
//                }
            }
        } else {
            if(imageFromMapObject){
                dataPath = LOCAL_mapObjImagePathPhotogall(imageName);
                imageUrl = URL_mapObjImagePathPhotogall(imageName);
            } else {
//                if(self.isTips){
//                    dataPath = LOCAL_informImagePathPhotogall(imageName);
//                    imageUrl = URL_informImagePathPhotogall(imageName);
//                } else {
                    dataPath = LOCAL_eventsImagePathPhotogall(imageName);
                    imageUrl = URL_eventsImagePathPhotogall(imageName);
//                }
            }
        }
        
        
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
            callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
        } else {
            AFImageDownloader* download = [[AFImageDownloader alloc] init];
            [self.downloads addObject:download]; //СОХРАНИТЬ ОБЪЕКТ В ПАМЯТИ
            
            NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: imageUrl] cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                                 timeoutInterval:1];
            [download downloadImageForURLRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
                [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
                
                callback(image);
                
                
            } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
                [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
                // . NSLog(@"Error download image");
            }];
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    /*
     
     if([[self.informTip.images allObjects] count] == 0 || ([self.informTip.previewImgName isEqualToString:@"no_image.jpg"] && self.informTip.mapObjects != nil)){
     PKMapObj *mapObject = [[self.informTip.mapObjects allObjects] firstObject];
     if(index == 0){
     if(![mapObject.prevImageName isEqualToString:@"no_image.jpg"]){
     dataPath = LOCAL_mapObjImagePathPreview(mapObject.prevImageName);
     if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
     // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
     callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
     imageWasSet = YES;
     } else {
     // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
     imageUrl = URL_mapObjImagePathPreview(mapObject.prevImageName);
     }
     } else {
     callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
     imageWasSet = YES;
     }
     } else {
     
     PKObjImages *image = [[mapObject.images allObjects] objectAtIndex:index - 1];
     
     if(![image.imageName isEqualToString:@"no_image.jpg"]){
     
     NSString* dataPath = LOCAL_mapObjImagePathPhotogall(image.imageName);
     if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
     // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
     callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
     imageWasSet = YES;
     } else {
     // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
     imageUrl = URL_mapObjImagePathPhotogall(image.imageName);
     }
     } else {
     callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
     imageWasSet = YES;
     }
     }
     } else {
     if(index == 0){
     if(![self.informTip.previewImgName isEqualToString:@"no_image.jpg"]){
     NSString* dataPath;
     if(self.isTips){
     dataPath = LOCAL_informImagePathPreview(self.informTip.previewImgName);
     } else {
     dataPath = LOCAL_newsImagePathPreview(self.informTip.previewImgName);
     }
     
     if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
     // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
     callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
     imageWasSet = YES;
     } else {
     // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
     if(self.isTips){
     imageUrl = URL_informImagePathPreview(self.informTip.previewImgName);
     } else {
     imageUrl = URL_newsImagePathPreview(self.informTip.previewImgName);
     }
     }
     } else {
     callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
     imageWasSet = YES;
     }
     } else {
     
     PKObjImages *image = [[self.informTip.images allObjects] objectAtIndex:index - 1];
     
     if(![image.imageName isEqualToString:@"no_image.jpg"]){
     
     NSString* dataPath;
     if(self.isTips){
     dataPath = LOCAL_informImagePathPhotogall(image.imageName);
     } else {
     dataPath = LOCAL_newsImagePathPhotogall(image.imageName);
     }
     if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
     // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
     callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
     imageWasSet = YES;
     } else {
     // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
     if(self.isTips){
     imageUrl = URL_informImagePathPhotogall(image.imageName);
     } else {
     imageUrl = URL_newsImagePathPhotogall(image.imageName);
     }
     }
     } else {
     callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
     imageWasSet = YES;
     }
     }
     }
     
     if(!imageWasSet){
     AFImageDownloader* download = [[AFImageDownloader alloc] init];
     
     [self.downloads addObject:download]; //СОХРАНИТЬ ОБЪЕКТ В ПАМЯТИ
     
     NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: imageUrl] cachePolicy:NSURLRequestReturnCacheDataElseLoad
     timeoutInterval:1];
     [download downloadImageForURLRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
     [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
     
     callback(image);
     
     
     } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
     [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
     // . NSLog(@"Error download image");
     }];
     }
     */
    //    // . NSLog(@"imageUrl = %@", imageUrl);
    
}
- (UIViewContentMode)imageGallery:(MAKImageGalleryView *)galleryView contentModeForImageAtIndex:(NSInteger)index {
    return UIViewContentModeScaleAspectFill;
}


#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toMapFromEvent"]){
        PKMainMapViewController *vc = (PKMainMapViewController*)[segue destinationViewController];
        [vc setShowMapObjects:self.mapObjects];
    }
}










@end
