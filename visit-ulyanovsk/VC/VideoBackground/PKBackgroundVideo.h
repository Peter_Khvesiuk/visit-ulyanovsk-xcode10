//
//  BackgroundVideoObjC.h
//  BackgroundVideoDemo
//
//  Created by Adam Albarghouthi on 2016-06-26.
//  Copyright © 2016 backgroundVideo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@protocol senddataProtocol <NSObject>

-(void)videoIsStoped; 

@end

@interface PKBackgroundVideo : NSObject {
    NSURL *videoURL;
    UIViewController *viewController;
}
@property (nonatomic,assign) id delegate;
@property (strong, nonatomic) AVPlayer *backgroundPlayer;
@property (nonatomic) BOOL hasBeenUsed;
- (id)initOnViewController:(UIViewController *)onViewController withVideoURL:(NSString *)url;
- (void)setUpBackground;

- (void)pause;
- (void)play;

@end
