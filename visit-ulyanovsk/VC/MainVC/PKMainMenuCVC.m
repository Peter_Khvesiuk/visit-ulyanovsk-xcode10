//
//  PKMainMenuCVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMainMenuCVC.h"
#import "PKMainMenuCVCell.h"
#import "PKUOStyleKit.h"
//#import "PKMapObjectCollectionViewController.h"
#import "PKDataManager.h"
#import "AppDelegate.h"


//For segue
//#import "PKMainMapViewController.h"
//#import "PKTipsViewController.h"


@interface PKMainMenuCVC ()

@property (nonatomic, strong) NSTimer* clockTimer;
//@property (strong, nonatomic) NSOperation *imageLoadingOperation;

//@property (assign, nonatomic) BOOL repeat;
@end
@interface PKMainMenuCVCollectionView()

@end


@implementation PKMainMenuCVC
@synthesize menuApear;
@synthesize delegate;
static NSString * const reuseIdentifier = @"Cell1";

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
//        // . NSLog(@"PKMainMenuCVC size %f %f",self.view.frame.size.width, self.frame.size.height);
//        self.collectionViewLayout = [self createLayout];
//        [self initialize];
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.repeat = YES;
    self.menuApear = 0;
    self.view.backgroundColor = [UIColor clearColor];
    // . NSLog(@"viewDidLoad size %f %f",self.view.bounds.size.width, self.view.bounds.size.height);
    
    if([self.collectionView isKindOfClass:[PKMainMenuCVCollectionView class]]){
        // . NSLog(@"viewDidLoad collectionView size %f %f",self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    }
    
     self.collectionView.collectionViewLayout = [self createLayout];
                       
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    [self.collectionView registerClass:[PKMainMenuCVCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
}
-(void)beginShowingMenu{
    self.clockTimer = [NSTimer scheduledTimerWithTimeInterval:0.2f target:self selector:@selector(addMenuItem) userInfo:nil repeats:NO];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsZero;
}
- (UICollectionViewLayout *)createLayout {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    
    flowLayout.minimumInteritemSpacing = cvcMenuMargin;
    flowLayout.minimumLineSpacing = cvcMenuMargin;
//    if (isiPhone5s){
//        flowLayout.minimumInteritemSpacing = cvcMenuMarginiPhone5s;
//        flowLayout.minimumLineSpacing = cvcMenuMarginiPhone5s;
//    } else if(isiPad){
//        flowLayout.minimumInteritemSpacing = cvcMenuMarginiPad;
//        flowLayout.minimumLineSpacing = cvcMenuMarginiPad;
//    } else {
//        flowLayout.minimumInteritemSpacing = cvcMenuMarginDefault;
//        flowLayout.minimumLineSpacing = cvcMenuMarginDefault;
//    }
    
    //    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    // . NSLog(@"CVC size %f %f",self.collectionView.bounds.size.width, self.collectionView.bounds.size.height);
    // . NSLog(@"CVC frame size %f %f",self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    
    //    // . NSLog(@"superview size %f %f",[self superview].bounds.size.width, self.bounds.size.height);
    CGSize sisss = CGSizeMake(cvcMenuSize/3 - cvcMenuMargin, cvcMenuSize/3 - cvcMenuMargin);
//    if (isiPhone5s){
//        NSLog(@"isiPhone5s");
//        sisss =  CGSizeMake(cvcMenuSizeiPhone5s/3 - cvcMenuMarginiPhone5s,cvcMenuSizeiPhone5s/3 - cvcMenuMarginiPhone5s);
//    } else if (isiPad){
//        NSLog(@"isiPad");
//        sisss =  CGSizeMake(cvcMenuSizeiPad/3 - cvcMenuMarginiPad,cvcMenuSizeiPad/3 -cvcMenuMarginiPad);
//    } else {
//        NSLog(@"no isiPhone5s");
//        sisss =  CGSizeMake(cvcMenuSizeDefault/3 - cvcMenuMarginDefault,cvcMenuSizeDefault/3 - cvcMenuMarginDefault);
//    }
    
    //CGSizeMake(self.collectionView.bounds.size.width/3-5,self.collectionView.bounds.size.width/3-5);
    //self.bounds.size.width/2, self.bounds.size.height);
    flowLayout.itemSize = sisss;//self.bounds.size;
    flowLayout.sectionInset = UIEdgeInsetsZero;
    return flowLayout;
}
-(void)hideMenu{
    self.menuApear = 0;
    [self.collectionView reloadData];
}
- (void)instanceShowMenu {
    self.menuApear = self.maxMenuApear;
    [self.collectionView reloadData];
    [self.clockTimer invalidate];
}
- (void)addMenuItem {
    self.menuApear ++;
    [self.collectionView reloadData];
    NSString *pizdec = [NSString stringWithFormat:@"%ld", (long)self.menuApear];
    CGFloat corretteFloat = [pizdec floatValue];
    self.clockTimer = [NSTimer scheduledTimerWithTimeInterval:((corretteFloat / self.maxMenuApear) / 4) target:self selector:@selector(addMenuItem) userInfo:nil repeats:NO];
    if(self.menuApear >= self.maxMenuApear){
        [self.clockTimer invalidate];
        [((AppDelegate*)[UIApplication sharedApplication].delegate) registerUserNotification];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {

    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.menuApear;
}

- (PKMainMenuCVCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    PKMainMenuCVCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    PKMainMenuCVCell *cell = (PKMainMenuCVCell *)[collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    CGFloat marginLeft = 0.f;
    CGFloat cellSize = 80.f;
    if (isiPhone5s){
        cellSize = 70.f;
        marginLeft = (self.collectionView.frame.size.width - cellSize)/2;
        
    }
    CGRect cellFrame = CGRectMake(marginLeft, 0.f, cellSize, cellSize);
    
    if (!cell) {
        cell = [[PKMainMenuCVCell alloc] initWithFrame:cellFrame];
    }
//    [self configureCell:cell atIndexPath:indexPath];
    [self loadImageForRow:indexPath.row withOperationToCell:cell];
    // Configure the cell
    
    
    return cell;
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(PKMainMenuCVCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath{
//    [self addBlurLayerToCell:cell];
//}
- (void)loadImageForRow:(NSInteger)row withOperationToCell:(PKMainMenuCVCell *)cell {
//    self.imageLoadingOperation = [NSBlockOperation blockOperationWithBlock:^{
//
//        self.imageLoadingOperation = nil;
    
    CGFloat imageSizeWidth = cell.frame.size.width /5 * 4;
    CGSize imageSize = CGSizeMake(imageSizeWidth, imageSizeWidth);
    
    switch (row) {
        case 0:
            cell.imageView.image = [PKUOStyleKit imageOfIconMapWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"map");
            break;
        case 1:
            cell.imageView.image = [PKUOStyleKit imageOfIconDiscoverWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"discover");
            break;
            
        case 2:
            cell.imageView.image = [PKUOStyleKit imageOfIconToursWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"tours");
            break;
            
        case 3:
            cell.imageView.image = [PKUOStyleKit imageOfIconAskWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"Ask");
            break;
            
        case 4:
            cell.imageView.image = [PKUOStyleKit imageOfIconFavoriteWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"favorites");
            break;
            
        case 5:
            cell.imageView.image = [PKUOStyleKit imageOfIconTipsWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"Tips");
            break;
            
        case 6:
            cell.imageView.image = [PKUOStyleKit imageOfIconProfileWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"profile");
            break;
            
        case 7:
            cell.imageView.image = [PKUOStyleKit imageOfIconCalendarWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"calendar");
            break;
            
        case 8:
            cell.imageView.image = [PKUOStyleKit imageOfIconFootballWithSize:imageSize strokeWidth:cvcMenuStrokeWidth];
            cell.btnLabel.text = TRANSLATE_UP(@"football_2018");
            break;
            
        default:
            break;
    }
        
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    UINavigationController *navController = [mainVC navigationControllerForIndexPathInLeftMenu:initialIndexPath];
//    AMSlideMenuContentSegue *segue = [[AMSlideMenuContentSegue alloc] initWithIdentifier:@"ContentSugue" source:leftMenu destination:navController];
//    [segue perform];
    //mainSegueToDiscover
    // . NSLog(@"didSelectItemAtIndexPath %ld", (long)indexPath.row);
    NSString *segueId = [[NSString alloc] init];
    switch (indexPath.row) {
        case 0:
            segueId = @"mainSegueToMap";
//            [self performSegueWithIdentifier:@"mainSegueToMap" sender:self];
            break;
            
        case 1:
            segueId = @"mainSegueToDiscover";
//            [self performSegueWithIdentifier:@"mainSegueToDiscover" sender:self];
            break;
            
        case 2:
            segueId = @"mainSegueToRoute";
//            [self performSegueWithIdentifier:@"mainSegueToRoute" sender:self];
            break;
            
        case 3:
            segueId = @"mainSegueToAsk";
//            [self performSegueWithIdentifier:@"mainSegueToAsk" sender:self];
            break;
            
        case 4:
            segueId = @"mainSegueToDiscoverFavorite";
//            [self performSegueWithIdentifier:@"mainSegueToDiscoverFavorite" sender:self];
            break;
            
        case 5:
            segueId = @"mainSegueToTips";
//            [self performSegueWithIdentifier:@"mainSegueToTips" sender:self];
            break;
            
        case 6:
            segueId = @"mainSegueToProfile";
//            [self performSegueWithIdentifier:@"mainSegueToProfile" sender:self];
            break;
            
        case 7:
            segueId = @"mainSegueToEvent";
//            [self performSegueWithIdentifier:@"mainSegueToDiscoverFavorite" sender:self];
            break;
            
        case 8:
            segueId = @"mainSegueToFifa2018";
//            [self performSegueWithIdentifier:@"mainSegueToCalendar" sender:self];
            break;
            
        default:
            segueId = @"mainSegueToDiscoverFavorite";
//            [self performSegueWithIdentifier:@"mainSegueToDiscoverFavorite" sender:self];
            break;
    }
    
    [delegate sendSegueToMain:segueId];
    
}

/*
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    // . NSLog(@"prepareForSegue %@", segue.identifier);
    
    if([segue.identifier isEqualToString:@"mainSegueToMap"]){
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
        
//        NSArray *tabBarControllers = [((UITabBarController*)segue.destinationViewController) viewControllers];
//        for (id navControl in tabBarControllers) {
//            
//            //        id navControl = [tabBarControllers objectAtIndex:1];
//            if([navControl isKindOfClass:[UINavigationController class]]){
//                // . NSLog(@"PKMapObjectCollectionViewController");
//                NSArray *viewContrlls=[navControl viewControllers];
//                for( int i=0;i<[ viewContrlls count];i++){
//                    id cvc=[viewContrlls objectAtIndex:i];
//                    // . NSLog(@"viewContrlls = %@", [cvc class]);
//                    if([cvc isKindOfClass:[PKMainMapViewController class]]){
//                        PKMainMapViewController* mapViewController = cvc;
//                        mapViewController.uploadMap = YES;
//                    }
//                }
//                
//            }
//        }
        
        
    } else if([segue.identifier isEqualToString:@"mainSegueToDiscover"]){
        // . NSLog(@"segue.destinationViewController = %@", segue.destinationViewController);
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:1];
    } else if([segue.identifier isEqualToString:@"mainSegueToDiscoverFavorite"]){
        // . NSLog(@"segue.destinationViewController = %@", segue.destinationViewController);
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
        NSArray *tabBarControllers = [((UITabBarController*)segue.destinationViewController) viewControllers];
        int indexPredicate = 1;
        for (id navControl in tabBarControllers) {
            
            //        id navControl = [tabBarControllers objectAtIndex:1];
            if([navControl isKindOfClass:[UINavigationController class]]){
                // . NSLog(@"PKMapObjectCollectionViewController");
                NSArray *viewContrlls=[navControl viewControllers];
                for( int i=0;i<[ viewContrlls count];i++){
                    id cvc=[viewContrlls objectAtIndex:i];
                    // . NSLog(@"viewContrlls = %@", [cvc class]);
                    if([cvc isKindOfClass:[PKMapObjectCollectionViewController class]]){
                        PKMapObjectCollectionViewController* collectionViewController = cvc;
                        collectionViewController.contentRecomended = YES;
                        collectionViewController.predicateRecomended = indexPredicate;
                        indexPredicate++;
                    }
                }
                
            }
        }

    } else if([segue.identifier isEqualToString:@"mainSegueToRoute"]){
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:2];
    } else if([segue.identifier isEqualToString:@"mainSegueToFavorite"]){
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
    } else if([segue.identifier isEqualToString:@"mainSegueToProfile"]){
        // . NSLog(@"mainSegueToProfile");
    } else if([segue.identifier isEqualToString:@"mainSegueToTips"]){
        
        UINavigationController *navControl = (UINavigationController*)[segue destinationViewController];
        
        PKTipsViewController *vc = (PKTipsViewController*)[[navControl viewControllers] firstObject];
        vc.showTips = YES;
        
    } else if([segue.identifier isEqualToString:@"mainSegueToCalendar"]){
        
        UINavigationController *navControl = (UINavigationController*)[segue destinationViewController];
        
        PKTipsViewController *vc = (PKTipsViewController*)[[navControl viewControllers] firstObject];
        vc.showTips = NO;
        
    }
    
    
}
*/
//-(void)performSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
//
//}
#pragma mark <UICollectionViewDelegate>

@end
@implementation PKMainMenuCVCollectionView



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame collectionViewLayout:[self createLayout]];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        self.collectionViewLayout = [self createLayout];
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
//    self.dataSource = self;
//    self.delegate = self;
    self.pagingEnabled = NO;
//    self.allowsSelection = NO;
    //    [self registerClass:[PKObjectInfoCVCell class] forCellWithReuseIdentifier:kImageCellReusableId];
    self.clipsToBounds = NO;
    self.showsHorizontalScrollIndicator = NO;
    self.showsVerticalScrollIndicator = NO;
}

- (void)setBounds:(CGRect)bounds {
    CGRect previous = self.bounds;
    [super setBounds:bounds];
    
    CGSize sisss = CGSizeMake(self.bounds.size.width/3, self.bounds.size.height/3);
//    // . NSLog(@"self.bounds.size.width/3 = %f", self.bounds.size.height/3);
    // . NSLog(@"self.bounds.size.width = %f", self.bounds.size.width);
    // . NSLog(@"self.bounds.size.height = %f", self.bounds.size.height);
    if (!CGSizeEqualToSize(previous.size, bounds.size)) {
        // . NSLog(@"CGSizeEqualToSize");
        //        [(UICollectionViewFlowLayout *)self.collectionViewLayout setItemSize:self.bounds.size];
        [(UICollectionViewFlowLayout *)self.collectionViewLayout setItemSize:sisss];
        
    }
}

#pragma mark - Private
- (UICollectionViewLayout *)createLayout {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 0;
    //    flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    // . NSLog(@"size %f %f",self.bounds.size.width, self.bounds.size.height);
    // . NSLog(@"frame size %f %f",self.frame.size.width, self.frame.size.height);
    
//    // . NSLog(@"superview size %f %f",[self superview].bounds.size.width, self.bounds.size.height);
    
    CGSize sisss = CGSizeMake(self.bounds.size.width/3,self.bounds.size.height/3);//self.bounds.size.width/2, self.bounds.size.height);
    flowLayout.itemSize = sisss;//self.bounds.size;
    flowLayout.sectionInset = UIEdgeInsetsZero;
    return flowLayout;
}

@end
