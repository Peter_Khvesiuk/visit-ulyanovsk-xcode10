//
//  PKMainMenuCVCell.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMainMenuCVCell.h"
#import "PKUOStyleKit.h"
#import <QuartzCore/QuartzCore.h>
//@interface PKMainMenuCVCell ()
//
//@property (strong, nonatomic) UIImageView *imageView;
//@property (strong, nonatomic) UILabel *btnLabel;
//
//@end




@implementation PKMainMenuCVCell


//- (void)awakeFromNib {
//    [super awakeFromNib];
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];//];
//    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    blurEffectView.frame = self.bounds;
//    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//    [self insertSubview:blurEffectView atIndex:0];
//    self.backgroundColor = [UIColor clearColor];
//    self.imageView.backgroundColor = [UIColor clearColor];
//    self.btnLabel.textColor = [UIColor whiteColor];
//    self.btnLabel.font = [UIFont systemFontOfSize:14];
//    self.layer.cornerRadius = 10.f;
////    [UIView animateWithDuration:0.5
////                          delay:0
////                        options:UIViewAnimationOptionCurveEaseInOut /*| UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse*/
////                     animations:^{
////
////                         self.hiddenBtn.alpha = 1;
////                         self.hiddenWorkHours1.alpha = 1;
////                         self.hiddenWorkHours2.alpha = 1;
////                         self.hiddenWorkHours3.alpha = 1;
////
////                     }
////                     completion:^(BOOL finished) {
////                         // . NSLog(@"animation finished! %d", finished);
////                         //                         // . NSLog(@"\nview frame = %@\nview bounds = %@", NSStringFromCGRect(view.frame), NSStringFromCGRect(view.bounds));
////
////
////                         // __weak UIView* v = view;
////                         // [self moveView:v];
////
////                     }];
////    self.layer.cornerRadius = 5.0f;
////    self.layer.masksToBounds = YES;
////
////    self.layer.borderColor = [UIColor whiteColor].CGColor;
////    self.layer.borderWidth = 1.0f;
////    [self addBlurLayerToCell];
//
//}


//-(void)addBlurLayerToCell{
//    dispatch_async(dispatch_get_main_queue(), ^{
//    
//        });
//}
-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
//        self.frame = frame;
        
        

        CGFloat imageSize = self.contentView.frame.size.width/5 * 4;
        CGFloat marginLeft = (self.contentView.frame.size.width - imageSize)/2;
        CGFloat fontSize = cvcFontSize;
//        if (isiPhone5s){
//            cellSize = 70.f;
//            fontSize = 13.f;
//            marginLeft = (self.contentView.frame.size.width - cellSize)/2;
//        }
//        NSLog(@"marginLeft = %f", marginLeft);
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(marginLeft, 0, imageSize, imageSize)];
        imageView.backgroundColor = [UIColor clearColor];
//        imageView.image = image;
        self.imageView = imageView;
        [self addSubview:self.imageView];
        
        UILabel *btnLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.contentView.frame.size.height - cvcFontSize *1.9f, self.contentView.frame.size.width, cvcFontSize *1.9f)];
        btnLabel.textColor = [UIColor whiteColor];
        btnLabel.font = [UIFont systemFontOfSize:fontSize];
        btnLabel.textAlignment = NSTextAlignmentCenter;
        
//        btnLabel.shadowColor = [UIColor blackColor];
//        btnLabel.shadowOffset = CGSizeMake(1.0f, 1.0f);
        
        
        btnLabel.layer.masksToBounds = NO;
//        CALayer *textLayer = ((CALayer *)[btnLabel.layer.sublayers objectAtIndex:0]);
//        textLayer.shadowColor = [UIColor blackColor].CGColor;
//        textLayer.shadowOffset = CGSizeMake(1.0f, 1.0f);
//        textLayer.shadowOpacity = 1.0f;
//        textLayer.shadowRadius = 6.0f;
        
        btnLabel.layer.shadowOffset = CGSizeMake(0, 0);
        btnLabel.layer.shadowColor = [[UIColor blackColor] CGColor];
        btnLabel.layer.shadowRadius = 5;
        btnLabel.layer.shadowOpacity = .35f;
        
        
        
        
        self.btnLabel = btnLabel;
        [self addSubview:self.btnLabel];
        
        self.backgroundColor = [UIColor clearColor];
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 10.f;
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];//];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self insertSubview:blurEffectView atIndex:0];
    }
    return self;
}
//- (void)layoutSubviews {
//    [super layoutSubviews];
////#warning layout need some help
//    if (isiPhone5s){
//        CGFloat marginLeft = (self.contentView.frame.size.width - 70.f)/2;
//        self.imageView.frame = CGRectMake(marginLeft, 0.f, 70.f, 70.f);
//    }
////    else {
////        CGFloat marginLeft = (self.contentView.frame.size.width - 80.f)/2;
////        self.imageView.frame = CGRectMake(marginLeft, 0.f, 80.f, 80.f);
////    }
//
//}
@end
