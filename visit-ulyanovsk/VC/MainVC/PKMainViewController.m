//
//  PKMainViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMainViewController.h"
#import "PKMainMenuCVC.h"
#import "PKMemCenterView.h"
#import <CoreMotion/CoreMotion.h>
#import "PKUOStyleKit.h"
//#import "PKInterfaceManager.h"
#import "PKServerManager.h"
#import "PKDataManager.h"
#import "PKMapObjectCollectionViewController.h"
#import "PKTipsViewController.h"
#import "PKEventDetailVC.h"
#import "PKEventViewController.h"
#import "PKProfileViewController.h"
#import "PKMainMapViewController.h"

#import <Mapbox/Mapbox.h>

#import "PKMapObjectDetailVC.h"
#import "PKGeoRouteDetailVC.h"
#import "PKInformTipsDetailVC.h"

//#define kCMDeviceMotionUpdateFrequency (1.f/20.f)

@interface PKMainViewController () <sendSegueToMain>

//@property (nonatomic, strong) CMMotionManager *motionManager;
//@property (nonatomic, strong) CADisplayLink *displayLink;
@property (nonatomic, strong) UIView *transparentView;
@property (strong, nonatomic) PKMemCenterView *memview;
@property (strong, nonatomic) UIImageView *skyImageView;
@property (nonatomic) CGFloat loadingProgressBar;
@property (nonatomic, strong) NSTimer* clockTimerTemp;

@property (nonatomic, strong) PKMainMenuCVC* menuCVC;
@property (nonatomic, strong) UIImageView *logo;
@property (nonatomic) BOOL displayLinkWasEnabled;
@property (nonatomic) BOOL animationShown;
@property (nonatomic, strong) NSString *localizedProgressString;
@property (nonatomic, strong) NSString *localizedProgressMapString;
@property (nonatomic) CGFloat memviewMarginY;
//@property (nonatomic, strong) NSString *alertTitle;
//@property (nonatomic, strong) NSString *alertMessage;
//@property (nonatomic, strong) NSString *alertYesBtnText;
//@property (nonatomic, strong) NSString *alertNoBtnText;
@property (nonatomic) BOOL isAskedToDownload;
@property (nonatomic) BOOL isFirstTimeJsonLoaded;
@property (nonatomic) BOOL isOfflineMode;

@property (nonatomic) BOOL mapWasObservered;
@property (nonatomic, strong) NSDictionary *zipFiles;
@property (nonatomic) NSUInteger zipFilesIndex;

@property (nonatomic) UIProgressView *progressView;


@end

@implementation PKMainViewController

@synthesize needOfflineContent;

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if(self.needOfflineContent){
         [self downLoadAfterCabinet];
    } else {
        [self leninskMemApear];//
    }

    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
    
    
#warning delete video here
//    if([[PKDataManager sharedManager] isSkipVideo]){
//        [[PKServerManager sharedManager] deleteVideo];
//    }
    
    
    [[PKServerManager sharedManager] clearTmpDirectory];
    
    [super viewDidLoad];
    
    [[[[UIApplication sharedApplication] delegate] window] setTintColor:[PKUOStyleKit bgColorGradientTop]];
    
    
    self.isAskedToDownload = [[PKDataManager sharedManager] isAskedToDownload];
    self.isFirstTimeJsonLoaded = [[PKDataManager sharedManager] isFirstTimeJsonLoaded];
    self.isOfflineMode = [[PKDataManager sharedManager] isUseOfflineMode];
    
    self.memviewMarginY = 100.f;
    PKMemCenterView *memview = [[PKMemCenterView alloc] initWithFrame:CGRectMake(0.f, self.memviewMarginY, self.view.frame.size.width, self.view.frame.size.height)];
    memview.backgroundColor = [UIColor clearColor];
    self.memview = memview;
    [self.view addSubview:self.memview];
    
    UIImageView *skyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(- 200.f + 0, - 100.f + 0, self.view.frame.size.width+200.f, self.view.frame.size.height-70.f+100.f)];
    
    [skyImageView setImage:[UIImage imageNamed:@"memBackground.jpg"]];
    skyImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.skyImageView = skyImageView;
    [self.view insertSubview:skyImageView atIndex:0];
    
    UIImageView *logo = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.frame.size.width - cvcMenuSize) / 2, (((cvcMenuSize)* 60 )/ 291) / 2, cvcMenuSize, ((cvcMenuSize)* 60 )/ 291)];
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PKMainMenuCVC *menuCVC= [storyBoard instantiateViewControllerWithIdentifier:@"MainMenuCVC"];
    
    CGFloat cvcMarginTop = (((cvcMenuSize)* 60 )/ 291)*2 ;

    CGRect menuCVCFrame = CGRectMake((self.view.frame.size.width - cvcMenuSize) / 2, cvcMarginTop, cvcMenuSize, cvcMenuSize);
    
    self.menuCVC = menuCVC;
    self.menuCVC.maxMenuApear = PKMaxMenuItemsCount;
    self.menuCVC.delegate = self;
    
    [self addChildViewController:self.menuCVC];
    self.menuCVC.view.frame = menuCVCFrame;
    [self.view addSubview:self.menuCVC.view];
    [self.menuCVC didMoveToParentViewController:self];
    
    
    self.localizedProgressString = ALERT_OFFLINE_localizedProgressString;
    self.localizedProgressMapString = ALERT_OFFLINE_localizedProgressMapString;
    
    [logo setImage:[PKUOStyleKit imageOfPKLogoWithSize:logo.frame.size isRussian:isLOCAL_RU]];
    self.logo = logo;
    [self.view addSubview:self.logo];
    
    
    self.memview.frame = CGRectMake(0.f, 0.f, self.memview.frame.size.width, self.memview.frame.size.height);
    
    if(self.instanceView){
        self.animationShown = YES;
        self.loadingProgressBar = 1.f;
        self.memview.loadingProgressBar = 1.f;
        [self.menuCVC instanceShowMenu];
        self.logo.alpha = 1;
//        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        self.memview.angle = 0.7f;
        self.skyImageView.frame = CGRectMake(- 200.f + 0, - 100.f + 0, self.view.frame.size.width+200.f, self.view.frame.size.height-70.f+100.f);
        if(self.needToShowContentAfterPush){
            [self showControllerAfterPush];
        }
    } else {
        [self initialProgressBar];
    }
}
-(void)initialProgressBar{
    self.loadingProgressBar = 0.f;
    self.animationShown = NO;
    self.logo.alpha = 0;
    [self.menuCVC hideMenu];
}
-(void)downLoadAfterCabinet{
    [self loadOflineZipsInit];
}

-(void)needToDownloadData{
    self.needOfflineContent = YES;
    [self initialProgressBar];
}
-(void)chooseVersionOnlineOrOfline{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle: ALERT_OFFLINE_Title
                                 message: ALERT_OFFLINE_Message
                                 preferredStyle:UIAlertControllerStyleAlert]; 
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle: ALERT_OFFLINE_YesBtnText
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    [self loadOflineZipsInit];
//                                    [[PKDataManager sharedManager] setUseOfflineMode:YES];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle: ALERT_OFFLINE_NoBtnText
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   [[PKDataManager sharedManager] setAskedToDownload:YES];
                                   [[PKDataManager sharedManager] setUseOfflineMode:NO];
                                   [self loadDefaultUsageInit];
                                   //Handle no, thanks button
                               }];

    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)leninskMemApear{
    if(!self.animationShown){
        self.memviewMarginY = 100.f;
        self.skyImageView.frame = CGRectMake(- 200.f, 0, self.view.frame.size.width+200.f, self.view.frame.size.height-70.f+100.f);
        self.memview.frame = CGRectMake(0.f, self.memviewMarginY, self.memview.frame.size.width, self.memview.frame.size.height);
        self.memview.angle = 0.f;
        [self manualAnimateLeninAngleAndFrame];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)loadDefaultUsage{
//    int pepcentOfTotal = 1;
//    NSLog(@"loadDefaultUsage");
    CGFloat prewProgress = _loadingProgressBar;
    if(prewProgress == 0.f){
//        NSLog(@"\nloadDefaultUsage progress == 0 %f", prewProgress);
        [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(BOOL isComplete) {
            if(isComplete){
                [[PKDataManager sharedManager] setAskedToDownload:YES];
            }
        } inProgress:^(CGFloat progress) {
//            // . NSLog(@"\nloadDefaultUsage progress == %f", (progress / 100) * pepcentOfTotal);
//            [self addPercentLoading: prewProgress + (progress / 100) * pepcentOfTotal];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            // . NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
        }];
    } else {
//        NSLog(@"\nloadDefaultUsage progress != 0 %f", prewProgress);
        [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(BOOL isComplete) {
            if(isComplete){
                [[PKDataManager sharedManager] setAskedToDownload:YES];
                self->_loadingProgressBar = 1.f;
            }
        } inProgress:^(CGFloat progress) {
//            // . NSLog(@"\nloadDefaultUsage progress == %f", (progress / 100) * 1);
            [self addPercentLoading: prewProgress + (progress / 100) * 1];
        } onFailure:^(NSError *error, NSInteger statusCode) {
            // . NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
        }];
    }
    
    
}
-(void)stopProgressAnimateLoader{
    _memview.loadingInProgress = NO;
}
-(void)switchToMapProgressAnimateLoader{
    _memview.localizedProgress = self.localizedProgressMapString;
}
-(void)runProgressAnimateLoader{
    _memview.localizedProgress = self.localizedProgressString;
    
     [self progressAnimateLoader];
}
-(void)loadDefaultUsageInit{
//    _memview.loadingInProgress = YES;
//    [self runProgressAnimateLoader];
    [self loadDefaultUsage];
    [self manualAnimateLoader];
}

-(void)loadOflineZipsInit{
    
    NSArray *zipfileNames = [[NSArray alloc] initWithObjects: @"inform.zip", @"events.zip", @"georoute.zip", @"mapobj.zip", nil];
    NSArray *zipfileSizes = [[NSArray alloc] initWithObjects:@(1), @(1), @(1), @(43), nil];
    
    NSMutableDictionary* zipfiles = [[NSMutableDictionary alloc] init];
    [zipfiles setValue:zipfileNames forKey:@"zipfileName"];
    [zipfiles setValue:zipfileSizes forKey:@"zipfileSize"];
    self.zipFiles = zipfiles;
    self.zipFilesIndex = 0;
    _memview.loadingInProgress = YES;
    [self runProgressAnimateLoader];
    [self loadOflineZips];
}
-(void)loadOflineZips{
    CGFloat prewProgress = _loadingProgressBar;
    if(self.zipFilesIndex <= [[self.zipFiles objectForKey:@"zipfileName"] count] - 1){
        NSLog(@"loadOflineZips %@", [[self.zipFiles objectForKey:@"zipfileName"] objectAtIndex:self.zipFilesIndex]);
        NSString *zipfileName = [[self.zipFiles objectForKey:@"zipfileName"] objectAtIndex:self.zipFilesIndex];
        NSInteger zipfileSize = [(NSNumber*)[[self.zipFiles objectForKey:@"zipfileSize"] objectAtIndex:self.zipFilesIndex] intValue];
        [[PKServerManager sharedManager]
         getZipDataWithName: zipfileName
         fromServerOnSuccess:^(BOOL isComplete) {
             if(isComplete){
                 
                 self->_loadingProgressBar = prewProgress + (1.f/100) * zipfileSize;
                 NSLog(@"_loadingProgressBar %lu complete %f", (unsigned long)self.zipFilesIndex, self.loadingProgressBar);
                 self.zipFilesIndex++;
                 [self loadOflineZips];
             }
             
         } inProgress:^(CGFloat progress) {
             NSLog(@"progress == %f", progress);
             [self addPercentLoading: prewProgress + (progress/100) * zipfileSize];
             NSLog(@"_loadingProgressBar %lu == %f", (unsigned long)self.zipFilesIndex, self.loadingProgressBar);
         } onFailure:^(NSError *error, NSInteger statusCode) {
             [self loadOflineZips];
             NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
         }];
    } else {
        
        self.zipFilesIndex = 0;
        [self unZipFiles];
    }
}


-(void)unZipFiles{
    CGFloat prewProgress = _loadingProgressBar;
    if(self.zipFilesIndex <= [[self.zipFiles objectForKey:@"zipfileName"] count] - 1){
        NSLog(@"unZipFiles %@", [[self.zipFiles objectForKey:@"zipfileName"] objectAtIndex:self.zipFilesIndex]);
        NSString *zipfileName = [[self.zipFiles objectForKey:@"zipfileName"] objectAtIndex:self.zipFilesIndex];
        NSInteger zipfileSize = [(NSNumber*)[[self.zipFiles objectForKey:@"zipfileSize"] firstObject] intValue];
        

        [[PKServerManager sharedManager] unzip:zipfileName
                                    inProgress:^(CGFloat progress) {
                                        NSLog(@"progress == %f", progress);
                                        NSLog(@"unZipFiles %lu complete %f", (unsigned long)self.zipFilesIndex, self.loadingProgressBar);
                                        
                                        [self addPercentLoading: prewProgress + (progress/100) * zipfileSize];
                                    }
                                    onComplete:^(BOOL isComplete) {
                                        if(isComplete){
                                            NSLog(@"_loadingProgressBar p   = %f", self->_loadingProgressBar);
                                            self->_loadingProgressBar = prewProgress + (1.f/100) * zipfileSize;
                                            NSLog(@"_loadingProgressBar p 2 = %f", self->_loadingProgressBar);
                                            self.zipFilesIndex++;
                                            [self unZipFiles];
//                                            // . NSLog(@"unzip isComplete");
//                                            success(isComplete);
                                            
                                            
                                        }
            
                                        
                                    }];
        
    } else {
        [[PKServerManager sharedManager] clearTmpDirectory];
        [self switchToMapProgressAnimateLoader];
        [self mapUpload];
    }
}

- (void)dealloc {
   
    // . NSLog(@"\ndealloc PKMainViewController");
     // Remove offline pack observers.
    if(self.mapWasObservered){
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
    
}


#pragma mark LOAD MAPBOX OFFLINE
-(void)mapUpload{
    NSLog(@"mapUpload");
    self.mapWasObservered = YES;
    // . NSLog(@"\nmap scenario uploadMap == YES");
    // Setup offline pack notification handlers.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offlinePackProgressDidChange:) name:MGLOfflinePackProgressChangedNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offlinePackDidReceiveError:) name:MGLOfflinePackErrorNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(offlinePackDidReceiveMaximumAllowedMapboxTiles:) name:MGLOfflinePackMaximumMapboxTilesReachedNotification object:nil];
    [self startOfflinePackDownloadGlobal];
}
- (void)startOfflinePackDownloadGlobal {
    //    [self deleteOfflinePackDownload];
    //
    CLLocationCoordinate2D sw3 = CLLocationCoordinate2DMake(52.98044, 44.59736);
    CLLocationCoordinate2D ne3 = CLLocationCoordinate2DMake(55.98172, 50.50465);
    MGLCoordinateBounds saranskKazanSamaraWithZoomFrom1to10 = MGLCoordinateBoundsMake(sw3, ne3);
    id <MGLOfflineRegion> region = [[MGLTilePyramidOfflineRegion alloc] initWithStyleURL:[NSURL URLWithString:mapStyleDefault] bounds:saranskKazanSamaraWithZoomFrom1to10 fromZoomLevel:5 toZoomLevel:9];
    // Store some data for identification purposes alongside the downloaded resources.
    NSDictionary *userInfo = @{ @"name": @"GlobalUlsk" };
    NSData *context = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    
    // Create and register an offline pack with the shared offline storage object.
    [[MGLOfflineStorage sharedOfflineStorage] addPackForRegion:region withContext:context completionHandler:^(MGLOfflinePack *pack, NSError *error) {
        if (error != nil) {
            // The pack couldn’t be created for some reason.
            NSLog(@"Error: %@", error.localizedFailureReason);
        } else {
            // Start downloading.
            [pack resume];
        }
    }];
}
- (void)startOfflinePackDownloadPraviyBereg {
    CLLocationCoordinate2D sw = CLLocationCoordinate2DMake(54.24208, 48.26315);
    CLLocationCoordinate2D ne = CLLocationCoordinate2DMake(54.36068, 48.41867);
    MGLCoordinateBounds boundsPraviyBereg = MGLCoordinateBoundsMake(sw, ne);
    id <MGLOfflineRegion> region = [[MGLTilePyramidOfflineRegion alloc] initWithStyleURL:[NSURL URLWithString:mapStyleDefault] bounds:boundsPraviyBereg fromZoomLevel:10 toZoomLevel:16];
    
    // Store some data for identification purposes alongside the downloaded resources.
    NSDictionary *userInfo = @{ @"name": @"PraviyBereg" };
    NSData *context = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    
    // Create and register an offline pack with the shared offline storage object.
    [[MGLOfflineStorage sharedOfflineStorage] addPackForRegion:region withContext:context completionHandler:^(MGLOfflinePack *pack, NSError *error) {
        if (error != nil) {
            // The pack couldn’t be created for some reason.
            NSLog(@"Error: %@", error.localizedFailureReason);
        } else {
            // Start downloading.
            [pack resume];
        }
    }];
}
- (void)startOfflinePackDownloadLeviyBereg {
    CLLocationCoordinate2D sw2 = CLLocationCoordinate2DMake(54.31884, 48.46314);
    CLLocationCoordinate2D ne2 = CLLocationCoordinate2DMake(54.39095, 48.62378);
    MGLCoordinateBounds boundsLeviyBereg = MGLCoordinateBoundsMake(sw2, ne2);
    id <MGLOfflineRegion> region = [[MGLTilePyramidOfflineRegion alloc] initWithStyleURL:[NSURL URLWithString:mapStyleDefault] bounds:boundsLeviyBereg fromZoomLevel:10 toZoomLevel:15];
    
    // Store some data for identification purposes alongside the downloaded resources.
    NSDictionary *userInfo = @{ @"name": @"LeviyBereg" };
    NSData *context = [NSKeyedArchiver archivedDataWithRootObject:userInfo];
    
    // Create and register an offline pack with the shared offline storage object.
    [[MGLOfflineStorage sharedOfflineStorage] addPackForRegion:region withContext:context completionHandler:^(MGLOfflinePack *pack, NSError *error) {
        if (error != nil) {
            // The pack couldn’t be created for some reason.
            NSLog(@"Error: %@", error.localizedFailureReason);
        } else {
            // Start downloading.
            [pack resume];
        }
    }];
}
- (void)offlinePackProgressDidChange:(NSNotification *)notification {
    // . NSLog(@"coo delete 1 = %lu", (unsigned long)[[[MGLOfflineStorage sharedOfflineStorage] packs] count]);
    MGLOfflinePack *pack = notification.object;
    
    // Get the associated user info for the pack; in this case, `name = My Offline Pack`
    NSDictionary *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:pack.context];
    
    MGLOfflinePackProgress progress = pack.progress;
    // or [notification.userInfo[MGLOfflinePackProgressUserInfoKey] MGLOfflinePackProgressValue]
    uint64_t completedResources = progress.countOfResourcesCompleted;
    uint64_t expectedResources = progress.countOfResourcesExpected;
    
    // Calculate current progress percentage.
    float progressPercentage = (float)completedResources / expectedResources;
    
    // Setup the progress bar.
//    if (!self.progressView) {
//        self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];
//        CGSize frame = self.view.bounds.size;
//        self.progressView.frame = CGRectMake(frame.width / 4, frame.height * 0.75, frame.width / 2, 10);
//        [self.view addSubview:self.progressView];
//    }
//
//    [self.progressView setProgress:progressPercentage animated:YES];
    
    // If this pack has finished, print its size and resource count.
    if (completedResources == expectedResources) {
//        NSString *byteCount = [NSByteCountFormatter stringFromByteCount:progress.countOfBytesCompleted countStyle:NSByteCountFormatterCountStyleMemory];
        // . NSLog(@"coo delete 2 = %lu", (unsigned long)[[[MGLOfflineStorage sharedOfflineStorage] packs] count]);
//        [self deleteOfflinePackDownload];
        // . NSLog(@"Offline pack “%@” completed: %@, %llu resources", userInfo[@"name"], byteCount, completedResources);
        if([userInfo[@"name"] isEqualToString:@"GlobalUlsk"]){
            [self startOfflinePackDownloadPraviyBereg];
        } else if([userInfo[@"name"] isEqualToString:@"PraviyBereg"]){
            [self startOfflinePackDownloadLeviyBereg];
        } else {
            [self finishLoading];
        }
    } else {

        
        if([userInfo[@"name"] isEqualToString:@"GlobalUlsk"]){
            
            [self addPercentLoading: .5f + (progressPercentage/100) * 16];
        } else if([userInfo[@"name"] isEqualToString:@"PraviyBereg"]){
            [self addPercentLoading: .66f + (progressPercentage/100) * 16];
        } else {
            [self addPercentLoading: .82f + (progressPercentage/100) * 16];
        }
        
        
        // Otherwise, print download/verification progress.
        // . NSLog(@"Offline pack “%@” has %llu of %llu resources — %.2f%%.", userInfo[@"name"], completedResources, expectedResources, progressPercentage * 100);
    }
}
-(void)finishLoading{
    [self loadDefaultUsage];
}
/*
- (void)offlinePackDidReceiveError:(NSNotification *)notification {
    MGLOfflinePack *pack = notification.object;
    NSDictionary *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:pack.context];
    NSError *error = notification.userInfo[MGLOfflinePackUserInfoKeyError];
    // . NSLog(@"Offline pack “%@” received error: %@", userInfo[@"name"], error.localizedFailureReason);
}

- (void)offlinePackDidReceiveMaximumAllowedMapboxTiles:(NSNotification *)notification {
    MGLOfflinePack *pack = notification.object;
    NSDictionary *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:pack.context];
    uint64_t maximumCount = [notification.userInfo[MGLOfflinePackUserInfoKeyMaximumCount] unsignedLongLongValue];
    // . NSLog(@"Offline pack “%@” reached limit of %llu tiles.", userInfo[@"name"], maximumCount);
}
*/

- (void)addPercentLoading:(CGFloat)newProgress {
    // . NSLog(@"\nnewProgress == %f", newProgress);
    _loadingProgressBar = newProgress;
}
- (void)progressAnimateLoader {
    
//    if(self.loadingProgressBar < 1.f){
//        // . NSLog(@"\nprogressAnimateLoader jjj < %f", self.loadingProgressBar);
//    } else if(self.loadingProgressBar > 1.f){
//        // . NSLog(@"\nprogressAnimateLoader jjj > %f", self.loadingProgressBar);
//    } else if(self.loadingProgressBar == 1.f){
//        // . NSLog(@"\nprogressAnimateLoader jjj = %f", self.loadingProgressBar);
//    } else {
//        // . NSLog(@"\nprogressAnimateLoader jjj ! %f", self.loadingProgressBar);
//    }
    if(self.loadingProgressBar >= 1.f){
        [self stopProgressAnimateLoader];
        [self.memview setNeedsDisplay];
        [self.clockTimerTemp invalidate];
        [self showLogo];
        
    } else {
        _memview.loadingProgressBar = self.loadingProgressBar;
        _memview.progressPercent = [NSString stringWithFormat:@"%1.f%@", (_loadingProgressBar * 100),@" %"];
        [self.memview setNeedsDisplay];
        self.clockTimerTemp = [NSTimer scheduledTimerWithTimeInterval: 0.02f target:self selector:@selector(progressAnimateLoader) userInfo:nil repeats:NO];
    }
}
- (void)manualAnimateLeninAngleAndFrame {
    _memviewMarginY = 100.f - (self.memview.angle + 0.2f)*100.f;
    // . NSLog(@"self.memview.angle = %f _memviewMarginY = %f   %f",self.memview.angle, _memviewMarginY, (self.memview.angle + 0.2f)*100.f);
//    self.memview.frame = CGRectMake(0.f, 100.f, self.memview.frame.size.width, self.memview.frame.size.height);
    CGFloat newVarible = self.memview.angle + 0.01;
    
    if(newVarible >= 0.8f){//exit here
        _memviewMarginY = 0.f;
        self.memview.angle = 0.8f;
        [self.clockTimerTemp invalidate];
        self.animationShown = YES;
        
        if(self.needOfflineContent){ //если переход из линого кабинета
            [self loadOflineZipsInit];
        } else if(self.isAskedToDownload){
            if(self.isFirstTimeJsonLoaded){
                [self loadDefaultUsage];
                [self manualAnimateLoader];
            } else {
                [self loadDefaultUsage];
            }
        } else {
            [self chooseVersionOnlineOrOfline];
        }
        
        
    } else if(newVarible>=0.1f){
        if(self.isAskedToDownload){
            if(self.isFirstTimeJsonLoaded){
                _loadingProgressBar = _memview.loadingProgressBar + 0.01;
                _memview.loadingProgressBar = _loadingProgressBar;
            }
        }
        [self animateAngelAndFrame:newVarible];
    } else {
        [self animateAngelAndFrame:newVarible];
    }
}
-(void)animateAngelAndFrame:(CGFloat)newVarible{
    self.skyImageView.frame = CGRectMake(- 200.f + _memviewMarginY, - 100.f + _memviewMarginY, self.view.frame.size.width+200.f, self.view.frame.size.height-70.f+100.f);
    self.memview.frame = CGRectMake(0.f, _memviewMarginY, self.memview.frame.size.width, self.memview.frame.size.height);
    self.memview.angle = newVarible;
    [self.memview setNeedsDisplay];
    self.clockTimerTemp = [NSTimer scheduledTimerWithTimeInterval: 0.02f target:self selector:@selector(manualAnimateLeninAngleAndFrame) userInfo:nil repeats:NO];
}
- (void)manualAnimateLoader {
    
    CGFloat newVarible = self.loadingProgressBar + 0.01;
    if(newVarible >= 1.f){
        self.loadingProgressBar = 1.f;
        [self.clockTimerTemp invalidate];
        [self showLogo];
        
    } else {
        self.loadingProgressBar = newVarible;
        self.memview.loadingProgressBar = self.loadingProgressBar;
        [self.memview setNeedsDisplay];
        self.clockTimerTemp = [NSTimer scheduledTimerWithTimeInterval: 0.02f target:self selector:@selector(manualAnimateLoader) userInfo:nil repeats:NO];
    }
}
- (void)showLogo {
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.logo.alpha = 1;
                         
                     }
                     completion:^(BOOL finished) {
                         // . NSLog(@"animation finished! %d", finished);
                         [self.menuCVC beginShowingMenu];
                     }];
    
    
    
}


#pragma mark - Navigation
-(void)showControllerAfterPush{
    
    UINavigationController *navigationController = (UINavigationController *)[[[UIApplication sharedApplication] keyWindow] rootViewController];
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if([self.contentType isEqualToString:@"mapObject"]){
        
        PKMapObjectDetailVC *vc= [storyBoard instantiateViewControllerWithIdentifier:@"mapObjDetail"];
        vc.mapObjectId = self.contentID;
        [navigationController pushViewController:vc animated:YES];
    } else if([self.contentType isEqualToString:@"mapGeoRoute"]){
        
        PKGeoRouteDetailVC *vc= [storyBoard instantiateViewControllerWithIdentifier:@"geoRouteDetail"];
        vc.geoRouteId = self.contentID;
        [navigationController pushViewController:vc animated:YES];
        
    } else if([self.contentType isEqualToString:@"tip"]){
        
        PKInformTipsDetailVC *vc= [storyBoard instantiateViewControllerWithIdentifier:@"tipsDetail"];
        vc.informTipId = self.contentID;
        [vc setIsTips:[self.contentType isEqualToString:@"tip"]];
        [navigationController pushViewController:vc animated:YES];
        
    } else if([self.contentType isEqualToString:@"calendar"]){
        
        PKEventDetailVC *vc= [storyBoard instantiateViewControllerWithIdentifier:@"eventDetail"];
        vc.eventId = self.contentID;
        [navigationController pushViewController:vc animated:YES];
        
    }
}
-(void)sendSegueToMain:(NSString *)segueId{
    [self performSegueWithIdentifier:segueId sender:self];
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // . NSLog(@"prepareForSegue %@", segue.identifier);
    
//    if([segue.identifier isEqualToString:@"segueToMainMenuCV"]){
////        PKMainMenuCVC* vc = [segue destinationViewController];
//        segue.destinationViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
//        self.menuCVC = segue.destinationViewController;
//        self.menuCVC.maxMenuApear = PKMaxMenuItemsCount;
//        self.menuCVC.delegate = self;
//
//    } else
    if([segue.identifier isEqualToString:@"mainSegueToMap"]){
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
        
        //        NSArray *tabBarControllers = [((UITabBarController*)segue.destinationViewController) viewControllers];
        //        for (id navControl in tabBarControllers) {
        //
        //            //        id navControl = [tabBarControllers objectAtIndex:1];
        //            if([navControl isKindOfClass:[UINavigationController class]]){
        //                // . NSLog(@"PKMapObjectCollectionViewController");
        //                NSArray *viewContrlls=[navControl viewControllers];
        //                for( int i=0;i<[ viewContrlls count];i++){
        //                    id cvc=[viewContrlls objectAtIndex:i];
        //                    // . NSLog(@"viewContrlls = %@", [cvc class]);
        //                    if([cvc isKindOfClass:[PKMainMapViewController class]]){
        //                        PKMainMapViewController* mapViewController = cvc;
        //                        mapViewController.uploadMap = YES;
        //                    }
        //                }
        //
        //            }
        //        }
        
        
    } else if([segue.identifier isEqualToString:@"mainSegueToDiscover"]){
        // . NSLog(@"segue.destinationViewController = %@", segue.destinationViewController);
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:1];
    } else if([segue.identifier isEqualToString:@"mainSegueToDiscoverFavorite"]){
        // . NSLog(@"segue.destinationViewController = %@", segue.destinationViewController);
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
        NSArray *tabBarControllers = [((UITabBarController*)segue.destinationViewController) viewControllers];
        int indexPredicate = 1;
        for (id navControl in tabBarControllers) {
            
            //        id navControl = [tabBarControllers objectAtIndex:1];
            if([navControl isKindOfClass:[UINavigationController class]]){
                // . NSLog(@"PKMapObjectCollectionViewController");
                NSArray *viewContrlls=[navControl viewControllers];
                for( int i=0;i<[ viewContrlls count];i++){
                    id cvc=[viewContrlls objectAtIndex:i];
                    // . NSLog(@"viewContrlls = %@", [cvc class]);
                    if([cvc isKindOfClass:[PKMapObjectCollectionViewController class]]){
                        PKMapObjectCollectionViewController* collectionViewController = cvc;
                        collectionViewController.contentRecomended = YES;
                        collectionViewController.predicateRecomended = indexPredicate;
                        indexPredicate++;
                    }
                }
                
            }
        }
        
    } else if([segue.identifier isEqualToString:@"mainSegueToRoute"]){
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:2];
    } else if([segue.identifier isEqualToString:@"mainSegueToFavorite"]){
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
    } else if([segue.identifier isEqualToString:@"mainSegueToProfile"]){
        PKProfileViewController *vc = (PKProfileViewController*)[segue destinationViewController];
        vc.delegate = self;
        // . NSLog(@"mainSegueToProfile");
    } else if([segue.identifier isEqualToString:@"mainSegueToTips"]){
        
//        UINavigationController *navControl = (UINavigationController*)[segue destinationViewController];
        
        PKTipsViewController *vc = (PKTipsViewController*)[segue destinationViewController];//[[navControl viewControllers] firstObject];
        vc.showTips = YES;
        
    } else if([segue.identifier isEqualToString:@"mainSegueToCalendar"]){
        
//        UINavigationController *navControl = (UINavigationController*)[segue destinationViewController];
        
        PKEventViewController *vc = (PKEventViewController*)[segue destinationViewController];//[[navControl viewControllers] firstObject];
        vc.showTips = NO;
        
    } else if([segue.identifier isEqualToString:@"mainSegueToFifa2018"]){
        PKMainMapViewController *vc = (PKMainMapViewController*)[segue destinationViewController];
        vc.fifa2018 = YES;
    }
}
@end



