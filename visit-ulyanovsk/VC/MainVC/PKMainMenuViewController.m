//
//  PKMainMenuViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 03.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMainMenuViewController.h"
#import "PKUOStyleKit.h"
#import "PKMemCenterView.h"
#import "PKMemCentrAnimationLayer.h"
#import <CoreMotion/CoreMotion.h>
#define kCMDeviceMotionUpdateFrequency (1.f/30.f)
@interface PKMainMenuViewController ()
@property (strong, nonatomic) UIImageView* bgImageArray;
@property (nonatomic, strong) PKMemCentrAnimationLayer* animatedLayer;
@property (strong, nonatomic) IBOutlet PKMemCenterView *memview;
@property (nonatomic) CGFloat loadingProgressBar;
@property (nonatomic, strong) NSTimer* clockTimer;
@property (nonatomic, strong) CMMotionManager *motionManager;
@property (nonatomic, strong) CADisplayLink *displayLink;
@property (strong, nonatomic) IBOutlet UIView *bgButtonView;


@property (strong, nonatomic) IBOutlet UIView *transparentLayer;


@end

@implementation PKMainMenuViewController



-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    // . NSLog(@"viewWillDisappear");
    [self.motionManager stopDeviceMotionUpdates];
    [self.displayLink removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.motionManager startDeviceMotionUpdates];
    [self.displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    // . NSLog(@"viewWillAppear");
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loadingProgressBar = 1.f;
//    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
//    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
//    //always fill the view
//    blurEffectView.frame = self.bgButtonView.bounds;
//    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//
//
//    [self.bgButtonView addSubview:blurEffectView]; //if you have more UIViews, use an insertSubview API to place it where needed
//    [self.bgButtonView sendSubviewToBack : blurEffectView];
    
//    CALayer *mask = [CALayer layer];
//    mask.contents = (id)[[PKUOStyleKit imageOfIconRoundHotel] CGImage];
//    mask.frame = CGRectMake(0, 0, self.bgButtonView.bounds.size.width, self.bgButtonView.bounds.size.height);
//    blurEffectView.layer.mask = mask;
//    blurEffectView.layer.masksToBounds = YES;
    
    
    
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    
    // . NSLog(@"4 navigationController = %lu , %lu", [self.navigationController.viewControllers count], [self.navigationController.viewControllers indexOfObject:self]);
    
    
    
//    self.animatedLayer = PKMemCentrAnimationLayer.layer;
//    self.animatedLayer.contentsScale = UIScreen.mainScreen.scale;
//    self.animatedLayer.frame = CGRectMake((self.buttonView.frame.size.width - 100.0)/2.0, (self.buttonView.frame.size.height - 100.0)/2.0, 100.0, 100.0);
//    [self.animatedLayer setValue: @(false) forKey: @"state"];
//    [self.buttonView.layer addSublayer: self.animatedLayer];
    
    //Animated Clock
//    [self updateMemCenterPosition: nil];
//    self.clockTimer = [NSTimer scheduledTimerWithTimeInterval: 0.5
//                                                       target: self
//                                                     selector: @selector(updateMemCenterPosition:)
//                                                     userInfo: nil
//                                                      repeats: YES];
    
    
    // Do any additional setup after loading the view.
    
    //  CGRectMake(x, y, width, height)
    
    
//    [PKUOStyleKit drawLeninmemorialWithElkafill:[UIColor colorWithRed: 1 green: 1 blue: 1 alpha: 1] camAngel:0.0f];
    
//    UIImageView* view = [[UIImageView alloc] initWithFrame:CGRectMake(-(414/2), -300, 414*2, 414+600)];
//    view.backgroundColor = [UIColor whiteColor];

//    UIImage* image1 = [UIImage imageNamed:@"pool.png"];
    //UIImage* image2 = [UIImage imageNamed:@"pool.png"];
    //UIImage* image3 = [UIImage imageNamed:@"pool.png"];

//    NSArray* images = [NSArray arrayWithObjects:image1,nil]; //image2, image1, image3, nil];

//    view.animationImages = images;
//    view.animationDuration = 15.f;
//    [view startAnimating];

//    [self.view addSubview:view];
//    [self.view sendSubviewToBack : view ];

    //self.testView = view;
//    self.bgImageArray = view;
    // Set vertical effect
    UIInterpolatingMotionEffect *verticalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.y"
     type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
    verticalMotionEffect.minimumRelativeValue = @(0);
    verticalMotionEffect.maximumRelativeValue = @(30);

    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffect =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffect.minimumRelativeValue = @(0);
    horizontalMotionEffect.maximumRelativeValue = @(70);

    // Create group to combine both
    UIMotionEffectGroup *group = [UIMotionEffectGroup new];
    group.motionEffects = @[horizontalMotionEffect,verticalMotionEffect];

    
    
    
    
    // Set horizontal effect
    UIInterpolatingMotionEffect *horizontalMotionEffectMem =
    [[UIInterpolatingMotionEffect alloc]
     initWithKeyPath:@"center.x"
     type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
    horizontalMotionEffectMem.minimumRelativeValue = @(-20);
    horizontalMotionEffectMem.maximumRelativeValue = @(20);
    
    // Create group to combine both
    UIMotionEffectGroup *groupmem = [UIMotionEffectGroup new];
    groupmem.motionEffects = @[horizontalMotionEffectMem];
    
    
    
    // Add both effects to your view
    [self.transparentLayer addMotionEffect:group];
    [self.memview addMotionEffect:groupmem];
    
    
    
}
//- (void)updateMemCenterPosition: (CGFloat *)angle
//{
//    PKMemCenterView* memview = self.memview;
//    memview.angle = 0.5;
//    
//    CMAcceleration acceleration = self.motionManager.deviceMotion.userAcceleration;
//    self.accelerationXVal.text = [NSString stringWithFormat:@"%.3f", acceleration.x];
////    // . NSLog(@" v = %@", [NSString stringWithFormat:@"%.3f", acceleration.x] );
//        [memview setNeedsDisplay];
//}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"mainSegueToMap"]){
        ////        PKCalendarDetailTableViewController *controller = segue.destinationViewController;
        // . NSLog(@"mainSegueToMap");
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:0];
//        UITabBarController* controller = segue.destinationViewController;
//        [controller.tabBarController setSelectedIndex:0];
//        controller.title = @"Map";
//    controller.tabBarController.selectedViewController=[controller.tabBarController.viewControllers objectAtIndex:0];
        
        
    } else if([segue.identifier isEqualToString:@"mainSegueToDiscover"]){
        // . NSLog(@"mainSegueToDiscover");
        
        [((UITabBarController*)segue.destinationViewController) setSelectedIndex:1];
        
        
//        UITabBarController* controller = segue.destinationViewController;
//        controller.title = @"Discover";
//        controller.tabBarController.selectedViewController=[controller.tabBarController.viewControllers objectAtIndex:1];
//        [controller.tabBarController setSelectedIndex:1];
        
    }
//    KMapBoxViewController *otherController = [[self.tabBarController viewControllers] objectAtIndex:0];
//
//    [otherController moveToDestanation]; // this is magic;
//    [self.tabBarController setSelectedIndex:0];
    
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(CMMotionManager *)motionManager {
    if (!_motionManager) {
        _motionManager = [[CMMotionManager alloc] init];
        _motionManager.deviceMotionUpdateInterval = kCMDeviceMotionUpdateFrequency;
    }
    
    return _motionManager;
}

-(CADisplayLink *)displayLink {
    if (!_displayLink) {
        _displayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateMotionData)];
    }
    
    return _displayLink;
}
- (void)updateMotionData {
    //1
//    CMAcceleration acceleration = self.motionManager.deviceMotion.userAcceleration;
//    self.accelerationX.text = [NSString stringWithFormat:@"%.1f", acceleration.x];
////    // . NSLog(@" f = %@", [NSString stringWithFormat:@"%.1f", acceleration.x] );
//    self.accelerationY.text = [NSString stringWithFormat:@"%.1f", acceleration.y];
//    self.accelerationZ.text = [NSString stringWithFormat:@"%.1f", acceleration.z];
    //
    
    //2
//    CMAcceleration gravity = self.motionManager.deviceMotion.gravity;
//    self.gravityX.text = [NSString stringWithFormat:@"%.1f", gravity.x];
//    self.gravityY.text = [NSString stringWithFormat:@"%.1f", gravity.y];
//    self.gravityZ.text = [NSString stringWithFormat:@"%.1f", gravity.z];
    //

//    CMAttitude *attitude = self.motionManager.deviceMotion.attitude;
//        self.attitudePitch.text = [NSString stringWithFormat:@"%.1f", attitude.pitch];
//        self.attitudeRoll.text = [NSString stringWithFormat:@"%.1f", attitude.roll];
//        self.attitudeYaw.text = [NSString stringWithFormat:@"%.1f", attitude.yaw];
    
//    CMRotationRate rotation = self.motionManager.deviceMotion.rotationRate;
//        self.rotationX.text = [NSString stringWithFormat:@"%.1f", rotation.x];
//        self.rotationY.text = [NSString stringWithFormat:@"%.1f", rotation.y];
//        self.rotationZ.text = [NSString stringWithFormat:@"%.1f", rotation.z];
    //
    
//    // . NSLog(@" pitch = %@", [NSString stringWithFormat:@"%.1f", attitude.pitch] );
    //3
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        CGRect frame = [[self.transparentLayer.layer presentationLayer] frame];
//        CGFloat y = 0;
        CGFloat x = 0;
//            PKMemCenterView* memview;
//        if(frame.origin.y < 0){
//            y = 100 + frame.origin.y;//(1.0 + rotation.y);
//        } else {
//            y = frame.origin.y;//gravity.y;
//        }
//        if(frame.origin.x < 0){
//            x = 100 + frame.origin.x;
//        } else if(frame.origin.x < -25){
//            x = frame.origin.x;
//        }
        
//        if(frame.origin.y + frame.origin.x > 0){
//            x = (frame.origin.y + frame.origin.x);
//        } else {
//            x = frame.origin.y + frame.origin.x;
//        }
        
        
        
    
//        // . NSLog(@" angle = %@", [NSString stringWithFormat:@"%.5f", frame.origin.x] );
        x = (((frame.origin.y + frame.origin.x)/100 + frame.origin.x / 70)/2)*0.7 + 0.3;
        self->_memview.angle = x;
        self->_memview.loadingProgressBar = self.loadingProgressBar;
        // . NSLog(@" angle = %@", [NSString stringWithFormat:@"%.5f", x] );
        [self.memview setNeedsDisplay];
    });
    
//     // . NSLog(@" angle = %@", [NSString stringWithFormat:@"%.5f", self.transparentLayer.frame.origin.x] );
    
    
//    self.memview = memview;
//    self.accelerationXVal.text = [NSString stringWithFormat:@"%.3f", acceleration.x];
    //    // . NSLog(@" v = %@", [NSString stringWithFormat:@"%.3f", acceleration.x] );
    
    

    

    //
}
@end
