//
//  PKMemCenterView.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 03.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMemCenterView.h"
#import "PKUOStyleKit.h"
@implementation PKMemCenterView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
//    [PKUOStyleKit drawLeninskMemorialWithFrame:self.bounds camAngel:self.angle loadingFractionX:self.loadingProgressBar];
    
    [PKUOStyleKit drawLeninskMemorialWithFrame: self.bounds camAngel: self.angle loadingFractionX: self.loadingProgressBar downoadingProgressString: self.localizedProgress downoadingProgressPercent: self.progressPercent loadingInProgress: self.loadingInProgress];
    
    
    
    
}


@end
