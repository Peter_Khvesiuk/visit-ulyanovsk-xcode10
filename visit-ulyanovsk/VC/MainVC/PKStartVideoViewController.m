//
//  ViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 19.12.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import "PKStartVideoViewController.h"
#import "PKDataManager.h"
#import "PKBackgroundVideo.h"
#import "PKUOStyleKit.h"

@interface PKStartVideoViewController ()
@property (strong, nonatomic) PKBackgroundVideo *backgroundVideo;
@property (strong, nonatomic) UIView *viewApearGradient;
@end

@implementation PKStartVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.backgroundVideo = [[PKBackgroundVideo alloc] initOnViewController:self withVideoURL:VisitUlyanovskVideo];
    [self.backgroundVideo setUpBackground];
    self.backgroundVideo.delegate = self;
    
    UILabel *bottomLabel = [[UILabel alloc] initWithFrame:CGRectMake(10.f, self.view.frame.size.height - 50.f, self.view.frame.size.width - 10.f*2, 40.f)];
    
    NSString *locale = [[NSLocale currentLocale] languageCode];

    if([locale isEqualToString:@"ru"]){
        bottomLabel.text = [NSString stringWithFormat:@"%@%@%@", @"[ ", @"Нажмите, чтобы продолжить",@" ]"];
    } else {
        bottomLabel.text = [NSString stringWithFormat:@"%@%@%@", @"[ ", @"Tap, to continue",@" ]"];
    }
//  #warning translate
    
    
    bottomLabel.textAlignment = NSTextAlignmentCenter;

    bottomLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:bottomLabel];
    
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];
    
//    [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(BOOL isComplete) {
//        if(isComplete){// . NSLog(@"getMainJSONDataFromServerOnSuccess");}
//
//    } onFailure:^(NSError *error, NSInteger statusCode) {
//        // . NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
//    }];
    
    
//    [[PKServerManager sharedManager] getMainZipDataFromServerOnSuccess:^(BOOL isComplete) {
//                if(isComplete){// . NSLog(@"getMainJSONDataFromServerOnSuccess");}
//        
//    } inProgress:^(CGFloat progress) {
//        // . NSLog(@"progress == %f", progress);
//    } onFailure:^(NSError *error, NSInteger statusCode) {
//                // . NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
//    }];
//    
    

}

//- (void)dealloc {
//        // . NSLog(@"dealloc PKStartVideoViewController NSNotificationCenter");
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
//        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
//}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer
{
    
//    // . NSLog(@"Never call!!!");
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    [[PKDataManager sharedManager] setSkipVideo];
    [self goToNextView];
    //Do stuff here...
}
-(void)goToNextView{
//    // . NSLog(@"Never call!!!");
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = self.view.bounds;
    gradient.colors = @[(id)[PKUOStyleKit bgColorGradientTop].CGColor, (id)[PKUOStyleKit bgColorGradientBottom].CGColor];
    [self.viewApearGradient.layer insertSublayer:gradient atIndex:0];
    self.viewApearGradient.alpha = 0;
    [self.view addSubview:self.viewApearGradient];
    
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut /*| UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse*/
                     animations:^{
                         
                         self.viewApearGradient.alpha = 1;
                         
                     }
                     completion:^(BOOL finished) {
                         // . NSLog(@"animation finished! %d", finished);
                         if(self.backgroundVideo.hasBeenUsed){
                             [[NSNotificationCenter defaultCenter] removeObserver:self.backgroundVideo name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
                             [[NSNotificationCenter defaultCenter] removeObserver:self.backgroundVideo name:UIApplicationWillEnterForegroundNotification object:nil];
                         }
                         
                         
                         
//                          [self performSegueWithIdentifier:@"fromVideoToMainMenu" sender:self];
                         UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                         UITabBarController *rootViewController = [storyboard instantiateViewControllerWithIdentifier:@"StartSkipVideo"];
                         [[UIApplication sharedApplication].keyWindow setRootViewController:rootViewController];
                         
                     }];
    
    
   
}

-(void)videoIsStoped{
    // . NSLog(@"Push");
    
    [self goToNextView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
}

@end
