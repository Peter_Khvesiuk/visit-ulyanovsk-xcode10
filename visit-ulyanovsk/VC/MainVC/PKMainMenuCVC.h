//
//  PKMainMenuCVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol sendSegueToMain <NSObject>
@required
-(void)sendSegueToMain:(NSString *)segueId; //I am thinking my data is NSArray, you can use another object for store your information.

@end

@interface PKMainMenuCVC : UICollectionViewController
@property (assign, nonatomic) NSInteger menuApear;
@property (assign, nonatomic) NSInteger maxMenuApear;

- (void)instanceShowMenu;
-(void)beginShowingMenu;
-(void)hideMenu;
@property(nonatomic,assign)id delegate;

@end
@interface PKMainMenuCVCollectionView : UICollectionView

@end
