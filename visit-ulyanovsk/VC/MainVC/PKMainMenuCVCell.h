//
//  PKMainMenuCVCell.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKMainMenuCVCell : UICollectionViewCell
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *btnLabel;

//-(instancetype)initWithFrame:(CGRect)frame andTitle:(NSString*)title andImage:(UIImage*)image;
@end
