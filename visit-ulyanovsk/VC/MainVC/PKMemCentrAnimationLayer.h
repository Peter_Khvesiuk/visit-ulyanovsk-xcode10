//
//  PKMemCentrAnimationLayer.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 03.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface PKMemCentrAnimationLayer : CALayer
@property  CGFloat angle;
@property  CGFloat loadingProgressBar;

@end
