//
//  PKMemCentrAnimationLayer.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 03.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMemCentrAnimationLayer.h"
#import "PKUOStyleKit.h"

@implementation PKMemCentrAnimationLayer
@dynamic angle;

- (instancetype)init
{
    if (self = [super init])
    {
        self.angle = 1.f;
        self.loadingProgressBar = 1.f;
        
    }
    return self;
}

- (instancetype)initWithLayer: (id)layer
{
    if ((self = [super initWithLayer: layer]))
    {
        if ([layer isKindOfClass: PKMemCentrAnimationLayer.class])
        {
            self.angle = ((PKMemCentrAnimationLayer *)layer).angle;
            
            
        }
    }
    return self;
}

+ (BOOL)needsDisplayForKey: (NSString*)key
{
    if([key isEqualToString: @"angle"])
    return YES;
    
    return [super needsDisplayForKey: key];
}


- (id<CAAction>)actionForKey: (NSString *)event
{
    
    if([event isEqualToString: @"angle"])
    {
        CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath: event];
        theAnimation.fromValue = [[self presentationLayer] valueForKey: event];
        return theAnimation;
    }
    return [super actionForKey: event];
}

- (void)drawInContext: (CGContextRef)ctx
{
    UIGraphicsPushContext(ctx);
    
//    [PKUOStyleKit drawLeninskMemorialWithFrame:self.bounds  camAngel:self.angle loadingFractionX:self.loadingProgressBar];
    
    
    UIGraphicsPopContext();
    
}
@end
