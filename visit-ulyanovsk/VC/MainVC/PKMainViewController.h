//
//  PKMainViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKMainViewController : UIViewController

@property (nonatomic) BOOL instanceView;
@property (nonatomic) BOOL needOfflineContent;
@property (nonatomic) BOOL needToShowContentAfterPush;
@property (nonatomic,strong) NSString* contentType;
@property (nonatomic) int contentID;

@end
