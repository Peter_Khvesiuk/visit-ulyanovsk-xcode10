//
//  PKMainBtn.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 04.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMainBtn.h"
#import "PKUOStyleKit.h"
#import <QuartzCore/QuartzCore.h>
@implementation PKMainBtn


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
//    CGSize size =CGSizeMake(self.bounds.size.width, self.bounds.size.height);
    
//    // . NSLog(@"", size.width);

    
//    [self setBackgroundImage:[PKUOStyleKit imageOfIconRoundDiscoverWithIconFill:[UIColor whiteColor]] forState:UIControlStateHighlighted];
    
    
    [self setTitleColor:[UIColor whiteColor]  forState:UIControlStateNormal];
    
    [self superview].backgroundColor = [UIColor clearColor];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];//];
    UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    //always fill the view

    CGRect size = CGRectMake([self superview].bounds.origin.x+5, [self superview].bounds.origin.y+5, [self superview].bounds.size.width - 10, [self superview].bounds.size.height - 10);
    blurEffectView.frame = size;
    blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    
    [[self superview] addSubview:blurEffectView]; //if you have more UIViews, use an insertSubview API to place it where needed
    [[self superview] sendSubviewToBack : blurEffectView];
    
//    CALayer *mask = [CALayer layer];
    
    
        
        
    
    
//    [self setBackgroundImage:[PKUOStyleKit drawIconRoundMapWithFrame:self.bounds resizing:PKUOStyleKitResizingBehaviorAspectFit] forState:UIControlStateNormal];
    
    
    
    
//    
//    [self superview].backgroundColor = [UIColor clearColor];
//    
//    if([[self accessibilityIdentifier] isEqualToString:@"Tours"]){
//        
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainTours] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainTours] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainTours] CGImage];
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Discover"]){
//        
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainDiscover] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainDiscover] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainDiscover] CGImage];
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Map"]){
//        
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainMap] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainMap] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainMap] CGImage];
//        
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Hotels"]){
//        
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainHotel] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainHotel] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainHotel] CGImage];
//        
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Rent a car"]){
//        
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainCarRent] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainCarRent] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainCarRent] CGImage];
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Souvenir"]){
//       
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainSouvenir] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainSouvenir] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainSouvenir] CGImage];
//        
//        
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Restaurant"]){
//       
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainRestaurant] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainRestaurant] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainRestaurant] CGImage];
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Taxi"]){
//       
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainTaxi] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainTaxi] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainTaxi] CGImage];
//        
//    } else if([[self accessibilityIdentifier] isEqualToString:@"Profile"]){
//        
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainProfile] forState:UIControlStateNormal];
////        [self setBackgroundImage:[PKUOStyleKit imageOfIconMainProfile] forState:UIControlStateHighlighted];
//        mask.contents = (id)[[PKUOStyleKit imageOfIconMainProfile] CGImage];
//        
//    }
//    

    
//    mask.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//    blurEffectView.layer.mask = mask;
//    blurEffectView.layer.masksToBounds = YES;
    
    

    
//    [PKUOStyleKit drawIconRoundMapWithFrame:self.bounds];
}


@end
