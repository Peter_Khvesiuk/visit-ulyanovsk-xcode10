//
//  PKMemCenterView.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 03.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface PKMemCenterView : UIView
@property (nonatomic) IBInspectable CGFloat angle;
@property (nonatomic) IBInspectable CGFloat loadingProgressBar;
@property (nonatomic,strong) NSString *localizedProgress;
@property (nonatomic,strong) NSString *progressPercent;
@property (nonatomic) BOOL loadingInProgress;

@end
