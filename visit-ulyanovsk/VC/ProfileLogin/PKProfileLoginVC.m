//
//  PKProfileLoginVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 06.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKProfileLoginVC.h"

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import "PKMapBoxViewController.h"
#import "PKProfileTableVC.h"
#import "PKDataManager.h"
#import "PKServerManager.h"

#import "PKUOStyleKit.h"
#import "PKInterfaceManager.h"
#import "PKUserDocs+CoreDataClass.h"



@interface PKProfileLoginVC ()<UITextViewDelegate>

@property (strong, nonatomic) UILabel *almostThereLabel;
@property (strong, nonatomic) UILabel *syncDeviceLabel;
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@property (strong, nonatomic) UILabel *neverLoseLabel;
@property (strong, nonatomic) UIButton *signUpBtn;
@property (strong, nonatomic) UIButton *signUpEmailBtn;
//@property (strong, nonatomic) UILabel *bySignInLabel;
//@property (strong, nonatomic) UIButton *termsButton;
//@property (strong, nonatomic) UILabel *andLabel;
//@property (strong, nonatomic) UIButton *privacyButton;

@property (strong, nonatomic) UITextView *textBottom;

@property (strong, nonatomic) NSLayoutManager *layoutManager;
@property (strong, nonatomic) NSTextContainer *textContainer;
@property (strong, nonatomic) NSTextStorage *textStorage;
@property (nonatomic) NSRange rangePrivacyPolicyText;
@property (nonatomic) NSRange rangeTermsAndConditionsText;

@end

@implementation PKProfileLoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UILabel *almostThereLabel = [[UILabel alloc] init];
    almostThereLabel.text = [TRANSLATE(@"almost_there") uppercaseString];
    almostThereLabel.textAlignment = NSTextAlignmentCenter;
    almostThereLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
    CGSize almostThereLabelTextViewSize = [almostThereLabel sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
    almostThereLabel.frame = CGRectMake(regularFZMargin2, 150.f, self.view.frame.size.width - regularFZMargin2 * 2, almostThereLabelTextViewSize.height);
    self.almostThereLabel = almostThereLabel;
    [self.view addSubview:self.almostThereLabel];
    
    UILabel *syncDeviceLabel = [[UILabel alloc] init];
    syncDeviceLabel.text = TRANSLATE_UP(@"synchronize_across_multiple_devices");
    syncDeviceLabel.numberOfLines = 0;
    syncDeviceLabel.textAlignment = NSTextAlignmentCenter;
    CGSize syncDeviceLabelTextViewSize = [syncDeviceLabel sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
    syncDeviceLabel.frame = CGRectMake(regularFZMargin2, almostThereLabel.frame.origin.y + almostThereLabel.frame.size.height + regularFZMargin2, self.view.frame.size.width - regularFZMargin2 * 2, syncDeviceLabelTextViewSize.height);
    self.syncDeviceLabel = syncDeviceLabel;
    [self.view addSubview:self.syncDeviceLabel];
    
    UILabel *neverLoseLabel = [[UILabel alloc] init];
    neverLoseLabel.text = TRANSLATE_UP(@"never_lose_your_saved_places");
    neverLoseLabel.numberOfLines = 0;
    neverLoseLabel.textAlignment = NSTextAlignmentCenter;
    CGSize neverLoseLabelTextViewSize = [neverLoseLabel sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
    neverLoseLabel.frame = CGRectMake(regularFZMargin2, syncDeviceLabel.frame.origin.y + syncDeviceLabel.frame.size.height + regularFZMargin2/2, self.view.frame.size.width - regularFZMargin2 * 2 +6.f, neverLoseLabelTextViewSize.height);
    self.neverLoseLabel = neverLoseLabel;
    [self.view addSubview:self.neverLoseLabel];
    
    CGFloat fontSize = 17.f;
    CGFloat btnHeight = 56.f;
    if(isiPhone5s){
//        NSLog(@"isiPhone5s");
        fontSize = 15.f;
        btnHeight = 46.f;
    }
    
    UIButton *signUpBtn = [[UIButton alloc] initWithFrame:CGRectMake(regularFZMargin2, neverLoseLabel.frame.origin.y + neverLoseLabel.frame.size.height + regularFZMargin2, self.view.frame.size.width - regularFZMargin2 * 2, btnHeight)];
    [signUpBtn setTitle:TRANSLATE(@"Sign_in_with_Facebook") forState:UIControlStateNormal];
    [signUpBtn setTitle:TRANSLATE(@"Sign_in_with_Facebook") forState:UIControlStateHighlighted];
    [signUpBtn setBackgroundColor:RGB(59,89,152)];
    signUpBtn.layer.cornerRadius = 10.f;
    signUpBtn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [signUpBtn
         addTarget:self
         action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.signUpBtn = signUpBtn;
    
    
    [self.view addSubview:self.signUpBtn];
    
    UIButton *signUpEmailBtn = [[UIButton alloc] initWithFrame:CGRectMake(regularFZMargin2, signUpBtn.frame.origin.y + signUpBtn.frame.size.height + regularFZMargin2 + 5.f, self.view.frame.size.width - regularFZMargin2 * 2, btnHeight)];
    [signUpEmailBtn setTitle:TRANSLATE(@"Sign_up_with_E-mail") forState:UIControlStateNormal];
    [signUpEmailBtn setTitle:TRANSLATE(@"Sign_up_with_E-mail") forState:UIControlStateHighlighted];
    [signUpEmailBtn setBackgroundColor:[PKUOStyleKit bgColorGradientBottom]];
    signUpEmailBtn.layer.cornerRadius = 10.f;
    signUpEmailBtn.titleLabel.font = [UIFont systemFontOfSize:fontSize];
    [signUpEmailBtn
     addTarget:self
     action:@selector(loginWithEmailButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.signUpEmailBtn = signUpEmailBtn;
    [self.view addSubview:self.signUpEmailBtn];
    
    
    
    
    UITextView *textBottom = [[UITextView alloc] init];//WithFrame:CGRectMake(regularFZMargin2, self.view.frame.size.height - 200.f, self.view.frame.size.width - regularFZMargin2 * 2, 150.f)];
    textBottom.editable = NO;
   
    textBottom.selectable = NO;
//    __weak PKProfileLoginVC *weakSelf = self;
    
    self.textBottom.delegate = self;
    
    NSArray<PKUserDocs*> *userDocs = [[PKDataManager sharedManager] getUserDocs];
    PKUserDocs *userDocs1 = (PKUserDocs*)[userDocs firstObject];
    PKUserDocs *userDocs2 = (PKUserDocs*)[userDocs lastObject];
    
    NSString *bySigningUp = TRANSLATE(@"By_signing_up,_I_agree_to_the");
    NSString *termsAndConditionsText = [[NSString alloc] initWithString: TRANSLATE(userDocs2.title)];
    NSString *privacyPolicyText = [[NSString alloc] initWithString: TRANSLATE(userDocs1.title)];
    NSString *andLabelText = [[NSString alloc] initWithString: TRANSLATE(@"&")];
    
    NSString *strBottom = [NSString stringWithFormat:@"%@\n%@ %@ %@", bySigningUp, termsAndConditionsText, andLabelText, privacyPolicyText];
    
    textBottom.text = strBottom;
    
    
    
    

    self.rangeTermsAndConditionsText = [strBottom rangeOfString: termsAndConditionsText];
    self.rangePrivacyPolicyText = [strBottom rangeOfString: privacyPolicyText];
    
    
//    textBottom.font = [UIFont fontWithName:@"Helvetica" size:16];
    
    NSDictionary *attribs = @{
                              NSForegroundColorAttributeName:[PKUOStyleKit uOTextColor],
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:16]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:strBottom attributes:attribs];
//    UIFont *boldFont = [UIFont fontWithName:@"Helvetica" size:16];
//    NSRange range = [strBottom rangeOfString:domain];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [PKUOStyleKit bgColorGradientBottom], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:16], NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)} range:self.rangeTermsAndConditionsText];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [PKUOStyleKit bgColorGradientBottom], NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:16], NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle)} range:self.rangePrivacyPolicyText];
    textBottom.attributedText = attributedText;
    textBottom.textAlignment = NSTextAlignmentCenter;
    
    CGSize textBottomTextViewSize = [textBottom sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
    textBottom.userInteractionEnabled = YES;
    [textBottom addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapTextBottom:)]];
    
    textBottom.frame = CGRectMake(regularFZMargin2, self.view.frame.size.height - textBottomTextViewSize.height - regularFZMargin2, self.view.frame.size.width - regularFZMargin2 * 2, textBottomTextViewSize.height);
    
    
    // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
    NSLayoutManager *layoutManager = [[NSLayoutManager alloc] init];
    NSTextContainer *textContainer = [[NSTextContainer alloc] initWithSize:CGSizeZero];
    NSTextStorage *textStorage = [[NSTextStorage alloc] initWithAttributedString:attributedText];
    
    // Configure layoutManager and textStorage
    [layoutManager addTextContainer:textContainer];
    [textStorage addLayoutManager:layoutManager];
    
    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0;
//    textContainer.lineBreakMode = textBottom.lineBreakMode;
//    textContainer.maximumNumberOfLines = textBottom.numberOfLines;
    self.layoutManager = layoutManager;
    self.textStorage = textStorage;
    self.textContainer = textContainer;
    
    [self.view addSubview:textBottom];
    
    
    
    
    
    
    
//
    
//    if(privacyPolicy
    
    
//    UILabel *andLabel = [[UILabel alloc] init];
//    andLabel.text = andLabelText;
//    CGSize andLabelTextViewSize = [andLabel sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
//
//
//    NSUInteger termsAndConditionsCount = [termsAndConditionsText length];
//    NSUInteger privacyPolicyCount = [privacyPolicyText length];
//    NSUInteger andLabelCount = [andLabelText length];
//
//    UIButton *termsButton = [[UIButton alloc] init];//WithFrame:CGRectMake(regularFZMargin2, signUpBtn.frame.origin.y + signUpBtn.frame.size.height + regularFZMargin2, self.view.frame.size.width - regularFZMargin2 * 2, 44.f)];
//    [termsButton setTitle:termsAndConditionsText forState:UIControlStateNormal];
//    [termsButton setTitle:termsAndConditionsText forState:UIControlStateHighlighted];
//    [termsButton setTintColor:[PKUOStyleKit bgColorGradientBottom]];
//    termsButton.titleLabel.numberOfLines = 0;
//    termsButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//
//
//    UIButton *privacyButton = [[UIButton alloc] init];//WithFrame:CGRectMake(regularFZMargin2, signUpBtn.frame.origin.y + signUpBtn.frame.size.height + regularFZMargin2, self.view.frame.size.width - regularFZMargin2 * 2, 44.f)];
//    [privacyButton setTitle:privacyPolicyText forState:UIControlStateNormal];
//    [privacyButton setTitle:privacyPolicyText forState:UIControlStateHighlighted];
//    [privacyButton setTintColor:[PKUOStyleKit bgColorGradientBottom]];
//    privacyButton.titleLabel.numberOfLines = 0;
//    privacyButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
//
//
//
//    CGSize termsAndConditionsViewSize = [termsButton sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
//
//    CGSize privacyLabelViewSize = [privacyButton sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
//
//
//    if(termsAndConditionsViewSize.width + privacyLabelViewSize.width + andLabelTextViewSize.width + regularFZMargin2 * 3.f > self.view.frame.size.width){
//
//        if(termsAndConditionsViewSize.width + privacyLabelViewSize.width + andLabelTextViewSize.width + regularFZMargin2 * 3.f > self.view.frame.size.width){
//
//        }
//        NSLog(@"\n total %.f > %.f \n termsAndConditionsViewSize.width = %.f \n privacyLabelViewSize.width = %.f \n andLabelTextViewSize.width = %.f", termsAndConditionsViewSize.width + privacyLabelViewSize.width + andLabelTextViewSize.width + regularFZMargin2 * 3.f, self.view.frame.size.width, termsAndConditionsViewSize.width, privacyLabelViewSize.width, andLabelTextViewSize.width);
//    } else {
//        NSLog(@"\n total %.f < %.f \n termsAndConditionsViewSize.width = %.f \n privacyLabelViewSize.width = %.f \n andLabelTextViewSize.width = %.f", termsAndConditionsViewSize.width + privacyLabelViewSize.width + andLabelTextViewSize.width + regularFZMargin2 * 3.f, self.view.frame.size.width, termsAndConditionsViewSize.width, privacyLabelViewSize.width, andLabelTextViewSize.width);
//    }
//
//
//    andLabel.frame = CGRectMake(regularFZMargin2, syncDeviceLabel.frame.origin.y + syncDeviceLabel.frame.size.height + regularFZMargin2/2, self.view.frame.size.width - regularFZMargin2 * 2, andLabelTextViewSize.height);
//    self.andLabel = andLabel;
//    [self.view addSubview:self.andLabel];
//
//
//    self.termsButton = termsButton;
//    [self.view addSubview:self.termsButton];
//
//    self.privacyButton = privacyButton;
//    [self.view addSubview:self.privacyButton];
//
//
//    UILabel *bySignInLabel = [[UILabel alloc] init];
//    bySignInLabel.text = TRANSLATE(@"By_signing_up,_I_agree_to_the");
//    CGSize bySignInLabelTextViewSize = [almostThereLabel sizeThatFits:CGSizeMake((self.view.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
//    neverLoseLabel.frame = CGRectMake(regularFZMargin2, syncDeviceLabel.frame.origin.y + syncDeviceLabel.frame.size.height + regularFZMargin2/2, self.view.frame.size.width - regularFZMargin2 * 2, bySignInLabelTextViewSize.height);
//    self.bySignInLabel = bySignInLabel;
//    [self.view addSubview:self.bySignInLabel];
    
    
    
    
//    self.bySignInLabel.text = TRANSLATE(@"By_signing_up,_I_agree_to_the");
    
    
//    self.andLabel.text = TRANSLATE(@"&");
    
//    [self.termsLabel setTitle:TRANSLATE(@"terms_and_conditions") forState:UIControlStateNormal];
//    [self.termsLabel setTitle:TRANSLATE(@"terms_and_conditions") forState:UIControlStateHighlighted];
    
    
//    [self.privacyLabel setTitle:TRANSLATE(@"privacy_policy") forState:UIControlStateNormal];
//    [self.privacyLabel setTitle:TRANSLATE(@"privacy_policy") forState:UIControlStateHighlighted];
    
    
    
//    _loginFBButton.readPermissions =
//    @[@"public_profile", @"email", @"user_friends"];
//    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
//    // Optional: Place the button in the center of your view.
//    loginButton.center = self.view.center;
//    // . NSLog(@"sd = %@", [FBSDKAccessToken currentAccessToken]);
//    [self.view addSubview:loginButton];
    
    // Do any additional setup after loading the view.
//    UIButton *myLoginButton=[UIButton buttonWithType:UIButtonTypeCustom];
    
//    _loginFBButton.backgroundColor=[UIColor darkGrayColor];
//    myLoginButton.frame=CGRectMake(0,0,180,40);
//    myLoginButton.center = self.view.center;
//    [_loginFBButton setTitle: TRANSLATE_UP(@"Sign_in_with_Facebook") forState: UIControlStateNormal];
//    _loginFBButton.layer.cornerRadius = 10.f;
//
//    _loginEmailButton.layer.cornerRadius = 10.f;
//    // Handle clicks on the button
//    [_loginFBButton
//     addTarget:self
//     action:@selector(loginButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    // Add the button to the view
//    [self.view addSubview:myLoginButton];
    
    
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, 50.f, self.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [self.view insertSubview:backgroundInfoBlockImage atIndex:0];
    self.view.clipsToBounds = YES;
    
    
}
-(void)handleTapTextBottom:(UITapGestureRecognizer *)tapGesture{
    CGPoint locationOfTouchInLabel = [tapGesture locationInView:tapGesture.view];
    CGSize labelSize = tapGesture.view.bounds.size;
    CGRect textBoundingBox = [self.layoutManager usedRectForTextContainer:self.textContainer];
    CGPoint textContainerOffset = CGPointMake((labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
                                              (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
    CGPoint locationOfTouchInTextContainer = CGPointMake(locationOfTouchInLabel.x - textContainerOffset.x,
                                                         locationOfTouchInLabel.y - textContainerOffset.y);
    NSInteger indexOfCharacter = [self.layoutManager characterIndexForPoint:locationOfTouchInTextContainer
                                                            inTextContainer:self.textContainer
                                   fractionOfDistanceBetweenInsertionPoints:nil];
//    NSRange linkRange = NSMakeRange(14, 4); // it's better to save the range somewhere when it was originally used for marking link in attributed string
    
    NSArray<PKUserDocs*> *userDocs = [[PKDataManager sharedManager] getUserDocs];
    PKUserDocs *userDocs1 = (PKUserDocs*)[userDocs firstObject];
    PKUserDocs *userDocs2 = (PKUserDocs*)[userDocs lastObject];
        
    if (NSLocationInRange(indexOfCharacter, self.rangePrivacyPolicyText)) {
        // Open an URL, or handle the tap on the link in any other way
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://stackoverflow.com/"]];
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", URL_main, userDocs2.url]];;
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
    if (NSLocationInRange(indexOfCharacter, self.rangeTermsAndConditionsText)) {
        NSURL *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", URL_main, userDocs1.url]];;
        // Open an URL, or handle the tap on the link in any other way
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://stackoverflow.com/"]];
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
    }
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.textContainer.size = self.textBottom.bounds.size;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
-(void)popBack:(id)sender{
    // . NSLog(@"popBack");
    UIButton* btn = (UIButton*)sender;
    CGRect frame = btn.frame;
    frame.origin.x = - btn.frame.size.width;
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         btn.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [[self navigationController] popViewControllerAnimated:YES];
                     }];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)loginWithEmailButtonClicked
{
   [self performSegueWithIdentifier:@"segueToLoginEmail" sender:self];
}
#pragma mark - Facebook login

-(void)loginButtonClicked
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithPermissions:@[@"public_profile"]
             fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                 if (error) {
                     // . NSLog(@"Process error");
                 } else if (result.isCancelled) {
                     // . NSLog(@"Cancelled");
                 } else {
                     // . NSLog(@"Logged in");
                     [FBSDKProfile loadCurrentProfileWithCompletion:
                      ^(FBSDKProfile *profile, NSError *error) {
                          if (profile) {
                              // . NSLog(@"Login, %@!", profile.firstName);
                              // . NSLog(@"userID, %@!", profile.userID);
                              [self getClientIdFromServerForFacebookUserID: profile];
                              
                          }
                          //                  [self dismissViewControllerAnimated:NO completion:nil];
                          //                  [self.navigationController popToRootViewControllerAnimated:YES];
                          [self.navigationController popViewControllerAnimated:YES];
                      }];
                     //             PKMapBoxViewController *otherController = [[self.tabBarController viewControllers] objectAtIndex:0];
                     //             [self performSegueWithIdentifier: @"showCalendarDetail" sender: self];
                     //             [otherController moveToDestanation]; // this is magic;
                     //             [self.tabBarController setSelectedIndex:0];
                 }
             }];
}
#pragma mark - get Client id from server

-(void)getClientIdFromServerForFacebookUserID:(FBSDKProfile *)profile{
    
    
    
    [[PKServerManager sharedManager]
     sendFacebookUserID:profile.userID
     firstName:profile.firstName
     lastName: profile.lastName
     onSuccess:^(NSString* clientID) {
         // . NSLog(@"clientID %@", clientID);
//         [self saveUserInfoUserID:clientID];
         [[PKDataManager sharedManager] updateCoreDataUserWithID: profile.userID
                                                    andFirstName: profile.firstName
                                                     andLastName: profile.lastName
                                                     andClientID: clientID];
     }
     onFailure:^(NSError *error, NSInteger statusCode) {
         // . NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
     }];
    
}
//-(void)saveUserInfoUserID: (NSString*) userID
//                firstName: (NSString*) firstName
//                 lastName: (NSString*) lastName
//                 clientID: (NSString*) clientID{
//    [[PKDataManager sharedManager] updateCoreDataUserWithID: userID
//                                               andFirstName: firstName
//                                                andLastName: lastName
//                                                andClientID:clientID];
//
//}

//#pragma mark - Segue

//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
//
//
//    if([segue.identifier isEqualToString:@"showCalendarDetail"]){
////        PKCalendarDetailTableViewController *controller = segue.destinationViewController;
////
////
////        controller.dateDetailStr = [df stringFromDate:self.selectedRow.dateStart];
////        // . NSLog(@"controller.dateDetailStr == %@",controller.dateDetailStr);
////        controller.durationDetailStr = [NSString stringWithFormat: @"%@ мин.", self.selectedRow.duration];
////
////        settingsPopoverController = [popoverSegue popoverControllerWithSender:sender
////                                                     permittedArrowDirections:WYPopoverArrowDirectionDown
////                                                                     animated:YES
////                                                                      options:WYPopoverAnimationOptionFadeWithScale];
////        settingsPopoverController.delegate = self;
//
//
//
//
//
//
//    }
//}


@end
