//
//  PKProfileLoginVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 06.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <FBSDKLoginKit/FBSDKLoginKit.h>


@interface PKProfileLoginVC : UIViewController
@property (weak, nonatomic)  UIButton *loginFBButton;
@property (weak, nonatomic)  UIButton *loginEmailButton;

@end
