//
//  PKMapTabBarFavorite.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 19.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapTabBarFavorite.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"

@implementation PKMapTabBarFavorite

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setBackgroundImage:[PKUOStyleKit imageOfTabbarBottomBackground]];
    [self setBackgroundColor: [UIColor clearColor]];
    [self setShadowImage:[UIImage new]];
    self.layer.borderWidth = 0.0f;
    self.clipsToBounds = YES;
    
    [self setTintColor:[PKUOStyleKit bgColorGradientBottom]];
    [self setUnselectedItemTintColor:[PKUOStyleKit uOTextColor]];
    NSInteger index = 0;
    for (UITabBarItem* item in [self items]) {

        switch (index) {
            case 0:
                [item setImage:[PKUOStyleKit imageOfIconTabBarRestaurant]];
                [item setTitle:TRANSLATE_UP(@"restaurants")];
                break;
            case 1:
                [item setImage:[PKUOStyleKit imageOfIconTabBarHotel]];
                [item setTitle:TRANSLATE_UP(@"Hotels")];
                break;
            case 2:
                [item setImage:[PKUOStyleKit imageOfIconTabBarMuseum]];
                [item setTitle:TRANSLATE_UP(@"interesting")];
                break;
            case 3:
                [item setImage:[PKUOStyleKit imageOfIconTabBarFavorite]];
                [item setTitle:TRANSLATE_UP(@"my_list")];
                break;

        }
        index++;
    }
    
}

@end
