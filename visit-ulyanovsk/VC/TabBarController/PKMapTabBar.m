//
//  PKMapTabBar.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 04.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapTabBar.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"
@implementation PKMapTabBar


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.

- (void)awakeFromNib
{
    [super awakeFromNib];
    
//    self.backgroundColor = [UIColor clearColor];
//    [self setBackgroundImage:[UIImage imageNamed:@"transparent"]];
//
//    UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
//    [self addSubview:imageView];
    
    [self setBackgroundImage:[PKUOStyleKit imageOfTabbarBottomBackground]];
    [self setBackgroundColor: [UIColor clearColor]];
    [self setShadowImage:[UIImage new]];
    self.layer.borderWidth = 0.0f;
//    // . NSLog(@"x = %.f, y = %.f, w = %.f, h = %.f", self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height);
//    self.frame = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, 156.f);
//    self.bounds = CGRectMake(self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, 156.f);
//    // . NSLog(@"x = %.f, y = %.f, w = %.f, h = %.f", self.bounds.origin.x, self.bounds.origin.y, self.bounds.size.width, self.bounds.size.height);
    self.clipsToBounds = YES;

    [self setTintColor:[PKUOStyleKit bgColorGradientBottom]];
    [self setUnselectedItemTintColor:[PKUOStyleKit uOTextColor]];
    
//    [[self superview] setBackgroundColor: [UIColor clearColor]];
    
    
//    [[self appearance] setShadowImage:[UIImage new]];
//    self sha
    
//    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"transparentShadow.png"]];
    NSInteger index = 0;
    for (UITabBarItem* item in [self items]) {

        switch (index) {
            case 0:
                [item setImage:[PKUOStyleKit imageOfIconTabBarMap]];
                [item setTitle:TRANSLATE_UP(@"map")];
                break;
            case 1:
                [item setImage:[PKUOStyleKit imageOfIconTabBarDiscover]];
                [item setTitle:TRANSLATE_UP(@"discover")];
                break;
            case 2:
                [item setImage:[PKUOStyleKit imageOfIconTabBarTour]];
                [item setTitle:TRANSLATE_UP(@"tours")];
                break;

        }
        index++;
    }
    
    
//    [[[self items] objectAtIndex:0] setImage:[PKUOStyleKit imageOfIconTabBarMap]];
//    [[[self items] objectAtIndex:1] setImage:[PKUOStyleKit imageOfIconTabBarDiscover]];
//    [[[self items] objectAtIndex:2] setImage:[PKUOStyleKit imageOfIconTabBarTour]];
    
//#warning TRANSLATE
//    [[[self items] objectAtIndex:0] setTitle:TRANSLATE_UP(@"map")];
//    [[[self items] objectAtIndex:1] setTitle:TRANSLATE_UP(@"discover")];
//    [[[self items] objectAtIndex:2] setTitle:TRANSLATE_UP(@"tours")];
//#warning TRANSLATE
    // If you want to show static blured image, then just cut it from your psd and set to imageView
    // imageView.image = [UIImage imageNamed:@"static_blured_bg"];
    
    // If you want to show dynamic blur, that will blur everything behind it, then you must
    // 1. make a screenshot of viewController's view that is currently showing.
    // 2. crop it with frame of tab bar.
    // 3. make it blur
    // 4. set it to image view.
    // And finally, for make it dynamic, you mast repeat stepst 1-4 every time when layout changes. (e.g. you can make a timer with 0.3 secs of tick, or something else).
    

    // Choose yourself :)
}
//- (void)drawRect:(CGRect)rect {
//
//
//
//    // Drawing code
//    [super drawRect:rect];
//
//    //    [[[self items] objectAtIndex:0] setImage:[PKUOStyleKit imageOfIconTabBarMap]];
//    //    [[[self items] objectAtIndex:1] setImage:[PKUOStyleKit imageOfIconTabBarDiscover]];
//}
@end
