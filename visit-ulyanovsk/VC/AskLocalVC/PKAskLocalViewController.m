//
//  PKAskLocalViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 16.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKAskLocalViewController.h"
#import "PKInterfaceManager.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"
#import "PKMainViewController.h"
#import "PKServerManager.h"
#import "PKPlaceHolderTextView.h"
#import <QuartzCore/QuartzCore.h>

@interface PKAskLocalViewController () <UITextViewDelegate>
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) PKPlaceHolderTextView *textField;
@property (strong, nonatomic) PKPlaceHolderTextView *nameField;
@property (strong, nonatomic) PKPlaceHolderTextView *phoneField;
@property (strong, nonatomic) PKPlaceHolderTextView *emailField;
@property (strong, nonatomic) UIButton *btnSend;

@end

@implementation PKAskLocalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect scrollViewFrame = self.view.frame;
    self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame];
    
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    
//    scrollView.scrollsToTop = NO;
    
    
    
    
    UITextView * firstTextView = [[UITextView alloc] init];//WithFrame:CGRectMake(0, 56.f+regularFZMargin2, self.view.frame.size.width, 200.f)];
    
    firstTextView.font = [UIFont systemFontOfSize:17.0];
    firstTextView.textAlignment = NSTextAlignmentJustified;
    firstTextView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
    //        _subTitleLabel.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    firstTextView.contentInset = UIEdgeInsetsZero;
    firstTextView.selectable = NO;
    firstTextView.editable = NO;
    firstTextView.scrollEnabled = NO;
    firstTextView.backgroundColor = [UIColor clearColor];
    if(isLOCAL_RU){
        firstTextView.text = @"Мы хотим знать, что интересует туристов. Таким образом, мы пополняем раздел СОВЕТЫ, нашего приложения.\nПожалуйста, задайте Ваш вопрос:";
    } else {
        firstTextView.text = @"We want to know, what tourists are interested in. Thus, we replenish the TIPS section of our app. \nPlease, ask Your question:";
    }
    
    CGSize firstTextViewSize = [firstTextView sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin2 * 2.0), CGFLOAT_MAX)];
    firstTextView.frame = CGRectMake(regularFZMargin2, zoomTopMargin+regularFZMargin2, firstTextViewSize.width, firstTextViewSize.height);
    [_scrollView addSubview:firstTextView];
    
    
    
    self.nameField = [self makeOneLineTextFiledWithCGRect:CGRectMake(regularFZMargin2, firstTextView.frame.origin.y + firstTextView.frame.size.height + regularFZMargin2, firstTextViewSize.width, 44.f) andImage:[PKUOStyleKit imageOfIconBlockNameWithIconColor: [UIColor whiteColor]] andPlaceHolder: TRANSLATE_UP(@"name")];
    self.nameField.autocapitalizationType = YES;
    
    self.phoneField = [self makeOneLineTextFiledWithCGRect:CGRectMake(regularFZMargin2, [_nameField superview].frame.origin.y + [_nameField superview].frame.size.height + regularFZMargin2, firstTextViewSize.width, 44.f) andImage:[PKUOStyleKit imageOfIconBlockCallWithIconColor: [UIColor whiteColor]] andPlaceHolder: TRANSLATE_UP(@"phone_number")];
    self.phoneField.keyboardType = UIKeyboardTypePhonePad;
    
    self.emailField = [self makeOneLineTextFiledWithCGRect:CGRectMake(regularFZMargin2, [_phoneField superview].frame.origin.y + [_phoneField superview].frame.size.height + regularFZMargin2, firstTextViewSize.width, 44.f) andImage:[PKUOStyleKit imageOfIconBlockEmailWithIconColor: [UIColor whiteColor]] andPlaceHolder: TRANSLATE_UP(@"e-mail")];
    self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.textField = [self makeOneLineTextFiledWithCGRect:CGRectMake(regularFZMargin2, [_emailField superview].frame.origin.y + [_emailField superview].frame.size.height + regularFZMargin2, firstTextViewSize.width, 300.f) andImage:[PKUOStyleKit imageOfIconBlockTextWithIconColor: [UIColor whiteColor]] andPlaceHolder: TRANSLATE_UP(@"message")];
    
    
    
    self.btnSend = [[PKInterfaceManager sharedManager] returnButtonWithCGrect:CGRectMake(regularFZMargin2, [_textField superview].frame.origin.y + [_textField superview].frame.size.height + regularFZMargin2, firstTextViewSize.width, 56.f) andTitle:TRANSLATE_UP(@"Submit")];
    
    
    
    [_btnSend
     addTarget:self
     action:@selector(sendToServer) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:_btnSend];
    
    
//    CGSize scrollViewContentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
//    [scrollView setContentSize:scrollViewContentSize];

    [_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width, _btnSend.frame.origin.y + _btnSend.frame.size.height)];
    
    
    [self.view addSubview:_scrollView];
    
    
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:TRANSLATE(@"Ask_locals") andTitleHide:NO];
    [[PKInterfaceManager sharedManager] backgroundTwoRoundShapesInVC:self];

    
    // Do any additional setup after loading the view.
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([textView isEqual:self.textField]){
        return YES;
    } else if([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}

-(PKPlaceHolderTextView*)makeOneLineTextFiledWithCGRect:(CGRect) frame andImage:(UIImage*)image andPlaceHolder:(NSString*) placeholder{
    
    UIView *leftView = [[UIView alloc] initWithFrame:frame];
    leftView.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor];
    leftView.clipsToBounds = YES;
    leftView.layer.cornerRadius = 10.f;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(7.f, 7.f, 30.f, 30.f)];//frame.size.height /2 - 15.f
    imageView.image = image;
    [leftView addSubview:imageView];
    
    
    PKPlaceHolderTextView *oneLineTextFiled = [[PKPlaceHolderTextView alloc] initWithFrame:CGRectMake(44.f, 0, frame.size.width - regularFZMargin2, frame.size.height)];
    oneLineTextFiled.placeholder = placeholder;
    
    oneLineTextFiled.backgroundColor = [UIColor whiteColor];
    //    _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentTop;
    //    _nameField.layer.cornerRadius = 10.f;
    //    _nameField.clipsToBounds = YES;
    oneLineTextFiled.spellCheckingType = YES;
    oneLineTextFiled.font = [UIFont systemFontOfSize:17.0];
    oneLineTextFiled.textContainerInset = UIEdgeInsetsMake(12.f, 10.f, 10.f, 10.f);
//    oneLineTextFiled.autocapitalizationType = YES;
    
    oneLineTextFiled.delegate = self;
    [leftView addSubview:oneLineTextFiled];
    
    
    
    
    [_scrollView addSubview:leftView];
    
    return oneLineTextFiled;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)aTextView{
    
    [aTextView superview].layer.backgroundColor = [PKUOStyleKit bgColorGradientBottom].CGColor;
    
    [UIView animateWithDuration:0.25f animations:^{
        [aTextView superview].layer.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor].CGColor;
    } completion:NULL];
    
    
//    [aTextView superview].backgroundColor = [PKUOStyleKit lightGrayBackgroundColor];
//
//    [UIView animateWithDuration:1.f animations:^{
//        [[aTextView superview] setAlpha:1];
//    }];
//    [[aTextView superview] setNeedsDisplay];
    return YES;
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView{
//    [aTextView superview].backgroundColor = [PKUOStyleKit bgColorGradientBottom];
    [self.btnSend setTitle:TRANSLATE_UP(@"Submit") forState:UIControlStateNormal];
    [aTextView superview].layer.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor].CGColor;
    
    [UIView animateWithDuration:0.25f animations:^{
        [aTextView superview].layer.backgroundColor = [PKUOStyleKit bgColorGradientBottom].CGColor;
    } completion:NULL];
    
    
//    [UIView animateWithDuration:1.f animations:^{
//        [[aTextView superview] setAlpha:0];
//    }];
//    [[aTextView superview] setNeedsDisplay];
    //Has Focus
    return YES;
}

//- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
//    [textView superview].backgroundColor = [UIColor greenColor];
//    [[textView superview] setNeedsDisplay];
//    if ([text isEqualToString:@"\n"]){
//        //Lost Focus
//        [textView resignFirstResponder];
//    }
//    return YES;
//}




-(void)sendToServer{//:(id) sender{
//    UIButton *btn = sender;
    
    if(self.textField.text != nil && ![self.textField.text isEqualToString:@""]){
        [[PKServerManager sharedManager]
         sendCommercialRequestToServerForOrganization:nil
         withName:self.nameField.text
         andMessage:self.textField.text
         andPhone:self.phoneField.text
         andEmail:self.emailField.text
         onSuccess:^(NSDictionary *responseObject) {
             NSString* error = [NSString stringWithFormat:@"%@", [responseObject objectForKey:@"error"]];
//             NSLog(@"\n nn error = %@", error);
             if([error isEqualToString:@"0"]){
                 //                 NSAlert
                 [self.btnSend setTitle:TRANSLATE_UP(@"thank_you") forState:UIControlStateNormal];
                 self.nameField.editable = NO;
                 self.textField.editable = NO;
                 self.phoneField.editable = NO;
                 self.emailField.editable = NO;
                 self.btnSend.userInteractionEnabled = NO;
             } else {
                 [self.btnSend setTitle:TRANSLATE_UP(@"please,_try_again") forState:UIControlStateNormal];
             }
             // . NSLog(@"done");
             
         } onFailure:^(NSError *error, NSInteger statusCode) {
             [self.btnSend setTitle:TRANSLATE_UP(@"please,_try_again") forState:UIControlStateNormal];
             // . NSLog(@"Failure");
         }];
    } else {
        [self.textField becomeFirstResponder];
//        [btn setTitle:@"fill" forState:UIControlStateNormal];
    }
    
    
    

    
}


- (CGRect)textRectForBounds:(CGRect)bounds {
    int margin = 10;
    CGRect inset = CGRectMake(bounds.origin.x + margin, bounds.origin.y, bounds.size.width - margin, bounds.size.height);
    return inset;
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    int margin = 10;
    CGRect inset = CGRectMake(bounds.origin.x + margin, bounds.origin.y, bounds.size.width - margin, bounds.size.height);
    return inset;
}
-(void)popBackToMainVC:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self performSegueWithIdentifier:@"toMainVC" sender:sender];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.nameField becomeFirstResponder];
    if((_btnSend.frame.origin.y + _btnSend.frame.size.height) > self.view.frame.size.height){
        [self.scrollView setContentInset:UIEdgeInsetsMake(10.f, 0, 50.f, 0)];
    } else {
        [self.scrollView setContentInset:UIEdgeInsetsMake(10.f, 0, self.view.frame.size.height - (_btnSend.frame.origin.y + _btnSend.frame.size.height) + 50.f, 0)];
    }
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"toMainVC"]){
        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
        NSArray *viewContrlls=[navControl viewControllers];
        for( int i=0;i<[ viewContrlls count];i++){
            id vcs=[viewContrlls objectAtIndex:i];
            if([vcs isKindOfClass:[PKMainViewController class]]){
                
                PKMainViewController *vc = (PKMainViewController*)vcs;
                [vc setInstanceView:YES];
                return;
            }
        }
    }
    
}

@end
