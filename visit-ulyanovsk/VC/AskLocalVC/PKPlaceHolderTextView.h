//
//  PKPlaceHolderTextView.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 29.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>


IB_DESIGNABLE
@interface PKPlaceHolderTextView : UITextView

@property (nonatomic, retain) IBInspectable NSString *placeholder;
@property (nonatomic, retain) IBInspectable UIColor *placeholderColor;

-(void)textChanged:(NSNotification*)notification;

@end
