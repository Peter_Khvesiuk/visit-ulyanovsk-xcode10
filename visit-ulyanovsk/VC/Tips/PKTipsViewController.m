//
//  PKTipsViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKTipsViewController.h"
#import "PKInterfaceManager.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"

#import "PKMainViewController.h"

#import "PKTipsCollectionViewController.h"
#import "PKTipsPageViewController.h"
#import "PKInformTips+CoreDataClass.h"

#import "PKInformTipsDetailVC.h"

@interface PKTipsViewController ()
{
    PKTipsPageViewController *vc;
}
@property (strong, nonatomic) PKInformTips *informTipsTransiton;
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@end

@implementation PKTipsViewController
@synthesize showTips;
@synthesize imageViewTransition;
@synthesize collectionViewTransition;
-(void)dealloc{
    // . NSLog(@"\ndealloc PKTipsViewController");
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.delegate = self;
    
    NSString *title = [[NSString alloc] init];
    if(showTips){
        title = TRANSLATE(@"Tips");
    } else {
        title = TRANSLATE_UP(@"calendar");
    }
    
    
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self andTitle:title andTitleHide:NO];
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, 50.f, self.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [self.view insertSubview:backgroundInfoBlockImage atIndex:0];
    self.view.clipsToBounds = YES;
    
    
    NSDictionary* blocksArray = [self blocksArray];

    if(blocksArray !=nil){
        vc = [[PKTipsPageViewController alloc]initWithViewControllerClasses:blocksArray[@"classes"] andPredicates:blocksArray[@"predicate"] andViewController:self];
        vc.itemWidth = 90;
        vc.style = PKTipsTopViewStyleNOLine;//PKTopViewStyleLine;
        vc.topViewBackGroundColor = [UIColor clearColor];
        vc.itemWidthArray = blocksArray[@"width"];
        
        
        CGRect infoBlockFrame = CGRectMake(0.f, marginTopNavigationHeight + 44.f + 20.f, self.view.bounds.size.width,  self.view.bounds.size.height - (marginTopNavigationHeight + 44.f + 20.f));
        
        [self addChildViewController:vc];
        vc.view.frame = infoBlockFrame;
        [self.view addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    }
    
    
    
    
    
    
    
    // Do any additional setup after loading the view.
}

-(void)sendDataToA:(NSArray *)array{
    imageViewTransition = [array firstObject];
    collectionViewTransition = [array objectAtIndex:1];
    self.informTipsTransiton = [array lastObject];
    // . NSLog(@"sendDataToA");// data will come here inside of ViewControllerA
    [self performSegueWithIdentifier:@"toInformTipsDetailVC" sender:self];
}


-(void)popBackToMainVC:(id)sender{
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    self.navigationController.delegate = nil;
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
-(NSArray*)informTipsFromBd{
    return [[PKDataManager sharedManager] informTipsOrNewsFromBd:self.showTips];
}

-(NSDictionary*)blocksArray{
    NSMutableArray* classMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* titleMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* widthMutableArray = [[NSMutableArray alloc] init];
//    NSMutableArray* predicateMutableArray = [[NSMutableArray alloc] init];
   
    NSArray *informTipsFromBd = [self informTipsFromBd];
    if(informTipsFromBd == nil || [informTipsFromBd count] == 0){
        return nil;
    } else {
//        NSLog(@"informTipsFromBd = %@", informTipsFromBd);
        for (NSString *predicate in informTipsFromBd) {
            [classMutableArray addObject:[PKTipsCollectionViewController class]];
            [titleMutableArray addObject: predicate];// @"Транспорт"];
            [widthMutableArray addObject:@([self lenghtForLabelFromString:TRANSLATE_UP(predicate)])];
            
//            [classMutableArray addObject:[PKTipsCollectionViewController class]];
//            [titleMutableArray addObject:@"Еда"];
//            [widthMutableArray addObject:@([self lenghtForLabelFromString:@"Еда"])];
//
//            [classMutableArray addObject:[PKTipsCollectionViewController class]];
//            [titleMutableArray addObject:@"Проживание"];
//            [widthMutableArray addObject:@([self lenghtForLabelFromString:@"Проживание"])];
//
//            [classMutableArray addObject:[PKTipsCollectionViewController class]];
//            [titleMutableArray addObject:@"Шопинг"];
//            [widthMutableArray addObject:@([self lenghtForLabelFromString:@"Шопинг"])];
//
//            [classMutableArray addObject:[PKTipsCollectionViewController class]];
//            [titleMutableArray addObject:@"Деньги"];
//            [widthMutableArray addObject:@([self lenghtForLabelFromString:@"Деньги"])];
//
//            [classMutableArray addObject:[PKTipsCollectionViewController class]];
//            [titleMutableArray addObject:@"Медицинская помощь"];
//            [widthMutableArray addObject:@([self lenghtForLabelFromString:@"Медицинская помощь"])];
            
            
            
            
            
        }
    }
    NSMutableDictionary* mutableDictionary = [[NSMutableDictionary alloc] init];
    [mutableDictionary setValue:classMutableArray forKey:@"classes"];
    [mutableDictionary setValue:titleMutableArray forKey:@"predicate"];
    [mutableDictionary setValue:widthMutableArray forKey:@"width"];
    NSDictionary* dict = [[NSDictionary alloc] initWithDictionary:mutableDictionary];
    return dict;
}
-(CGFloat)lenghtForLabelFromString:(NSString*)string{
    // . NSLog(@"\nlenghtForLabel %@ = %lu %f", string, (unsigned long)[string length], [string length]*7.f);
    return ([string length]*10.f + 24.f);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toMainVC"]){
        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
        NSArray *viewContrlls=[navControl viewControllers];
        for( int i=0;i<[ viewContrlls count];i++){
            id vcs=[viewContrlls objectAtIndex:i];
            if([vcs isKindOfClass:[PKMainViewController class]]){
                
                PKMainViewController *vc = (PKMainViewController*)vcs;
                [vc setInstanceView:YES];
                return;
            }
        }
    } else if([segue.identifier isEqualToString:@"toInformTipsDetailVC"]){ 
        PKInformTipsDetailVC*vc = [segue destinationViewController] ;
        [vc setInformTip:self.informTipsTransiton];
        vc.isTips = self.showTips;
        
    }
}







#pragma mark - UINavigationControllerDelegate Methods

- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    // Sanity
    if (fromVC != self && toVC != self) return nil;
    
    // Determine if we're presenting or dismissing
    ZOTransitionType type = (fromVC == self) ? ZOTransitionTypePresenting : ZOTransitionTypeDismissing;
    
    // Create a transition instance with the selected cell's imageView as the target view
    ZOZolaZoomTransition *zoomTransition = [ZOZolaZoomTransition transitionFromView: imageViewTransition
                                                                               type:type
                                                                           duration:0.5
                                                                           delegate:self];
    zoomTransition.fadeColor = [UIColor colorWithWhite:0.94 alpha:1.0];
    
    return zoomTransition;
}

#pragma mark - ZOZolaZoomTransitionDelegate Methods

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
        startingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The starting frame is taken from the selected cell's imageView.
        return [imageViewTransition convertRect:imageViewTransition.bounds toView:relativeView];
    } else if ([fromViewController isKindOfClass:[PKInformTipsDetailVC class]]) {
        // We're popping back to this master controller. The starting frame is taken from the detailController's imageView.
        PKInformTipsDetailVC *detailController = (PKInformTipsDetailVC *)fromViewController;
        return [detailController.imageGalleryView
                convertRect:detailController.imageGalleryView.bounds toView:relativeView];
    }
    
    return CGRectZero;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
       finishingFrameForView:(UIView *)targetView
              relativeToView:(UIView *)relativeView
          fromViewController:(UIViewController *)fromViewController
            toViewController:(UIViewController *)toViewController {
    
    if (fromViewController == self) {
        // We're pushing to the detail controller. The finishing frame is taken from the detailController's imageView.
        PKInformTipsDetailVC *detailController = (PKInformTipsDetailVC *)toViewController;
    
        return [detailController.imageGalleryView convertRect:CGRectMake(0, marginTopNavigationHeight, detailController.imageGalleryView.bounds.size.width, detailController.imageGalleryView.bounds.size.height) toView:relativeView];
        
#warning Navigator bar height epxected as 22? Okay?
    } else if ([fromViewController isKindOfClass:[PKInformTipsDetailVC class]]) {
        // We're popping back to this master controller. The finishing frame is taken from the selected cell's imageView.
        return [imageViewTransition convertRect:imageViewTransition.bounds toView:relativeView];
    }
    
    return CGRectZero;
    
}

- (NSArray *)supplementaryViewsForZolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition {
    // Here we're returning all UICollectionViewCells that are clipped by the edge
    // of the screen. These will be used as "supplementary views" so that the clipped
    // cells will be drawn in their entirety rather than appearing cut off during the
    // transition animation.
    
    NSMutableArray *clippedCells = [NSMutableArray arrayWithCapacity:[[collectionViewTransition visibleCells] count]];
    for (UICollectionViewCell *visibleCell in collectionViewTransition.visibleCells) {
        CGRect convertedRect = [visibleCell convertRect:visibleCell.bounds toView:self.view];
        if (!CGRectContainsRect(self.view.frame, convertedRect)) {
            [clippedCells addObject:visibleCell];
        }
    }
    return clippedCells;
}

- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
   frameForSupplementaryView:(UIView *)supplementaryView
              relativeToView:(UIView *)relativeView {
    
    return [supplementaryView convertRect:supplementaryView.bounds toView:relativeView];
}


@end
