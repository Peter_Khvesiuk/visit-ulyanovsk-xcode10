//
//  PKInformTipsDetailVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAKImageGalleryView.h"

@class PKInformTips;

@interface PKInformTipsDetailVC : UIViewController
@property (strong, nonatomic) PKInformTips *informTip;
@property (weak, nonatomic) MAKImageGalleryView *imageGalleryView;
@property (nonatomic) BOOL isTips;
@property (nonatomic) int informTipId;

@end
