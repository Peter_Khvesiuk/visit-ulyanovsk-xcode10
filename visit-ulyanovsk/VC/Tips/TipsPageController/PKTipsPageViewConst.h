//
//  Header.h
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//




#define PKTipsItemTitleColorSelected [UIColor colorWithRed: 0 green: 0.796 blue: 0.996 alpha: 1]
//[UIColor colorWithRed:168.0/255.0 green:20.0/255.0 blue:4/255.0 alpha:1]
#define PKTipsItemTitleColorNormal   [UIColor colorWithRed: 0.318 green: 0.293 blue: 0.293 alpha: 1]
//[UIColor colorWithRed:0 green:0 blue:0 alpha:1]
//  Навигация по меню Цвет фона
#define PKTipsTopViewBGColor      [UIColor clearColor]
//[UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0]
//Подчеркивание по умолчанию цвет
#define PKTipsBottomLineColor              [UIColor redColor]
//Выберите пункт ширина по умолчанию
static CGFloat const PKTipsItemWidth        = 65.0f;
//Навигации меню по умолчанию высота
static CGFloat const PKTipsItemHeight       = 50.0f;
//  Название Размер(выбран/не выбран)
static CGFloat const PKTipsItemTitleFontSelected = 15.0f;
static CGFloat const PKTipsItemTitlefontNormal   = 15.0f;

//Подчеркивание по умолчанию высота
static CGFloat const PKTipsBottomLineHeight   = 0.f;


//Используется, чтобы установить значение тега метки 
static CGFloat const PKTipsTopLabelTag   = 8956;
