//
//  ASPageViewController.h
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKTipsPageViewConst.h"
#import "PKTipsTopView.h"
@class PKTipsViewController;
@interface PKTipsPageViewController : UIViewController
//Сверху по обе стороны пространства,например, можно использовать режим введите время, вы можете опустошить сегмент с кнопку Set вернуться и т. д.
@property (nonatomic, assign) CGFloat sideBothWidth;
/**
 *  В массив, чтобы установить ширину каждого элемента
 */
@property(nonatomic, strong) NSArray *itemWidthArray;
/**
 *  一Ключ для установки элемента Ширина
 */
@property (nonatomic, assign) CGFloat itemWidth;

//Установить верхний пытается style
@property (nonatomic, assign) PKTipsTopViewStyle style;

//Установить каждый пункт в Имя шрифта
@property (nonatomic, copy) NSString *titleName;
//Цвет подчеркивания
@property (nonatomic, strong) UIColor *lineColor;

//Не выбран, когда размер шрифта
@property (nonatomic, assign) CGFloat normalSize;
//Выбранный размер шрифта
@property (nonatomic, assign) CGFloat selectedSize;
//Не выбран цвет шрифта
@property (nonatomic, strong) UIColor *normalTitleColor;
//Выбранный цвет шрифта
@property (nonatomic, strong) UIColor *selectTitleColor;
//Верхняя часть представления цвета фона
@property (nonatomic, strong) UIColor *topViewBackGroundColor;
-(id)initWithViewControllerClasses:(NSArray<Class> *)classes andPredicates:(NSArray <NSString *> *)titles andViewController:(PKTipsViewController*)vc;
@end
