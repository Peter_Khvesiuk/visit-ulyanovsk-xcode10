
//
//  ASPageViewController.m
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//

#import "PKTipsPageViewController.h"
#import "PKTipsCollectionViewController.h"
#import "PKTipsViewController.h"

@interface PKTipsPageViewController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate, PKTipsTopViewDelegate>
{
    UIPageViewController *_pageViewController;
    NSArray<Class> *_classesArray;
    NSMutableArray *_vcArray;
    NSArray<NSString *> *_predicatesArray;
    //Запись текущего индекса
    NSInteger _currentVCIndex;
    //В верхней части экрана
    PKTipsTopView *_topView;
    //Сверху по обе стороны разрыва
    CGFloat _sideWidth;
    //Каждый элемент Ширина
    NSMutableArray *_itemWidthArrayInternal;
    //Установить переменную использовать для разделения нажать на слайд или боковой слайд слайд
    BOOL _isClick;
    //Для получения нажмите на метку индекса стоимости
    NSInteger _willToIndex;
//    UINavigationController* navController;
    BOOL _isTips;
}
@end

@implementation PKTipsPageViewController


-(void)setSideBothWidth:(CGFloat)sideWidth
{
    _sideWidth = sideWidth;
}
-(instancetype)init{
     // . NSLog(@"\nalloc init PKTipsPageViewController");
    if (self = [super init]) {
        
    }
    return self;
}
//Создать метод экземпляра
-(id)initWithViewControllerClasses:(NSArray<Class> *)classes andPredicates:(NSArray <NSString *> *)predicate andViewController:(PKTipsViewController*)vc
{
    
    // . NSLog(@"\nalloc initWithViewControllerClasses PKTipsPageViewController");
    if (self = [super init]) {
        NSAssert(classes.count == predicate.count, @"You set the number of the controller and the title number is inconsistent");
        _itemWidthArrayInternal = [NSMutableArray arrayWithCapacity:predicate.count];
        _vcArray = [NSMutableArray arrayWithCapacity:predicate.count];
        _classesArray = [NSArray arrayWithArray:classes];
        _predicatesArray = [NSArray arrayWithArray:predicate];
        _isTips = vc.showTips;
        self.style = PKTipsTopViewStyleLine;
        self.lineColor = PKTipsBottomLineColor;
        self.normalSize = PKTipsItemTitlefontNormal;
        self.selectedSize = PKTipsItemTitleFontSelected;
        self.normalTitleColor = PKTipsItemTitleColorNormal;
        self.selectTitleColor = PKTipsItemTitleColorSelected;
        //Попытки создать экземпляр
        [self creatVC:vc];
    }
    return self;
}
-(void)dealloc{
    // . NSLog(@"\ndealloc PKTipsPageViewController");
}
//MARK:Попытки создать экземпляр
-(void)creatVC:(UIViewController*)vc
{
    for (int i = 0; i < _classesArray.count; i++) {
//        id newVc = [[_classesArray[i] alloc]init];
        if([[_classesArray[i] class] isEqual:[PKTipsCollectionViewController class]]){
//            PKTipsCollectionViewController *newVcTips = [[PKTipsCollectionViewController alloc]initWithPredicate:_predicatesArray[i] andTips:_isTips];
            
            UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            PKTipsCollectionViewController *newVcTips= [storyBoard instantiateViewControllerWithIdentifier:@"TipsCollection"];
            newVcTips.predicate = _predicatesArray[i];
            newVcTips.isTips = _isTips;
//        }
        
//        if([newVc isKindOfClass: [PKTipsCollectionViewController class]]){
//            PKTipsCollectionViewController *newVcTips = (PKTipsCollectionViewController*)newVc;
            newVcTips.delegate = vc;
            [_vcArray addObject:newVcTips];
        } else {
            [_vcArray addObject:[[_classesArray[i] alloc]init]];
        }
    }
}

//Создание каждой части пользовательского интерфейса
-(void)creatUI
{
    //Экземпляр контроллера pageViewController
    [self creatPageViewController];
}

//MARK: Экземпляр контроллера pageViewController
-(void)creatPageViewController
{
    _pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    //Укажите первый ВК
    [_pageViewController setViewControllers:@[_vcArray[0]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    //Настроить прокси
    _pageViewController.delegate = self;
    _pageViewController.dataSource = self;
    //Набор pageViewControler кадр
    [self setPageViewControllerFrame];
    
    //В pageViewController над функцией scrollview добавить прокси
    for (UIView *view in _pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            [view setValue:self forKey:@"delegate"];
            break;
        }
    }
    [self.view addSubview:_pageViewController.view];
}

//MARK: Набор pageViewControler кадр
-(void)setPageViewControllerFrame
{
    //Нет навигации нулю
//    UIView *tabBar = self.tabBarController.tabBar ? self.tabBarController.tabBar : self.navigationController.toolbar;
    CGFloat height = 0.f;//tabBar && !tabBar.hidden ? CGRectGetHeight(tabBar.frame) : 0;
    _pageViewController.view.frame = CGRectMake(0, 0 + PKTipsItemHeight, kScreenWidth, kScreenHeight - height - PKTipsItemHeight);
//    if (self.navigationController.navigationBar) {
//        //По умолчанию 44 высота до верха зрения
//        _pageViewController.view.frame = CGRectMake(0, 64 + PKTipsItemHeight, kScreenWidth, kScreenHeight - 64 - height - PKTipsItemHeight);
//    }else{
//        _pageViewController.view.frame = CGRectMake(0, 20 + PKTipsItemHeight, kScreenWidth, kScreenHeight - 20 - height - PKTipsItemHeight);
//    }
}

//MARK: -- UIPageViewController Метод протокола
//Возвращает следующий ВК
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSInteger index = [_vcArray indexOfObject:viewController];
    if (index == _vcArray.count - 1) {
        return nil;
    }
    return _vcArray[index + 1];
}

//Вернуться к предыдущему VC
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSInteger index = [_vcArray indexOfObject:viewController];
    if (index == 0) {
        return nil;
    }
    return _vcArray[index - 1];
}

//После окончания вызова
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (completed && finished) {
        _currentVCIndex = [_vcArray indexOfObject:_pageViewController.viewControllers[0]];
    }
}

//MARK:scrollView Метод протокола
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > kScreenWidth * 2 || scrollView.contentOffset.x < 0) {
        return;
    }
    if (_isClick) {
        //Если это щелчок при реализации этого метода
        [_topView clickLabelMethod:(int)_currentVCIndex withContentOffSetX:scrollView.contentOffset.x withToIndex:(int)_willToIndex];
//        // . NSLog(@"PKPageViewController scrollViewDidScroll NO x = %f == %f", scrollView.contentOffset.x, kScreenWidth);
        if ((scrollView.contentOffset.x - kScreenWidth) == 0) {
            // . NSLog(@"PKPageViewController scrollViewDidScroll NO");
            _isClick = NO;
        }
    }else{
        [_topView hhhhhWithCurrent:(int)_currentVCIndex withconX:scrollView.contentOffset.x];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    if (self.itemWidthArray.count > 0) {
        NSAssert(_predicatesArray.count == self.itemWidthArray.count, @"You set the width of the item array not isEqual to the controllers count");
    }
    if (self.itemWidth > 0) {
        for (int i = 0; i < _predicatesArray.count; i++) {
            [_itemWidthArrayInternal addObject:@(self.itemWidth)];
        }
    }else{
        for (int i = 0; i < _predicatesArray.count; i++) {
            [_itemWidthArrayInternal addObject:@(PKTipsItemWidth)];
        }
    }
    //Создание каждой части сборки
    [self creatUI];
    
    //Создать вид сверху
    [self creatTopView];
}

//Создание верхней части View
-(void)creatTopView
{
    CGFloat topViewY = 0.f;
//    if (self.navigationController.navigationBar) {
//        topViewY = 64;
//    }else{
//        topViewY = 20;
//    }

    _topView = [[PKTipsTopView alloc]initWithFrame:CGRectMake(_sideWidth, topViewY, kScreenWidth - _sideWidth * 2, PKTipsItemHeight) andPredicates:_predicatesArray];
    if (self.topViewBackGroundColor) {
        _topView.topBackColor = self.topViewBackGroundColor;
    }
    _topView.delegate = self;
    _topView.style = self.style;
    _topView.lineColor = self.lineColor;
    _topView.normalSize = self.normalSize;
    _topView.selectedSize = self.selectedSize;
    _topView.normalItemTitleColor = self.normalTitleColor;
    _topView.selectItemTitleColor = self.selectTitleColor;
    if (_itemWidthArrayInternal.count > 0) {
        if (self.itemWidthArray.count > 0) {
            [_itemWidthArrayInternal removeAllObjects];
            [_itemWidthArrayInternal addObjectsFromArray:self.itemWidthArray];
        }
    }
    //Надо посчитать, если вы установите общая Ширина не достигает ширины интерфейса, на этот раз мы должны рассчитать, добавьте пробелы
    CGFloat max = 0.0;
    for (int i = 0; i < _itemWidthArrayInternal.count; i++) {
        max = max + [_itemWidthArrayInternal[i] floatValue];
    }
    _topView.itemWidthArray = [NSArray arrayWithArray:_itemWidthArrayInternal];
    [self.view addSubview:_topView];
}

-(void)scrollTopDestinationVC:(NSInteger)index
{
    _isClick = YES;
    _willToIndex = index;
    [_pageViewController setViewControllers:@[_vcArray[index]] direction:index < _currentVCIndex animated:YES completion:^(BOOL finished) {
        _currentVCIndex = index;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
