//
//  PKTipsViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZOZolaZoomTransition.h"
@class UICollectionView;

@interface PKTipsViewController : UIViewController<ZOZolaZoomTransitionDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic) UIImageView *imageViewTransition;
@property (strong, nonatomic) UICollectionView *collectionViewTransition;
@property (nonatomic) BOOL showTips;
@end
