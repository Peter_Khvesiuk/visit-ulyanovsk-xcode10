//
//  PKTipsCollectionViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKTipsCollectionViewController.h"

#import "PKServerManager.h"
#import "PKDataManager.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "PKUOStyleKit.h"
//#import "PKInterfaceManager.h"
//#import "PKTopNavButtonView.h"
#import "PKInformTips+CoreDataClass.h"

#import "PKInformTipsDetailVC.h"
#import "PKMapObj+CoreDataClass.h"

static NSString * PKTipsCellId           = @"PKTipsCell";
//static CGFloat zoomCellMargin          = 10.0;
//static CGFloat zoomCellSpacing         = 10.0;
//static CGFloat zoomCellTextAreaHeight  = 100.0;
//static CGFloat cellsInRow                     = 1.0;
//static CGFloat detailImageHeight              = 300.0;



@interface PKTipsCollectionViewController ()<NSFetchedResultsControllerDelegate>
//@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;


@property (nonatomic) CGFloat cellsInRow;
@property (nonatomic) CGFloat detailImageHeight;

@property NSMutableArray *sectionChanges;
@property NSMutableArray *itemChanges;

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;


@property (strong, nonatomic) NSPredicate * predicate1;
@property (strong, nonatomic) NSPredicate * predicate2;
@property (strong, nonatomic) NSNumber * flagForPredicatSelected1;
@property (strong, nonatomic) NSString * flagForPredicatSelected2;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (strong, nonatomic) PKTipsCell *selectedCell;

@end

@implementation PKTipsCollectionViewController
@synthesize predicate;
@synthesize delegate;
@synthesize isTips;
//-(instancetype)initWithPredicate:(NSString*)predicate andTips:(BOOL)isTips{
//    // . NSLog(@"\nalloc initWithPredicate PKTipsCollectionViewController");
////    self = [super init];
////    if (self) {
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        PKTipsCollectionViewController *vc= [storyBoard instantiateViewControllerWithIdentifier:@"TipsCollection"];
//        vc.predicate = predicate;
//        vc.isTips = isTips;
//        return vc;
////    }
////    return self;
//}

//- (instancetype)init
//{
//    // . NSLog(@"\nalloc init PKTipsCollectionViewController");
//    self = [super init];
//    if (self) {
//        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        
//        PKTipsCollectionViewController *vc= [storyBoard instantiateViewControllerWithIdentifier:@"TipsCollection"];
//        return vc;
//    }
//    return self;
//}

-(void)dealloc{
    // . NSLog(@"\ndealloc PKTipsCollectionViewController");
}

//-(instancetype)initWithCoder:(NSCoder *)aDecoder{
//    // . NSLog(@"ddf = %f",self.view.frame.size.width);
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
////    [flowLayout setSectionInset:UIEdgeInsetsMake(50.f, 0.f, 0.f, 0.f)];
//    self = [super initWithCollectionViewLayout:flowLayout];
//    if (self) {
//        //        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
//    }
//    return self;
//}


//- (instancetype)init{
//    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
//    //[flowLayout setSectionInset:UIEdgeInsetsMake(50.f, 0.f, 0.f, 0.f)];
//    self = [super initWithCollectionViewLayout:flowLayout];
//    if (self) {
////        self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
//    }
//    return self;
//}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.detailImageHeight = zoomDetailImageHeight;
    self.cellsInRow = zoomCellsTips;
//    #warning delegate????????
//    self.navigationController.delegate = self;
    
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    self.collectionView.backgroundColor = [UIColor colorWithWhite:0.94 alpha:1.0];//clearColor];//
    [self.collectionView registerClass:[PKTipsCell class] forCellWithReuseIdentifier:PKTipsCellId];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDelegate & Data Source Methods
//
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    // . NSLog(@"numberOfRowsInSection = %lu", (unsigned long)[sectionInfo numberOfObjects]);
    //warning Incomplete implementation, return the number of items
    return [sectionInfo numberOfObjects];
    //    return [_products count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PKTipsCell *cell = (PKTipsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PKTipsCellId forIndexPath:indexPath];
    if (!cell) {
        cell = [[PKTipsCell alloc] init];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedCell = (PKTipsCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSArray* sendData = [[NSArray alloc] initWithObjects:self.selectedCell.imageView, self.collectionView, (PKInformTips *) [self.fetchedResultsController objectAtIndexPath:indexPath], nil];
    
    [delegate sendDataToA:sendData];
//    [self performSegueWithIdentifier:@"toInformTipsDetailVC" sender:self.selectedCell];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
//    if([segue.identifier isEqualToString:@"toInformTipsDetailVC"]){
//
//        NSIndexPath *indexPath = [self.collectionView indexPathForCell:sender];
//        PKInformTipsDetailVC*vc = [segue destinationViewController] ;
//        [vc setInformTip:(PKInformTips *) [self.fetchedResultsController objectAtIndexPath:indexPath]];
//
//    }
    
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    // Cell width is half the screen width - edge margin - half the center margin
    
//    PKTipsCell *cell = (PKTipsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:PKTipsCellId forIndexPath:indexPath];
    
    PKInformTips *infoTip = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    CGFloat width = (self.collectionView.frame.size.width / self.cellsInRow) - zoomCellMargin - (zoomCellSpacing / self.cellsInRow);
    CGFloat newHeight = [PKTipsCell heightForText:infoTip.title andWidth:width];
//    if(newHeight < cellTextAreaHeightTips){
//        newHeight = cellTextAreaHeightTips;
//    }
    
    
    
    
    CGFloat height = ((self.detailImageHeight / self.collectionView.frame.size.width) * width) + newHeight;
    return CGSizeMake(width, height);
}

#pragma mark - UICollectionViewDelegateFlowLayout Methods

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0.f, zoomCellMargin, 70.f, zoomCellMargin);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return zoomCellSpacing;
}

//#pragma mark - UINavigationControllerDelegate Methods
//
//- (id <UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
//    // Sanity
//    if (fromVC != self && toVC != self) return nil;
//    
//    // Determine if we're presenting or dismissing
//    ZOTransitionType type = (fromVC == self) ? ZOTransitionTypePresenting : ZOTransitionTypeDismissing;
//    
//    // Create a transition instance with the selected cell's imageView as the target view
//    ZOZolaZoomTransition *zoomTransition = [ZOZolaZoomTransition transitionFromView:_selectedCell.imageView
//                                                                               type:type
//                                                                           duration:0.5
//                                                                           delegate:self];
//    zoomTransition.fadeColor = self.collectionView.backgroundColor;
//    
//    return zoomTransition;
//}
//
//#pragma mark - ZOZolaZoomTransitionDelegate Methods
//
//- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
//        startingFrameForView:(UIView *)targetView
//              relativeToView:(UIView *)relativeView
//          fromViewController:(UIViewController *)fromViewController
//            toViewController:(UIViewController *)toViewController {
//    
//    if (fromViewController == self) {
//        // We're pushing to the detail controller. The starting frame is taken from the selected cell's imageView.
//        return [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView];
//    } else if ([fromViewController isKindOfClass:[PKInformTipsDetailVC class]]) {
//        // We're popping back to this master controller. The starting frame is taken from the detailController's imageView.
//        PKInformTipsDetailVC *detailController = (PKInformTipsDetailVC *)fromViewController;
//        return [detailController.imageGalleryView
//                convertRect:detailController.imageGalleryView.bounds toView:relativeView];
//    }
//    
//    return CGRectZero;
//}
//
//- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
//       finishingFrameForView:(UIView *)targetView
//              relativeToView:(UIView *)relativeView
//          fromViewController:(UIViewController *)fromViewController
//            toViewController:(UIViewController *)toViewController {
//    
//    if (fromViewController == self) {
//        // We're pushing to the detail controller. The finishing frame is taken from the detailController's imageView.
//        PKInformTipsDetailVC *detailController = (PKInformTipsDetailVC *)toViewController;
//        return [detailController.imageGalleryView convertRect:CGRectMake(0, 0, detailController.imageGalleryView.bounds.size.width, detailController.imageGalleryView.bounds.size.height) toView:relativeView];
//#warning Navigator bar height epxected as 0? Okay?
//    } else if ([fromViewController isKindOfClass:[PKInformTipsDetailVC class]]) {
//        // We're popping back to this master controller. The finishing frame is taken from the selected cell's imageView.
//        return [_selectedCell.imageView convertRect:_selectedCell.imageView.bounds toView:relativeView];
//    }
//    
//    return CGRectZero;
//}
//
//- (NSArray *)supplementaryViewsForZolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition {
//    // Here we're returning all UICollectionViewCells that are clipped by the edge
//    // of the screen. These will be used as "supplementary views" so that the clipped
//    // cells will be drawn in their entirety rather than appearing cut off during the
//    // transition animation.
//    
//    NSMutableArray *clippedCells = [NSMutableArray arrayWithCapacity:[[self.collectionView visibleCells] count]];
//    for (UICollectionViewCell *visibleCell in self.collectionView.visibleCells) {
//        CGRect convertedRect = [visibleCell convertRect:visibleCell.bounds toView:self.view];
//        if (!CGRectContainsRect(self.view.frame, convertedRect)) {
//            [clippedCells addObject:visibleCell];
//        }
//    }
//    return clippedCells;
//}
//
//- (CGRect)zolaZoomTransition:(ZOZolaZoomTransition *)zoomTransition
//   frameForSupplementaryView:(UIView *)supplementaryView
//              relativeToView:(UIView *)relativeView {
//    
//    return [supplementaryView convertRect:supplementaryView.bounds toView:relativeView];
//}








#pragma mark - Collection view content controll

- (void)refershControlAction{
    [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(BOOL isComplete){
        if(isComplete){
            [self.refreshControl endRefreshing];
            //            [self.collectionView reloadData];
            [self reloadwithPredicateDefault];
        }
        
    } inProgress:^(CGFloat progress) {
        // . NSLog(@"progress == %f", progress);
    } onFailure:^(NSError* error, NSInteger statusCode) {
        [self.refreshControl endRefreshing];
    }];
}
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    _sectionChanges = [[NSMutableArray alloc] init];
    _itemChanges = [[NSMutableArray alloc] init];
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKInformTips"
                inManagedObjectContext:self.managedObjectContext];
    
    [fetchRequest setEntity:description];
    
    
    
    
    NSSortDescriptor* sortAscending =
    [[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES];
    
    
//    self.selectionsTypes = [[PKDataManager sharedManager] getFilterTypeObjectsArray];
    
//    if(contentRecomended){
    _predicate1 = [NSPredicate predicateWithFormat:@"symbolCode == %@", predicate];
//    }
//    else {
//        _predicate1 = [NSPredicate predicateWithFormat:@"types.typeObjValue IN %@", self.selectionsTypes];
//    }
    [fetchRequest setSortDescriptors:@[sortAscending]];
    //    [fetchRequest setSortDescriptors:@[titleAscending,nameDescription]];
    
    //    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[_predicate1, _predicate2]];
    //    [fetchRequest setPredicate:predicate];
    [fetchRequest setPredicate:_predicate1];
    
    
    
    
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil//@"ageTo"
                                                   cacheName:nil];//@"Master"
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        // . NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    
    
    
    
    
    
    return _fetchedResultsController;
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    change[@(type)] = @(sectionIndex);
    [_sectionChanges addObject:change];
    
}

-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
      atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
     newIndexPath:(NSIndexPath *)newIndexPath {
    NSMutableDictionary *change = [[NSMutableDictionary alloc] init];
    switch(type) {
        case NSFetchedResultsChangeInsert:
            change[@(type)] = newIndexPath;
            break;
        case NSFetchedResultsChangeDelete:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeUpdate:
            change[@(type)] = indexPath;
            break;
        case NSFetchedResultsChangeMove:
            change[@(type)] = @[indexPath, newIndexPath];
            break;
    }
    [_itemChanges addObject:change];
}
- (void)reloadwithPredicateDefault {
    // Make sure to delete the cache
    // (using the name from when you created the fetched results controller)
    [NSFetchedResultsController deleteCacheWithName:nil];
    // Delete the old fetched results controller
    self.fetchedResultsController = nil;
    // TODO: Handle error!
    // This will cause the fetched results controller to be created
    // with the new predicate
    [self.fetchedResultsController performFetch:nil];
    [self.collectionView reloadData];
}


#pragma mark - UICollectionViewDataSource
- (NSManagedObjectContext*) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[PKDataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
}

//- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    // . NSLog(@"numberOfSectionsInCollectionView = %lu", [[self.fetchedResultsController sections] count]);
//    return [[self.fetchedResultsController sections] count];
//}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    [self.collectionView performBatchUpdates:^{
        for (NSDictionary *change in self->_sectionChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteSections:[NSIndexSet indexSetWithIndex:[obj unsignedIntegerValue]]];
                        break;
                    case NSFetchedResultsChangeMove:
                        // . NSLog(@"NSFetchedResultsChangeMove empty!!!!!!!!!");
                        break;
                    case NSFetchedResultsChangeUpdate:
                        // . NSLog(@"NSFetchedResultsChangeUpdate empty!!!!!!!!!");
                        break;
                }
            }];
        }
        for (NSDictionary *change in self->_itemChanges) {
            [change enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                NSFetchedResultsChangeType type = [key unsignedIntegerValue];
                switch(type) {
                    case NSFetchedResultsChangeInsert:
                        [self.collectionView insertItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeDelete:
                        [self.collectionView deleteItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeUpdate:
                        [self.collectionView reloadItemsAtIndexPaths:@[obj]];
                        break;
                    case NSFetchedResultsChangeMove:
                        [self.collectionView moveItemAtIndexPath:obj[0] toIndexPath:obj[1]];
                        break;
                }
            }];
        }
    } completion:^(BOOL finished) {
        self->_sectionChanges = nil;
        self->_itemChanges = nil;
    }];
}



//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
//    // . NSLog(@"numberOfRowsInSection = %lu", (unsigned long)[sectionInfo numberOfObjects]);
//    //warning Incomplete implementation, return the number of items
//    return [sectionInfo numberOfObjects];
//}

#pragma mark - ConfigureCell



-(void)configureCell:(PKTipsCell*) cell atIndexPath:(NSIndexPath*)indexPath{
    PKInformTips *infoTip = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    // . NSLog(@"cell height = %.f %.f", cell.bounds.size.height, cell.bounds.size.width);
    //    [cell setBackgroundColor: [UIColor redColor]];
    //    cell.mapObject = mapObj;
    //    [self configureCell:cell atIndexPath:indexPath];
    cell.imageView.image = nil;//[UIImage imageNamed:@"icon2.png"];// //
//    cell.imageView.backgroundColor = [UIColor clearColor];
    cell.titleLabel.text = infoTip.title;
//    cell.subTitleLabel.text = infoTip.descriptionss;
    
    
    //******
    cell.layer.masksToBounds = YES;
    cell.layer.cornerRadius = 10;
    //******
    //    cell.layer.shadowOffset = CGSizeMake(1, 0);
    //    cell.layer.shadowColor = [[UIColor blackColor] CGColor];
    //    cell.layer.shadowRadius = 5;
    //    cell.layer.shadowOpacity = .25;
    //    cell.contentView.layer.cornerRadius = 10.0;
    //    cell.contentView.layer.borderColor = [[UIColor clearColor] CGColor];
    //    cell.contentView.layer.masksToBounds = YES;
    //******
    
    //    cell.contentView.layer.borderWidth = 1.0;
    //    cell.contentView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    
//    [cell.likesButton setImageWithLikeEnabled:mapObj.mylike withLikeCount:mapObj.likes andIsDetail:NO];
//    [cell.likesButton addTarget:self
//                         action:@selector(pushBtnLike:)
//               forControlEvents:UIControlEventTouchUpInside];
    
    
    
    cell.imageView.backgroundColor = [UIColor lightGrayColor];
    NSString* dataPath;
    NSString* imageUrl;
    
    if([infoTip.previewImgName isEqualToString:@"no_image.jpg"] && infoTip.mapObjects != nil){
        PKMapObj *mapObject = [[infoTip.mapObjects allObjects] firstObject];
        dataPath = LOCAL_mapObjImagePathPreview(mapObject.prevImageName);
        imageUrl = URL_mapObjImagePathPreview(mapObject.prevImageName);
    } else {
        if(self.isTips){
            dataPath = LOCAL_informImagePathPreview(infoTip.previewImgName);
            imageUrl = URL_informImagePathPreview(infoTip.previewImgName);
        } else {
            dataPath = LOCAL_newsImagePathPreview(infoTip.previewImgName);
            imageUrl = URL_newsImagePathPreview(infoTip.previewImgName);
        }
    }
    
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        [cell.imageView setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
    } else {
        NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
        __weak PKTipsCell* weakCellImage = cell;
        //CellWherePhoto.trenerPhotoDetail.image = nil;
        [cell.imageView setImageWithURLRequest:requestwhereImage
                              placeholderImage:nil
                                       success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                           
                                           //         // . NSLog(@"requestwhereImage success");
                                           
                                           
                                           weakCellImage.imageView.image = image;
                                           
                                           //                                           weakCellImage.cellImageView.contentMode = UIViewContentModeScaleAspectFill;
                                           [weakCellImage layoutSubviews];
                                           
                                       }
                                       failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                           // . NSLog(@"failure imageUrl = %@",imageUrl);
                                       }];
    }
}

#pragma mark - Popup control

@end

#pragma mark - PKTipsCell Implementation

@interface PKTipsCell ()

@property (strong, nonatomic, readwrite) UIImageView *imageView;
//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;



@end

@implementation PKTipsCell

#pragma mark - Constructors

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.backgroundColor = [UIColor whiteColor];
        
        self.imageView = [[UIImageView alloc] init];
        _imageView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_imageView];
        
        self.titleLabel = [[UILabel alloc] init];
        _titleLabel.font = [UIFont systemFontOfSize:cellTextFontTitleTips];
        _titleLabel.numberOfLines = 0;
        [self.contentView addSubview:_titleLabel];
        
//        self.subTitleLabel = [[UITextView alloc] init];
//        _subTitleLabel.font = [UIFont systemFontOfSize:cellTextFontSubTitleTips];
//        _subTitleLabel.textAlignment = NSTextAlignmentJustified;
//        _subTitleLabel.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
////        _subTitleLabel.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
//        _subTitleLabel.contentInset = UIEdgeInsetsZero;
//        _subTitleLabel.selectable = YES;
//        _subTitleLabel.editable = NO;
//        _subTitleLabel.scrollEnabled = NO;
//        _subTitleLabel.backgroundColor = [UIColor clearColor];
//        [self.contentView addSubview:_subTitleLabel];
        
//        self.likesButton = [[PKButtonLike alloc] init];
//
//        [self.contentView addSubview:_likesButton];
        
        self.layer.borderWidth = 0.0;
        self.layer.borderColor = [UIColor colorWithWhite:0.9 alpha:1.0].CGColor;
        
        
    }
    return self;
}

#pragma mark - Setters

//- (void)setProduct:(ZOProduct *)product {
//    _product = product;
//    _titleLabel.text = product.title;
//    _imageView.image = [UIImage imageNamed:product.imageName];
//
//    [self setNeedsLayout];
//}

#pragma mark - Layout

- (void)layoutSubviews {
    [super layoutSubviews];

    CGFloat labelWidth = self.contentView.frame.size.width - 15.f*2;
    CGFloat labelHeight = [_titleLabel sizeThatFits:CGSizeMake(labelWidth, CGFLOAT_MAX)].height;
    
    //    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
    _imageView.frame = CGRectMake(0.f, 0.f, self.contentView.frame.size.width, self.contentView.frame.size.height - (labelHeight + 30.f));//cellTextAreaHeightTips);//zoomCellTextAreaHeight
    _imageView.contentMode = UIViewContentModeScaleAspectFill;
    _imageView.clipsToBounds = YES;
    
    _titleLabel.frame = CGRectMake(15.f, _imageView.frame.size.height +10.f, labelWidth, labelHeight);
    
    
//    CGFloat subTitleLabelWidth = self.contentView.frame.size.width - 12.f*2;
//    CGFloat subTitleLabelHeight = [_subTitleLabel sizeThatFits:CGSizeMake(subTitleLabelWidth, CGFLOAT_MAX)].height;
//    _subTitleLabel.frame = CGRectMake(12.f, _imageView.frame.size.height + labelHeight + 10.f, labelWidth, cellTextAreaHeightTips - labelHeight - 10.f);
    // . NSLog(@"fffh = %f", zoomCellTextAreaHeight - labelHeight - 10.f);
//    _likesButton.frame = CGRectMake(self.contentView.frame.size.width - zoomCellTextAreaHeight, self.contentView.frame.size.height - zoomCellTextAreaHeight, zoomCellTextAreaHeight, zoomCellTextAreaHeight);
}
+ (CGFloat) heightForText:(NSString*) text andWidth:(CGFloat)width {
    
    CGFloat offset = 15.0;
    
    UIFont* font = [UIFont systemFontOfSize:cellTextFontTitleTips];
    
    NSShadow* shadow = [[NSShadow alloc] init];
    NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraph setAlignment:NSTextAlignmentCenter];
    
    NSDictionary* attributes =
    [NSDictionary dictionaryWithObjectsAndKeys:
     font, NSFontAttributeName,
     paragraph, NSParagraphStyleAttributeName,
     shadow, NSShadowAttributeName, nil];
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width - 2 * offset, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];
    
    return CGRectGetHeight(rect) + 2 * offset;
}

@end
