//
//  PKTipsCollectionViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ZOZolaZoomTransition.h"


@protocol senddataProtocol <NSObject>

-(void)sendDataToA:(NSArray *)array; //I am thinking my data is NSArray, you can use another object for store your information.

@end



@interface PKTipsCollectionViewController : UICollectionViewController
//ZOZolaZoomTransitionDelegate, UINavigationControllerDelegate>

@property(nonatomic,assign)id delegate;
@property (strong, nonatomic) NSString* predicate;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (nonatomic) BOOL isTips;
//-(instancetype)initWithPredicate:(NSString*)predicate andTips:(BOOL)isTips;
//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;


@end
@interface PKTipsCell : UICollectionViewCell

//@property (strong, nonatomic) ZOProduct *product;
@property (strong, nonatomic, readonly) UIImageView *imageView;
@property (strong, nonatomic) UILabel *titleLabel;
+ (CGFloat) heightForText:(NSString*) text andWidth:(CGFloat)width;
//@property (strong, nonatomic) UITextView *subTitleLabel;
//@property (strong, nonatomic) PKButtonLike *likesButton;

@end
