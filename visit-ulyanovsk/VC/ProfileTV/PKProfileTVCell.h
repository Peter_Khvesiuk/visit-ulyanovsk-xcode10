//
//  PKProfileTVCell.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface PKProfileTVCell : UITableViewCell
//@property (strong, nonatomic) IBOutlet UIImageView *userImageView;
@property (strong, nonatomic) UILabel *userNameLabel;
@property (strong, nonatomic) UILabel *userSubNameLabel;
@property (strong, nonatomic) FBSDKProfilePictureView *facebookPictureView;
@property (strong, nonatomic) UIButton *login;//FBSDKLoginButton

@end
