//
//  PKAboutTextTableViewCell.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 30.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKAboutTextTableViewCell.h"

@implementation PKAboutTextTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textLabel.numberOfLines = 0;
//    self.textLabel.frame.
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (CGFloat) heightForText:(NSString*) text {
    
    CGFloat offset = 25.0;
    
    UIFont* font = [UIFont systemFontOfSize:17.f];
    
    NSShadow* shadow = [[NSShadow alloc] init];
    // shadow.shadowOffset = CGSizeMake(0, -1);
    // shadow.shadowBlurRadius = 0.5;
    
    NSMutableParagraphStyle* paragraph = [[NSMutableParagraphStyle alloc] init];
    [paragraph setLineBreakMode:NSLineBreakByWordWrapping];
    [paragraph setAlignment:NSTextAlignmentCenter];
    
    NSDictionary* attributes =
    [NSDictionary dictionaryWithObjectsAndKeys:
     font, NSFontAttributeName,
     paragraph, NSParagraphStyleAttributeName,
     shadow, NSShadowAttributeName, nil];
    
//    scscreenWidth = [[PKFrameSize sharedManager] getWidth];
    CGRect rect = [text boundingRectWithSize:CGSizeMake(kScreenWidth - 2 * offset, CGFLOAT_MAX)
                                     options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading
                                  attributes:attributes
                                     context:nil];
//    NSInteger imageHeight;
//    if(kScreenWidth==1024 || kScreenWidth==768 || kScreenWidth==1366){
//        imageHeight = 750;
//    } else {
//        imageHeight = 454;
//    }
    
    return CGRectGetHeight(rect) + 2 * offset;//+imageHeight + 8+50;
}

@end
