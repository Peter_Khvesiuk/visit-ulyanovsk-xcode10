//
//  PKVCLogoTableViewCell.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 30.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKVCLogoTableViewCell.h"
#import "PKUOStyleKit.h"
//#import "PKvectorConsultingLogo.h"
@interface PKVCLogoTableViewCell()
@property (strong, nonatomic) UIButton *vcButton;
@property (strong, nonatomic) UIButton *atButton;

@end

@implementation PKVCLogoTableViewCell
-(id)init{
    self = [super init];
    
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
        CGFloat left = (self.contentView.frame.size.width - 120.f - 120.f)/3;
        
        UIButton *btn1 = [[UIButton alloc] init];
        [btn1 setImage:[PKUOStyleKit imageOfLogoVCWithPressed:NO]  forState:UIControlStateNormal];
        [btn1 setImage:[PKUOStyleKit imageOfLogoVCWithPressed:YES]  forState:UIControlStateHighlighted];
        [btn1 addTarget:self action:@selector(goToVc) forControlEvents:UIControlEventTouchUpInside];
        self.vcButton = btn1;
        [self.contentView addSubview:self.vcButton];
        UIButton *btn2 = [[UIButton alloc] init];
        [btn2 setImage:[PKUOStyleKit imageOfLogoATWithPressed:NO]  forState:UIControlStateNormal];
        [btn2 setImage:[PKUOStyleKit imageOfLogoATWithPressed:YES]  forState:UIControlStateHighlighted];
        [btn2 addTarget:self action:@selector(goToAt) forControlEvents:UIControlEventTouchUpInside];
        self.atButton = btn2;
        [self.contentView addSubview:self.atButton];
    }
    return self;
}
-(id)initWithCoder:(NSCoder*)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
        CGFloat left = (self.contentView.frame.size.width - 120.f - 120.f)/3;
        
        UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(left, 20.f, 120.f, 120.f)];
        [btn1 setImage:[PKUOStyleKit imageOfLogoVCWithPressed:NO]  forState:UIControlStateNormal];
        [btn1 setImage:[PKUOStyleKit imageOfLogoVCWithPressed:YES]  forState:UIControlStateHighlighted];
        [btn1 addTarget:self action:@selector(goToVc) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn1];
        UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(left*2+120.f, 20.f, 120.f, 120.f)];
        [btn2 setImage:[PKUOStyleKit imageOfLogoATWithPressed:NO]  forState:UIControlStateNormal];
        [btn2 setImage:[PKUOStyleKit imageOfLogoATWithPressed:YES]  forState:UIControlStateHighlighted];
        [btn2 addTarget:self action:@selector(goToAt) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:btn2];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)goToVc{
    NSURL* url = [NSURL URLWithString:@"http://vc73.ru/?app18"];
    NSLog(@"Link : %@", url);
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}
-(void)goToAt{
    NSURL* url = [NSURL URLWithString:@"http://visit-ulyanovsk.ru/?app18"];
    NSLog(@"Link : %@", url);
    [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews {
    [super layoutSubviews];
    NSLog(@"self.contentView.frame.size.width = %f", self.contentView.frame.size.width);
    CGFloat left = (self.contentView.frame.size.width - 120.f - 120.f)/3;
    self.vcButton.frame = CGRectMake(left, 20.f, 120.f, 120.f);
    self.atButton.frame = CGRectMake(left*2+120.f, 20.f, 120.f, 120.f);
}

@end
