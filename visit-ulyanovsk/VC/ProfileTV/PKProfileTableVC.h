//
//  PKProfileTableVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@protocol senddataProtocol <NSObject>

-(void)sendDataToController:(NSString *)str; //I am thinking my data is NSArray, you can use another object for store your information.

@end
@interface PKProfileTableVC : UITableViewController <NSFetchedResultsControllerDelegate>
@property (nonatomic,assign) id delegate;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext* managedObjectContext;
@property (nonatomic,assign) BOOL showAbout;

@end
