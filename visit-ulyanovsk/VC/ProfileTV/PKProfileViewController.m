//
//  PKProfileViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 15.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKProfileViewController.h"
#import "PKInterfaceManager.h"
#import "PKProfileTableVC.h"
#import "PKUOStyleKit.h"

#import "PKMainViewController.h"

@interface PKProfileViewController ()<senddataProtocol>
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@end

@implementation PKProfileViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    PKProfileTableVC *tableController = (PKProfileTableVC*)[mainStoryboard
                                                       instantiateViewControllerWithIdentifier: @"ProfileTableVC"];
    tableController.delegate = self;
    CGRect tableFrame = CGRectMake(0, marginTopNavigationHeight, self.view.frame.size.width, self.view.frame.size.height - marginTopNavigationHeight);
    [self addChildViewController:tableController];
    tableController.view.frame = tableFrame;
    tableController.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableController.tableView];
    [tableController didMoveToParentViewController:self];
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, 50.f, self.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [self.view insertSubview:backgroundInfoBlockImage atIndex:0];
    self.view.clipsToBounds = YES;
    
    
    // Do any additional setup after loading the view.
}
-(void)popBackToMainVC:(id)sender{
    [self.navigationController popToRootViewControllerAnimated:YES];
//    [self performSegueWithIdentifier:@"toMainVC" sender:sender];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
-(void)sendDataToController:(NSString *)str{
    if([str isEqualToString:@"toMainVC"]){
        [delegate needToDownloadData];
        [self popBackToMainVC:self];
    }
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toMainVC"]){
        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
        NSArray *viewContrlls=[navControl viewControllers];
        for( int i=0;i<[ viewContrlls count];i++){
            id vcs=[viewContrlls objectAtIndex:i];
            if([vcs isKindOfClass:[PKMainViewController class]]){
                
                PKMainViewController *vc = (PKMainViewController*)vcs;
                [vc setInstanceView:YES];
                return;
            }
        }
    }
}


@end
