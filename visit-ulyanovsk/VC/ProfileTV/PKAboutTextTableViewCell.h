//
//  PKAboutTextTableViewCell.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 30.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKAboutTextTableViewCell : UITableViewCell

+ (CGFloat) heightForText:(NSString*) text;

@end
