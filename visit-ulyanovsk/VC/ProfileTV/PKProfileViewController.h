//
//  PKProfileViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 15.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol downloadDataProtocol <NSObject>

-(void)needToDownloadData; //I am thinking my data is NSArray, you can use another object for store your information.

@end

@interface PKProfileViewController : UIViewController

@property (nonatomic,assign) id delegate;

@end
