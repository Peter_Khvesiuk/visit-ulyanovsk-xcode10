//
//  PKProfileTableVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKProfileTableVC.h"

#import "PKServerManager.h"
#import "PKDataManager.h"

#import "PKProfileTVCell.h"
#import "PKVCLogoTableViewCell.h"
#import "PKAboutTextTableViewCell.h"

#import "PKUserOptions+CoreDataClass.h"
#import "PKUserDocs+CoreDataClass.h"
#import "PKUser+CoreDataClass.h"
#import "PKInformation+CoreDataClass.h"


#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import "PKUOStyleKit.h"
#import "PKInterfaceManager.h"
#import "PKAboutViewController.h"
#import "PKMainViewController.h"



#import <Mapbox/Mapbox.h>


@interface PKProfileTableVC ()

//@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@property (strong, nonatomic) NSMutableArray* groupsArray;
@property (nonatomic) BOOL mapWasObservered;
@property (nonatomic) BOOL deleteMapPacks;
@property (strong, nonatomic) NSPredicate * predicate;
@property (nonatomic, strong) NSSortDescriptor *sortDescriptors;
@end

@implementation PKProfileTableVC

@synthesize delegate;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize showAbout;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.deleteMapPacks = NO;
    
    if(showAbout){
        [[PKInterfaceManager sharedManager] slimVC:self andTitle:TRANSLATE_UP(@"About_app")];
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    } else {
        [[PKInterfaceManager sharedManager] slimVC:self andTitle:TRANSLATE_UP(@"profile")];
    }
    
    
//     self.additionalSafeAreaInsets = UIEdgeInsetsMake(70.f, 0, 0, 0);
//    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];// andTitle:@"Профиль" andTitleHide:NO];
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.contentInsetAdjustmentBehavior = NO;//automatic
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self.tableView setContentInset:UIEdgeInsetsMake(zoomTopMargin, 0, 0, 0)];
    
    
    
    
//    [self.tableView setContentOffset:(CGPoint):UIEdgeInsetsMake(66.f, 0, 0, 0)];
//    [self.tableView reloadData];
    
//    self.groupsArray = [NSMutableArray array];
//    for (int i = 0; i < ((arc4random() % 6) + 5); i++) {
//        
//        ASGroup* group = [[ASGroup alloc] init];
//        group.name = [NSString stringWithFormat:@"Group #%d", i];
//        
//        NSMutableArray* array = [NSMutableArray array];
//        
//        for (int j = 0; j < ((arc4random() % 11) + 15); j++) {
//            [array addObject:[ASStudent randomStudent]];
//        }
//        
//        group.students = array;
//        
//        [self.groupsArray addObject:group];
//    }
    
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)configureCellWithPhoto:(PKProfileTVCell*) cell atIndexPath:(NSIndexPath *)indexPath {
    // . NSLog(@"isKindOfClass PKUser");
    PKUserOptions* userOptions = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if([userOptions.user.lastName isEqualToString:@"My"] && [userOptions.user.firstName isEqualToString:@"Name"]){
        cell.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", TRANSLATE_UP(@"My"), TRANSLATE_UP(@"name")];
    } else {
        cell.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", userOptions.user.lastName, userOptions.user.firstName];
    }
        
//        NSString *firstName = TRANSLATE_UP(@"name"); NSString *lastName = TRANSLATE_UP(@"My");
//    cell.userNameLabel.text = [NSString stringWithFormat:@"%@ %@", userOptions.user.lastName, userOptions.user.firstName];
    cell.userSubNameLabel.text = @"test";
    
    NSString* clientID = [[PKDataManager sharedManager] getClientID];

        
        
    if ([FBSDKAccessToken currentAccessToken] || ![clientID isEqualToString:@"null"]) {
        

        [self setTitle:TRANSLATE_UP(@"Log_out") forButton:cell.login];

        [cell.login addTarget:self
                       action:@selector(logOutFacebook:)
             forControlEvents:UIControlEventTouchUpInside];
        
        FBSDKProfilePictureView *profilePictureView = [[FBSDKProfilePictureView alloc] init];
        //        profilePictureView.frame = CGRectMake(0,0,100,100);
        profilePictureView.profileID = [[FBSDKAccessToken currentAccessToken] userID];
        cell.facebookPictureView = profilePictureView;
        
        [FBSDKProfile loadCurrentProfileWithCompletion:
         ^(FBSDKProfile *profile, NSError *error) {
             if (profile) {
                 // . NSLog(@"Table Hello, %@!", profile.firstName);
                 // . NSLog(@"userID, %@!", profile.userID);
             }
         }];
        
    } else {

        [self setTitle:TRANSLATE_UP(@"sign_up") forButton:cell.login];

        [cell.login addTarget:self
                       action:@selector(segueToLogin:)
             forControlEvents:UIControlEventTouchUpInside];
        
    }
}

- (void)configureCellPush:(UITableViewCell*) cell atIndexPath:(NSIndexPath *)indexPath{
    
    PKUserOptions* userOptions = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = userOptions.docs.title;
    cell.detailTextLabel.text = userOptions.docs.type;

    // . NSLog(@"isKindOfClass %@ ",userOptions.docs.title);
}
//- (void)configureCell:(id) cell atIndexPath:(NSIndexPath *)indexPath {
//
//    PKUserOptions *userOptions = [self.fetchedResultsController objectAtIndexPath:indexPath];
//
//    if(userOptions.assort == 1){
//        [self configureCellWithPhoto:cell atIndexPath:(NSIndexPath *)indexPath];
//    } else if(userOptions.assort == 2){
//        [self configureCellInfo:cell atIndexPath:(NSIndexPath *)indexPath];
//    }
////    if ([[self.fetchedResultsController objectAtIndexPath:indexPath] isKindOfClass:[PKUser class]]) {
////        [self configureCellWithPhoto:cell atIndexPath:(NSIndexPath *)indexPath];
////    } else if ([[self.fetchedResultsController objectAtIndexPath:indexPath] isKindOfClass:[PKUserInfo class]]){
////        [self configureCellInfo:cell atIndexPath:(NSIndexPath *)indexPath];
////    }
//}

-(void)setTitle:(NSString*) title forButton:(UIButton*) button{
    [button setTitle: title forState: UIControlStateNormal];
    [button setTitle: title forState: UIControlStateHighlighted];
}

- (void)logOutFacebook:(UIButton*) sender{
    
//    [self setTitle:@"Sing up" forButton:sender];
//    [sender addTarget:self
//                   action:@selector(segueToLogin:)
//         forControlEvents:UIControlEventTouchUpInside];
    
    // . NSLog(@"Facebook logout");
    [[FBSDKLoginManager new] logOut];
    [[PKDataManager sharedManager] logOutUser];
    
}
-(void)segueToLogin:(id) sender{
    [self performSegueWithIdentifier: @"showProfileLoginVC" sender: self];
}



//- (void)configureCellForEmptyCoreData:(PKProfileTVCell *)cell atIndexPath:(int)index {
//
//    cell.userNameLabel.text = @"My name";
//    cell.userSubNameLabel.text = @"test2";
//    [self setTitle:@"Sing up" forButton:cell.login];
//    [cell.login addTarget:self
//                   action:@selector(segueToLogin:)
//         forControlEvents:UIControlEventTouchUpInside];
//
//}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    PKUserOptions *userOptions= [self.fetchedResultsController objectAtIndexPath:indexPath];
    if (userOptions.assort == 1){
        return 200;
    } else if (userOptions.assort == 5 && [userOptions.information.simbolCode isEqualToString:@"zzlogo"]){
        return 160;
    } else if (userOptions.assort == 5 && [userOptions.information.name isEqualToString:@"TextAboutApp"]){
        return [PKAboutTextTableViewCell heightForText:TRANSLATE(userOptions.information.simbolCode)];
    } else{
        return 64;
    }
    
}

#pragma mark - Table view data source
- (NSManagedObjectContext*) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[PKDataManager sharedManager] managedObjectContext];
    }
    return _managedObjectContext;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // . NSLog(@"numberOfSectionsInTableView = %lu", [[self.fetchedResultsController sections] count]);
    return [[self.fetchedResultsController sections] count]; //1
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    
    // . NSLog(@"numberOfRowsInSection = %lu", (unsigned long)[sectionInfo numberOfObjects]);
//    if([sectionInfo numberOfObjects] == 0){
//        return 1;
//    }
    return [sectionInfo numberOfObjects];
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    PKUserOptions *userOptions= [self.fetchedResultsController objectAtIndexPath:indexPath];
    switch(userOptions.assort) {
        case 1: {//CellWithPhoto
            [self configureCellWithPhoto:(PKProfileTVCell*)cell atIndexPath:(NSIndexPath *)indexPath];
        }
            break;
        case 2: {//CellInfo
            cell.textLabel.text = TRANSLATE_UP(userOptions.docs.title);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
            break;
        case 4: {//Push
            if([userOptions.information.name isEqualToString:@"Use_offline_mode"]){
                cell.textLabel.text = TRANSLATE_UP(userOptions.information.name);
                cell.detailTextLabel.text = TRANSLATE_UP(userOptions.information.simbolCode);
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
                cell.accessoryView = switchView;
                [switchView setOn:[[PKDataManager sharedManager] isUseOfflineMode] animated:NO];
                [switchView addTarget:self action:@selector(switchDownloadContent:) forControlEvents:UIControlEventValueChanged];
            } else {
                cell.textLabel.text = TRANSLATE_UP(userOptions.information.simbolCode);
            }
        }
            break;
        case 5: {//Push
            if([userOptions.information.name isEqualToString:@"zzlogo"]){
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            } else if([userOptions.information.name isEqualToString:@"TextAboutApp"]){
                cell.textLabel.text = TRANSLATE(userOptions.information.simbolCode);
                cell.backgroundColor = [UIColor clearColor];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            } else {
                cell.backgroundColor = [UIColor clearColor];
                if([userOptions.information.name isEqualToString:@"AppVersion"]){//app_version
                    NSString *version = [NSString stringWithFormat:@" - %@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
                    cell.textLabel.text = PK_st_TRANSLATE_UP(userOptions.information.simbolCode,version);
                } else {
                    cell.textLabel.text = TRANSLATE_UP(userOptions.information.simbolCode);
                }
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
        }
            break;
    }
}
- (id )tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PKUserOptions *userOptions= [self.fetchedResultsController objectAtIndexPath:indexPath];
    // . NSLog(@"userOptions indexPath = %@ assort = %hd", indexPath, userOptions.assort);
    switch(userOptions.assort) {
        case 1: {//User
            NSString* identifier = @"CellWithPhoto";
            PKProfileTVCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[PKProfileTVCell alloc] init];
            }
            [self configureCell:cell atIndexPath:indexPath];
            return cell;
        }
        break;
        case 2: {//Push
            NSString* identifier = @"CellInfo";
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] init];
            }
            [self configureCell:cell atIndexPath:indexPath];
           
            return cell;
            
            
        }
        break;
        case 4: {//Push
            
            PKUserOptions *userOptions = [self.fetchedResultsController objectAtIndexPath:indexPath];
            NSString* identifier;
            if([userOptions.information.simbolCode isEqualToString:@"About_app"]){
                identifier = @"CellForward";
            } else if([userOptions.information.name isEqualToString:@"Use_offline_mode"]){
                identifier = @"CellDetail";
            } else {
                identifier = @"CellBasic";
            }
            UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
            if (!cell) {
                cell = [[UITableViewCell alloc] init];
            }
            
            [self configureCell:cell atIndexPath:indexPath];
            return cell;
            
            
        }
            break;
        case 5: {//Push
            
            NSLog(@"userOptions.information.simbolCode %@", userOptions.information.simbolCode);
            if([userOptions.information.name isEqualToString:@"zzlogo"]){
                
                NSString* identifier = @"CellAboutLogo";
                PKVCLogoTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if (!cell) {
                    cell = [[PKVCLogoTableViewCell alloc] init];
                }
                [self configureCell:cell atIndexPath:indexPath];
                
                return cell;
            } else if([userOptions.information.name isEqualToString:@"TextAboutApp"]){
                
                NSString* identifier = @"CellAboutText";
                PKAboutTextTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if (!cell) {
                    cell = [[PKAboutTextTableViewCell alloc] init];
                }
                [self configureCell:cell atIndexPath:indexPath];
                return cell;
                
                    
                
            } else {
                
                NSString* identifier = @"CellBasic";
                UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
                if (!cell) {
                    cell = [[UITableViewCell alloc] init];
                }
                [self configureCell:cell atIndexPath:indexPath];
                
                return cell;
            }
                
            
            
            
            
        }
            break;
    }
    return nil;
}
-(void)choseVersionOnlineOrOffline{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle: ALERT_OFFLINE_Title
                                 message: [NSString stringWithFormat:@"%@%@", TRANSLATE_UP(@"Do_you_want_to_download_data_for_offline_mode"), @"?"]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle: ALERT_OFFLINE_YesBtnText
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    [[PKDataManager sharedManager] setUseOfflineMode:YES];
                                    [[PKDataManager sharedManager] setAskedToDownload:NO];
//                                    [self performSegueWithIdentifier:@"toMainVC" sender:self];
                                    [self->delegate sendDataToController:(NSString *)@"toMainVC"];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle: ALERT_OFFLINE_NoBtnText
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   
                                   [[PKDataManager sharedManager] setUseOfflineMode:NO];
                                   [[PKDataManager sharedManager] setAskedToDownload:YES];
                                   [self.tableView reloadData];
                                   //Handle no, thanks button
                               }];
    
    
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)doUWannaDeleteOflineData{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle: TRANSLATE_UP(@"Delete_offline_data")
                                 message: [NSString stringWithFormat:@"%@%@%@%@", TRANSLATE_UP(@"Do_you_want_to_delete_data"),@", ",TRANSLATE_UP(@"downloaded_for_offline_mode"), @"?"]
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton = [UIAlertAction
                                actionWithTitle: ALERT_OFFLINE_YesBtnText
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    //Handle your yes please button action here
                                    [alert dismissViewControllerAnimated:YES completion:nil];
                                    [self deleteOfflineData];
                                }];
    
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle: ALERT_OFFLINE_NoBtnText
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   [alert dismissViewControllerAnimated:YES completion:nil];
                                   [[PKDataManager sharedManager] setAskedToDownload:YES];
                                   [[PKDataManager sharedManager] setUseOfflineMode:YES];
                                   [self.tableView reloadData];
                                   //Handle no, thanks button
                               }];
    [alert addAction:yesButton];
    [alert addAction:noButton];
    [self presentViewController:alert animated:YES completion:nil];
    
}
-(void)deleteOfflineData{
    [[PKServerManager sharedManager] clearImagesDirectory];
    [[PKServerManager sharedManager] clearTmpDirectory];
    [self deleteAllMapPacks];
    [[PKDataManager sharedManager] setAskedToDownload:YES];
    [[PKDataManager sharedManager] setUseOfflineMode:NO];
}
- (void)switchDownloadContent:(id)sender {
    if([[PKDataManager sharedManager] isUseOfflineMode]){
        [self doUWannaDeleteOflineData];
    } else {
        [self choseVersionOnlineOrOffline];
    }
//    UISwitch *switchControl = sender;
    // . NSLog( @"The Download content switch is %@", switchControl.on ? @"ON" : @"OFF" );
}

//- (void)switchPushChanged:(id)sender {
//    UISwitch *switchControl = sender;
    // . NSLog( @"The switch is %@", switchControl.on ? @"ON" : @"OFF" );
//}


//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the specified item to be editable.
//    return NO;
//}
////удалить ячейку
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
//        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
//
//        NSError *error = nil;
//        if (![context save:&error]) {
//            // Replace this implementation with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            // . NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
//    }
//}

//- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // The table view should not be re-orderable.
//    return NO;
//}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKUserOptions"
                inManagedObjectContext:self.managedObjectContext];
    fetchRequest.includesSubentities = YES;
//    NSPredicate *predicate;
    NSSortDescriptor *sortDescriptor;
    if(!showAbout){ //== %i", 2
        _predicate = [NSPredicate predicateWithFormat:@"assort = %i OR assort = %i OR assort = %i", 1, 2, 4];
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assort" ascending:YES];
    } else {
        _predicate = [NSPredicate predicateWithFormat:@"assort = %i", 5];
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"information.simbolCode" ascending:YES];
    }
    
    
    [fetchRequest setPredicate:_predicate];//
    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"assort" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setEntity:description];
    NSFetchedResultsController *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil//@"assort"
                                                   cacheName:nil];//@"Master"
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        // . NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    

    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}
- (void)reloadwithPredicateDefault {

    [NSFetchedResultsController deleteCacheWithName:nil];
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}
- (void)reloadwithPredicate:(NSPredicate *)newPredicate  {
    _predicate = [newPredicate copy];
    [NSFetchedResultsController deleteCacheWithName:nil];
    self.fetchedResultsController = nil;
    [self.fetchedResultsController performFetch:nil];
    [self.tableView reloadData];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
//#warning No code
            break;
        case NSFetchedResultsChangeUpdate:
//#warning No code
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            // . NSLog(@"NSFetchedResultsChangeInsert");
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            // . NSLog(@"NSFetchedResultsChangeDelete");
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            // . NSLog(@"NSFetchedResultsChangeUpdate");
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            [tableView reloadData];
            break;
            
        case NSFetchedResultsChangeMove:
            // . NSLog(@"NSFetchedResultsChangeMove");
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PKUserOptions *userOptions = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if(userOptions.assort == 1){
        // . NSLog(@"didSelectRowAtIndexPath PKUser");
    } else if(userOptions.assort == 2){
        // . NSLog(@"isKindOfClass not PKUserDocs");
        NSURL* url = [NSURL URLWithString: [NSString stringWithFormat:@"%@%@", URL_main, userOptions.docs.url]];
        // . NSLog(@"Link : %@", url);
        [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:^(BOOL success) {
            if (success) {
                // . NSLog(@"Opened url");
            }
        }];
    } else if(userOptions.assort == 4){
        
        if([userOptions.information.simbolCode isEqualToString:@"About_app"]){
            [self performSegueWithIdentifier:@"toAboutVC" sender:userOptions];
            
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                     bundle: nil];
//            PKProfileTableVC *tableController = (PKProfileTableVC*)[mainStoryboard
//                                                                    instantiateViewControllerWithIdentifier: @"ProfileTableVC"];
//            self.
        }
        
    }
    

    
}
#pragma mark - API


- (void) getDataFromServer:(UIRefreshControl *)refreshControl {
    
    // . NSLog(@"bdupdate");
    
    
    // [self.refreshControl endRefreshing];
//    [[PKServerManager sharedManager] getMainJSONDataFromServerOnSuccess:^(NSArray *friends) {
//        
//    }
//                                                              onFailure:^(NSError *error, NSInteger statusCode) {
//                                                                  // . NSLog(@"error = %@, code = %ld", [error localizedDescription], (long)statusCode);
//                                                                  
//                                                                  
//                                                              }];
    
    
    
}


//- (void)dealloc {
//    // Remove offline pack observers.
//    if(self.mapWasObservered){
//        [[NSNotificationCenter defaultCenter] removeObserver:self];
//    }
//
//}
#pragma mark LOAD MAPBOX OFFLINE
-(void)deleteAllMapPacks{
    self.mapWasObservered = YES;
    
    [[MGLOfflineStorage sharedOfflineStorage] addObserver:self
              forKeyPath:@"packs"
                 options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                 context:NULL];
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    
    // . NSLog(@"\nobserveValueForKeyPath: %@\nofObject: %@\nchange: %@", keyPath, object, change);
    if([keyPath isEqualToString:@"packs"]){
        if(!self.deleteMapPacks){
            self.deleteMapPacks = YES;
            [self deleteOfflinePackDownload];
        }
       
    }
}
-(void) deleteOfflinePackDownload{
    //    NSArray *as = [[MGLOfflineStorage sharedOfflineStorage] packs];
    // . NSLog(@"coo delete 3 = %lu", (unsigned long)[[[MGLOfflineStorage sharedOfflineStorage] packs] count]);
    for(MGLOfflinePack *pack in [[MGLOfflineStorage sharedOfflineStorage] packs]){
        
//        NSDictionary *userInfo = [NSKeyedUnarchiver unarchiveObjectWithData:pack.context];
        // . NSLog(@"Offline pack “%@”",userInfo[@"name"]);
        [[MGLOfflineStorage sharedOfflineStorage] removePack:pack withCompletionHandler:^(NSError *error) {
            if (error != nil) {
                // The pack couldn’t be delete for some reason.
                // . NSLog(@"Error: %@", error.localizedFailureReason);
            }
        }];
        
        
        
    }
}

//#pragma mark NAVIGATION
//
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if([segue.identifier isEqualToString:@"toMainVC"]){
//        UINavigationController * navControl = (UINavigationController*)[segue destinationViewController];
//        NSArray *viewContrlls=[navControl viewControllers];
//        for( int i=0;i<[ viewContrlls count];i++){
//            id vcs=[viewContrlls objectAtIndex:i];
//            if([vcs isKindOfClass:[PKMainViewController class]]){
//
//                PKMainViewController *vc = (PKMainViewController*)vcs;
//                vc.needOfflineContent = YES;
//                return;
//            }
//        }
//    }
//}



@end
