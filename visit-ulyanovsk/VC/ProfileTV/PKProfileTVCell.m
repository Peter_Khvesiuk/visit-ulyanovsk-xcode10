//
//  PKProfileTVCell.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKProfileTVCell.h"
#import "PKUOStyleKit.h"





@implementation PKProfileTVCell
-(id)initWithCoder:(NSCoder*)aDecoder{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        self.backgroundColor = [UIColor clearColor];
        
        FBSDKProfilePictureView *facebookPictureView = [[FBSDKProfilePictureView alloc] initWithFrame:CGRectMake(15.f, 0.f, 180.f, 180.f)];
        facebookPictureView.backgroundColor = [UIColor whiteColor];
        facebookPictureView.layer.cornerRadius = 10.f;
        facebookPictureView.clipsToBounds = YES;
        //    facebookPictureView.layer.borderWidth = 1.f;
        //    facebookPictureView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        self.facebookPictureView = facebookPictureView;
        [self addSubview:self.facebookPictureView];
        // . NSLog(@"self.facebookPictureView.frame %f %f %f %f", self.facebookPictureView.frame.origin.x, self.facebookPictureView.frame.origin.y, self.facebookPictureView.frame.size.width, self.facebookPictureView.frame.size.height);
        //    self.facebookPictureView.frame = CGRectMake(0, 0, 200.f, 200.f);
        [self.facebookPictureView setNeedsDisplay];
        // . NSLog(@"self.facebookPictureView.frame %f %f %f %f", self.facebookPictureView.frame.origin.x, self.facebookPictureView.frame.origin.y, self.facebookPictureView.frame.size.width, self.facebookPictureView.frame.size.height);
        
        // . NSLog(@"self.facebookPictureView.frame %f - %f = %f", self.contentView.frame.size.width, self.facebookPictureView.frame.size.width, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3);
        
        UILabel *userNameLabel = [[UILabel alloc] init];//WithFrame:CGRectMake(self.facebookPictureView.frame.size.width + 15.f*2, 0.f, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3, 120.f)];
        userNameLabel.numberOfLines = 0;
        userNameLabel.font = [UIFont systemFontOfSize:19];
        
        self.userNameLabel = userNameLabel;
        [self addSubview:self.userNameLabel];
        
        
        UIButton* login = [[UIButton alloc] init];//WithFrame:CGRectMake(15.f + self.facebookPictureView.frame.size.width + 15.f, userNameLabel.frame.origin.y + userNameLabel.frame.size.height +15.f, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3, self.facebookPictureView.frame.size.height - (userNameLabel.frame.origin.y + userNameLabel.frame.size.height +15.f))];
        login.backgroundColor = [PKUOStyleKit bgColorGradientBottom];
        login.layer.cornerRadius = 10.f;
        login.clipsToBounds = YES;
        self.login = login;
        [self addSubview:self.login];
    }
    
    return self;
}
//- (void)awakeFromNib {
//    [super awakeFromNib];
//    self.backgroundColor = [UIColor clearColor];
//
//    FBSDKProfilePictureView *facebookPictureView = [[FBSDKProfilePictureView alloc] initWithFrame:CGRectMake(15.f, 0.f, 180.f, 180.f)];
//    facebookPictureView.backgroundColor = [UIColor whiteColor];
//    facebookPictureView.layer.cornerRadius = 10.f;
//    facebookPictureView.clipsToBounds = YES;
////    facebookPictureView.layer.borderWidth = 1.f;
////    facebookPictureView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
//    self.facebookPictureView = facebookPictureView;
//    [self addSubview:self.facebookPictureView];
//    // . NSLog(@"self.facebookPictureView.frame %f %f %f %f", self.facebookPictureView.frame.origin.x, self.facebookPictureView.frame.origin.y, self.facebookPictureView.frame.size.width, self.facebookPictureView.frame.size.height);
////    self.facebookPictureView.frame = CGRectMake(0, 0, 200.f, 200.f);
//    [self.facebookPictureView setNeedsDisplay];
//    // . NSLog(@"self.facebookPictureView.frame %f %f %f %f", self.facebookPictureView.frame.origin.x, self.facebookPictureView.frame.origin.y, self.facebookPictureView.frame.size.width, self.facebookPictureView.frame.size.height);
//
//    // . NSLog(@"self.facebookPictureView.frame %f - %f = %f", self.contentView.frame.size.width, self.facebookPictureView.frame.size.width, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3);
//
//    UILabel *userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.facebookPictureView.frame.size.width + 15.f*2, 0.f, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3, 120.f)];
//    userNameLabel.numberOfLines = 0;
//    userNameLabel.font = [UIFont systemFontOfSize:19];
//
//    self.userNameLabel = userNameLabel;
//    [self addSubview:self.userNameLabel];
//
//
//    UIButton* login = [[UIButton alloc] initWithFrame:CGRectMake(15.f + self.facebookPictureView.frame.size.width + 15.f, userNameLabel.frame.origin.y + userNameLabel.frame.size.height +15.f, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3, self.facebookPictureView.frame.size.height - (userNameLabel.frame.origin.y + userNameLabel.frame.size.height +15.f))];
//    login.backgroundColor = [PKUOStyleKit bgColorGradientBottom];
//    login.layer.cornerRadius = 10.f;
//    login.clipsToBounds = YES;
//    self.login = login;
//    [self addSubview:self.login];
    
    
    // Initialization code
//}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubviews {
    [super layoutSubviews];
    self.facebookPictureView.frame = CGRectMake(15.f, 0.f, 180.f, 180.f);
    self.userNameLabel.frame = CGRectMake(self.facebookPictureView.frame.size.width + 15.f*2, 0.f, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3, 120.f);
    //    _imageView.frame = CGRectMake(0.0, 0.0, self.contentView.frame.size.width, self.contentView.frame.size.width);
    self.login.frame = CGRectMake(15.f + self.facebookPictureView.frame.size.width + 15.f, self.userNameLabel.frame.origin.y + self.userNameLabel.frame.size.height +15.f, self.contentView.frame.size.width - self.facebookPictureView.frame.size.width - 15.f*3, self.facebookPictureView.frame.size.height - (self.userNameLabel.frame.origin.y + self.userNameLabel.frame.size.height +15.f));
}


@end
