//
//  PKAboutViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 16.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKAboutViewController.h"
#import "PKProfileTableVC.h"
#import "PKInterfaceManager.h"
#import "PKUOStyleKit.h"

@interface PKAboutViewController ()
@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;
@end

@implementation PKAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    PKProfileTableVC *tableController = (PKProfileTableVC*)[mainStoryboard
                                                            instantiateViewControllerWithIdentifier: @"ProfileTableVC"];
    [tableController setShowAbout:YES];
    
    CGRect tableFrame = CGRectMake(0, marginTopNavigationHeight, self.view.frame.size.width, self.view.frame.size.height - marginTopNavigationHeight);
    [self addChildViewController:tableController];
    tableController.view.frame = tableFrame;
    tableController.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableController.tableView];
    [tableController didMoveToParentViewController:self];
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:self];
    
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, 50.f, self.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [self.view insertSubview:backgroundInfoBlockImage atIndex:0];
    self.view.clipsToBounds = YES;
}
-(void)popBack:(id)sender{
    // . NSLog(@"popBack");
    UIButton* btn = (UIButton*)sender;
    CGRect frame = btn.frame;
    frame.origin.x = - btn.frame.size.width;
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         btn.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [[self navigationController] popViewControllerAnimated:YES];
                     }];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.topNavButtonView.open = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
