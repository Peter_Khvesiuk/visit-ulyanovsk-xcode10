//
//  PKInfoBlockViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 09.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoBlockViewController.h"
#import "PKUOStyleKit.h"
#import "UILabel+Copyable.h"

@interface PKInfoBlockViewController ()

//@property (nonatomic, strong) UIViewController *vc;

@end

@implementation PKInfoBlockViewController

@synthesize delegate;
@synthesize viewBg;
@synthesize btnBg;
@synthesize mapObject;
@synthesize geoRoute;
@synthesize informTips;

- (instancetype)initWithObject:(id)object andVC:(UIViewController*)vc
{
    self = [super init];
    if (self) {
//        self.vc = vc;
        self.delegate = vc;

        if([object isKindOfClass:[PKMapObj class]]){
            self.mapObject =  (PKMapObj *)object;
        } else if([object isKindOfClass:[PKGeoRoute class]]){
            self.geoRoute =  (PKGeoRoute *)object;
        } else if([object isKindOfClass:[PKInformTips class]]){
            self.informTips = (PKInformTips *)object;
        } else if([object isKindOfClass:[PKEvent class]]){
            self.event = (PKEvent *)object;
        }
        
        
    }
    return self;
}
//- (instancetype)initWithGeoRoute:(PKGeoRoute*)geoRoute andVC:(UIViewController*)vc{
//    self = [super init];
//    if (self) {
//        //        self.vc = vc;
//        self.delegate = vc;
//        self.geoRoute = geoRoute;
//    }
//    return self;
//}
-(void)tapShowOnMapIsTappedWithInformTips{
    // . NSLog(@"JKD delegate = %@", delegate);
    if(self.informTips != nil){
         [delegate showOnMapWithMapObjects:self.informTips.mapObjects];
    } else if(self.event != nil) {
         [delegate showOnMapWithMapObjects:self.event.mapObjects];
    }
   
}
-(void)tapShowOnMapIsTapped{
    // . NSLog(@"JKD delegate = %@", delegate);
    [delegate showOnMap];
}
- (void)viewDidLoad {
    [super viewDidLoad];
//    // . NSLog(@"\ninfoblockviewcontroller2 width = %f", self.view.frame.size.width);
//    // . NSLog(@"\ninfoblockviewcontroller2 width = %f", self.view.bounds.size.width);
    
    
    
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    // . NSLog(@"\ninfoblockviewcontroller2 viewDidAppear width = %f", self.view.frame.size.width);
//    // . NSLog(@"\ninfoblockviewcontroller2 viewDidAppear width = %f", self.view.bounds.size.width);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(CGSize)addInfoLabelWithString:(NSString*) string{
    UILabel *infoLabel = [[UILabel alloc] init];
    infoLabel.font = [UIFont systemFontOfSize:14.0];
    infoLabel.numberOfLines = 0;
    infoLabel.text = string;
    infoLabel.textColor = [UIColor grayColor];
    CGSize adressLabelSize = [infoLabel sizeThatFits:CGSizeMake((self.viewBg.frame.size.width - 12.f * 2.0), CGFLOAT_MAX)];
    infoLabel.frame = CGRectMake(12.f, 10.f, adressLabelSize.width, adressLabelSize.height);
    [self.viewBg addSubview:infoLabel];
    return adressLabelSize;
}
-(void)addTitle:(NSString*)title titleCopyable:(BOOL)copyable andInfoString:(NSString*) string{
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:16.0];
    titleLabel.numberOfLines = 3;
    if(![string isEqualToString:@""]){
        
       
        CGSize infoLabelSize =  [self addInfoLabelWithString:string];
        
        
        titleLabel.text = title;
        titleLabel.copyingEnabled = copyable;
        CGSize titleLabelSize = [titleLabel sizeThatFits:CGSizeMake((self.viewBg.frame.size.width - 12.f * 2.0), CGFLOAT_MAX)];
        titleLabel.frame = CGRectMake(12.f, infoLabelSize.height + 12.f, titleLabelSize.width, titleLabelSize.height);
    } else {
        titleLabel.text = title;
        CGSize titleLabelSize = [titleLabel sizeThatFits:CGSizeMake((self.viewBg.frame.size.width - 12.f * 2.0), CGFLOAT_MAX)];
        titleLabel.frame = CGRectMake(12.f, 20.f, titleLabelSize.width, titleLabelSize.height);
    }
    
    [self.viewBg addSubview:titleLabel];
}
-(NSString*)addSiteUrl:(NSString*)siteUrlFull titleCopyable:(BOOL)copyable andInfoString:(NSString*) string{
    
    NSString *prefixUrl = @"http://";
    NSString *siteUrl = siteUrlFull;
    if([[siteUrlFull lowercaseString] hasPrefix:prefixUrl]){
        siteUrl = [siteUrlFull stringByReplacingOccurrencesOfString:@"http://" withString:@""];
    } else if([[siteUrlFull lowercaseString] hasPrefix:@"https://"]){
        prefixUrl = @"https://";
        siteUrl = [siteUrlFull stringByReplacingOccurrencesOfString:@"https://" withString:@""];
    }
    
    
    NSString *folderPath;
    NSString *domain;
    NSRange rangeSlash = [siteUrl rangeOfString: @"/"];
    
    if(rangeSlash.length == 0){
        folderPath = @"";
        domain = siteUrl;
    } else {
        folderPath = [siteUrl substringFromIndex:rangeSlash.location];
        domain = [siteUrl stringByReplacingOccurrencesOfString:folderPath withString:@""];
    }
    
    
//    if(folderPath.length > 1){
//        folderPath = [NSString stringWithFormat:@"%@...",[folderPath substringToIndex:5]];
//    } else
    if(folderPath.length == 1){
        folderPath = @"";
    }
    // . NSLog(@"siteUrl = %@ prefixUrl = %@%@", siteUrl, prefixUrl, domain);
//    NSString *folderPath = [siteUrl stringByReplacingOccurrencesOfString:domain withString:domain];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfSize:16.0];
    titleLabel.numberOfLines = 2;
    CGSize infoLabelSize =  [self addInfoLabelWithString:string];
    NSString *urlSiteFull = [NSString stringWithFormat:@"%@%@%@",prefixUrl,domain,folderPath];
    NSDictionary *attribs = @{
                                                            NSForegroundColorAttributeName:[UIColor grayColor],
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:17]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:urlSiteFull attributes:attribs];
    UIFont *boldFont = [UIFont fontWithName:@"Helvetica" size:17];
    NSRange range = [urlSiteFull rangeOfString:domain];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor blackColor],
                                    NSFontAttributeName:boldFont} range:range];
    
    
//    titleLabel.attributedText = attributedText;
    titleLabel.attributedText = attributedText;
    titleLabel.copyingEnabled = copyable;
    CGSize titleLabelSize = [titleLabel sizeThatFits:CGSizeMake((self.viewBg.frame.size.width - 12.f * 2.0), CGFLOAT_MAX)];
    titleLabel.frame = CGRectMake(12.f, infoLabelSize.height + 12.f, titleLabelSize.width, titleLabelSize.height);

    [self.viewBg addSubview:titleLabel];
    return urlSiteFull;
}
-(void)addLayerBgWithButton{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(2.f, 8.f, self.view.frame.size.width - 170.f - 4.f, 84.f)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 10.f;
    view.clipsToBounds = YES;
    self.viewBg = view;
    [self.view addSubview:self.viewBg];
}
-(void)addLayerBgWithOutButton{
    UIView* view = [[UIView alloc] initWithFrame:CGRectMake(2.f, 8.f, self.view.frame.size.width - 60.f, 84.f)];
    view.backgroundColor = [UIColor whiteColor];
    view.layer.cornerRadius = 10.f;
    view.clipsToBounds = YES;
    self.viewBg = view;
    [self.view addSubview:self.viewBg];
}
-(UIButton*)addTransparentButtonWithTitle:(NSString*)title andImage:(UIImage*)image{//} highlightedImage:(UIImage *)highlightedImage{
    
    
    UILabel* titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont boldSystemFontOfSize:14.0];//[UIFont systemFontOfSize:14.0];
    titleLabel.numberOfLines = 0;
    titleLabel.text = title;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.btnBg addSubview:titleLabel];
    CGSize titleLabelSize = [titleLabel sizeThatFits:CGSizeMake((self.btnBg.frame.size.width - 7.f * 2.0), CGFLOAT_MAX)];
    CGFloat titleLabelSizeHeight = titleLabelSize.height;
    if(titleLabelSizeHeight < 33.f){
        titleLabelSizeHeight = 33.f;
    }
    titleLabel.frame = CGRectMake((self.btnBg.frame.size.width - titleLabelSize.width) / 2, self.btnBg.frame.size.height - titleLabelSizeHeight - 7.f , titleLabelSize.width, titleLabelSizeHeight);
    // . NSLog(@"titleLabelSize.height = %f", titleLabelSize.height);
    
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake((self.btnBg.frame.size.width - 30.f)/2, 8.f, 30.f, 30.f)];//self.btnBg.frame.size.height - titleLabelSizeHeight - 10.f)];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = YES;
    [self.btnBg addSubview:imageView];
//                                                  initWithImage:image highlightedImage:highlightedImage];
//    imageViewz
    
    
    CGRect btnSize = CGRectMake(0, 0, self.btnBg.frame.size.width, self.btnBg.frame.size.height);
    UIButton * btnCall = [[UIButton alloc] initWithFrame:btnSize];
//    [btnCall setTitle:@"Показать на карте" forState:UIControlStateNormal];
//    [btnCall setTitle:@"Показать на карте" forState:UIControlStateHighlighted];
    
//    btnCall.clipsToBounds = YES;
//    btnCall.layer.cornerRadius = 10.f;
//    [btnCall setTitleColor:[UIColor whiteColor] forState: UIControlStateNormal];
    btnCall.backgroundColor = [UIColor clearColor];//[PKUOStyleKit bgColorGradientBottom];
    
    return btnCall;
}
-(void)addLayerBgForButton:(UIColor*)color{
    CGRect btnSize = CGRectMake(self.view.frame.size.width - 164.f, 8.f, 110.f - 4.f, 82.f);
    UIView* view = [[UIView alloc] initWithFrame:btnSize];
    view.backgroundColor = color;
    view.layer.cornerRadius = 10.f;
    view.clipsToBounds = YES;
    self.btnBg = view;
    [self.view addSubview:self.btnBg];
}


-(void)labelForUrl:(NSString*)siteUrl andPrefix:(NSString*)prefixUrl{
    NSString *text = [NSString stringWithFormat:@"%@\n%@",prefixUrl,siteUrl];
    
    
    // Define general attributes for the entire text
    NSDictionary *attribs = @{
//                              NSForegroundColorAttributeName:[UIColor blackColor],
                              NSFontAttributeName: [UIFont fontWithName:@"Helvetica" size:17]
                              };
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:text attributes:attribs];
    
    UIFont *boldFont = [UIFont fontWithName:@"Helvetica" size:17];
    NSRange range = [text rangeOfString:prefixUrl];
    [attributedText setAttributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor],
                                    NSFontAttributeName:boldFont} range:range];
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectZero];
    label.attributedText = attributedText;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
