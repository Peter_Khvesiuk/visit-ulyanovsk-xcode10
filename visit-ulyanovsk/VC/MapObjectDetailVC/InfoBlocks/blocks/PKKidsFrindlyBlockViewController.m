//
//  PKKidsFrindlyBlockViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 12/02/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKKidsFrindlyBlockViewController.h"
#import "PKDataManager.h"

@interface PKKidsFrindlyBlockViewController ()

@end

@implementation PKKidsFrindlyBlockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithOutButton];
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
    }
    
//    if(self.mapObject.kidsfriendly){
    [super addTitle:TRANSLATE_UP(@"kid-friendly") titleCopyable:NO andInfoString:@""];
//    }
}

@end
