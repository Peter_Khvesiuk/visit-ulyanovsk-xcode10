//
//  PKBlockIsFreeViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 06.06.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKBlockIsFreeViewController.h"
#import "PKDataManager.h"

@interface PKBlockIsFreeViewController ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subTitleLabel;

@end

@implementation PKBlockIsFreeViewController
//- (instancetype)initWithMapObject:(PKMapObj*)mapObject
//{
//    self = [super initWithMapObject:mapObject];
//    //    self = [super init];
//    //    if (self) {
//    //        self.mapObject = mapObject;
//    //    }
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithOutButton];
    
    
    if(self.informTips != nil){
        if(self.informTips.isFree){
            [super addTitle:TRANSLATE_UP(@"chargeable") titleCopyable:NO andInfoString:@""];
        }
    } else if(self.event !=nil){
        BOOL isFree = YES;
        if(self.event.price > 0){
            isFree = NO;
        }
        
        if(!isFree){
            [super addTitle:TRANSLATE_UP(@"chargeable") titleCopyable:NO andInfoString:@""];
        } else {
            [super addTitle:TRANSLATE_UP(@"free") titleCopyable:NO andInfoString:@""];
        }
    } else {
        [super addTitle:TRANSLATE_UP(@"free") titleCopyable:NO andInfoString:@""];
    }
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
