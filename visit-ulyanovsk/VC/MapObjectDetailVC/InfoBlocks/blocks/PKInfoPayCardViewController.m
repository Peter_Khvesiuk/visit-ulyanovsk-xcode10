//
//  PKInfoPayCardViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 11.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoPayCardViewController.h"
#import "PKDataManager.h"

@interface PKInfoPayCardViewController ()

@end

@implementation PKInfoPayCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super addLayerBgWithOutButton];
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
    }
    
    
    if(self.mapObject.payCard){
        [super addTitle:TRANSLATE_UP(@"yes") titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"payment_by_card",@":")];
    } else {
        [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:@""];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
