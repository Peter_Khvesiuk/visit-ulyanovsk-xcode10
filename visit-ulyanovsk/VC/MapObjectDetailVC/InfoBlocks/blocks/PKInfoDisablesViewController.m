//
//  PKInfoDisablesViewController.m
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 25.08.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoDisablesViewController.h"
#import "PKDataManager.h"

@interface PKInfoDisablesViewController ()

@end

@implementation PKInfoDisablesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.[super addLayerBgWithOutButton];
    
    [super addLayerBgWithOutButton];
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
    }
    
    if(self.mapObject.avalibleForDisables){
        [super addTitle:TRANSLATE_UP(@"available_for_disabled") titleCopyable:NO andInfoString:PK_st_TRANSLATE_UP(@"accessibility",@":")];
    } else {
        [super addTitle:TRANSLATE_UP(@"not_available_for_disabled") titleCopyable:NO andInfoString:@""];
    }
    
    
    
    
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
