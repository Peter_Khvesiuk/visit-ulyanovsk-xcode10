//
//  PKBlockTimeViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 09.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKBlockTimeViewController.h"
#import "PKTimeObject+CoreDataClass.h"
#import "PKUOStyleKit.h"
#import "PKPopUp.h"
#import "PKDataManager.h"

@interface PKBlockTimeViewController ()
@property (strong, nonatomic) NSString* workingTimeFullTableString;
@end

@implementation PKBlockTimeViewController

//- (instancetype)initWithMapObject:(PKMapObj*)mapObject
//{
//    self = [super initWithMapObject:mapObject];
////    self = [super init];
////    if (self) {
////        self.mapObject = mapObject;
////    }
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
    }
    

    NSString *workingTimeString;
    
    
    
    //    NSDateFormatter* withOutTime = [[NSDateFormatter alloc] init];
    //    [withOutTime setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    //    withOutTime.dateFormat = @"yyyy-MM-dd";//HH:mm
    
    
     NSMutableArray *workingTimeFullTableArray = [[NSMutableArray alloc] init];
    
    /*
    NSDateFormatter* dayOfWeekFormat = [[NSDateFormatter alloc] init];
    [dayOfWeekFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    dayOfWeekFormat.dateFormat = @"e";

    NSInteger todayInt = [[dayOfWeekFormat stringFromDate: [NSDate date]] integerValue];
    */
    
    defineDayOfWeekFormat
    NSInteger todayInt = defineTodayInt;
//    NSInteger index = 1;
    NSArray *workingTime = [self.mapObject.workingTime allObjects];
    for (int index=1;index<=7;index++){//PKTimeObject *works in workingTime;
        
        PKTimeObject *works;
        for(PKTimeObject *worky in workingTime){
            if(index == worky.day){
                // NSLog(@"worky.day = %hd == %d %@ - %@", worky.day, index, [self timeStringFromInterval:worky.begin], [self timeStringFromInterval:worky.end]);
                works = worky;
                break;
                
            }
            
        }
        
        NSString * oneDayString;
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
        [formatter setDateFormat:@"HH:mm"];
        
        NSDateFormatter *formatterWithSeconds = [[NSDateFormatter alloc] init];
        [formatterWithSeconds setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
        [formatterWithSeconds setDateFormat:@"HH:mm:ss"];
        
        if(works == nil){
            oneDayString = [self stringForEmptyDay];
            workingTimeString = TRANSLATE_UP(@"now_closed");
        } else {
            
            
            if(todayInt == works.day){
                // . NSLog(@"todayInt == %ld", (long)todayInt);
                NSDate *begin = [formatter dateFromString:[self timeStringFromInterval:works.begin]];
                
                NSDate *now = [formatter dateFromString:[formatter stringFromDate:[NSDate date]]];
                NSComparisonResult checkTimeForBegin = [begin compare:now];
                
                NSDate *end = [formatter dateFromString:[self timeStringFromInterval:works.end]];
                NSComparisonResult checkTimeForEnd = [end compare:now];
                
                NSComparisonResult checkTimeForEndBeforeBegin = [end compare:begin];

                NSDate *end2359 = [formatterWithSeconds dateFromString:@"23:59:59"];
                NSComparisonResult checkTimeForEnd2359 = [end2359 compare:now];
                    
                NSDate *begin000 = [formatter dateFromString:@"00:00"];
                NSComparisonResult checkTimeForBegin000 = [begin000 compare:now];
                
                
                
                if ( (checkTimeForBegin == NSOrderedAscending || checkTimeForBegin == NSOrderedSame)
                    && (checkTimeForEnd == NSOrderedDescending || checkTimeForEnd == NSOrderedSame)) {
                    
                    NSDate *breakBegin = [formatter dateFromString:[self timeStringFromInterval:works.breakBegin]];
                    
                    NSDate *breakEnd = [formatter dateFromString:[self timeStringFromInterval:works.breakEnd]];
                    
                    NSComparisonResult checkTimeForBreak = [breakBegin compare:breakEnd];
                    
                    if(checkTimeForBreak == NSOrderedAscending){//Перерыв установлен коректно и не равен 0
                        
                        NSComparisonResult checkNowTimeForbreakBegin = [now compare:breakBegin];
                        
                        if(checkNowTimeForbreakBegin == NSOrderedAscending){
                            workingTimeString = [NSString stringWithFormat:@"%@, %@", TRANSLATE_UP(@"now_open"), TRANSLATE_UP(@"break_after")];
                        } else {
                            
                            NSComparisonResult checkNowTimeForbreakEnd = [now compare:breakEnd];
                            if(checkNowTimeForbreakEnd == NSOrderedAscending){
                                workingTimeString = [NSString stringWithFormat:@"%@, %@", TRANSLATE_UP(@"now_break"), TRANSLATE_UP(@"opens_at")];
                            } else {
                                workingTimeString = [NSString stringWithFormat:@"%@, %@", TRANSLATE_UP(@"now_open"), TRANSLATE_UP(@"closes_after")];
                            }
                        }
                        
                    } else if(checkTimeForBreak == NSOrderedDescending){//Перерыв установлен НЕ коректно и не равен 0
                        
                        
                    } else if(checkTimeForBreak == NSOrderedSame){ //Перерыв НЕ установлен или равен 0
                        
                        workingTimeString = TRANSLATE_UP(@"now_open");
                        
                    }
                    
                    // some code
                } else if ((checkTimeForEndBeforeBegin == NSOrderedAscending  || checkTimeForEndBeforeBegin == NSOrderedSame)
                           && (checkTimeForBegin == NSOrderedAscending || checkTimeForBegin == NSOrderedSame)
                           && (checkTimeForEnd2359 == NSOrderedDescending || checkTimeForEnd2359 == NSOrderedSame)){//Если время задано в формате работаем с 12:00 до 3:00 ночи следующего дня, а сейчас время после 12:00
                    
                    workingTimeString = TRANSLATE_UP(@"now_open");
                    
                } else if ((checkTimeForEndBeforeBegin == NSOrderedAscending  || checkTimeForEndBeforeBegin == NSOrderedSame)
                           && (checkTimeForBegin000 == NSOrderedAscending || checkTimeForBegin000 == NSOrderedSame)
                           && (checkTimeForEnd == NSOrderedDescending || checkTimeForEnd == NSOrderedSame)){//Если время задано в формате работаем с 12:00 до 3:00 ночи следующего дня, а сейчас время до 3:00
                    
                    workingTimeString = TRANSLATE_UP(@"now_open");
                    
                } else {
                    workingTimeString = [NSString stringWithFormat:@"%@, %@ %@", TRANSLATE_UP(@"now_closed"), TRANSLATE_UP(@"opens_at"), [self timeStringFromInterval:works.begin]];
                }
                
                
            }
            oneDayString = [self stringForOneDay: works];
        }
        
        
        
        
        
//        NSMutableDictionary *workingStringDictionary = [[NSMutableDictionary alloc] init];
        
        if((index - 1) > 0){
    
            PKTimeObject *prewDay;
            for (PKTimeObject *wtime in workingTime) {
                if(wtime.day == index-1){
                    prewDay = wtime;
                }
            }
            if(prewDay == nil){
                [workingTimeFullTableArray addObject: [self returnWorkingStringDictionaryWithIndex:index andOneDayString:oneDayString]];
            } else {
                NSDate *begin = [formatter dateFromString:[self timeStringFromInterval:works.begin]];
                NSDate *beginPrewDay = [formatter dateFromString:[self timeStringFromInterval:prewDay.begin]];
                NSComparisonResult checkTimeForBegin = [begin compare:beginPrewDay];
                
                NSDate *end = [formatter dateFromString:[self timeStringFromInterval:works.end]];
                NSDate *endPrewDay = [formatter dateFromString:[self timeStringFromInterval:prewDay.end]];
                NSComparisonResult checkTimeForEnd = [end compare:endPrewDay];
                //NSLog(@"begin = %@ - %@, beginPrewDay = %@ - %@", [formatter stringFromDate: begin], [formatter stringFromDate: end], [formatter stringFromDate: beginPrewDay], [formatter stringFromDate: endPrewDay]);
                if(checkTimeForBegin == NSOrderedSame && checkTimeForEnd == NSOrderedSame){
                    NSMutableDictionary *prewWorkingStringDictionary = [workingTimeFullTableArray lastObject];
                    [prewWorkingStringDictionary setValue:[self weekDayStringNameForDar:index] forKey:@"intervalDayEnd"];
                    
                } else {
                    [workingTimeFullTableArray addObject: [self returnWorkingStringDictionaryWithIndex:index andOneDayString:oneDayString]];
                }
            }
//            PKTimeObject *prewDay = [workingTime objectAtIndex:index-1];
            
            
            
            
//            if(nextDay){}
        } else {
             [workingTimeFullTableArray addObject: [self returnWorkingStringDictionaryWithIndex:index andOneDayString:oneDayString]];
        }
        
        
        
//        index++;
    }
    self.workingTimeFullTableString = [self workingTimeFullTableString:workingTimeFullTableArray];
    // . NSLog(@"self.workingTimeFullTableString = %@", self.workingTimeFullTableString);
    
    if(!([workingTime count] == 0)){
        [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
        [super addLayerBgWithButton];
        [super addTitle:workingTimeString titleCopyable:YES andInfoString: PK_st_TRANSLATE_UP(@"work_schedule", @":")];
        UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"show_schedule") andImage:[PKUOStyleKit imageOfIconBlockTimeWithIconColor:[UIColor whiteColor]]];
        [btnShowAdress addTarget:self
                          action:@selector(showTimeTable)
                forControlEvents:UIControlEventTouchUpInside];
        [self.btnBg addSubview:btnShowAdress];
    } else {
        [super addLayerBgWithOutButton];
        [super addTitle:TRANSLATE_UP(@"no_work_schedule") titleCopyable:NO andInfoString:@""];
    }
    
}
-(NSString*)timeStringFromInterval:(NSInteger)time{
    NSInteger hours = (int) time / 3600;
    //NSLog(@"hours = %li %li",(long)time, (long)hours);
    NSInteger minutes = (time - hours*3600) / 60;
    //NSLog(@"minutes = %li %li %li",(long)minutes, hours*3600, time - hours*3600);
    
    
//    NSString *hoursStr;
//    if(hours<10){
//        hoursStr = [NSString stringWithFormat:@"0%li", (long)hours];
//    } else {
//        hoursStr = [NSString stringWithFormat:@"%li", (long)hours];
//    }
    NSString *minutesStr;
    if(minutes<10){
        minutesStr = [NSString stringWithFormat:@"0%li", (long)minutes];
    } else {
        minutesStr = [NSString stringWithFormat:@"%li", (long)minutes];
    }
    NSString *result = [NSString stringWithFormat:@"%li:%@", hours, minutesStr];
//    NSLog(@"hours:minutes = %@",result);
    return result;
}
-(NSDictionary*)returnWorkingStringDictionaryWithIndex:(NSInteger)index andOneDayString:(NSString*)oneDayString{
    NSMutableDictionary*workingStringDictionary = [[NSMutableDictionary alloc] init];
    [workingStringDictionary setValue:oneDayString forKey: @"stringOneDay"];
    [workingStringDictionary setValue: [self weekDayStringNameForDar:index] forKey: @"intervalDayBegin"];
    [workingStringDictionary setValue: [self weekDayStringNameForDar:index] forKey: @"intervalDayEnd"];
    return workingStringDictionary;
}

-(NSString*)stringForEmptyDay{
    
    return TRANSLATE_UP(@"nonbusiness_day");
}
-(NSString*)stringForOneDay:(PKTimeObject*)timeObject{
    
    return [NSString stringWithFormat:@"%@ %@ %@ %@", TRANSLATE(@"from"), [self timeStringFromInterval:timeObject.begin], TRANSLATE(@"to"), [self timeStringFromInterval:timeObject.end]];
}

-(NSString*)workingTimeFullTableString:(NSArray*) workingTimeFullTableArray{
    NSString* returnString = @"";
    for (NSDictionary *wtimeDictionary in workingTimeFullTableArray) {
        if([[wtimeDictionary objectForKey:@"intervalDayBegin"] isEqualToString:[wtimeDictionary objectForKey:@"intervalDayEnd"]]){
            returnString = [NSString stringWithFormat: @"%@%@: %@\n", returnString, UPFirst([wtimeDictionary objectForKey:@"intervalDayBegin"]), [wtimeDictionary objectForKey:@"stringOneDay"]];
        } else {
            returnString = [NSString stringWithFormat: @"%@%@-%@: %@\n", returnString, UPFirst([wtimeDictionary objectForKey:@"intervalDayBegin"]), UPFirst([wtimeDictionary objectForKey:@"intervalDayEnd"]), [wtimeDictionary objectForKey:@"stringOneDay"]];
        }
    }
    return returnString;
}


-(NSString*)weekDayStringNameForDar:(NSInteger) integer{

    NSDateFormatter* dayOfWeekFormat = [[NSDateFormatter alloc] init];
    [dayOfWeekFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    dayOfWeekFormat.dateFormat = @"EE";
    
    NSDateFormatter* dateformat = [[NSDateFormatter alloc] init];
    [dateformat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    dateformat.dateFormat = @"dd-MM-yyyy";
    switch(integer) {
        case 1: {//monday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"01-01-2018"]];
        }
            break;
        case 2: {//tuesday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"02-01-2018"]];
        }
            break;
        case 3: {//wensday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"03-01-2018"]];
        }
            break;
        case 4: {//thursday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"04-01-2018"]];
        }
            break;
        case 5: {//friday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"05-01-2018"]];
        }
            break;
        case 6: {//sarturday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"06-01-2018"]];
        }
            break;
        case 7: {//sunday
            return [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"07-01-2018"]];
        }
            break;
    }
            
//    NSString *monday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"01-01-2018"]];
//    NSString *tuesday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"02-01-2018"]];
//    NSString *wensday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"03-01-2018"]];
//    NSString *thursday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"04-01-2018"]];
//    NSString *friday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"05-01-2018"]];
//    NSString *sarturday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"06-01-2018"]];
//    NSString *sunday = [dayOfWeekFormat stringFromDate: [dateformat dateFromString:@"07-01-2018"]];
    
//    // . NSLog(@"monday = %@, tuesday = %@, wensday = %@, thursday = %@, friday = %@, sarturday = %@, sunday = %@", monday, tuesday, wensday, thursday, friday, sarturday, sunday);
    return nil;
}
-(void)showTimeTable{
//    [self timeTableString];
    // . NSLog(@"showTimeTable");
    [[PKPopUp showMessage:self.workingTimeFullTableString withTitle:TRANSLATE_UP(@"work_schedule")]
     withConfirm:TRANSLATE_UP(@"ok") onConfirm:^{
         
     }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
