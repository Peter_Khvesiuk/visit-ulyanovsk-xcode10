//
//  PKBlockAgeViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 06.06.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKBlockAgeViewController.h"
#import "PKDataManager.h"

@interface PKBlockAgeViewController ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subTitleLabel;

@end

@implementation PKBlockAgeViewController
//- (instancetype)initWithMapObject:(PKMapObj*)mapObject
//{
//    self = [super initWithMapObject:mapObject];
//    //    self = [super init];
//    //    if (self) {
//    //        self.mapObject = mapObject;
//    //    }
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithOutButton];
    if(self.informTips != nil || self.event != nil){
        if(self.informTips != nil){
            if(!([self.informTips.ageString integerValue] == 0)){
                [super addTitle:[NSString stringWithFormat:@"%@+",self.informTips.ageString] titleCopyable:NO andInfoString:PK_st_TRANSLATE_UP(@"there_is_an_age_restriction",@":")];
            } else {
                [super addTitle:TRANSLATE_UP(@"there_are_no_age_restrictions") titleCopyable:NO andInfoString:@""];
            }
        }
        if(self.event != nil){
            if(self.event.ageFrom != 0){
                [super addTitle:[NSString stringWithFormat:@"%i+",self.event.ageFrom] titleCopyable:NO andInfoString:PK_st_TRANSLATE_UP(@"there_is_an_age_restriction",@":")];
            } else {
                [super addTitle:TRANSLATE_UP(@"there_are_no_age_restrictions") titleCopyable:NO andInfoString:@""];
            }
            
        }
    } else if(self.event != nil){
        
    } else {
        [super addTitle:TRANSLATE_UP(@"there_are_no_age_restrictions") titleCopyable:NO andInfoString:@""];
    }
    
    
    
    
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
