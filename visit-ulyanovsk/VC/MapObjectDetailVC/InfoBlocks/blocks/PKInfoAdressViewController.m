//
//  PKInfoAdressViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 10.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoAdressViewController.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"
@interface PKInfoAdressViewController ()

@end

@implementation PKInfoAdressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithButton];
    
    SEL goToMApSelector = @selector(showOnMap);
//    PKMapObj *mapObject;
    if(self.informTips != nil || self.event != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            goToMApSelector = @selector(tapShowOnMapIsTappedWithInformTips);
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        if(self.event.mapObjects != nil && [self.event.mapObjects count] > 0){
            goToMApSelector = @selector(tapShowOnMapIsTappedWithInformTips);
            if([self.event.mapObjects count] == 1){
                self.mapObject = [[self.event.mapObjects allObjects] firstObject];
            }
        }
        
    } else if(self.mapObject != nil){
//        mapObject = self.mapObject;
        goToMApSelector = @selector(showOnMap);
    }
    
    
    if(self.mapObject != nil && ![self.mapObject.adress isEqualToString:@""]){
        [super addTitle:self.mapObject.adress titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"address", @":")];
    } else if(self.informTips != nil || self.event != nil){
        NSString *counts = [NSString stringWithFormat:@" ( %lu )", [self.informTips.mapObjects count]];
        
        [super addTitle:PK_st_TRANSLATE_UP(@"several_address", counts)  titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"where_to_find", @":")];
        
    } else {
        [super addTitle:TRANSLATE_UP(@"no_address") titleCopyable:NO andInfoString:@""];
    }
    
    
    UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"show_on_map") andImage:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[UIColor whiteColor]]];
    [btnShowAdress addTarget:self
                action:goToMApSelector
      forControlEvents:UIControlEventTouchUpInside];
    [self.btnBg addSubview:btnShowAdress];
    
    
    
    
}
-(void)tapShowOnMapIsTappedWithInformTips{
    [super tapShowOnMapIsTappedWithInformTips];
}
-(void)showOnMap{
    [super tapShowOnMapIsTapped];
    // . NSLog(@"showOnMap");
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
