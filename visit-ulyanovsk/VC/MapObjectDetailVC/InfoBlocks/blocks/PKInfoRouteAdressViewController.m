//
//  PKInfoRouteAdressViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 14.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoRouteAdressViewController.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"

@interface PKInfoRouteAdressViewController ()

@end

@implementation PKInfoRouteAdressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithButton];
//    if(![self.mapObject.adress isEqualToString:@""]){
    
    NSArray* mapObjects = [[NSArray alloc] init];
    mapObjects = [self.geoRoute.mapObject allObjects];
    NSString* message;
    
    
    
    if([mapObjects count] > 0){
        NSString *count = [NSString stringWithFormat:@" ( %lu )", (unsigned long)[mapObjects count]];
        message = count;//PK_st_TRANSLATE_UP(@"Passes_through_several_objects",  count);
    } else {
        message = TRANSLATE_UP(@"Does_not_pass_through_objects");
    }
    [super addTitle:message titleCopyable:NO andInfoString:PK_st_TRANSLATE_UP(@"Passes_through_several_objects", @":")];
//    } else {
//        [super addTitle:@"Нет адреса" titleCopyable:NO andInfoString:@""];
//    }
    UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"Show_route") andImage:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[UIColor whiteColor]]];
    [btnShowAdress addTarget:self
                      action:@selector(showOnMap)
            forControlEvents:UIControlEventTouchUpInside];
    [self.btnBg addSubview:btnShowAdress];
    
    
    
}
-(void)showOnMap{
    [super tapShowOnMapIsTapped];
    // . NSLog(@"showOnMap");
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
