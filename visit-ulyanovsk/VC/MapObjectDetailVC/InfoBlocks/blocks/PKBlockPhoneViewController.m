//
//  PKBlockPhoneViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 10.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKBlockPhoneViewController.h"
#import "PKUOStyleKit.h"
#import "UILabel+Copyable.h"
#import "PKDataManager.h"
#import "PKComercialOrganization+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import "PKContact+CoreDataClass.h"

@interface PKBlockPhoneViewController ()

@end

@implementation PKBlockPhoneViewController
//- (instancetype)initWithMapObject:(PKMapObj*)mapObject
//{
//    self = [super initWithMapObject:mapObject];
//    //    self = [super init];
//    //    if (self) {
//    //        self.mapObject = mapObject;
//    //    }
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithButton];
    
    
    
    if(self.informTips != nil){
        if(self.informTips != nil){
            if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
                if([self.informTips.mapObjects count] == 1){
                    self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
                }
            }
        }
    }
    NSString *phone;
    if(self.event != nil){
        if(![self.event.brand.contact.phone isEqualToString:@"0"] && self.event.brand.contact.phone != nil){
            phone = self.event.brand.contact.phone;
            [super addTitle:[NSString stringWithFormat:@"%@%@", @"+7", phone] titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"phone_number",@":")];
            
        } else if([[self.event.organizations allObjects] count] > 0){
            for (PKComercialOrganization *org in [self.event.organizations allObjects]) {
                if(![org.phoneOrg isEqualToString:@"0"]){
                    phone = org.phoneOrg;
                }
            }
        }
        if(phone == nil){
            for (PKMapObj *mapObject in [self.event.mapObjects allObjects]) {
                if(![mapObject.phone isEqualToString:@"0"]){
                    self.mapObject = mapObject;
                    phone = mapObject.phone;
                }
            }
        }
            // . NSLog(@"self-site = %@", self.geoRoute.geoOrganization.siteOrg);
        if(phone != nil){
            [super addTitle:[NSString stringWithFormat:@"%@%@", @"+7", phone] titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"phone_number",@":")];
            
        } else {
            [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:@""];
        }
        
    } else if(![self.mapObject.phone isEqualToString:@"0"] && self.mapObject != nil){
        phone = self.mapObject.phone;
        [super addTitle:[NSString stringWithFormat:@"%@%@", @"+7", phone] titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"phone_number",@":")];
        
    } else if(![self.mapObject.brand.contact.phone isEqualToString:@"0"] && self.mapObject.brand.contact.phone != nil){
        phone = self.mapObject.brand.contact.phone;
        [super addTitle:[NSString stringWithFormat:@"%@%@", @"+7", phone] titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"phone_number",@":")];
        
    } else {
        phone = @"0";
        [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:@""];
    }
    if([phone integerValue] > 0){
        UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"Call") andImage:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[UIColor whiteColor]]];
        [btnShowAdress addTarget:self
                          action:@selector(calltonumber:)
                forControlEvents:UIControlEventTouchUpInside];
        btnShowAdress.tag = [phone integerValue];
        [self.btnBg addSubview:btnShowAdress];
        
    }
    
    
    
    // Do any additional setup after loading the view.
}
-(void)calltonumber:(id)sender{
    UIButton* btn = (UIButton*)sender;
    NSInteger phoneNumber = btn.tag;
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *url = [[ NSURL alloc ] initWithString:[NSString stringWithFormat:@"tel://%@",[NSString stringWithFormat:@"%@%li", @"+7", (long)phoneNumber]]];
//    [application openURL:url options:@{} completionHandler:^(BOOL success) {
//        if (success) {
//            // . NSLog(@"Opened url");
//        }
//    }];
//#warning metrica!
    [application openURL:url options:@{} completionHandler:nil];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
