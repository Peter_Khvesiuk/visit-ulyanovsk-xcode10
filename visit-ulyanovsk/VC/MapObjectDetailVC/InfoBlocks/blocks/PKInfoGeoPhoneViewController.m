//
//  PKInfoGeoPhoneViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 14.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoGeoPhoneViewController.h"
#import "PKUOStyleKit.h"
#import "PKComercialOrganization+CoreDataClass.h"
#import "PKDataManager.h"
@interface PKInfoGeoPhoneViewController ()

@end

@implementation PKInfoGeoPhoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithButton];
    
    
    if((self.geoRoute != nil && ![self.geoRoute.geoOrganization.phoneOrg isEqualToString:@"0"]) ||  (self.informTips != nil && ![self.informTips.organization.phoneOrg isEqualToString:@"0"])){
        
        
        if(self.geoRoute != nil && ![self.geoRoute.geoOrganization.phoneOrg isEqualToString:@"0"]){
            [super addTitle:[NSString stringWithFormat:@"%@%@", @"+7", self.geoRoute.geoOrganization.phoneOrg] titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"phone_number",@":")];
        } else if(self.informTips != nil && ![self.informTips.organization.phoneOrg isEqualToString:@"0"]){
            [super addTitle:[NSString stringWithFormat:@"%@%@", @"+7", self.informTips.organization.phoneOrg] titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"phone_number",@":")];
        }
        
    } else {
        [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:@""];
        
    }
    UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"Call") andImage:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[UIColor whiteColor]]];
    [btnShowAdress addTarget:self
                      action:@selector(calltonumber)
            forControlEvents:UIControlEventTouchUpInside];
    [self.btnBg addSubview:btnShowAdress];
    
    
    // Do any additional setup after loading the view.
}
-(void)calltonumber{
    
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *url = [[ NSURL alloc ] initWithString:[NSString stringWithFormat:@"tel://%@",[NSString stringWithFormat:@"%@%@", @"+7", self.mapObject.phone]]];
    //    [application openURL:url options:@{} completionHandler:^(BOOL success) {
    //        if (success) {
    //            // . NSLog(@"Opened url");
    //        }
    //    }];
//#warning metrica!
    [application openURL:url options:@{} completionHandler:nil];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
