//
//  PKInfoEmailViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 10.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoEmailViewController.h"
#import "PKUOStyleKit.h"
#import "PKComercialOrganization+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import "PKContact+CoreDataClass.h"
#import "PKDataManager.h"


@interface PKInfoEmailViewController ()

@property (strong, nonatomic) NSString *emailadress;
@end

@implementation PKInfoEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
    }
    
//    NSString* emailMsg;
    NSString* emailInfoStr;
    if(self.event != nil){

        if([[self.event.organizations allObjects] count] > 0){
            
            for (PKComercialOrganization *org in [self.event.organizations allObjects]) {
                if(![org.emailOrg isEqualToString:@""]){
                    self.emailadress = org.emailOrg;
                }
            }
            
        }
        if(self.emailadress == nil  && [[self.event.mapObjects allObjects] count] > 0){
            
            for (PKMapObj *mapObject in [self.event.mapObjects allObjects]) {
                if(![mapObject.email isEqualToString:@""]){
                    self.emailadress = mapObject.email;
                }
            }
            
        }
        
        
//        else {
//            [super addLayerBgWithOutButton];
//            emailMsg = TRANSLATE_UP(@"not_set");
//            emailInfoStr = @"";
//            [super addTitle:self.email titleCopyable:NO andInfoString:emailInfoStr];
//        }
    } else if((self.mapObject.email != nil && ![self.mapObject.email isEqualToString:@""]) || (self.geoRoute.geoOrganization.emailOrg != nil && ![self.geoRoute.geoOrganization.emailOrg isEqualToString:@""])){

        if((self.mapObject.email != nil && ![self.mapObject.email isEqualToString:@""])){
            self.emailadress = self.mapObject.email;
        } else if((self.geoRoute.geoOrganization.emailOrg != nil && ![self.geoRoute.geoOrganization.emailOrg isEqualToString:@""])){
             self.emailadress = self.geoRoute.geoOrganization.emailOrg;
        }

        
    } else if(![self.mapObject.brand.contact.email isEqualToString:@""] && self.mapObject.brand.contact != nil){
        self.emailadress = self.mapObject.brand.contact.email;
    }
    
    if(self.emailadress != nil){

        emailInfoStr = @"E-mail:";
        [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
        [super addLayerBgWithButton];
        [super addTitle:self.emailadress titleCopyable:NO andInfoString:emailInfoStr];
        
        UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"send_e-mail") andImage:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[UIColor whiteColor]]];
        [btnShowAdress addTarget:self
                          action:@selector(email)
                forControlEvents:UIControlEventTouchUpInside];
        [self.btnBg addSubview:btnShowAdress];
        [super addTitle:self.emailadress titleCopyable:YES andInfoString:emailInfoStr];
    } else{
        [super addLayerBgWithOutButton];
        emailInfoStr = @"";
        [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:emailInfoStr];
    }
    
//    if(![self.mapObject.email isEqualToString:@""]){
//        [super addTitle:emailMsg titleCopyable:YES andInfoString:emailInfoStr];
//    } else {
//        [super addTitle:emailMsg titleCopyable:NO andInfoString:emailInfoStr];
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)email{
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:"]];
    UIApplication *application = [UIApplication sharedApplication];
    NSString *url = [NSString stringWithFormat:@"mailto:%@", self.emailadress];
    NSURL* mailURL = [NSURL URLWithString:url];
    if ([application canOpenURL:mailURL]) {
        [application openURL:mailURL options:@{} completionHandler:nil];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
