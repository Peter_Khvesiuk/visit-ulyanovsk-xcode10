//
//  PKBlockCalendarViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 06.06.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKBlockCalendarViewController.h"
#import "PKDataManager.h"

@interface PKBlockCalendarViewController ()

@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *subTitleLabel;

@end

@implementation PKBlockCalendarViewController
//- (instancetype)initWithMapObject:(PKMapObj*)mapObject
//{
//    self = [super initWithMapObject:mapObject];
//    //    self = [super init];
//    //    if (self) {
//    //        self.mapObject = mapObject;
//    //    }
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithOutButton];
    
    if(self.informTips != nil){
        if(![self.informTips.dateString isEqualToString:@""]){
            [super addTitle:self.informTips.dateString titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"date_of_event",@":")];
        }
    } else if(self.event != nil){
        
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
        NSDateComponents *dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:[NSDate date]];
        dateComponents.day = 1;
        dateComponents.hour = 0;
        dateComponents.minute = 0;
        dateComponents.second = 0;
        
        NSDate *dateCurrentMonth = [calendar dateFromComponents: dateComponents];
        
        
        
        NSComparisonResult checkEventPass = [self.event.dateStart compare:dateCurrentMonth];
        
        
        NSDateComponents *dateStartComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:self.event.dateStart];
        NSDateComponents *dateEndComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:self.event.dateEnd];
        
        NSString *dateString = [[NSString alloc] init];
        dateString = @"";
        BOOL isProlong = NO;
        if(checkEventPass == NSOrderedDescending || checkEventPass == NSOrderedSame) {//настоящее и будущие время
            
        } else { //"прогнозируемым на следующий год", нет точной даты
            isProlong = YES;
//            newDateOfEventsComponents.year += 1;
        }
        
        NSLocale *locale = [NSLocale currentLocale];
        NSDateFormatter *dF = [[NSDateFormatter alloc] init];
        [dF setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
        [dF setLocale:locale];
        
        if((dateStartComponents.day == 1 && dateStartComponents.hour == 0 && dateStartComponents.minute == 0 && dateStartComponents.second == 0) || isProlong){//нет точной даты
            [dF setDateFormat:@"LLLL"];
            dateString = [NSString stringWithFormat:@"%@ *", [dF stringFromDate:[calendar dateFromComponents: dateStartComponents]]];
        } else if(dateStartComponents.day == dateEndComponents.day){//даты начала и конца совпадают
            [dF setDateFormat:@"d MMMM"];
            dateString = [NSString stringWithFormat:@"%@ *", [dF stringFromDate:[calendar dateFromComponents: dateStartComponents]]];
        } else {//даты начала и конца не совпадают
            [dF setDateFormat:@"MMMM"];
            
            if([(NSString*)[dF stringFromDate:[calendar dateFromComponents: dateStartComponents]] isEqualToString:(NSString*)[dF stringFromDate: self.event.dateEnd]]){// если месяц тот же
                [dF setDateFormat:@"d"];
                dateString = [NSString stringWithFormat:@"%@", [dF stringFromDate: self.event.dateStart]];
                [dF setDateFormat:@"d MMMM"];
                dateString = [NSString stringWithFormat:@"%@ - %@", dateString, [dF stringFromDate: self.event.dateEnd]];
            } else {
                [dF setDateFormat:@"d MMMM"];
                dateString = [NSString stringWithFormat:@"%@", [dF stringFromDate: self.event.dateStart]];
                dateString = [NSString stringWithFormat:@"%@ - %@", dateString, [dF stringFromDate: self.event.dateEnd]];
            }
        }
        
        [super addTitle:dateString titleCopyable:YES andInfoString:PK_st_TRANSLATE_UP(@"date_of_event",@":")];
    }else {
        [super addTitle:TRANSLATE_UP(@"no_date") titleCopyable:NO andInfoString:@""];
    }
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
