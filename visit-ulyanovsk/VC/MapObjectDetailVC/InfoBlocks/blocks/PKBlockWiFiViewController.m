//
//  PKBlockWiFiViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 10.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKBlockWiFiViewController.h"
#import "PKDataManager.h"
//#import "PKUOStyleKit.h"
@interface PKBlockWiFiViewController ()

//@property (retain, nonatomic) UIScrollView * scrollView;
//@property (strong, nonatomic) UILabel *titleLabel;
//@property (strong, nonatomic) UILabel *subTitleLabel;

@end

@implementation PKBlockWiFiViewController
//- (instancetype)initWithMapObject:(PKMapObj*)mapObject
//{
//    self = [super initWithMapObject:mapObject];
//    //    self = [super init];
//    //    if (self) {
//    //        self.mapObject = mapObject;
//    //    }
//    return self;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
    [super addLayerBgWithOutButton];
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
    }
    if(self.event != nil){
        if(self.event.mapObjects != nil && [self.event.mapObjects count] > 0){
            for (PKMapObj* mapObject in [self.event.mapObjects allObjects]) {
                if(mapObject.wiFi){
                    self.mapObject = mapObject;
                }
            }
            
        }
        
    }
    
    if(self.mapObject.wiFi){
        [super addTitle:TRANSLATE_UP(@"there_is_free_WiFi") titleCopyable:NO andInfoString:PK_st_TRANSLATE_UP(@"Free_WiFi",@":")];
    } else {
        [super addTitle:TRANSLATE_UP(@"No_free_WiFi") titleCopyable:NO andInfoString:@""];
    }
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
