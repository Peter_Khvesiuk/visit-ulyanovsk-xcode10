//
//  PKInfoBlockSiteViewController.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 11.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoSiteViewController.h"
#import "PKUOStyleKit.h"
#import "PKComercialOrganization+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import "PKContact+CoreDataClass.h"
#import "PKDataManager.h"

@interface PKInfoSiteViewController ()
@property (strong, nonatomic) NSString *urlAddress;
@end

@implementation PKInfoSiteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    if(self.informTips != nil){
        if(self.informTips.mapObjects != nil && [self.informTips.mapObjects count] > 0){
            if([self.informTips.mapObjects count] == 1){
                self.mapObject = [[self.informTips.mapObjects allObjects] firstObject];
            }
        }
        
        
        
    } else if(self.event != nil){
        if(self.event.mapObjects != nil && [self.event.mapObjects count] > 0){
            if([self.event.mapObjects count] == 1){
                self.mapObject = [[self.event.mapObjects allObjects] firstObject];
            }
        }
        
        
        
    }
    
    
    
    NSString* siteMsg;
    NSString* siteInfoStr;
    // . NSLog(@"site ==>%@<==", self.mapObject.siteurl);
    // . NSLog(@"site ==>%@<==", self.geoRoute.geoOrganization.siteOrg);
    
    if(self.event != nil){
        if(![self.event.brand.contact.siteurl isEqualToString:@""] && (self.event.brand.contact != nil)){
            siteMsg = self.event.brand.contact.siteurl;
            // . NSLog(@"self-site = %@", self.mapObject.siteurl);
        } else if([[self.event.organizations allObjects] count] > 0){
            
            for (PKComercialOrganization *org in [self.event.organizations allObjects]) {
                if(![org.siteOrg isEqualToString:@""]){
                    siteMsg = org.siteOrg;
                }
            }
            // . NSLog(@"self-site = %@", self.geoRoute.geoOrganization.siteOrg);
        }
        if(siteMsg == nil){
            if([[self.event.mapObjects allObjects] count] > 0){
                
                for (PKMapObj *mapObject in [self.event.mapObjects allObjects]) {
                    if(![mapObject.siteurl isEqualToString:@""]){
                        siteMsg = mapObject.siteurl;
                    }
                }
                // . NSLog(@"self-site = %@", self.geoRoute.geoOrganization.siteOrg);
            }
        }
        [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
        [super addLayerBgWithButton];
        if(siteMsg != nil){
            siteInfoStr = PK_st_TRANSLATE_UP(@"site_address",@":");
            self.urlAddress = [super addSiteUrl:siteMsg titleCopyable:YES andInfoString:siteInfoStr];
            
            
            UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"go_to_website") andImage:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[UIColor whiteColor]]];
            [btnShowAdress addTarget:self
                              action:@selector(openSite)
                    forControlEvents:UIControlEventTouchUpInside];
            [self.btnBg addSubview:btnShowAdress];
        } else {
            [super addLayerBgWithOutButton];
            
            [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:@""];
        }
    } else if((![self.mapObject.siteurl isEqualToString:@""] && (self.mapObject.siteurl != nil)) || (![self.geoRoute.geoOrganization.siteOrg isEqualToString:@""] && (self.geoRoute.geoOrganization.siteOrg != nil)) || (self.informTips.organization.siteOrg != nil && ![self.informTips.organization.siteOrg isEqualToString:@""] ) || (![self.mapObject.brand.contact.siteurl isEqualToString:@""] && (self.mapObject.brand.contact != nil))){
        
        [super addLayerBgForButton:[PKUOStyleKit bgColorGradientBottom]];
        [super addLayerBgWithButton];
        
        if(self.informTips != nil){
            if((![self.informTips.organization.siteOrg isEqualToString:@""] && (self.informTips.organization.siteOrg != nil))){
                // . NSLog(@"self-site = %@", self.geoRoute.geoOrganization.siteOrg);
                siteMsg = self.informTips.organization.siteOrg;
            }
        } else if((![self.geoRoute.geoOrganization.siteOrg isEqualToString:@""] && (self.geoRoute.geoOrganization.siteOrg != nil))){
            // . NSLog(@"self-site = %@", self.geoRoute.geoOrganization.siteOrg);
            siteMsg = self.geoRoute.geoOrganization.siteOrg;
        } else if(![self.mapObject.siteurl isEqualToString:@""] && (self.mapObject.siteurl != nil)){
            siteMsg = self.mapObject.siteurl;
            // . NSLog(@"self-site = %@", self.mapObject.siteurl);
        } else if(![self.mapObject.brand.contact.siteurl isEqualToString:@""] && (self.mapObject.brand.contact != nil)){
            siteMsg = self.mapObject.brand.contact.siteurl;
            // . NSLog(@"self-site = %@", self.mapObject.siteurl);
        }
        siteInfoStr = PK_st_TRANSLATE_UP(@"site_address",@":");
        
        self.urlAddress = [super addSiteUrl:siteMsg titleCopyable:YES andInfoString:siteInfoStr];
        
        
        UIButton* btnShowAdress = [self addTransparentButtonWithTitle:TRANSLATE_UP(@"go_to_website") andImage:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[UIColor whiteColor]]];
        [btnShowAdress addTarget:self
                          action:@selector(openSite)
                forControlEvents:UIControlEventTouchUpInside];
        [self.btnBg addSubview:btnShowAdress];
        
    } else {
        [super addLayerBgWithOutButton];

        [super addTitle:TRANSLATE_UP(@"not_set") titleCopyable:NO andInfoString:@""];
    }
    
    // Do any additional setup after loading the view.
}
-(void)openSite{
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:"]];
    UIApplication *application = [UIApplication sharedApplication];
    NSURL* mailURL = [NSURL URLWithString:self.urlAddress];
    if ([application canOpenURL:mailURL]) {
        [application openURL:mailURL options:@{} completionHandler:nil];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
