//
//  PKKidsFrindlyBlockViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 12/02/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKInfoBlockViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface PKKidsFrindlyBlockViewController : PKInfoBlockViewController

@end

NS_ASSUME_NONNULL_END
