//
//  PKInfoBlockViewController.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 09.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKMapObj+CoreDataClass.h"
#import "PKMapObjectDetailVC.h"
#import "PKGeoRoute+CoreDataClass.h"
#import "PKInformTips+CoreDataClass.h"
#import "PKEvent+CoreDataClass.h"

@protocol senddataProtocol <NSObject>

-(void)showOnMap; //I am thinking my data is NSArray, you can use another object for store your information.
-(void)showOnMapWithMapObjects:(NSSet<PKMapObj *> *)mapObjects;
@end


@interface PKInfoBlockViewController : UIViewController

@property (nonatomic,assign) id delegate;
@property (strong, nonatomic) UIView* viewBg;
@property (strong, nonatomic) UIView* btnBg;
@property (weak, nonatomic) PKMapObj* mapObject;
@property (weak, nonatomic) PKGeoRoute* geoRoute;
@property (weak, nonatomic) PKInformTips* informTips;
@property (weak, nonatomic) PKEvent* event;

- (instancetype)initWithObject:(id)object andVC:(UIViewController*)vc;
//- (instancetype)initWithGeoRoute:(PKGeoRoute*)geoRoute andVC:(UIViewController*)vc;

-(void)addLayerBgForButton:(UIColor*)color;
-(void)addLayerBgWithOutButton;
-(void)addLayerBgWithButton;
-(void)tapShowOnMapIsTapped;
-(void)tapShowOnMapIsTappedWithInformTips;


-(UIButton*)addTransparentButtonWithTitle:(NSString*)title andImage:(UIImage*)image;
-(void)addTitle:(NSString*)title titleCopyable:(BOOL)copyable andInfoString:(NSString*) string;
-(NSString*)addSiteUrl:(NSString*)siteUrl titleCopyable:(BOOL)copyable andInfoString:(NSString*) string;
@end
