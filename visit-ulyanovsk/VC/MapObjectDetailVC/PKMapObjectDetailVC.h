//
//  PKMapObjectDetailVC.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 12.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAKImageGalleryView.h"
@class PKMapObj;

@interface PKMapObjectDetailVC : UIViewController
@property (strong, nonatomic) PKMapObj *mapObject;
@property (nonatomic) int mapObjectId;
@property (weak, nonatomic) MAKImageGalleryView *imageGalleryView;

-(void)showOnMap;
@end
