//
//  PKMapObjectDetailVC.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 12.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKMapObjectDetailVC.h"

//#import "PKMapObjectInfoBlocksView.h"
#import "PKBlockWiFiViewController.h"
#import "PKKidsFrindlyBlockViewController.h"

#import "PKMapObj+CoreDataClass.h"
#import "PKTypeObj+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import "PKContact+CoreDataClass.h"
#import "PKObjImages+CoreDataClass.h"

#import "PKUOStyleKit.h"

#import "PKInterfaceManager.h"
#import "PKTopNavButtonView.h"

//#import "PKObjectInfoBlockCVC.h"
//#import <AFNetworking/UIImageView+AFNetworking.h>
//#import "AFNetworking+ImageActivityIndicator.h"

//#import "PKObjectInfoBlockCV.h"

#import "PKMainMapViewController.h"

#import "PKUserInteraction.h"
#import "PKButtonLike.h"
#import "PKTurIndustButton.h"
#import "PKInfoDisablesViewController.h"

#import <AFNetworking/AFImageDownloader.h>

#import "UILabel+Copyable.h"


#import "PKPageViewController.h"
#import "PKBrandLogo.h"
#import "PKBlockPhoneViewController.h"
#import "PKBlockTimeViewController.h"
#import "PKInfoAdressViewController.h"
#import "PKInfoEmailViewController.h"
#import "PKInfoSiteViewController.h"
#import "PKInfoPayCardViewController.h"
#import "PKMapObjectCollectionViewController.h"
#import "PKDataManager.h"

//static CGFloat regularFZMargin = 14.0;
static CGFloat PKGeoRouteCellTextAreaHeight  = 44.0;

@interface PKMapObjectDetailVC () <MAKImageGalleryViewDataSource, UIScrollViewDelegate>{
    PKPageViewController *vc;
}

@property (strong, nonatomic) PKTopNavButtonView *topNavButtonView;

//, PKObjectInfoBlockCVDataSource, PKObjectInfoBlockViewCollectionDataSource>
//@property (weak, nonatomic) IBOutlet MAKImageGalleryView *imageGalleryView;
//@property (weak, nonatomic) PKMapObjectInfoBlocksView *infoBlocksView;
@property (strong, nonatomic) UILabel *titleLabel;
@property (strong, nonatomic) UILabel *adressLabel;
@property (strong, nonatomic) UILabel *subTitleLabel;
@property (strong, nonatomic) UITextView *descriptionLabel;
@property (strong, nonatomic) UILabel *categoryLabel;
@property (strong, nonatomic) UIButton* btnBack;
@property (retain, nonatomic) UIScrollView * scrollView;
@property (assign, nonatomic) BOOL scrollWasAnimated;
//@property (strong, nonatomic) IBOutlet PKObjectInfoBlockCV *infoBlocksView;

//@property (strong, nonatomic) PKObjectInfoBlockViewCollection *infoBlockCVC;

@property (strong, nonatomic) UIImageView *imagggview;
@property (strong, nonatomic) NSMutableArray *downloads;

@property (strong, nonatomic) UISelectionFeedbackGenerator *feedbackGenerator;

@property (nonatomic) NSInteger detailMapObjectID;

@end

@implementation PKMapObjectDetailVC
@synthesize mapObject;
@synthesize mapObjectId;
@synthesize imageGalleryView;

//- (instancetype)initWithMapObject:(PKMapObj *)mapObject {
//    self = [super init];
//    if (self) {
//        self.mapObject = mapObject;
////        self.title = mapObject.title;
//    }
//    return self;
//}
-(void)dealloc{
    // . NSLog(@"\ndealloc PKMapObjectDetailVC");
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // . NSLog(@"viewDidAppear");
    self.topNavButtonView.open = YES;
//    if(self.btnBack != nil){
//        [self.view addSubview:self.btnBack];
//        CGRect frame = self.btnBack.frame;
//        frame.origin.x = 0;
//        [UIView animateWithDuration:0.2
//                              delay:0.0
//                            options: UIViewAnimationOptionCurveEaseOut
//                         animations:^{
//                             self.btnBack.frame = frame;
//                         }
//                         completion:^(BOOL finished){
//                             // . NSLog(@"complete");
//                             self.scrollWasAnimated = YES;
//                         }];
//    }
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
//    [self.tabBarController.tabBar setHidden:NO];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.topNavButtonView.open = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.mapObject == nil){
        self.mapObject = [[PKDataManager sharedManager] oneMapObjectWithId: mapObjectId];
    }
    [self clearSameControllersInNavigationControllers];
    
    self.view.backgroundColor = [UIColor colorWithWhite:1.0 alpha:1.0];
    
    

    self.scrollWasAnimated = NO;
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];//
    [self.view addSubview:_scrollView];
    self.scrollView.delegate = self;
    
    
    MAKImageGalleryView * imageGalleryView = [[MAKImageGalleryView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, zoomDetailImageHeight)];
    self.imageGalleryView = imageGalleryView;
    self.imageGalleryView.imageGalleryDataSource = self;
    self.imageGalleryView.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor];
    [_scrollView addSubview:self.imageGalleryView];
    
    _scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(zoomDetailImageHeight+10.f, 0, 0, 0);
    
    if(self.mapObject.brand.logoImage && ![self.mapObject.brand.logoImage isEqualToString:@"no_image.jpg"]){
        PKBrandLogo *logo = [[PKBrandLogo alloc] initWithFrame:CGRectMake(0, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height - (80.f + 0), 128.f, 80.f)];
        [_scrollView addSubview:logo];
        logo.clipsToBounds = YES;
        [logo setImageWithName:self.mapObject.brand.logoImage andContentModeTop:NO];
    }
    
    // . NSLog(@"imageGalleryView width = %.f", self.imageGalleryView.bounds.size.width);
    CGRect sizeButtonLike = CGRectMake(_scrollView.frame.size.width - PKGeoRouteCellTextAreaHeight - regularFZMargin, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height + regularFZMargin, PKGeoRouteCellTextAreaHeight, PKGeoRouteCellTextAreaHeight);
    PKButtonLike * likesButton = [[PKButtonLike alloc] initWithFrame:sizeButtonLike];
    [likesButton setImageWithLikeEnabled:self.mapObject.mylike withLikeCount:self.mapObject.likes andIsDetail:YES];
    [likesButton addTarget:self
                    action:@selector(pushBtnLike:)
          forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:likesButton];
    
    
    CGRect sizeTurIndust = CGRectMake(_scrollView.frame.size.width - (PKGeoRouteCellTextAreaHeight + regularFZMargin), self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height + regularFZMargin, PKGeoRouteCellTextAreaHeight, PKGeoRouteCellTextAreaHeight);
    if(self.mapObject.turIndust){
        sizeTurIndust = CGRectMake(_scrollView.frame.size.width - (regularFZMargin + 155), self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height - (regularFZMargin + 73) , 155, 73);
        PKTurIndustButton * turIndustBtn = [[PKTurIndustButton alloc] initWithFrame:sizeTurIndust];
        [turIndustBtn setImageOfTurIndustLogo];
        //    [turIndustBtn addTarget:self
        //                    action:@selector(pushBtnLike:)
        //          forControlEvents:UIControlEventTouchUpInside];
        [self.imageGalleryView addSubview:turIndustBtn];
    }
    
    
    
    
    
    
    
    
    
    
    self.titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:detailFZFontSizeTitle];
    _titleLabel.numberOfLines = 0;
    _titleLabel.text = self.mapObject.title;
    [_scrollView addSubview:_titleLabel];
    CGSize titleLabelSize = [_titleLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - PKGeoRouteCellTextAreaHeight - regularFZMargin * 2.0), CGFLOAT_MAX)];
    _titleLabel.frame = CGRectMake(regularFZMargin, self.imageGalleryView.frame.origin.y + self.imageGalleryView.frame.size.height + regularFZMargin, titleLabelSize.width, titleLabelSize.height);
    
//    // . NSLog(@"_titleLabel.frame = %@ %f %f %f %f",self.mapObject.title, _titleLabel.frame.origin.x, _titleLabel.frame.origin.y, _titleLabel.frame.size.width, _titleLabel.frame.size.height);
    
    self.subTitleLabel = [[UILabel alloc] init];
    if(![self.mapObject.subtitle isEqualToString:@""]){
        // . NSLog(@"isEqualToString %@",self.mapObject.subtitle);
        _subTitleLabel.font = [UIFont systemFontOfSize:detailFZFontSizeDescription];
        _subTitleLabel.numberOfLines = 0;
        _subTitleLabel.text = self.mapObject.subtitle;
        [_scrollView addSubview:_subTitleLabel];
        CGSize subTitleLabelSize = [_subTitleLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - PKGeoRouteCellTextAreaHeight - regularFZMargin - regularFZMargin), CGFLOAT_MAX)];
        _subTitleLabel.frame = CGRectMake(regularFZMargin, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + regularFZMargin, subTitleLabelSize.width, subTitleLabelSize.height);
    } else {
        [_scrollView addSubview:_subTitleLabel];
        _subTitleLabel.frame = CGRectMake(regularFZMargin, _titleLabel.frame.origin.y + _titleLabel.frame.size.height, 1, 1);
    }
    
    
    
//    // . NSLog(@"subTitleLabel = %f %f %f %f", _subTitleLabel.frame.origin.x, _subTitleLabel.frame.origin.y, _subTitleLabel.frame.size.width, _subTitleLabel.frame.size.height);
//    // . NSLog(@"subTitleLabel = %f", _subTitleLabel.frame.origin.y);
//    // . NSLog(@"subTitleLabel = %f", _subTitleLabel.frame.size.height);
//    // . NSLog(@"subTitleLabel = %f", regularFZMargin);
//    // . NSLog(@"subTitleLabel = %f", _subTitleLabel.frame.origin.y + _subTitleLabel.frame.size.height + regularFZMargin);
    
//    self.categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(regularFZMargin, _subTitleLabel.frame.origin.y + _subTitleLabel.bounds.size.height + regularFZMargin, imageGalleryView.frame.size.width - regularFZMargin - regularFZMargin, 20.f)];
//    _categoryLabel.font = [UIFont systemFontOfSize:16.0];
//    _categoryLabel.numberOfLines = 1;
//    self.categoryLabel.text = self.mapObject.types.typeObjValue;
    
    
    
    self.categoryLabel = [[UILabel alloc] init];
    _categoryLabel.font = [UIFont systemFontOfSize:14.0];
    _categoryLabel.numberOfLines = 1;
    self.categoryLabel.text = self.mapObject.types.typeObjValue;
    [_scrollView addSubview:_categoryLabel];
    CGSize categoryLabelSize = [_categoryLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin - regularFZMargin), CGFLOAT_MAX)];
    _categoryLabel.frame = CGRectMake(regularFZMargin, _subTitleLabel.frame.origin.y + _subTitleLabel.frame.size.height + regularFZMargin/2, categoryLabelSize.width, categoryLabelSize.height);
    
    
//    // . NSLog(@"categoryLabel = %f %f %f %f", _categoryLabel.frame.origin.x, _categoryLabel.frame.origin.y, _categoryLabel.frame.size.width, _categoryLabel.frame.size.height);
//    // . NSLog(@"gh imageGalleryView = %f  %f", self.imageGalleryView.frame.origin.y, self.imageGalleryView.frame.size.height);
//    // . NSLog(@"gh subTitleLabel = %f  %f", self.subTitleLabel.frame.origin.y, self.subTitleLabel.frame.size.height);
//    // . NSLog(@"gh categoryLabel = %f  %f", self.categoryLabel.frame.origin.y, self.categoryLabel.frame.size.height);
    
    
    
    
//    CGRect infoBlockFrame = CGRectMake(50.f, _categoryLabel.frame.origin.y + _categoryLabel.frame.size.height + 20.f, self.view.bounds.size.width - 50.f, 160.f);
//    CGSize cellSize = CGSizeMake(infoBlockFrame.size.width/2 - 10.f, infoBlockFrame.size.height/2 - 5.f);
//    PKMapObjectInfoBlocksView* infoBlocks = [[PKMapObjectInfoBlocksView alloc] initWithFrame:infoBlockFrame andLayoutSize:cellSize];
//    [infoBlocks setBackgroundColor: [UIColor clearColor]];
    
    __weak PKMapObjectDetailVC *weakSelf = self;
    NSDictionary* blocksArray = [self blocksArray];
    // . NSLog(@"JKD self = %@", self);
    CGRect infoBlockFrame;
    
    if([blocksArray[@"classes"] count] != 0){
        
        vc = [[PKPageViewController alloc]initWithViewControllerClasses:blocksArray[@"classes"]  andIconImage:blocksArray[@"images"] andIconImageSelected:blocksArray[@"imagesSelected"] andObject: self.mapObject andSelfVC: weakSelf];
        
        
        //Установите ширину каждого элемента
        vc.itemWidth = 60;
        //Выберите стиль подчеркивания
        vc.style = PKTopViewStyleNOLine;//PKTopViewStyleLine;
        //（Не подчеркнул）
        //        vc.style = ASTopViewStyleNOLine;
        //Установить по обе стороны пустого Ширина
        //        vc.sideBothWidth = 20;
        //Установить нормальный цвет шрифта
        //        vc.normalTitleColor = [UIColor orangeColor];
        //Настройки, выбранные при изменении шрифта, цвета
        //        vc.selectTitleColor = [UIColor blueColor];
        //Установить верхний элемент Цвет фона
        vc.topViewBackGroundColor = [UIColor clearColor];
        //Задать цвет подчеркивания
        //        vc.lineColor = [UIColor blueColor];
        //Набор всех предметов Ширина массива
        //    vc.itemWidthArray = @[@(150),@(80),@(70),@(90),@(150)];
        
        //#warning device size
        CGFloat marginLeftBlock = 50.f;//regularFZMargin;
        infoBlockFrame = CGRectMake(marginLeftBlock, _categoryLabel.frame.origin.y + _categoryLabel.frame.size.height + 20.f, self.view.bounds.size.width - marginLeftBlock, 160.f);
        //        CGSize cellSize = CGSizeMake(infoBlockFrame.size.width/2 - 10.f, infoBlockFrame.size.height/2 - 5.f);
        //        UIView* infoBlocks = [[UIView alloc] initWithFrame:infoBlockFrame];
        //        [infoBlocks setBackgroundColor: [UIColor redColor]];
        
        
        
        
        [self addChildViewController:vc];
        vc.view.frame = infoBlockFrame;
        [_scrollView addSubview:vc.view];
        [vc didMoveToParentViewController:self];
    } else {
        infoBlockFrame = CGRectMake(0, _titleLabel.frame.origin.y + _titleLabel.frame.size.height + 20.f, 0, 0);
    }

    
    //    // . NSLog(@"infoBlockFrame = %f %f %f %f", infoBlockFrame.origin.x, infoBlockFrame.origin.y, infoBlockFrame.size.width, infoBlockFrame.size.height);
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, _categoryLabel.frame.origin.y + _categoryLabel.frame.size.height - 50.f, self.view.bounds.size.width + 70.f, zoomDetailImageHeight)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [_scrollView insertSubview:backgroundInfoBlockImage atIndex:0];
    //    [[self.imageGalleryView superview] sendSubviewToBack : backgroundInfoBlockImage];
    
    
//    self.infoBlocksView = infoBlocks;
//    [_scrollView addSubview:self.infoBlocksView];
//    self.infoBlocksView.infoBlocksDataSource = self;
    
    
    self.descriptionLabel = [[UITextView alloc] init];
    _descriptionLabel.font = [UIFont systemFontOfSize:16.0];
    //    _descriptionLabel.textAlignment = UITextAlignmentRight;
    _descriptionLabel.textAlignment = NSTextAlignmentJustified;
    _descriptionLabel.selectable = YES;
    _descriptionLabel.editable = NO;
    _descriptionLabel.scrollEnabled = NO;
    _descriptionLabel.backgroundColor = [UIColor clearColor];
    
    NSString *descriptions = self.mapObject.descr;
    
    if(self.mapObject.brand){
        if(![self.mapObject.brand.descriptionss isEqualToString:@""]){
            descriptions = [NSString stringWithFormat:@"%@ \n%@", descriptions, self.mapObject.brand.descriptionss];
        }
    }
    
    if([descriptions isEqualToString:@""]){
        
//        CGFloat height = kScreenWidth - (infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin) - ;
        _descriptionLabel.frame = CGRectMake(regularFZMargin, infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin, regularFZMargin, 200.f);
    } else {
        
    //    _descriptionLabel.lineBreakMode = NSLineBreakByCharWrapping;
    //    _descriptionLabel.numberOfLines = 0;
    //    _descriptionLabel.copyingEnabled = YES;
        _descriptionLabel.text = descriptions;
        
    //    CGSize lLabelSize = [self.mapObject.descr sizeWithFont: _descriptionLabel.font forWidth:(_scrollView.frame.size.width - regularFZMargin * 2.0) lineBreakMode:_descriptionLabel.lineBreakMode];
        CGSize descriptionSize = [_descriptionLabel sizeThatFits:CGSizeMake((_scrollView.frame.size.width - regularFZMargin * 2.0), CGFLOAT_MAX)];
        _descriptionLabel.frame = CGRectMake(regularFZMargin, infoBlockFrame.origin.y + infoBlockFrame.size.height + regularFZMargin, descriptionSize.width, descriptionSize.height);
        
        
    }
    
    CGFloat contentHeight = _descriptionLabel.frame.origin.y + _descriptionLabel.frame.size.height + regularFZMargin;
    
    if([self.mapObject.brand.mapObjects count] > 1){
        CGFloat topMargin = contentHeight;
        UILabel *anotherPlacesLbl = [[UILabel alloc] initWithFrame:CGRectMake(regularFZMargin, topMargin, _scrollView.frame.size.width - regularFZMargin * 2.0, 44.f)];
        anotherPlacesLbl.text = [NSString stringWithFormat:@"%@:", TRANSLATE_UP(@"another_addresses")];
        [_scrollView addSubview:anotherPlacesLbl];
        topMargin = anotherPlacesLbl.frame.origin.y + anotherPlacesLbl.frame.size.height;
        for (PKMapObj *mapObject in [self.mapObject.brand.mapObjects allObjects]) {
            if(mapObject != self.mapObject){
                UIButton *anotherPlacesBtn = [[UIButton alloc] initWithFrame:CGRectMake(regularFZMargin, topMargin, 44.f, 44.f)];
                [anotherPlacesBtn setBackgroundColor:[PKUOStyleKit bgColorGradientBottom]];
                anotherPlacesBtn.layer.cornerRadius = 10.f;
                anotherPlacesBtn.clipsToBounds = YES;
                
                [anotherPlacesBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [anotherPlacesBtn setTitleColor:[PKUOStyleKit lightGrayBackgroundColor] forState:UIControlStateHighlighted];
                [anotherPlacesBtn setTitle:mapObject.adress forState:UIControlStateNormal];
                [anotherPlacesBtn setTitle:mapObject.adress forState:UIControlStateHighlighted];
                anotherPlacesBtn.titleLabel.lineBreakMode = NSLineBreakByClipping;
                anotherPlacesBtn.contentEdgeInsets = UIEdgeInsetsMake(8.f, regularFZMargin, 8.f, regularFZMargin);
                [anotherPlacesBtn sizeToFit];
                anotherPlacesBtn.tag = mapObject.idobj;
                [anotherPlacesBtn addTarget:self action:@selector(showAnotherAddresses:) forControlEvents:UIControlEventTouchUpInside];
                
                [_scrollView addSubview:anotherPlacesBtn];
                topMargin = anotherPlacesBtn.frame.origin.y + anotherPlacesBtn.frame.size.height + regularFZMargin;
            }

        }
        contentHeight = topMargin + regularFZMargin*2;
    }
    
    
    if(contentHeight < self.view.bounds.size.height){
        contentHeight = self.view.bounds.size.height + 20.f;
    }
    _scrollView.contentSize = CGSizeMake(_scrollView.frame.size.width, contentHeight);
    [_scrollView addSubview:_descriptionLabel];
    
    
    //+++++++++++++++++++++++++++++

//    UIButton* btnBack = [PKInterfaceManager slimNavbar:self];
    self.topNavButtonView = [[PKInterfaceManager sharedManager] slimNavbar:weakSelf];
//    self.btnBack = btnBack;
    
//    if(btnBack != nil){
//        [self.view addSubview:self.btnBack];
//    }
    
    //    self.tabBarController.view.backgroundColor = [UIColor clearColor];
    //+++++++++++++++++++++++++++++
    
    
}
//    self.objectInfoCollectionViewHeight.constant = self.objectInfoCollectionView.contentSize.height;
    
//    // . NSLog(@"self.infoBlocksView %@", self.infoBlocksView);
//    self.infoBlocksView.blockViewDataSource = self;
//    // . NSLog(@"self.infoBlocksView.blockViewDataSource %@", self.infoBlocksView.blockViewDataSource);
   
    
    
//    // . NSLog(@"self.infoBlockCVC %@", self.infoBlockCVC);
//    self.infoBlockCVC.blockViewCollectionDataSource = self;
//    // . NSLog(@"self.infoBlockCVC.blockViewCollectionDataSource %@", self.infoBlockCVC.blockViewCollectionDataSource);
    
//    self.infoBlockCVC.blockViewDataSource = self;
    
//    self.titleLabel.text = self.mapObject.title;
//    // . NSLog(@"self.mapObject.title =%@", self.mapObject.title);
    
//    // . NSLog(@"self.mapObject.title =%@", self.mapObject.);
    
    
    
//- (void)startDisplayLinkIfNeeded
//{
//    if (!_displayLink) {
//        // do not change the method it calls as its part of the GLKView
//        _displayLink = [CADisplayLink displayLinkWithTarget:self.view selector:@selector(display)];
//        [_displayLink addToRunLoop:[NSRunLoop mainRunLoop] forMode:UITrackingRunLoopMode];
//    }
//}

-(NSDictionary*)blocksArray{
    NSMutableArray* classMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* imageMutableArray = [[NSMutableArray alloc] init];
    NSMutableArray* imageSelectedMutableArray = [[NSMutableArray alloc] init];
    
    
    NSArray *workingTime = [self.mapObject.workingTime allObjects];
    if(!([workingTime count] == 0)){
        [classMutableArray addObject:[PKBlockTimeViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockTimeWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockTimeWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(self.mapObject.latitude != 0 && self.mapObject.longitude != 0){
        [classMutableArray addObject:[PKInfoAdressViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockMapWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(![self.mapObject.phone isEqualToString:@"0"] || ![self.mapObject.brand.contact.phone isEqualToString:@"0"]){
        [classMutableArray addObject:[PKBlockPhoneViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCallWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(![self.mapObject.email isEqualToString:@""] || ![self.mapObject.brand.contact.email isEqualToString:@""]){
        [classMutableArray addObject:[PKInfoEmailViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockEmailWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(![self.mapObject.siteurl isEqualToString:@""] || ![self.mapObject.brand.contact.siteurl isEqualToString:@""]){
        [classMutableArray addObject:[PKInfoSiteViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockSiteWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    
    


    
    if(mapObject.wiFi){
        [classMutableArray addObject:[PKBlockWiFiViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockWiFiWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockWiFiWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(mapObject.kidsfriendly){
        [classMutableArray addObject:[PKKidsFrindlyBlockViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconKidsFriendlyWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconKidsFriendlyWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(mapObject.payCard){
        [classMutableArray addObject:[PKInfoPayCardViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockCardWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockCardWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    if(mapObject.avalibleForDisables){
        [classMutableArray addObject:[PKInfoDisablesViewController class]];
        [imageMutableArray addObject:[PKUOStyleKit imageOfIconBlockDisablesWithIconColor:[PKUOStyleKit uOTextColor]]];
        [imageSelectedMutableArray addObject:[PKUOStyleKit imageOfIconBlockDisablesWithIconColor:[PKUOStyleKit bgColorGradientBottom]]];
    }
    
    NSMutableDictionary* mutableDictionary = [[NSMutableDictionary alloc] init];
    [mutableDictionary setValue:classMutableArray forKey:@"classes"];
    [mutableDictionary setValue:imageMutableArray forKey:@"images"];
    [mutableDictionary setValue:imageSelectedMutableArray forKey:@"imagesSelected"];
    NSDictionary* dict = [[NSDictionary alloc] initWithDictionary:mutableDictionary];
    return dict;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.feedbackGenerator = [[UISelectionFeedbackGenerator alloc] init];
    [self.feedbackGenerator prepare];
}
    
    // Do any additional setup after loading the view.
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat y = scrollView.contentOffset.y;
    
    
    if(y< -64.f){
        
        // Keep the generator in a prepared state.
        CGFloat fraction = ( 1 - (-1* (y+ 64.f)) / ( 32.f) );
        
        [self.topNavButtonView setHideFraction:fraction];
        if(fraction>0){
            
            //            NSLog(@"y = %f fraction = %f YES", y, fraction);
        } else {
            [self.feedbackGenerator selectionChanged];
            self.feedbackGenerator = nil;
            //            NSLog(@"y = %f fraction = %f", y, fraction);
        }
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    //    NSLog(@"y2 = %f", scrollView.contentOffset.y);
    if(scrollView.contentOffset.y>= -64.f){
        
        [self.topNavButtonView setHideFraction:1.f];
    }
    self.feedbackGenerator = nil;
    
}
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate{
    
    CGFloat y = scrollView.contentOffset.y;
    //    NSLog(@"y3 = %f", y );
    if(y< -(64.f + 32.f)){
        [[self navigationController] popViewControllerAnimated:YES];
    }
    self.feedbackGenerator = nil;
}
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    // . NSLog(@"scrollView.contentOffset2 %.f", scrollView.contentOffset.y);
//
//    CGFloat y = scrollView.contentOffset.y;
//    if(y> 250.f){
//        if(y< 300.f){
//            CGFloat alpha = 0.8f * 50.f / (300.f - y);
//            self.navigationController.view.backgroundColor = [UIColor colorWithWhite:1.f alpha:alpha];
//            [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
//                                                          forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
//            self.navigationController.navigationBar.shadowImage = [UIImage new];
//        } else {
//            self.navigationController.view.backgroundColor = [UIColor colorWithWhite:1.f alpha:0.8f];
//        }
//
//
//    } else {
//            [self.navigationController.navigationBar setBackgroundImage: [UIImage new]//[PKUOStyleKit imageOfDetailNavBarTranslucentWithSize: CGSizeMake(self.navigationController.navigationBar.bounds.size.width, self.navigationController.navigationBar.bounds.size.height+20.f)] //[UIImage new]
//                                                              forBarMetrics:UIBarMetricsDefault];
//    }
//}
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    // . NSLog(@"scrollView.contentOffset2 %.f", scrollView.contentOffset.y);
//}
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
//                  willDecelerate:(BOOL)decelerate{
//    // . NSLog(@"scrollView.contentOffset1 %.f", scrollView.contentOffset.y);
//}

-(void)popBack:(id)sender{
    // . NSLog(@"popBack");
    UIButton* btn = (UIButton*)sender;
    CGRect frame = btn.frame;
    frame.origin.x = - btn.frame.size.width;
    [UIView animateWithDuration:0.2
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         btn.frame = frame;
                     }
                     completion:^(BOOL finished){
                         [[self navigationController] popViewControllerAnimated:YES];
                     }];

}
-(void)clearSameControllersInNavigationControllers{
    NSArray * viewControllers = [self.navigationController viewControllers];
//    NSLog(@"viewControllers = %@", [self.navigationController viewControllers]);
    if([viewControllers count] > 3){
        if([[viewControllers firstObject] isKindOfClass:[PKMapObjectCollectionViewController class]]){//если переход с CVC
//            NSLog(@"viewControllers 1 = %@", [viewControllers firstObject]);
            if([[viewControllers objectAtIndex:([viewControllers count] - 1)] isKindOfClass:[self class]]){
//                NSLog(@"\nviewControllers 2 = %@", [viewControllers objectAtIndex:([viewControllers count] - 1)]);
                NSMutableArray *newViewControllers = [[NSMutableArray alloc] init];
                for (int vcIndex = 0; vcIndex<([viewControllers count] - 1); vcIndex++) {
                    if(vcIndex !=([viewControllers count] - 2)){
                        [newViewControllers addObject:[viewControllers objectAtIndex:vcIndex]];
                    }
                }
                [newViewControllers addObject:self];
                [self.navigationController setViewControllers:newViewControllers];
            }
        }
    }
#warning Need clear method for segue from Push Message
}
-(void)showAnotherAddresses:(id)sender{
    
    UIButton *btn = (UIButton*)sender;
    self.detailMapObjectID = btn.tag;

    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PKMapObjectDetailVC *vc= [storyBoard instantiateViewControllerWithIdentifier:@"mapObjDetail"];
    vc.mapObjectId = (int)self.detailMapObjectID;
    [self.navigationController pushViewController:vc animated:YES];
    
}
-(void)showOnMap{
    
    NSArray * viewControllers = [self.navigationController viewControllers];
    if([viewControllers count] > 3){
        if([[viewControllers objectAtIndex:([viewControllers count] - 2)] isKindOfClass:[PKMainMapViewController class]]){
            // . NSLog(@"\nviewControllers 1 = %@", [viewControllers objectAtIndex:([viewControllers count] - 2)]);
            if([[viewControllers objectAtIndex:([viewControllers count] - 3)] isKindOfClass:[self class]]){
                // . NSLog(@"\nviewControllers 2 = %@", [viewControllers objectAtIndex:([viewControllers count] - 3)]);
                NSMutableArray *newViewControllers = [[NSMutableArray alloc] init];
                for (int vcIndex = 0; vcIndex<([viewControllers count] - 3); vcIndex++) {
                    [newViewControllers addObject:[viewControllers objectAtIndex:vcIndex]];
                }
                [newViewControllers addObject:self];
                [self.navigationController setViewControllers:newViewControllers];
            }
        }
    }
    // . NSLog(@"\nviewControllers count = %lu",  [[self.navigationController viewControllers] count]);
//    NSInteger index = 0;
//    NSInteger mapIndex = 0;
//    for (id viewController in viewControllers) {
//
//
//        if([viewController isKindOfClass:[self class]]){
//            // . NSLog(@"viewControllers = %@", viewController);
//        }
//        if ([viewController isKindOfClass:[PKMainMapViewController class]]) {
//            // . NSLog(@"viewControllers = %@", viewController);
//            mapIndex =
//        }
//        index++;
//    }
    
    //
    [self performSegueWithIdentifier:@"toMapFromMapObjectDetail" sender:self];
//    [self.tabBarController.tabBar setHidden:YES];
    
//
//    NSArray *viewContrlls=[[self navigationController] viewControllers];
//    for( int i=0;i<[ viewContrlls count];i++){
//        id map=[viewContrlls objectAtIndex:i];
//        // . NSLog(@"viewContrlls = %@", [map class]);
//        if([map isKindOfClass:[PKMainMapViewController class]]){
//            // A is your class where to popback
//            [[self navigationController] popToViewController:map animated:YES];
//            return;
//        }
//    }
//
//    NSArray *tabBarControllers = [self.tabBarController viewControllers];
//    for( int i=0;i<[ tabBarControllers count];i++){
//        id map2=[tabBarControllers objectAtIndex:i];
//        if([map2 isKindOfClass:[UINavigationController class]]){
//            UINavigationController * navControl = (UINavigationController*)map2;
//            NSArray *viewContrlls=[navControl viewControllers];
//            for( int i=0;i<[ viewContrlls count];i++){
//                id map=[viewContrlls objectAtIndex:i];
//                // . NSLog(@"viewContrlls = %@", [map class]);
//                if([map isKindOfClass:[PKMainMapViewController class]]){
//
//                    PKMainMapViewController *mapViewController = (PKMainMapViewController*)map;
//                    [mapViewController setMoveToDestanationMapObject:self.mapObject];
//                    // . NSLog(@"found i = %d %lu", i, [self.tabBarController.viewControllers indexOfObject:map]);
//                    self.tabBarController.selectedIndex = i;//[self.tabBarController.viewControllers indexOfObject:map];
//
//                    // A is your class where to popback
//                    //                    [[self navigationController] popToViewController:map animated:YES];
//                    return;
//                }
//            }
//        }
//    }
    //    PKMapBoxViewController *otherController = [[self.tabBarController viewControllers] objectAtIndex:0];
    //
    //    [otherController moveToDestanation]; // this is magic;
    //    [self.tabBarController setSelectedIndex:0];
    
}


-(void)pushBtnLike: (PKButtonLike*) sender{
    if(self.mapObject.mylike){
        if(self.mapObject.likes>0){
            [sender setImageWithLikeEnabled:NO withLikeCount:self.mapObject.likes - 1 andIsDetail:YES];
        } else {
            [sender setImageWithLikeEnabled:NO withLikeCount:self.mapObject.likes andIsDetail:YES];
        }
    } else {
        [sender setImageWithLikeEnabled:YES withLikeCount:self.mapObject.likes + 1 andIsDetail:YES];
    }
    [sender setNeedsDisplay];
    [[PKUserInteraction sharedManager] switchObjectLike:self.mapObject.objectID];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//#pragma mark - PKObjectInfoBlockCVCDataSource
//- (NSInteger)numberOfBlockInObjectInfoBlockViewCollection:(PKObjectInfoBlockCVC *)infoBlockVC {
//    return 4;
//}
//- (PKMapObj*)mapObjectInObjectInfoBlockViewCollection:(PKObjectInfoBlockCVC *)infoBlockVC atIndex:(NSInteger)index{
//
//    return self.mapObject;
//}


//#pragma mark - PKObjectInfoBlockCVDataSource
//- (NSInteger)numberOfBlockInBlocksView:(MAKImageGalleryView *)galleryView {
//    return 4;
//}
//- (PKMapObj*)mapObjectInBlocksView:(MAKImageGalleryView *)galleryView atIndex:(NSInteger)index{
//
//    return self.mapObject;
//}




#pragma mark - PKMapObjectInfoBlocksViewDataSource
//- (NSInteger)numberOfBlockInBlocksView:(MAKImageGalleryView *)blockView {
//    return 7;
//}
- (PKMapObj*)blockInfoFromMapObject{
    
    return self.mapObject;
}

//-(CGSize)blockSize{
//    //UICollectionViewCell
//    // . NSLog(@"infoBlocksView self.view.bounds.size.width = %f", self.view.bounds.size.width);
//    CGSize cellSize = CGSizeMake(self.view.bounds.size.width, 60);
//    return cellSize;
//}



#pragma mark - MAKImageGalleryViewDataSource
- (NSInteger)numberOfImagesInGallery:(MAKImageGalleryView *)galleryView {
    return [[self.mapObject.images allObjects] count] + 1;
}
- (void)loadImageInGallery:(MAKImageGalleryView *)galleryView atIndex:(NSInteger)index callback:(void(^)(UIImage *))callback{
    
    NSString* imageUrl;
    BOOL imageWasSet = NO;
    
    if(index == 0){
        if(![self.mapObject.prevImageName isEqualToString:@"no_image.jpg"]){
            NSString* dataPath = LOCAL_mapObjImagePathPreview(self.mapObject.prevImageName);
            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
                // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
                callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
                imageWasSet = YES;
            } else {
                // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
                imageUrl = URL_mapObjImagePathPreview(self.mapObject.prevImageName);
            }
        } else {
            callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
                imageWasSet = YES;
        }
    } else {
        
        PKObjImages *image = [[self.mapObject.images allObjects] objectAtIndex:index - 1];
        
        if(![image.imageName isEqualToString:@"no_image.jpg"]){
            
            NSString* dataPath = LOCAL_mapObjImagePathPhotogall(image.imageName);
            if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
                // . NSLog(@"\nfileExistsAtPath!! %@", dataPath);
                callback([[UIImage alloc] initWithContentsOfFile:dataPath]);
                imageWasSet = YES;
            } else {
                // . NSLog(@"\nNOT fileExistsAtPath!! %@", dataPath);
                imageUrl = URL_mapObjImagePathPhotogall(image.imageName);
            }
        } else {
            callback([PKUOStyleKit imageOfNoFotoWithSize:CGSizeMake(self.imageGalleryView.frame.size.width, self.imageGalleryView.frame.size.height)]);
            imageWasSet = YES;
        }
    }
    
    
    
    
        
        
    if(!imageWasSet){
        AFImageDownloader* download = [[AFImageDownloader alloc] init];
        
        [self.downloads addObject:download]; //СОХРАНИТЬ ОБЪЕКТ В ПАМЯТИ
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString: imageUrl] cachePolicy:NSURLRequestReturnCacheDataElseLoad
                                             timeoutInterval:1];
        [download downloadImageForURLRequest:request success:^(NSURLRequest * request, NSHTTPURLResponse * response, UIImage * image) {
            [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
            
            callback(image);
            
            
        } failure:^(NSURLRequest * request, NSHTTPURLResponse * response, NSError * error) {
            [self.downloads removeObject:download];//УДАЛИТЬ ОБЪЕКТ ИЗ ПАМЯТИ
            // . NSLog(@"Error download image");
        }];
    }
    
}
- (UIViewContentMode)imageGallery:(MAKImageGalleryView *)galleryView contentModeForImageAtIndex:(NSInteger)index {
    return UIViewContentModeScaleAspectFill;
}


#pragma mark - Segue

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"toMapFromMapObjectDetail"]){
        PKMainMapViewController *vc = (PKMainMapViewController*)[segue destinationViewController];
        [vc setMoveToDestanationMapObject:self.mapObject];
    }
}


@end
