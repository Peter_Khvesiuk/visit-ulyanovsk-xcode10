//
//  PKTurIndust.h
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 23.07.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKTurIndustButton : UIButton

-(void)setImageOfTurIndustIcon;
-(void)setImageOfTurIndustIconBig;
-(void)setImageOfTurIndustLogo;
@end
