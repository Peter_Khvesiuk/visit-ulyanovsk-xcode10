//
//  PKTurIndust.m
//  Visit-Ulyanovsk
//
//  Created by Peter Khvesiuk on 23.07.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKTurIndustButton.h"
#import "PKUOStyleKit.h"

@implementation PKTurIndustButton

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


-(void)setImageOfTurIndustIcon{
    [self setImage:[PKUOStyleKit imageOfTurIndustIcon]  forState:UIControlStateNormal];
    [self setImage:[PKUOStyleKit imageOfTurIndustIcon]  forState:UIControlStateHighlighted];
}
-(void)setImageOfTurIndustIconBig{
    [self setImage:[PKUOStyleKit imageOfTurIndustIconBig]  forState:UIControlStateNormal];
    [self setImage:[PKUOStyleKit imageOfTurIndustIconBig]  forState:UIControlStateHighlighted];
}
-(void)setImageOfTurIndustLogo{
    [self setImage:[PKUOStyleKit imageOfTurIndustILogo]  forState:UIControlStateNormal];
    [self setImage:[PKUOStyleKit imageOfTurIndustILogo]  forState:UIControlStateHighlighted];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
