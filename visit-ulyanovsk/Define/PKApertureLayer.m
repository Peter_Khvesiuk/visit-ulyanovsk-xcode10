//
//  PKApertureLayer.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 30/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKApertureLayer.h"

@implementation PKApertureLayer

- (instancetype)init
{
    if (self = [super init])
    {
        self.apertureValue = 0.0;
        
    }
    return self;
}

- (instancetype)initWithLayer: (id)layer
{
    if ((self = [super initWithLayer: layer]))
    {
        if ([layer isKindOfClass: PKApertureLayer.class])
        {
            self.apertureValue = ((PKApertureLayer *)layer).apertureValue;
        }
    }
    return self;
}

+ (BOOL)needsDisplayForKey: (NSString*)key
{
    if([key isEqualToString: @"apertureValue"])
        return YES;
    
    return [super needsDisplayForKey: key];
}


- (id<CAAction>)actionForKey: (NSString *)event
{
    
    if([event isEqualToString: @"apertureValue"])
    {
        
        CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath: event];
        theAnimation.fromValue = [[self presentationLayer] valueForKey: event];
        
        return theAnimation;
    }
    
    return [super actionForKey: event];
}

@end

