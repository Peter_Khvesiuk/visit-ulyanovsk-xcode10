//
//  PKUOStyleKit.h
//  Visit Ulyanovsk
//
//  Created by Peter Khvesiuk on 07/10/2019.
//  Copyright © 2019 Vector consulting. All rights reserved.
//
//  Generated by PaintCode
//  http://www.paintcodeapp.com
//

#import <UIKit/UIKit.h>



@interface UIColor (PKUOStyleKitAdditions)

- (UIColor*)blendedColorWithFraction: (CGFloat)fraction ofColor: (UIColor*)color;

@end



typedef NS_ENUM(NSInteger, PKUOStyleKitResizingBehavior)
{
    PKUOStyleKitResizingBehaviorAspectFit, //!< The content is proportionally resized to fit into the target rectangle.
    PKUOStyleKitResizingBehaviorAspectFill, //!< The content is proportionally resized to completely fill the target rectangle.
    PKUOStyleKitResizingBehaviorStretch, //!< The content is stretched to match the entire target rectangle.
    PKUOStyleKitResizingBehaviorCenter, //!< The content is centered in the target rectangle, but it is NOT resized.

};

extern CGRect PKUOStyleKitResizingBehaviorApply(PKUOStyleKitResizingBehavior behavior, CGRect rect, CGRect target);


@interface PKUOStyleKit : NSObject

// iOS Controls Customization Outlets
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconHotelTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarMapTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarDiscoverTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* rowToRightTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* shadowBackgroundTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* tabbarBottomBackgroundTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarTourTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarFavoriteTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarRestaurantTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarHotelTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* iconTabBarMuseumTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* turIndustIconTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* turIndustIconBigTargets;
@property (strong, nonatomic) IBOutletCollection(NSObject) NSArray* turIndustILogoTargets;

// Colors
+ (UIColor*)bgColorGradientBottom;
+ (UIColor*)bgColorGradientTop;
+ (UIColor*)grass;
+ (UIColor*)red;
+ (UIColor*)uOTextColor;
+ (UIColor*)lightGrayBackgroundColor;
+ (UIColor*)enabledColor;
+ (UIColor*)disabledColor;
+ (UIColor*)uOTextColor50;
+ (UIColor*)lightNotWhite;

// Images
+ (UIImage*)icon;
+ (UIImage*)iconMapUO;

// Drawing Methods
+ (void)drawLeninskMemorialWithFrame: (CGRect)frame camAngel: (CGFloat)camAngel loadingFractionX: (CGFloat)loadingFractionX downoadingProgressString: (NSString*)downoadingProgressString downoadingProgressPercent: (NSString*)downoadingProgressPercent loadingInProgress: (BOOL)loadingInProgress;
+ (void)drawNavButtonFilterWithFilterEnable: (BOOL)filterEnable pressedFilter: (BOOL)pressedFilter;
+ (void)drawNavButtonFilterWithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing filterEnable: (BOOL)filterEnable pressedFilter: (BOOL)pressedFilter;
+ (void)drawTopNavBarFilterSatelliteWithNavButtonHideFraction: (CGFloat)navButtonHideFraction pressedBack: (BOOL)pressedBack pressedFilter: (BOOL)pressedFilter pressedSatellite: (BOOL)pressedSatellite home: (BOOL)home filterLang: (NSString*)filterLang;
+ (void)drawNavButtonSatellite;
+ (void)drawNavButtonSatelliteWithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;
+ (void)drawTopNavBarFilterWithNavButtonHideFraction: (CGFloat)navButtonHideFraction pressedBack: (BOOL)pressedBack pressedFilter: (BOOL)pressedFilter home: (BOOL)home filterLang: (NSString*)filterLang;
+ (void)drawTopNavBarBasicWithNavButtonHideFraction: (CGFloat)navButtonHideFraction pressedBack: (BOOL)pressedBack home: (BOOL)home;
+ (void)drawTopNavBarSatelliteWithNavButtonHideFraction: (CGFloat)navButtonHideFraction pressedBack: (BOOL)pressedBack pressedSatellite: (BOOL)pressedSatellite home: (BOOL)home;
+ (void)drawCanvas10;
+ (void)drawCanvas10WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;
+ (void)drawCanvas6;
+ (void)drawCanvas6WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;
+ (void)drawLoadingProgressWithDownoadingProgressString: (NSString*)downoadingProgressString downoadingProgressPercent: (NSString*)downoadingProgressPercent;
+ (void)drawLoadingProgressWithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing downoadingProgressString: (NSString*)downoadingProgressString downoadingProgressPercent: (NSString*)downoadingProgressPercent;
+ (void)drawIconTabInformation;
+ (void)drawIconTabInformationWithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;
+ (void)drawCanvas5WithStrokeWidth: (CGFloat)strokeWidth;
+ (void)drawCanvas5WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing strokeWidth: (CGFloat)strokeWidth;
+ (void)drawCanvas12;
+ (void)drawCanvas12WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;
+ (void)drawCanvas1;
+ (void)drawCanvas1WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;
+ (void)drawTopNavBarLocationWithNavButtonHideFraction: (CGFloat)navButtonHideFraction activeDistance: (BOOL)activeDistance sortDistance: (BOOL)sortDistance closestText: (NSString*)closestText;
+ (void)drawCanvas16WithIconColor: (UIColor*)iconColor;
+ (void)drawCanvas16WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing iconColor: (UIColor*)iconColor;
+ (void)drawLenin150;
+ (void)drawLenin150WithFrame: (CGRect)targetFrame resizing: (PKUOStyleKitResizingBehavior)resizing;

// Generated Images
+ (UIImage*)imageOfPKLogoWithSize: (CGSize)imageSize isRussian: (BOOL)isRussian;
+ (UIImage*)imageOfIconHotel;
+ (UIImage*)imageOfIconDiscoverWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconToursWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconMapWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconSouvenirWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconRestaurantWithSize: (CGSize)imageSize;
+ (UIImage*)imageOfIconProfileWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconTabBarMap;
+ (UIImage*)imageOfIconTabBarDiscover;
+ (UIImage*)imageOfIconBlockTimeWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfLikedIconWithFillHeart: (BOOL)fillHeart likeCount: (NSString*)likeCount;
+ (UIImage*)imageOfRowToRight;
+ (UIImage*)imageOfIconBlockMapWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockEmailWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockSiteWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockBookingWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfShadowBackground;
+ (UIImage*)imageOfDetailNavBarTranslucentWithSize: (CGSize)imageSize;
+ (UIImage*)imageOfNavButtonBackWithPressed: (BOOL)pressed pressedBack: (BOOL)pressedBack home: (BOOL)home;
+ (UIImage*)imageOfTopNavBarWhiteWithShadowWithSize: (CGSize)imageSize;
+ (UIImage*)imageOfTabbarBottomBackground;
+ (UIImage*)imageOfIconTabBarTour;
+ (UIImage*)imageOfNoFotoWithSize: (CGSize)imageSize;
+ (UIImage*)imageOfIconBlockCardWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockCallWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockWiFiWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconFavoriteWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconTipsWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfLikedIconBigWithFillHeart: (BOOL)fillHeart likeCount: (NSString*)likeCount;
+ (UIImage*)imageOfIconAskWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconFootballWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconCalendarWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconTabBarFavorite;
+ (UIImage*)imageOfIconTabBarRestaurant;
+ (UIImage*)imageOfIconTabBarHotel;
+ (UIImage*)imageOfMapMarkerFoodWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfMapMarkerHotelWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfMapMarkerMuseumWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconTabBarMuseum;
+ (UIImage*)imageOfMapMarkerTrainWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfMapMarkerDeafaultWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfMapMarkerFavoriteWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfMapMarkerAttractionWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconMarkerPicturesqueWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconMarkerTheatreWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconMarkerParkWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconMarkerMonumentWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconMarkerInformationWithMarkerFavorite: (BOOL)markerFavorite turIndust: (BOOL)turIndust;
+ (UIImage*)imageOfIconMarkerFifa2018WithStadiumName: (NSString*)stadiumName;
+ (UIImage*)imageOfIconMarkerFifaUlskWithStadiumName: (NSString*)stadiumName;
+ (UIImage*)imageOfIconBlockNameWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockTextWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfLogoVCWithPressed: (BOOL)pressed;
+ (UIImage*)imageOfLogoATWithPressed: (BOOL)pressed;
+ (UIImage*)imageOfIconFootballDarkWithSize: (CGSize)imageSize strokeWidth: (CGFloat)strokeWidth;
+ (UIImage*)imageOfIconBlockCalendarWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockAgeWithIconColor: (UIColor*)iconColor ageString: (NSString*)ageString;
+ (UIImage*)imageOfIconBlockPaymentWithIconColor: (UIColor*)iconColor pressed: (BOOL)pressed;
+ (UIImage*)imageOfIconBlockDisablesWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockTurIndustWithIconColor: (UIColor*)iconColor pressed: (BOOL)pressed;
+ (UIImage*)imageOfTurIndustIcon;
+ (UIImage*)imageOfTurIndustIconBig;
+ (UIImage*)imageOfTurIndustILogo;
+ (UIImage*)imageOfIconBlockCodeWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockCheckBoxWithIconColor: (UIColor*)iconColor pressed: (BOOL)pressed manyCondition: (BOOL)manyCondition;
+ (UIImage*)imageOfIconBlockAdditionalWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconBlockPlacesWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconFilterDisablesWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconFilterCardWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconFilterTimeWithIconColor: (UIColor*)iconColor;
+ (UIImage*)imageOfIconKidsFriendlyWithIconColor: (UIColor*)iconColor;

@end
