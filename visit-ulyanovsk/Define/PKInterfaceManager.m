//
//  PKInterfaceManager.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 30.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKInterfaceManager.h"
#import <UIKit/UIKit.h>
#import "PKUOStyleKit.h"
#import "PKMainMapViewController.h"
#import "PKMapObjectCollectionViewController.h"
#import "PKGeoRouteCollectionViewController.h"

@interface PKInterfaceManager ()

@property (weak,nonatomic) PKTopNavButtonView *topNavBarButtons;

@end

@implementation PKInterfaceManager

@synthesize topNavBarButtons;

+(PKInterfaceManager*) sharedManager{
    
    static PKInterfaceManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PKInterfaceManager alloc] init];
    });
    
    return manager;
}

-(void)slimTitleVC:(UIViewController*)vc andTitle:(NSString*)title andFrame:(CGRect)frame{
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:frame];
    titleLabel.text = [title uppercaseString];
    titleLabel.textColor = [PKUOStyleKit uOTextColor];
    UIFontDescriptor * fontD = [titleLabel.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:
                                //UIFontDescriptorTraitBold |
                                UIFontDescriptorTraitItalic];
    titleLabel.font = [UIFont fontWithDescriptor:fontD size:cvcTitleSize];
    
    
    if([vc isKindOfClass:[UITableViewController class]]){
        UITableViewController*vct = (UITableViewController*)vc;
        [vct.tableView addSubview:titleLabel];
    } else if([vc isKindOfClass:[UICollectionViewController class]]){
        UICollectionViewController*vcc = (UICollectionViewController*)vc;
        [vcc.collectionView addSubview:titleLabel];
    } else if([[[vc.view subviews] firstObject] isKindOfClass:[UIScrollView class]]){
        UIScrollView *scrollView = (UIScrollView*)[[vc.view subviews] firstObject];
        [scrollView addSubview:titleLabel];
    } else {
        [vc.view addSubview:titleLabel];
    }
    
    
    
    
}
- (void) slimVC:(UIViewController*)vc andTitle:(NSString*)title {
    
    [self slimTitleVC:vc andTitle:title andFrame:CGRectMake(100.f, -zoomTopMargin + 8.f, vc.view.frame.size.width - 100.f, 40.f)];
    //    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(100.f, -56.f, vc.view.frame.size.width - 100.f, 40.f)];
    
    //    titleLabel.text = [title uppercaseString];
    //    titleLabel.textColor = [PKUOStyleKit uOTextColor];
    //    UIFontDescriptor * fontD = [titleLabel.font.fontDescriptor
    //                                fontDescriptorWithSymbolicTraits:
    //                                //UIFontDescriptorTraitBold |
    //                                UIFontDescriptorTraitItalic];
    //    titleLabel.font = [UIFont fontWithDescriptor:fontD size:22];
    //    if([vc isKindOfClass:[UITableViewController class]]){
    //        UITableViewController*vct = (UITableViewController*)vc;
    //        [vct.tableView addSubview:titleLabel];
    //    } else {
    //        [vc.view addSubview:titleLabel];
    //    }
    
}
//- (PKTopNavButtonView*) slimNavbarWithView:(UIView*) view andTitle:(NSString*)title andTitleHide:(BOOL)hide{
//
//}

- (UIButton*)returnButtonWithCGrect:(CGRect)frame andTitle:(NSString*)title{
    UIButton *btn = [[UIButton alloc] initWithFrame:frame];
    
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateHighlighted];
    [btn setBackgroundColor:[PKUOStyleKit bgColorGradientBottom]];
    btn.layer.cornerRadius = 10.f;
    btn.clipsToBounds = YES;
    
    return btn;
}
- (void)backgroundOneRoundShapesInVC: (UIViewController*) vc inCGrect: (CGRect) frame{
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:frame];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    vc.view.clipsToBounds = YES;
    [vc.view insertSubview:backgroundInfoBlockImage atIndex:0];
}
- (void)backgroundTwoRoundShapesInVC: (UIViewController*) vc{
    
    UIImageView *backgroundInfoBlockImage = [[UIImageView alloc] initWithFrame:CGRectMake(-20.f, 50.f, vc.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage setImage: [PKUOStyleKit imageOfShadowBackground]];
    [vc.view insertSubview:backgroundInfoBlockImage atIndex:0];
    vc.view.clipsToBounds = YES;
    UIImageView *backgroundInfoBlockImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(-70.f, backgroundInfoBlockImage.frame.origin.y + backgroundInfoBlockImage.frame.size.height + 50.f, vc.view.bounds.size.width + 70.f, 300.f)];
    [backgroundInfoBlockImage2 setImage: [PKUOStyleKit imageOfShadowBackground]];
    [vc.view insertSubview:backgroundInfoBlockImage2 atIndex:0];
    
    
}
- (PKTopNavButtonView*) slimNavbar:(UIViewController*) vc andTitle:(NSString*)title andTitleHide:(BOOL)hide{
    
    
    
    PKTopNavButtonView*topNavButtonView = [self slimNavbar:vc];
    CGFloat marginTop;
    if([vc isKindOfClass:[UICollectionViewController class]]){
        marginTop = 8.f;
    }else {
        marginTop = marginTopNavigationHeight + 8.f;
    }
    
    [self slimTitleVC:vc andTitle:title andFrame:CGRectMake(topNavButtonView.frame.size.width + cvcTitleOfsetLeft, marginTop, vc.view.bounds.size.width - topNavButtonView.frame.size.width - 20.f, 40.f)];
    
    
    //    UILabel* titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(topNavButtonView.frame.size.width + 20.f, 10.f + marginTop, vc.view.bounds.size.width - topNavButtonView.frame.size.width - 20.f, 40.f)];
    //
    //    titleLabel.text = [title uppercaseString];
    //    titleLabel.textColor = [PKUOStyleKit uOTextColor];
    //    UIFontDescriptor * fontD = [titleLabel.font.fontDescriptor
    //                                fontDescriptorWithSymbolicTraits:
    //                                //UIFontDescriptorTraitBold |
    //                                UIFontDescriptorTraitItalic];
    //    titleLabel.font = [UIFont fontWithDescriptor:fontD size:22];
    //    titleLabel.font = [UIFont fontWithName:@"TrebuchetMS-Bold" size:22];
    //    if([vc isKindOfClass:[UICollectionViewController class]]){
    //        UICollectionViewController*vcc = (UICollectionViewController*)vc;
    //        [vcc.collectionView addSubview:titleLabel];
    //    } else {
    //        [vc.view addSubview:titleLabel];
    //    }
    
    //    else if([vc isKindOfClass:[UITableViewController class]]){
    //        UITableViewController*vct = (UITableViewController*)vc;
    //        [vct.tableView addSubview:titleLabel];
    //    }
    
    
    
    return topNavButtonView;
}
-(PKTopNavButtonLocationView*)loactionViewForCVC:(UIViewController*)vc{
    
    if([vc respondsToSelector:@selector(calcDistance:)]){
        
        PKTopNavButtonLocationView * loactionView = [[PKTopNavButtonLocationView alloc] initWithFrame:CGRectMake(vc.view.frame.size.width - 74.f, marginTopNavigationHeight, 74.f, 56.f) andActiveDistance:NO andSortDistance:NO];
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(15.f, 0.f, 59.f, 50.f)];

        [btn addTarget:vc action:@selector(calcDistance:) forControlEvents:UIControlEventTouchUpInside];
        [loactionView addSubview:btn];
        [vc.view addSubview:loactionView];
        return loactionView;
    }
    
    return nil;
}
-(PKTopNavButtonView*)slimNavbar:(UIViewController*)vc{
    
    [self style_default];
    [vc.navigationController setNavigationBarHidden:YES];
    CGFloat height = 8.f + marginTopNavigationHeight;
//    CGFloat height = 30.f;
//    if(kScreenHeight==812.f){
//        height = 52.f;
//    }
    UIImageView* topNavBarWhiteWithShadow = [[UIImageView alloc] initWithFrame:CGRectMake(0.f, 0.f, vc.view.frame.size.width, height)];
    [topNavBarWhiteWithShadow setImage: [PKUOStyleKit imageOfTopNavBarWhiteWithShadowWithSize:CGSizeMake(vc.view.frame.size.width, height)]];
    [vc.view addSubview:topNavBarWhiteWithShadow];
    
    
    
    BOOL popBack = [vc respondsToSelector:@selector(popBack:)];
    BOOL popBackToMainVC = [vc respondsToSelector:@selector(popBackToMainVC:)];
    BOOL isPushedAndNeedBack = NO;
    BOOL isShowSatelite = NO;
    BOOL isHideFilter = NO;
    if([vc isKindOfClass:[PKMainMapViewController class]]){
        PKMainMapViewController *vcmap = (PKMainMapViewController*)vc;
        if(vcmap.fifa2018){
            isShowSatelite = NO;
            isHideFilter = YES;
        } else if(vcmap.moveToMapObject && vcmap.showManyDestanations){
            isPushedAndNeedBack = YES;
            isShowSatelite = YES;
        } else if(vcmap.showManyDestanations){
            isPushedAndNeedBack = YES;
            isShowSatelite = NO;
        } else if(vcmap.moveToMapObject){
            isPushedAndNeedBack = YES;
            isShowSatelite = YES;
        } else {
            isPushedAndNeedBack = NO;
            isShowSatelite = YES;
        }
        
    } else if([vc isKindOfClass:[PKMapObjectCollectionViewController class]]){
        PKMapObjectCollectionViewController *vcmap = (PKMapObjectCollectionViewController*)vc;
        isHideFilter = vcmap.contentRecomended;
    } else if([vc isKindOfClass:[PKGeoRouteCollectionViewController class]]){
        isHideFilter = YES;
    }
    
    
    
    if(popBack || popBackToMainVC){
        // . NSLog(@"respondsToSelector popBack = YES");
        BOOL satellite = NO, filter = NO;
        
        if([vc respondsToSelector:@selector(segueFilter:)]){
            filter = !isHideFilter;
            // . NSLog(@"respondsToSelector segueFilter = YES");
        }
        if([vc respondsToSelector:@selector(changeMapStyle:)]){
            satellite = isShowSatelite;
            // . NSLog(@"respondsToSelector changeMapStyle = YES");
        }
        PKTopNavButtonView *topNavBarButtons;
        if(popBackToMainVC && !isPushedAndNeedBack){
            topNavBarButtons = [[PKTopNavButtonView alloc] initWithFilter:filter andSatellite:satellite andHome:YES];
        } else {
            
            if(isPushedAndNeedBack){
                topNavBarButtons = [[PKTopNavButtonView alloc] initWithFilter:NO andSatellite:satellite andHome:NO];
            } else {
                topNavBarButtons = [[PKTopNavButtonView alloc] initWithFilter:filter andSatellite:satellite andHome:NO];
            }
            
        }
        
        
        if(satellite){
            CGRect rect;
            if(isPushedAndNeedBack){
                rect = CGRectMake(69.f, 0.f, 68.f, 50.f);
            } else {
                rect = CGRectMake(137.f, 0.f, 70.f, 50.f);
            }
            UIButton* buttonSatellite = [[UIButton alloc] initWithFrame:rect];
            [buttonSatellite
             addTarget:vc
             action:@selector(changeMapStyle:) forControlEvents:UIControlEventTouchUpInside];
            [buttonSatellite
             addTarget:self
             action:@selector(satellitePressed) forControlEvents:UIControlEventTouchDown];
            [buttonSatellite
             addTarget:self
             action:@selector(dragButtonOutside:) forControlEvents:UIControlEventTouchDragOutside];
            [buttonSatellite
             addTarget:self
             action:@selector(satelliteUnPressed) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchUpInside];
            [topNavBarButtons addSubview:buttonSatellite];
        }
        if(filter && !isPushedAndNeedBack){
            UIButton* buttonFilter = [[UIButton alloc] initWithFrame:CGRectMake(69.f, 0.f, 68.f, 50.f)];
            [buttonFilter
             addTarget:vc
             action:@selector(segueFilter:) forControlEvents:UIControlEventTouchUpInside];
            [buttonFilter
             addTarget:self
             action:@selector(filterPressed) forControlEvents:UIControlEventTouchDown];
            [buttonFilter
             addTarget:self
             action:@selector(dragButtonOutside:) forControlEvents:UIControlEventTouchDragOutside];
            [buttonFilter
             addTarget:self
             action:@selector(filterUnPressed) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchUpInside];
            [topNavBarButtons addSubview:buttonFilter];
        }
        
        
        
        UIButton* buttonBack = [[UIButton alloc] initWithFrame:CGRectMake(0.f, 0.f, 69.f, 50.f)];
        
        
        if(popBackToMainVC && !isPushedAndNeedBack){
            [buttonBack
             addTarget:vc
             action:@selector(popBackToMainVC:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            [buttonBack
             addTarget:vc
             action:@selector(popBack:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        
        
        [buttonBack
         addTarget:self
         action:@selector(backPressed) forControlEvents:UIControlEventTouchDown];
        [buttonBack
         addTarget:self
         action:@selector(dragButtonOutside:) forControlEvents:UIControlEventTouchDragOutside];
        [buttonBack
         addTarget:self
         action:@selector(backUnPressed) forControlEvents:UIControlEventTouchUpOutside | UIControlEventTouchUpInside];
        
        [topNavBarButtons addSubview:buttonBack];
        
        self.topNavBarButtons = topNavBarButtons;
        [vc.view addSubview:self.topNavBarButtons];
    }
    return self.topNavBarButtons;
    
}

- (void)style_light{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)style_default{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}


-(void)dragButtonOutside:(UIButton*)sender{
    // . NSLog(@"dragButtonOutside");
    //    [sender sendActionsForControlEvents:UIControlEventTouchDragExit];
    [sender sendActionsForControlEvents:UIControlEventTouchUpOutside];
}

-(void)backPressed{
    
    [self.topNavBarButtons setPressedBack:YES];
}
-(void)backUnPressed{
    [self.topNavBarButtons setPressedBack:NO];
}
-(void)satellitePressed{
    [self.topNavBarButtons setPressedSatellite:YES];
}
-(void)satelliteUnPressed{
    [self.topNavBarButtons setPressedSatellite:NO];
}
-(void)filterPressed{
    [self.topNavBarButtons setPressedFilter:YES];
}
-(void)filterUnPressed{
    [self.topNavBarButtons setPressedFilter:NO];
}
-(void)popBack:(id)sender{
    // . NSLog(@"Never call");
    //    [[self navigationController] popViewControllerAnimated:YES];
}
-(void)popBackToMainVC:(id)sender{
    
}
-(void)segueFilter:(id)sender{
    // . NSLog(@"Never call");
}
-(void)changeMapStyle:(id)sender{
    // . NSLog(@"Never call");
}
@end
