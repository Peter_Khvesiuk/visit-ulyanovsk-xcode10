//
//  PKTopNavButtonView.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 09.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PKTopNavButtonView : UIView
@property (nonatomic) BOOL open;
@property (nonatomic) CGFloat hideFraction;
@property (nonatomic) BOOL pressedBack;
@property (nonatomic) BOOL pressedFilter;
@property (nonatomic) BOOL pressedSatellite;
@property (nonatomic) BOOL home;
- (instancetype)initWithFilter:(BOOL) filterEnable andSatellite:(BOOL) satelliteEnable andHome:(BOOL) home;
@end
