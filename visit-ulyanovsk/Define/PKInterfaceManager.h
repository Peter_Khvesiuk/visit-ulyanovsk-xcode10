//
//  PKInterfaceManager.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 30.03.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PKTopNavButtonView.h"
#import "PKTopNavButtonLocationView.h"
@class UIViewController;

@interface PKInterfaceManager : NSObject



+ (PKInterfaceManager*) sharedManager;

- (PKTopNavButtonView*) slimNavbar:(UIViewController*) vc;
- (void) slimVC:(UIViewController*)vc andTitle:(NSString*)title;
- (PKTopNavButtonView*) slimNavbar:(UIViewController*) vc andTitle:(NSString*)title andTitleHide:(BOOL)hide;
- (PKTopNavButtonLocationView*) loactionViewForCVC:(UIViewController*) vc;
- (void)backgroundOneRoundShapesInVC: (UIViewController*) vc inCGrect: (CGRect) frame;
- (void)backgroundTwoRoundShapesInVC: (UIViewController*) vc;
- (UIButton*)returnButtonWithCGrect:(CGRect)frame andTitle:(NSString*)title;

@end
