//
//  PKBrandLogo.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 27/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKBrandLogo : UIImageView

-(void)setImageWithName:(NSString *)imageName andFillBackgroundWithHex:(NSString*) hexColor andContentModeTop:(BOOL)toTop;
-(void)setImageWithName:(NSString *)imageName andContentModeTop:(BOOL)toTop;
+(UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
@end

NS_ASSUME_NONNULL_END
