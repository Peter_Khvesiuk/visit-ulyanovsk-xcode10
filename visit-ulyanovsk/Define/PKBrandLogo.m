//
//  PKBrandLogo.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 27/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKBrandLogo.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@implementation PKBrandLogo

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void)setImageWithName:(NSString *)imageName andContentModeTop:(BOOL)toTop{
    [self setImageWithName:imageName andFillBackgroundWithHex:@"" andContentModeTop:toTop];
}

-(void)setImageWithName:(NSString *)imageName andFillBackgroundWithHex:(NSString*) hexColor andContentModeTop:(BOOL)toTop{
    NSString* dataPath;
    NSString* imageUrl;
    dataPath = LOCAL_LogosPath(imageName);
    imageUrl = URL_LogosPath(imageName);
    
    if([hexColor isEqualToString:@""]){
        [self setBackgroundColor: [self colorWithHexString:hexColor]];
    }
    self.clipsToBounds = YES;
    if(toTop){
        self.contentMode = UIViewContentModeTopLeft;
    } else {
        self.contentMode = UIViewContentModeBottomLeft;
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:dataPath]){
        [self setImage:[[UIImage alloc] initWithContentsOfFile:dataPath]];
    } else {
        NSURLRequest* requestwhereImage = [NSURLRequest requestWithURL:[NSURL URLWithString:imageUrl]];
        __weak UIImageView* weakImageView = self;
        self.image = nil;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [self setImageWithURLRequest:requestwhereImage
                    placeholderImage:nil
                             success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                 
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                 CGSize size = CGSizeMake(weakImageView.frame.size.width, weakImageView.frame.size.height);
                                
                                 UIImage* imageRef = [PKBrandLogo imageWithImage:image scaledToSize:CGSizeMake(size.width, size.height)];
                            
                                     weakImageView.image = imageRef;
                                 });
//                                 [weakImageView layoutSubviews];
//                                 [[weakImageView superview] setNeedsLayout];
                             }
                                 
                             failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                 NSLog(@"failure imageUrl = %@",imageUrl);
                             }];
        
        });
    }
}
- (UIColor *)colorWithHexString:(NSString *)stringToConvert
{
    if([stringToConvert isEqualToString:@""]){
        return [UIColor clearColor];
    }
    NSString *noHashString = [stringToConvert stringByReplacingOccurrencesOfString:@"#" withString:@""]; // remove the #
    NSScanner *scanner = [NSScanner scannerWithString:noHashString];
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]]; // remove + and $
    
    unsigned hex;
    if (![scanner scanHexInt:&hex]) return nil;
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f green:g / 255.0f blue:b / 255.0f alpha:1.0f];
}
+ (UIImage*)imageWithImage:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    CGSize imgSize = image.size;
    
    float ratio;
    float scaledHeight;
    float scaledWidth;
    
    if(imgSize.width > imgSize.height){
        ratio = newSize.width / imgSize.width;
        scaledWidth = newSize.width;
        scaledHeight = imgSize.height * ratio;
        if(scaledHeight > newSize.height){
            ratio = newSize.height / imgSize.height;
            scaledWidth = imgSize.width * ratio;
            scaledHeight = newSize.height;
        } else {
            scaledWidth = newSize.width;
        }
    } else {
        ratio = newSize.height / imgSize.height;
        scaledWidth = imgSize.height * ratio;
        if(scaledWidth > newSize.width){
            ratio = newSize.width / imgSize.width;
            scaledHeight = imgSize.height * ratio;
            scaledWidth = newSize.width;
        } else {
            scaledHeight = newSize.height;
        }
    }
   
    CGSize sized = CGSizeMake(scaledWidth, scaledHeight);
    
    //UIGraphicsBeginImageContext( sized );
    
    float scale;
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00) {
        // RETINA DISPLAY
        scale = 2.0;
    } else if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 3.00){
        scale = 3.0;
    } else {
        scale = 1.0;
    }
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [image drawInRect:CGRectMake(0,0,sized.width,sized.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    
    UIGraphicsEndImageContext();
    
    return newImage;
}
@end


