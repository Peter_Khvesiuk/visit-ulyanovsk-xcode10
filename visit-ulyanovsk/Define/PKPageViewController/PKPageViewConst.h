//
//  Header.h
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//



#define PKItemTitleColorSelected [UIColor colorWithRed:168.0/255.0 green:20.0/255.0 blue:4/255.0 alpha:1]
#define PKItemTitleColorNormal   [UIColor colorWithRed:0 green:0 blue:0 alpha:1]
//  Навигация по меню Цвет фона
#define PKTopViewBGColor        [UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0]
//Подчеркивание по умолчанию цвет
#define PKBottomLineColor              [UIColor redColor]
//Выберите пункт ширина по умолчанию
static CGFloat const PKItemWidth        = 65.0f;
//Навигации меню по умолчанию высота
static CGFloat const PKItemHeight       = 44.0f;
//  Название Размер(выбран/не выбран)
//static CGFloat const PKItemTitleFontSelected = 18.0f;
//static CGFloat const PKItemTitlefontNormal   = 15.0f;

//Подчеркивание по умолчанию высота
static CGFloat const PKBottomLineHeight   = PKItemHeight;//2.0f;


//Используется, чтобы установить значение тега метки
static CGFloat const PKTopLabelTag   = 8956;
