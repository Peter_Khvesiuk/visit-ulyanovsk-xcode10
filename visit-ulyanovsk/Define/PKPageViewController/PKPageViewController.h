//
//  ASPageViewController.h
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PKPageViewConst.h"
#import "PKTopView.h"


@class PKMapObj, PKGeoRoute;
@interface PKPageViewController : UIViewController
//Сверху по обе стороны пространства,например, можно использовать режим введите время, вы можете опустошить сегмент с кнопку Set вернуться и т. д.
@property (nonatomic, assign) CGFloat sideBothWidth;
/**
 *  В массив, чтобы установить ширину каждого элемента
 */
@property(nonatomic, strong) NSArray *itemWidthArray;
/**
 *  一Ключ для установки элемента Ширина
 */
@property (nonatomic, assign) CGFloat itemWidth;

//Установить верхний пытается style
@property (nonatomic, assign) PKTopViewStyle style;

//Установить каждый пункт в Имя шрифта
@property (nonatomic, copy) NSString *titleName;
//Цвет подчеркивания
@property (nonatomic, strong) UIColor *lineColor;

//Не выбран, когда размер шрифта
@property (nonatomic, assign) CGFloat normalSize;
//Выбранный размер шрифта
@property (nonatomic, assign) CGFloat selectedSize;
//Не выбран цвет шрифта
@property (nonatomic, strong) UIColor *normalTitleColor;
//Выбранный цвет шрифта
@property (nonatomic, strong) UIColor *selectTitleColor;
//Верхняя часть представления цвета фона
@property (nonatomic, strong) UIColor *topViewBackGroundColor;
-(id)initWithViewControllerClasses:(NSArray<Class> *)classes andIconImage:(NSArray <NSString *> *)iconImages andIconImageSelected:(NSArray <NSString *> *)iconImagesSelected andObject: (id)object andSelfVC:(UIViewController*) vc;
//-(id)initWithViewControllerClasses:(NSArray<Class> *)classes andIconImage:(NSArray <NSString *> *)iconImages andIconImageSelected:(NSArray <NSString *> *)iconImagesSelected andGeoRoute: (PKGeoRoute *)geoRoute andSelfVC:(UIViewController*) vc;
@end
