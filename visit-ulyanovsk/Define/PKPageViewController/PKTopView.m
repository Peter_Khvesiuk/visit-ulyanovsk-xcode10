//
//  SSTopView.m
//  SSPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//

#import "PKTopView.h"
#import "PKUOStyleKit.h"
@interface PKTopView()<PKTopIconImageDelegate>
{
    //title数组
    NSArray *_iconImagesArray;
    NSArray *_iconImagesSelectedArray;
    UIScrollView *_scrollView;
    //下划线View
    PKTopLine *_bottomLine;
    NSMutableArray *_eachItemFrameArray;
    //需要设置的空格的大小
    CGFloat _addSpace;
    //用来保存当前所在的label
    PKTopIconImage *_selectLabel;
}
@end

@implementation PKTopView

-(id)initWithFrame:(CGRect)frame andIconImages:(NSArray *)iconImages andIconImagesSelected:(NSArray *)iconImageSelected
{
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor whiteColor];
        _iconImagesArray = [NSArray arrayWithArray:iconImages];
        _iconImagesSelectedArray = [NSArray arrayWithArray:iconImageSelected];
        _eachItemFrameArray = [NSMutableArray array];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    //创建View上面的ScrollerView
    [self creatScrollView];
    //创建scrollView上面的每一个Item的frame
    [self creatItem];
    [self creatWidthStyle];
    //创建上面的每一个label
    [self creatItemLabel];
}

//MARK:创建View上面的ScrollerView
-(void)creatScrollView
{
    _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.backgroundColor = [UIColor whiteColor];
    _scrollView.scrollsToTop = NO;
    if (self.topBackColor) {
        _scrollView.backgroundColor = self.topBackColor;
    }
    [self addSubview:_scrollView];
}

-(void)creatWidthStyle
{
    switch (self.style) {
        case PKTopViewStyleLine:
            //创建下划线的view
            [self creatBottomLine];
            break;
        case PKTopViewStyleNOLine:
            
            break;
        default:
            break;
    }
}

-(void)creatBottomLine
{
    //Ширина имеет значение и scrollView то contentsize Равных
//    _bottomLine = [[PKTopLine alloc]initWithFrame:CGRectMake(0, self.frame.size.height - 2, _scrollView.contentSize.width, PKBottomLineHeight)];
    _bottomLine = [[PKTopLine alloc]initWithFrame:CGRectMake(0, 0, _scrollView.contentSize.width, PKBottomLineHeight)];
    _bottomLine.backgroundColor = [UIColor clearColor];
    _bottomLine.color = self.lineColor.CGColor;
//    С этой ширины, то мы назначаем друг item то frame
    _bottomLine.itemFrames = _eachItemFrameArray;
    [_scrollView addSubview:_bottomLine];
    [_bottomLine setwithCurrentIndex:0 WithContentOffSetX:kScreenWidth WithTopSpaceWidth:self.topSpaceWidth];
}

//Нажмите на метку, когда выполнение метода
-(void)clickLabelMethod:(int)currentIndex withContentOffSetX:(CGFloat)contentOffSetX withToIndex:(int)toIndex
{
    [_bottomLine clickLabelWithCurrentIndex:currentIndex withContentOffSetX:contentOffSetX WithTopSpaceWidth:self.topSpaceWidth withToIndex:toIndex];
    //Следующие используется для работы над label Состояние
//    NSInteger currentTag = currentIndex + PKTopLabelTag;
    NSInteger toTag = PKTopLabelTag + toIndex;
    if ((contentOffSetX - kScreenWidth + 50.f) != 0) {
//        PKTopIconImage *currentLabel = (PKTopIconImage *)[self viewWithTag:currentTag];
        PKTopIconImage *nextlabel = (PKTopIconImage *)[self viewWithTag:toTag];
        _selectLabel = nextlabel;
        // . NSLog(@"\nselectLabel 1 = %ld", (long)_selectLabel.tag);
        //求出rate
//        CGFloat rate = (fabs(contentOffSetX - kScreenWidth - 50.f)) / kScreenWidth;
//        currentLabel.rate = 1 - rate;
//        nextlabel.rate = rate;
    }
    [self refreshCurrenrItemToCenter];
}

//Руководство слайд, когда выполнение метода
-(void)hhhhhWithCurrent:(int)curindex withconX:(CGFloat)XXX
{
    [_bottomLine setwithCurrentIndex:curindex WithContentOffSetX:XXX WithTopSpaceWidth:self.topSpaceWidth];
    //Следующие используется для работы над меткой государства
    NSInteger currentTag = curindex + PKTopLabelTag;
    if ((XXX - kScreenWidth + 50.f) < 0) {
        // . NSLog(@"hhhhhWithCurrent XXX - kScreenWidth");
//        PKTopIconImage *currentLabel = (PKTopIconImage *)[self viewWithTag:currentTag];
        PKTopIconImage *nextlabel = (PKTopIconImage *)[self viewWithTag:currentTag - 1];
        _selectLabel = nextlabel;
        // . NSLog(@"\nselectLabel 01 = %ld   %f", (long)_selectLabel.tag, (XXX - kScreenWidth + 50.f));
        //Поиском rate
//        CGFloat rate = (fabs(XXX - kScreenWidth + 50.f)) / kScreenWidth;
//        currentLabel.rate = 1 - rate;
//        nextlabel.rate = rate;
    }
    if ((XXX - kScreenWidth + 50.f) > 0) {
        // . NSLog(@"hhhhhWithCurrent XXX - kScreenWidth else");
//        PKTopIconImage *currentLabel = (PKTopIconImage *)[self viewWithTag:currentTag];
        PKTopIconImage *nextlabel = (PKTopIconImage *)[self viewWithTag:currentTag + 1];
        //Перейти к последнему
        if (curindex == self.itemWidthArray.count - 1) {
            _selectLabel = (PKTopIconImage *)[self viewWithTag:self.itemWidthArray.count - 1 + PKTopLabelTag];
            // . NSLog(@"\nselectLabel 2 = %ld %f", (long)_selectLabel.tag, (XXX - kScreenWidth + 50.f));
        }else{
            _selectLabel = nextlabel;
            // . NSLog(@"\nselectLabel 3 = %ld %f", (long)_selectLabel.tag, (XXX - kScreenWidth + 50.f));
        }
        
        //Поиском rate
//        CGFloat rate = (fabs(XXX + 50.f - kScreenWidth)) / kScreenWidth;
//        currentLabel.rate = 1 - rate;
//        nextlabel.rate = rate;
    }
    if ((XXX - kScreenWidth + 50.f) == 0) {
        PKTopIconImage *label = (PKTopIconImage *)[self viewWithTag:currentTag];
        _selectLabel = label;
        // . NSLog(@"\nselectLabel 4 = %ld %f", (long)_selectLabel.tag, (XXX - kScreenWidth + 50.f));
        [self setSelectedImageFromSelected];
    }
    [self refreshCurrenrItemToCenter];
}

//MARK: Создать ScrollView Выше item
-(void)creatItem
{
    //Набор каждого item То frame
    CGFloat max = 0.0;
    for (int i = 0; i < self.itemWidthArray.count; i++) {
        max += [self.itemWidthArray[i] floatValue];
    }
//    if (max < kScreenWidth) {
//        Пространства
        //Нарисовать космос Размер
//        _addSpace = (kScreenWidth - max) / (self.itemWidthArray.count + 1);
//        for (int i = 0; i < self.itemWidthArray.count; i++) {
//            CGFloat X = 0.0;
//            for (int j = 0; j < i; j++) {
//                X = X +[self.itemWidthArray[j] floatValue];
//            }
//            CGRect rect = CGRectMake(X + _addSpace * (i + 1), 0, [self.itemWidthArray[i] floatValue], PKBottomLineHeight);
//            [_eachItemFrameArray addObject:[NSValue valueWithCGRect:rect]];
//        }
//    } else {
        //На этот раз нет места
        for (int i = 0; i < self.itemWidthArray.count; i++) {
            CGFloat X = 0.0;
            for (int j = 0; j < i; j++) {
                X += [self.itemWidthArray[j] floatValue];
            }
            CGRect rect = CGRectMake(X, 0, [self.itemWidthArray[i] floatValue], PKBottomLineHeight);
            [_eachItemFrameArray addObject:[NSValue valueWithCGRect:rect]];
        }
//    }
    
    _scrollView.contentSize = CGSizeMake([_eachItemFrameArray.lastObject CGRectValue].origin.x + [self.itemWidthArray.lastObject floatValue] + _addSpace, 0);
}
//Создайте выше всех label
-(void)creatItemLabel
{
    for (int i = 0; i < _eachItemFrameArray.count; i++) {
        CGRect frame = [_eachItemFrameArray[i] CGRectValue];
        
        
        PKTopIconImage *imageView = [[PKTopIconImage alloc]initWithFrame:CGRectMake(11.f, 6.f, 30.f, 30.f)];
        imageView.tag = PKTopLabelTag + i;
        imageView.image = _iconImagesArray[i];
//        imageView.image.size
//        imageView.backgroundColor = [UIColor whiteColor];
//        imageView.clipsToBounds = [UIColor whiteColor];
        imageView.delegate = self;
//        label.textAlignment = NSTextAlignmentCenter;
//        label.normalTitleSize = self.normalSize;
//        label.selectedTilteSize = self.selectedSize;
//        label.normalTitleColor = self.normalItemTitleColor;
//        label.selectedTitleColor = self.selectItemTitleColor;
        imageView.userInteractionEnabled = YES;
        if (i == 0) {
            _selectLabel = imageView;
        }
        //Вызвать метод, чтобы установить первую выбранном состоянии, другой не выбранного состояния
//        if (i == 0) {
//            [imageView selectedLabelNOAnimation];
//        }else{
//            [imageView deselectedlabelNOAnimation];
//        }
//        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, PKItemHeight - PKBottomLineHeight)];
        UIView *viewItem = [[UIView alloc]initWithFrame:CGRectMake(4, 0, frame.size.width-8, PKItemHeight)];
        viewItem.backgroundColor = [UIColor whiteColor];
        viewItem.layer.cornerRadius = 10.f;
        viewItem.layer.masksToBounds = YES;
        [viewItem addSubview:imageView];
        UIView *view = [[UIView alloc]initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, PKItemHeight)];
        view.backgroundColor = [UIColor clearColor];
//        view.layer.cornerRadius = 5.f;
//        view.layer.masksToBounds = YES;
        [view addSubview:viewItem];
        [_scrollView addSubview:view];
    }
    [self setSelectedImageFromSelected];
}

//Обновить местоположение пункт, что он находится в Центральном положении
-(void)refreshCurrenrItemToCenter
{
    CGRect willToLabelFrame = [[_selectLabel superview] superview].frame;//_selectLabel.frame;
    CGFloat itemX = willToLabelFrame.origin.x;
    CGFloat width = _scrollView.frame.size.width - 50.f;
    CGSize contentSize = _scrollView.contentSize;
    //Х-координата по оси у интерфейса, как правило, больше времени, чтобы рассмотреть, является ли или не слайд scrollView Он расположен в середине
    // . NSLog(@"refreshCurrenrItemToCenter willToLabelFrame = %f, itemX = %f, width = %f, contentSize = %f", willToLabelFrame.size.width, itemX, width, contentSize.width);
    if (itemX > width/2) {
        //
        CGFloat targetX = 0.0;
        if ((contentSize.width - itemX) <= width/2) {
            targetX = contentSize.width - width;
        }else{
            targetX = willToLabelFrame.origin.x - width / 2 + willToLabelFrame.size.width / 2;
        }
        if (targetX + width > contentSize.width) {
            targetX = contentSize.width - width;
        }
        [_scrollView setContentOffset:CGPointMake(targetX, 0) animated:YES];
    }else{
        [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

#pragma mark - SSTopLabelDelegate
-(void)didClickTopItemScrollToVC:(PKTopIconImage *)item
{
    _selectLabel = item;
    // . NSLog(@"\nselectLabel 4 = %ld", (long)_selectLabel.tag);
    [self setSelectedImageFromSelected];
    
    // . NSLog(@"didClickTopItemScrollToVC");
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollTopDestinationVC:)]) {
        [self.delegate scrollTopDestinationVC:(item.tag - PKTopLabelTag)];
    }
}

-(void)setSelectedImageFromSelected{
    // . NSLog(@"\nselectLabel ===== %ld", (long)_selectLabel.tag);
    [self setSelectedItemImage:_selectLabel];
}
-(void)setSelectedItemImage:(PKTopIconImage *)item{
    
    
//    item.image = _iconImagesArray[i];
    
    
    
//    UIView *view = [item superview];
    
    NSArray *views = [_scrollView subviews];
    NSUInteger i = 0;
    for (UIView *subview in views) {
        PKTopIconImage *subviewBackground = (PKTopIconImage *)[[[[subview subviews] firstObject] subviews] firstObject];
        
        if([subviewBackground isEqual:item]){
            subviewBackground.image = _iconImagesSelectedArray[i];
        } else {
            subviewBackground.image = _iconImagesArray[i];
//            NSUInteger index = item.tag - PKTopLabelTag;
            
        }
//        subviewBackground.backgroundColor = [UIColor whiteColor];
        
        [subviewBackground setNeedsDisplay];
        i++;
        
    }
    
//    view.backgroundColor = [PKUOStyleKit lightGrayBackgroundColor];
    [item setNeedsDisplay];
}
- (CATransition *)slideAnimation
{
    CATransition *transition = [CATransition animation];
    [transition setType:kCATransitionMoveIn];
    [transition setSubtype:kCATransitionFromRight];
    return transition;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
