
//
//  ASPageViewController.m
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//

#import "PKPageViewController.h"
#import "PKInfoBlockViewController.h"
//#import "PKMapObject+CoreDataClass.h"
//#import "PKGeoRoute+CoreDataClass.h"
//#import "PKInformTips+CoreDataClass.h"

//@class PKMapObj, PKGeoRoute, PKInformTips;

@interface PKPageViewController ()<UIPageViewControllerDelegate, UIPageViewControllerDataSource, UIScrollViewDelegate, PKTopViewDelegate>
{
    id _object;
    
    UIPageViewController *_pageViewController;
    NSArray<Class> *_classesArray;
    NSMutableArray *_vcArray;
    NSArray<NSString *> *_iconImageAray;
    NSArray<NSString *> *_iconImageSelectedAray;
    //Запись текущего индекса
    NSInteger _currentVCIndex;
    //В верхней части экрана
    PKTopView *_topView;
    //Сверху по обе стороны разрыва
    CGFloat _sideWidth;
    //Каждый элемент Ширина
    NSMutableArray *_itemWidthArrayInternal;
    //Установить переменную использовать для разделения нажать на слайд или боковой слайд слайд
    BOOL _isClick;
    //Для получения нажмите на метку индекса стоимости
    NSInteger _willToIndex;
//    UIViewController* _vc;
}
@property (nonatomic, weak) id _vc;
@end

@implementation PKPageViewController

@synthesize _vc;

-(void)setSideBothWidth:(CGFloat)sideWidth
{
    _sideWidth = sideWidth;
}
//Создать метод экземпляра

-(id)initWithViewControllerClasses:(NSArray<Class> *)classes andIconImage:(NSArray <NSString *> *)iconImages andIconImageSelected:(NSArray <NSString *> *)iconImagesSelected andObject: (id)object andSelfVC:(UIViewController*) vc
{
    if (self = [super init]) {
        
        
        _object = object;
        
        
        
        NSAssert(classes.count == iconImages.count, @"You set the number of the controller and the title number is inconsistent");
        
        _itemWidthArrayInternal = [NSMutableArray arrayWithCapacity:iconImages.count];
        _vcArray = [NSMutableArray arrayWithCapacity:iconImages.count];
        _classesArray = [NSArray arrayWithArray:classes];
        _iconImageAray = [NSArray arrayWithArray:iconImages];
        _iconImageSelectedAray = [NSArray arrayWithArray:iconImagesSelected];
        self.style = PKTopViewStyleLine;
        self.lineColor = PKBottomLineColor;
        _vc = vc;
//        self.normalSize = ASItemTitlefontNormal;
//        self.selectedSize = ASItemTitleFontSelected;
//        self.normalTitleColor = ASItemTitleColorNormal;
//        self.selectTitleColor = ASItemTitleColorSelected;
        //Попытки создать экземпляр
        [self creatVC];
    }
    return self;
}
//MARK:Попытки создать экземпляр
//-(void)creatVCGeoRoute
//{
//    for (int i = 0; i < _classesArray.count; i++) {
//        [_vcArray addObject:(PKInfoBlockViewController*)[[_classesArray[i] alloc]initWithGeoRoute:(PKGeoRoute *)_geoRoute andVC:_vc]];
//    }
//}
-(void)creatVC
{
    for (int i = 0; i < _classesArray.count; i++) {
        [_vcArray addObject:(PKInfoBlockViewController*)[[_classesArray[i] alloc]initWithObject:(id)_object andVC:_vc]];
    }
}

//Создание каждой части пользовательского интерфейса
-(void)creatUI
{
    //Экземпляр контроллера pageViewController
    [self creatPageViewController];
}

//MARK: Экземпляр контроллера pageViewController
-(void)creatPageViewController
{
    _pageViewController = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    //Укажите первый ВК
    [_pageViewController setViewControllers:@[_vcArray[0]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    //Настроить прокси
    _pageViewController.delegate = self;
    _pageViewController.dataSource = self;
    //Набор pageViewControler кадр
    [self setPageViewControllerFrame];
    
    //В pageViewController над функцией scrollview добавить прокси
    for (UIView *view in _pageViewController.view.subviews) {
        if ([view isKindOfClass:[UIScrollView class]]) {
            [view setValue:self forKey:@"delegate"];
            break;
        }
    }
    [self.view addSubview:_pageViewController.view];
//    // . NSLog(@"_pageViewController.view = %f", _pageViewController.view.frame.size.width);
}

//MARK: Набор pageViewControler кадр
-(void)setPageViewControllerFrame
{
    //Нет навигации нулю
//    UIView *tabBar = self.tabBarController.tabBar ? self.tabBarController.tabBar : self.navigationController.toolbar;
    CGFloat height = 0;//tabBar && !tabBar.hidden ? CGRectGetHeight(tabBar.frame) : 0;
//    if (self.navigationController.navigationBar) {
//        //По умолчанию 44 высота до верха зрения
//        _pageViewController.view.frame = CGRectMake(0, 64 + ASItemHeight, kScreenWidth, kScreenHeight - 64 - height - ASItemHeight);
//    }else{
//        _pageViewController.view.frame = CGRectMake(0, 20 + ASItemHeight, kScreenWidth, kScreenHeight - 20 - height - ASItemHeight);
//    }
    _pageViewController.view.frame = CGRectMake(0, 0 + PKItemHeight, kScreenWidth, kScreenHeight - 20 - height - PKItemHeight);
}

//MARK: -- UIPageViewController Метод протокола
//Возвращает следующий ВК
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    // . NSLog(@"PKPageViewController viewControllerAfterViewController");
    NSInteger index = [_vcArray indexOfObject:viewController];
    if (index == _vcArray.count - 1) {
        return nil;
    }
    return _vcArray[index + 1];
}

//Вернуться к предыдущему VC
-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    // . NSLog(@"PKPageViewController viewControllerBeforeViewController");
    NSInteger index = [_vcArray indexOfObject:viewController];
    if (index == 0) {
        return nil;
    }
    return _vcArray[index - 1];
}

//После окончания вызова
-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    // . NSLog(@"PKPageViewController didFinishAnimating");
    if (completed && finished) {
        _currentVCIndex = [_vcArray indexOfObject:_pageViewController.viewControllers[0]];
    }
}

//MARK:scrollView Метод протокола
-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // . NSLog(@"PKPageViewController scrollViewDidScroll");
    if (scrollView.contentOffset.x > (kScreenWidth + 50.f) * 2 || scrollView.contentOffset.x < 0) {
        // . NSLog(@"PKPageViewController scrollViewDidScroll return");
        return;
    }
    if (_isClick) {
        //Если это щелчок при реализации этого метода
//        // . NSLog(@"PKPageViewController scrollViewDidScroll _isClick");
        [_topView clickLabelMethod:(int)_currentVCIndex withContentOffSetX:scrollView.contentOffset.x withToIndex:(int)_willToIndex];
        // . NSLog(@"PKPageViewController scrollViewDidScroll NO x = %f == %f", scrollView.contentOffset.x, kScreenWidth);
        if ((scrollView.contentOffset.x - kScreenWidth + 50.f) == 0) {
            // . NSLog(@"PKPageViewController scrollViewDidScroll NO");
            _isClick = NO;
        }
    }else{
        
        // . NSLog(@"PKPageViewController scrollViewDidScroll !_isClick");
        [_topView hhhhhWithCurrent:(int)_currentVCIndex withconX:scrollView.contentOffset.x];
    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    // . NSLog(@"PKPageViewController stopp!!!!");
//    [_topView setSelectedImageFromSelected];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    if (self.itemWidthArray.count > 0) {
        NSAssert(_iconImageAray.count == self.itemWidthArray.count, @"You set the width of the item array not isEqual to the controllers count");
    }
    if (self.itemWidth > 0) {
        for (int i = 0; i < _iconImageAray.count; i++) {
            [_itemWidthArrayInternal addObject:@(self.itemWidth)];
        }
    }else{
        for (int i = 0; i < _iconImageAray.count; i++) {
            [_itemWidthArrayInternal addObject:@(PKItemWidth)];
        }
    }
    //Создание каждой части сборки
    [self creatUI];
    
    //Создать вид сверху
    [self creatTopView];
//    [_topView setSelectedImageFromSelected];
}

//Создание верхней части View
-(void)creatTopView
{
    CGFloat topViewY;
//    if (self.navigationController.navigationBar) {
//        topViewY = 64;
//    }else{
//        topViewY = 20;
//    }
    topViewY = 0;
    
    _topView = [[PKTopView alloc]initWithFrame:CGRectMake(_sideWidth, topViewY, kScreenWidth - 50.f - _sideWidth * 2, PKItemHeight) andIconImages:_iconImageAray andIconImagesSelected:_iconImageSelectedAray];
    if (self.topViewBackGroundColor) {
        _topView.topBackColor = self.topViewBackGroundColor;
    }
    _topView.delegate = self;
    _topView.style = self.style;
    _topView.lineColor = self.lineColor;
    _topView.normalSize = self.normalSize;
    _topView.selectedSize = self.selectedSize;
//    _topView.normalItemTitleColor = self.normalTitleColor;
//    _topView.selectItemTitleColor = self.selectTitleColor;
    if (_itemWidthArrayInternal.count > 0) {
        if (self.itemWidthArray.count > 0) {
            [_itemWidthArrayInternal removeAllObjects];
            [_itemWidthArrayInternal addObjectsFromArray:self.itemWidthArray];
        }
    }
    //Надо посчитать, если вы установите общая Ширина не достигает ширины интерфейса, на этот раз мы должны рассчитать, добавьте пробелы
    CGFloat max = 0.0;
    for (int i = 0; i < _itemWidthArrayInternal.count; i++) {
        max = max + [_itemWidthArrayInternal[i] floatValue];
    }
    _topView.itemWidthArray = [NSArray arrayWithArray:_itemWidthArrayInternal];
    [self.view addSubview:_topView];
}

-(void)scrollTopDestinationVC:(NSInteger)index
{
    // . NSLog(@"PKPageViewController scrollTopDestinationVC");
    _isClick = YES;
    _willToIndex = index;
    [_pageViewController setViewControllers:@[_vcArray[index]] direction:index < _currentVCIndex animated:YES completion:^(BOOL finished) {
        _currentVCIndex = index;
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
