//
//  ASTopLabel.h
//  ASPageViewController-OC
//
//  Created by haohao on 16/9/21.
//  Copyright © 2016年 haohao. All rights reserved.
//

#import <UIKit/UIKit.h>
@class PKTopIconImage;
@protocol PKTopIconImageDelegate <NSObject>
//Нажмите на метку, перейти к соответствующей попытка контролировать регулятор
-(void)didClickTopItemScrollToVC:(PKTopIconImage *)item;

@end
@interface PKTopIconImage : UIImageView
//Нормальное состояние шрифта, по умолчанию-15
//@property (nonatomic, assign) CGFloat normalTitleSize;
////Выберите шрифт времени. Значение по умолчанию-18
//@property (nonatomic, assign) CGFloat selectedTilteSize;
////Нормальный цвет шрифта
//@property (nonatomic, strong) UIColor *normalTitleColor;
//Когда выбран цвет шрифта
@property (nonatomic, strong) UIColor *selectedTitleColor;

@property (nonatomic, assign) BOOL selected;


//Установить курс
@property (nonatomic, assign) CGFloat rate;
//Агент
@property (nonatomic, weak) id <PKTopIconImageDelegate>delegate;

-(void)selectedLabelNOAnimation;
-(void)deselectedlabelNOAnimation;
@end
