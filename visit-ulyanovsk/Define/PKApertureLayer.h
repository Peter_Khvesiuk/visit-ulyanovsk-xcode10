//
//  PKApertureLayer.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 30/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKApertureLayer : CALayer

@property  CGFloat apertureValue;

@end

NS_ASSUME_NONNULL_END
