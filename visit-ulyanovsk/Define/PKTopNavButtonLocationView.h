//
//  PKTopNavButtonLocationView.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 30/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKTopNavButtonLocationView : UIView
@property (nonatomic) BOOL open;
@property (nonatomic) BOOL activeDistance;
@property (nonatomic) BOOL sortDistance;
@property (nonatomic) CGFloat hideFraction;

- (instancetype)initWithFrame:(CGRect)frame andActiveDistance:(BOOL) activeDistance andSortDistance:(BOOL) sortDistance;

@end

NS_ASSUME_NONNULL_END
