//
//  PKTopNavButtonLocationView.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 30/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

//#import <QuartzCore/QuartzCore.h>
#import "PKTopNavButtonLocationView.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"
#import "PKApertureLayer.h"

@implementation PKTopNavButtonLocationView
+ (Class)layerClass
{
    return [PKApertureLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _open = NO;
        [self setBackgroundColor:[UIColor clearColor]];
        self.layer.contentsScale = UIScreen.mainScreen.scale;
        [self.layer setNeedsDisplay];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame andActiveDistance:(BOOL) activeDistance andSortDistance:(BOOL) sortDistance{
    
   
    self = [super initWithFrame:frame];
    if (self) {
        //        // . NSLog(@"initWithFrame");
        //        [super awakeFromNib];
        _open = NO;
        _sortDistance = sortDistance;
        _activeDistance = activeDistance;
        [self setBackgroundColor:[UIColor clearColor]];
        self.layer.contentsScale = UIScreen.mainScreen.scale;
        [self.layer setNeedsDisplay];
    }
    return self;
}

-(void)setActiveDistance:(BOOL)activeDistance{
    if (activeDistance != _activeDistance)
    {
        [self.layer setNeedsDisplay];
    }
    
    _activeDistance = activeDistance;
}
-(void)setSortDistance:(BOOL)sortDistance{
    if (sortDistance != _sortDistance)
    {
        [self.layer setNeedsDisplay];
    }
    
    _sortDistance = sortDistance;
}

-(void)setHideFraction:(CGFloat)hideFraction{
    if (hideFraction != _hideFraction)
    {
        [self animateTo: (CGFloat)hideFraction];
    }
    
    _hideFraction = hideFraction;
}
- (void)setOpen: (BOOL)open
{
    
    if (open != _open)
    {
        [self animateTo: (CGFloat)open];
    }
    
    _open = open;
}

- (void)animateTo: (CGFloat)toValue
{
    PKApertureLayer* layer = (PKApertureLayer*)self.layer;
    CAMediaTimingFunction* timing = [CAMediaTimingFunction functionWithName:  kCAMediaTimingFunctionEaseInEaseOut];
    CGFloat duration = 0.3;
    
    //    Explicit animation - START
    
    CABasicAnimation* theAnimation = [CABasicAnimation animationWithKeyPath: @"apertureValue"];
    theAnimation.additive = YES;
    theAnimation.duration = duration;
    theAnimation.fillMode = kCAFillModeBoth;
    theAnimation.timingFunction = timing;
    theAnimation.fromValue = @(layer.apertureValue - toValue);
    theAnimation.toValue = @(0);
    
    
    [layer addAnimation: theAnimation forKey: nil];
    
    [CATransaction begin];
    [CATransaction setDisableActions: YES];
    layer.apertureValue = toValue;
    [CATransaction commit];
    
    //    Explicit animation - END
    
}

- (void)drawLayer: (CALayer *)layer inContext: (CGContextRef)ctx
{
    
    UIGraphicsPushContext(ctx);
    
    [PKUOStyleKit drawTopNavBarLocationWithNavButtonHideFraction:[((PKApertureLayer*)layer) apertureValue] activeDistance:_activeDistance sortDistance:_sortDistance closestText:TRANSLATE_UP(@"nearest")];

    UIGraphicsPopContext();
}

@end
