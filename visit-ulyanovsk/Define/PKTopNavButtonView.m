//
//  PKTopNavButtonView.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 09.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

//#import <QuartzCore/QuartzCore.h>
#import "PKTopNavButtonView.h"
#import "PKUOStyleKit.h"
#import "PKDataManager.h"
#import "PKApertureLayer.h"

@interface PKTopNavButtonView()

@property (nonatomic) BOOL filterEnable;
@property (nonatomic) BOOL satelliteEnable;

@end

@implementation PKTopNavButtonView

+ (Class)layerClass
{
    return [PKApertureLayer class];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
//        // . NSLog(@"initWithFrame");
        //        [super awakeFromNib];
        _open = NO;
        [self setBackgroundColor:[UIColor clearColor]];
        self.layer.contentsScale = UIScreen.mainScreen.scale;
        [self.layer setNeedsDisplay];
    }
    return self;
}

- (instancetype)initWithFilter:(BOOL) filterEnable andSatellite:(BOOL) satelliteEnable andHome:(BOOL) home{
    
    if(filterEnable && satelliteEnable){
//        // . NSLog(@"respondsToSelector satelliteEnable = YES filterEnable = YES");
        self = [super initWithFrame:CGRectMake(0, marginTopNavigationHeight, 214.f, 56.f)];
    } else if(filterEnable || satelliteEnable) {
//        // . NSLog(@"respondsToSelector satelliteEnable = YES filterEnable = YES");
        self = [super initWithFrame:CGRectMake(0, marginTopNavigationHeight, 144.f, 56.f)];
    } else {
//        // . NSLog(@"respondsToSelector satelliteEnable = NO filterEnable = NO");
        self = [super initWithFrame:CGRectMake(0, marginTopNavigationHeight, 74.f, 56.f)];
    }
    
    if (self) {
//        // . NSLog(@"initWithFrame");
//        [super awakeFromNib];
        _open = NO;
        _pressedBack = NO;
        _pressedFilter = NO;
        _pressedSatellite = NO;
        _home = home;
        _filterEnable = filterEnable;
        _satelliteEnable = satelliteEnable;
        [self setBackgroundColor:[UIColor clearColor]];
        self.layer.contentsScale = UIScreen.mainScreen.scale;
        [self.layer setNeedsDisplay];
    }
    return self;
}
//- (void)awakeFromNib
//{
//    // . NSLog(@"awakeFromNib");
//    [super awakeFromNib];
//    _open = NO;
//    self.layer.contentsScale = UIScreen.mainScreen.scale;
//    [self.layer setNeedsDisplay];
//}
-(void)setPressedBack:(BOOL)pressedBack{
    if (pressedBack != _pressedBack)
    {
        [self.layer setNeedsDisplay];
    }
    
    _pressedBack = pressedBack;
}
-(void)setPressedFilter:(BOOL)pressedFilter{
    if (pressedFilter != _pressedFilter)
    {
        [self.layer setNeedsDisplay];
    }
    
    _pressedFilter = pressedFilter;
}
-(void)setPressedSatellite:(BOOL)pressedSatellite{
    if (pressedSatellite != _pressedSatellite)
    {
        [self.layer setNeedsDisplay];
    }
    
    _pressedSatellite = pressedSatellite;
}
-(void)setHideFraction:(CGFloat)hideFraction{
    if (hideFraction != _hideFraction)
    {
        [self animateTo: (CGFloat)hideFraction];
    }
    
    _hideFraction = hideFraction;
}
- (void)setOpen: (BOOL)open
{
    
    if (open != _open)
    {
        [self animateTo: (CGFloat)open];
    }
    
    _open = open;
}

- (void)animateTo: (CGFloat)toValue
{
    PKApertureLayer* layer = (PKApertureLayer*)self.layer;
    CAMediaTimingFunction* timing = [CAMediaTimingFunction functionWithName:  kCAMediaTimingFunctionEaseInEaseOut];
    CGFloat duration = 0.3;
    
    //    Implicit animation - START
    /*
     [CATransaction setAnimationDuration: duration];
     [CATransaction setAnimationTimingFunction: timing];
     ((PKApertureLayer *)self.layer).apertureValue = toValue;
     */
    
    //    Implicit animation - END
    
    //    Explicit animation - START
    
    CABasicAnimation* theAnimation = [CABasicAnimation animationWithKeyPath: @"apertureValue"];
    theAnimation.additive = YES;
    theAnimation.duration = duration;
    theAnimation.fillMode = kCAFillModeBoth;
    theAnimation.timingFunction = timing;
    theAnimation.fromValue = @(layer.apertureValue - toValue);
    theAnimation.toValue = @(0);
    
    
    [layer addAnimation: theAnimation forKey: nil];
    
    [CATransaction begin];
    [CATransaction setDisableActions: YES];
    layer.apertureValue = toValue;
    [CATransaction commit];
    
    //    Explicit animation - END
    
}

- (void)drawLayer: (CALayer *)layer inContext: (CGContextRef)ctx
{
    
    UIGraphicsPushContext(ctx);
    
    if(_satelliteEnable && _filterEnable){
        [PKUOStyleKit drawTopNavBarFilterSatelliteWithNavButtonHideFraction:[((PKApertureLayer*)layer) apertureValue]  pressedBack:_pressedBack pressedFilter:_pressedFilter pressedSatellite:_pressedSatellite home: _home filterLang: TRANSLATE_UP(@"filters")];
    } else if(_satelliteEnable){
        [PKUOStyleKit drawTopNavBarSatelliteWithNavButtonHideFraction:[((PKApertureLayer*)layer) apertureValue] pressedBack:_pressedBack pressedSatellite:_pressedSatellite home: _home];
    } else if(_filterEnable){
        [PKUOStyleKit drawTopNavBarFilterWithNavButtonHideFraction:[((PKApertureLayer*)layer) apertureValue] pressedBack:_pressedBack pressedFilter:_pressedFilter home: _home filterLang: TRANSLATE_UP(@"filters")];
    } else {
//        // . NSLog(@"respondsToSelector drawLayer = NO = NO");
        [PKUOStyleKit drawTopNavBarBasicWithNavButtonHideFraction:[((PKApertureLayer*)layer) apertureValue] pressedBack:_pressedBack home:_home];
    }

    
    UIGraphicsPopContext();
}

//- (void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *)event
//{
//    for (UITouch *touch in [touches allObjects]) {
//        // . NSLog(@"object = %@", touch);
////        for (UIGestureRecognizer *gesture in touch.gestureRecognizers) {
////            // . NSLog(@"object = %@", gesture);
////        }
//        
//    }
//    
////    [self setOpen: !self.open];
//}

@end
