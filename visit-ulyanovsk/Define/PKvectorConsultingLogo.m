//
//  PKvectorConsultingLogo.m
//  ProjectName
//
//  Created by Khvesiuk Peter on 25/11/2018.
//  Copyright © 2018 CompanyName. All rights reserved.
//
//  Generated by PaintCode
//  http://www.paintcodeapp.com
//

#import "PKvectorConsultingLogo.h"


@implementation PKvectorConsultingLogo

#pragma mark Initialization

+ (void)initialize
{
}

#pragma mark Drawing Methods

+ (void)drawLogotypeWithAngle: (CGFloat)angle
{
    [PKvectorConsultingLogo drawLogotypeWithFrame: CGRectMake(0, 0, 100, 100) resizing: PKvectorConsultingLogoResizingBehaviorStretch angle: angle];
}

+ (void)drawLogotypeWithFrame: (CGRect)targetFrame resizing: (PKvectorConsultingLogoResizingBehavior)resizing angle: (CGFloat)angle
{
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Resize to Target Frame
    CGContextSaveGState(context);
    CGRect resizedFrame = PKvectorConsultingLogoResizingBehaviorApply(resizing, CGRectMake(0, 0, 100, 100), targetFrame);
    CGContextTranslateCTM(context, resizedFrame.origin.x, resizedFrame.origin.y);
    CGContextScaleCTM(context, resizedFrame.size.width / 100, resizedFrame.size.height / 100);
    CGFloat resizedShadowScale = MIN(resizedFrame.size.width / 100, resizedFrame.size.height / 100);


    //// Color Declarations
    UIColor* blueVc = [UIColor colorWithRed: 0 green: 0.269 blue: 0.756 alpha: 1];

    //// Shadow Declarations
    NSShadow* shadow = [[NSShadow alloc] init];
    shadow.shadowColor = [blueVc colorWithAlphaComponent: CGColorGetAlpha(blueVc.CGColor) * 0.13];
    shadow.shadowOffset = CGSizeMake(0, 0);
    shadow.shadowBlurRadius = 5;

    //// Variable Declarations
    CGFloat expression3 = (1 - angle) * 5940;
    CGFloat expression4 = angle * 5940;
    CGFloat expression5 = 1 * (1 - angle);

    //// Bezier Drawing
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(93.48, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(92.88, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 91.58)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 89.7)];
    [bezierPath addLineToPoint: CGPointMake(91.7, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(90.52, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 87.82)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 85.94)];
    [bezierPath addLineToPoint: CGPointMake(89.34, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(88.16, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 84.06)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 82.17)];
    [bezierPath addLineToPoint: CGPointMake(86.98, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(85.8, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 80.29)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 78.41)];
    [bezierPath addLineToPoint: CGPointMake(84.62, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(83.44, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 76.53)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 74.65)];
    [bezierPath addLineToPoint: CGPointMake(82.26, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(81.08, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 72.76)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 70.88)];
    [bezierPath addLineToPoint: CGPointMake(79.9, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(78.72, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 69)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 67.12)];
    [bezierPath addLineToPoint: CGPointMake(77.54, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(76.36, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 65.24)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 63.36)];
    [bezierPath addLineToPoint: CGPointMake(75.18, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(74, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 61.47)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 59.59)];
    [bezierPath addLineToPoint: CGPointMake(72.82, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(71.64, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 57.71)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 55.83)];
    [bezierPath addLineToPoint: CGPointMake(70.45, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(69.27, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 53.95)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 52.06)];
    [bezierPath addLineToPoint: CGPointMake(68.09, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(66.91, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 50.18)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 48.3)];
    [bezierPath addLineToPoint: CGPointMake(65.73, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(64.55, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 46.42)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 33.25)];
    [bezierPath addLineToPoint: CGPointMake(56.29, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(55.11, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 31.36)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 29.48)];
    [bezierPath addLineToPoint: CGPointMake(53.93, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(52.75, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 27.6)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 25.72)];
    [bezierPath addLineToPoint: CGPointMake(51.57, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(50.39, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 23.84)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 10.66)];
    [bezierPath addLineToPoint: CGPointMake(42.12, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(40.94, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 8.78)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 6.9)];
    [bezierPath addLineToPoint: CGPointMake(39.76, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(38.58, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(92.87, 6)];
    [bezierPath addLineToPoint: CGPointMake(91.69, 6)];
    [bezierPath addLineToPoint: CGPointMake(37.4, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(36.22, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(90.51, 6)];
    [bezierPath addLineToPoint: CGPointMake(82.16, 6)];
    [bezierPath addLineToPoint: CGPointMake(82.16, 6)];
    [bezierPath addLineToPoint: CGPointMake(31.85, 86.33)];
    [bezierPath addLineToPoint: CGPointMake(31.26, 85.39)];
    [bezierPath addLineToPoint: CGPointMake(81.06, 6)];
    [bezierPath addLineToPoint: CGPointMake(79.88, 6)];
    [bezierPath addLineToPoint: CGPointMake(30.67, 84.44)];
    [bezierPath addLineToPoint: CGPointMake(30.08, 83.5)];
    [bezierPath addLineToPoint: CGPointMake(78.7, 6)];
    [bezierPath addLineToPoint: CGPointMake(77.52, 6)];
    [bezierPath addLineToPoint: CGPointMake(29.5, 82.56)];
    [bezierPath addLineToPoint: CGPointMake(28.91, 81.62)];
    [bezierPath addLineToPoint: CGPointMake(76.34, 6)];
    [bezierPath addLineToPoint: CGPointMake(75.16, 6)];
    [bezierPath addLineToPoint: CGPointMake(28.32, 80.67)];
    [bezierPath addLineToPoint: CGPointMake(27.73, 79.73)];
    [bezierPath addLineToPoint: CGPointMake(73.98, 6)];
    [bezierPath addLineToPoint: CGPointMake(72.8, 6)];
    [bezierPath addLineToPoint: CGPointMake(27.14, 78.79)];
    [bezierPath addLineToPoint: CGPointMake(26.55, 77.85)];
    [bezierPath addLineToPoint: CGPointMake(71.62, 6)];
    [bezierPath addLineToPoint: CGPointMake(70.44, 6)];
    [bezierPath addLineToPoint: CGPointMake(25.96, 76.9)];
    [bezierPath addLineToPoint: CGPointMake(25.37, 75.96)];
    [bezierPath addLineToPoint: CGPointMake(69.26, 6)];
    [bezierPath addLineToPoint: CGPointMake(68.08, 6)];
    [bezierPath addLineToPoint: CGPointMake(24.78, 75.02)];
    [bezierPath addLineToPoint: CGPointMake(24.19, 74.08)];
    [bezierPath addLineToPoint: CGPointMake(66.9, 6)];
    [bezierPath addLineToPoint: CGPointMake(65.72, 6)];
    [bezierPath addLineToPoint: CGPointMake(23.6, 73.13)];
    [bezierPath addLineToPoint: CGPointMake(23.01, 72.19)];
    [bezierPath addLineToPoint: CGPointMake(64.54, 6)];
    [bezierPath addLineToPoint: CGPointMake(63.36, 6)];
    [bezierPath addLineToPoint: CGPointMake(22.43, 71.25)];
    [bezierPath addLineToPoint: CGPointMake(21.84, 70.3)];
    [bezierPath addLineToPoint: CGPointMake(62.17, 6)];
    [bezierPath addLineToPoint: CGPointMake(60.99, 6)];
    [bezierPath addLineToPoint: CGPointMake(21.25, 69.36)];
    [bezierPath addLineToPoint: CGPointMake(20.66, 68.42)];
    [bezierPath addLineToPoint: CGPointMake(59.81, 6)];
    [bezierPath addLineToPoint: CGPointMake(58.63, 6)];
    [bezierPath addLineToPoint: CGPointMake(20.07, 67.48)];
    [bezierPath addLineToPoint: CGPointMake(19.48, 66.53)];
    [bezierPath addLineToPoint: CGPointMake(57.45, 6)];
    [bezierPath addLineToPoint: CGPointMake(56.27, 6)];
    [bezierPath addLineToPoint: CGPointMake(18.89, 65.59)];
    [bezierPath addLineToPoint: CGPointMake(18.3, 64.65)];
    [bezierPath addLineToPoint: CGPointMake(55.09, 6)];
    [bezierPath addLineToPoint: CGPointMake(53.91, 6)];
    [bezierPath addLineToPoint: CGPointMake(17.71, 63.71)];
    [bezierPath addLineToPoint: CGPointMake(17.12, 62.76)];
    [bezierPath addLineToPoint: CGPointMake(52.73, 6)];
    [bezierPath addLineToPoint: CGPointMake(51.55, 6)];
    [bezierPath addLineToPoint: CGPointMake(16.53, 61.82)];
    [bezierPath addLineToPoint: CGPointMake(15.94, 60.88)];
    [bezierPath addLineToPoint: CGPointMake(50.37, 6)];
    [bezierPath addLineToPoint: CGPointMake(49.19, 6)];
    [bezierPath addLineToPoint: CGPointMake(15.36, 59.94)];
    [bezierPath addLineToPoint: CGPointMake(14.77, 58.99)];
    [bezierPath addLineToPoint: CGPointMake(48.01, 6)];
    [bezierPath addLineToPoint: CGPointMake(46.83, 6)];
    [bezierPath addLineToPoint: CGPointMake(14.18, 58.05)];
    [bezierPath addLineToPoint: CGPointMake(13.59, 57.11)];
    [bezierPath addLineToPoint: CGPointMake(45.65, 6)];
    [bezierPath addLineToPoint: CGPointMake(44.47, 6)];
    [bezierPath addLineToPoint: CGPointMake(13, 56.17)];
    [bezierPath addLineToPoint: CGPointMake(12.41, 55.22)];
    [bezierPath addLineToPoint: CGPointMake(43.29, 6)];
    [bezierPath addLineToPoint: CGPointMake(42.11, 6)];
    [bezierPath addLineToPoint: CGPointMake(11.82, 54.28)];
    [bezierPath addLineToPoint: CGPointMake(11.23, 53.34)];
    [bezierPath addLineToPoint: CGPointMake(40.93, 6)];
    [bezierPath addLineToPoint: CGPointMake(39.75, 6)];
    [bezierPath addLineToPoint: CGPointMake(10.64, 52.4)];
    [bezierPath addLineToPoint: CGPointMake(10.05, 51.45)];
    [bezierPath addLineToPoint: CGPointMake(38.57, 6)];
    [bezierPath addLineToPoint: CGPointMake(37.38, 6)];
    [bezierPath addLineToPoint: CGPointMake(9.46, 50.51)];
    [bezierPath addLineToPoint: CGPointMake(8.87, 49.57)];
    [bezierPath addLineToPoint: CGPointMake(36.2, 6)];
    [bezierPath addLineToPoint: CGPointMake(35.02, 6)];
    [bezierPath addLineToPoint: CGPointMake(8.29, 48.63)];
    [bezierPath addLineToPoint: CGPointMake(7.7, 47.68)];
    [bezierPath addLineToPoint: CGPointMake(33.84, 6)];
    [bezierPath addLineToPoint: CGPointMake(32.66, 6)];
    [bezierPath addLineToPoint: CGPointMake(7.11, 46.74)];
    [bezierPath addLineToPoint: CGPointMake(7, 46.57)];
    [bezierPath addLineToPoint: CGPointMake(7, 45.03)];
    [bezierPath addLineToPoint: CGPointMake(31.48, 6)];
    [bezierPath addLineToPoint: CGPointMake(30.3, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 43.15)];
    [bezierPath addLineToPoint: CGPointMake(7, 41.26)];
    [bezierPath addLineToPoint: CGPointMake(29.12, 6)];
    [bezierPath addLineToPoint: CGPointMake(27.94, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 39.38)];
    [bezierPath addLineToPoint: CGPointMake(7, 37.5)];
    [bezierPath addLineToPoint: CGPointMake(26.76, 6)];
    [bezierPath addLineToPoint: CGPointMake(25.58, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 35.62)];
    [bezierPath addLineToPoint: CGPointMake(7, 33.74)];
    [bezierPath addLineToPoint: CGPointMake(24.4, 6)];
    [bezierPath addLineToPoint: CGPointMake(23.22, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 31.86)];
    [bezierPath addLineToPoint: CGPointMake(7, 29.97)];
    [bezierPath addLineToPoint: CGPointMake(22.04, 6)];
    [bezierPath addLineToPoint: CGPointMake(20.86, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 28.09)];
    [bezierPath addLineToPoint: CGPointMake(7, 26.21)];
    [bezierPath addLineToPoint: CGPointMake(19.68, 6)];
    [bezierPath addLineToPoint: CGPointMake(18.5, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 24.33)];
    [bezierPath addLineToPoint: CGPointMake(7, 22.45)];
    [bezierPath addLineToPoint: CGPointMake(17.32, 6)];
    [bezierPath addLineToPoint: CGPointMake(16.14, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 20.56)];
    [bezierPath addLineToPoint: CGPointMake(7, 18.68)];
    [bezierPath addLineToPoint: CGPointMake(14.96, 6)];
    [bezierPath addLineToPoint: CGPointMake(13.78, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 16.8)];
    [bezierPath addLineToPoint: CGPointMake(7, 14.92)];
    [bezierPath addLineToPoint: CGPointMake(12.59, 6)];
    [bezierPath addLineToPoint: CGPointMake(11.41, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 13.04)];
    [bezierPath addLineToPoint: CGPointMake(7, 11.16)];
    [bezierPath addLineToPoint: CGPointMake(10.23, 6)];
    [bezierPath addLineToPoint: CGPointMake(9.05, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 9.27)];
    [bezierPath addLineToPoint: CGPointMake(7, 7.39)];
    [bezierPath addLineToPoint: CGPointMake(7.87, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 6)];
    [bezierPath addLineToPoint: CGPointMake(7, 59.15)];
    [bezierPath addCurveToPoint: CGPointMake(27.79, 92.54) controlPoint1: CGPointMake(13.63, 69.81) controlPoint2: CGPointMake(20.8, 81.32)];
    [bezierPath addLineToPoint: CGPointMake(26.61, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 61.04) controlPoint1: CGPointMake(20.03, 81.96) controlPoint2: CGPointMake(13.29, 71.15)];
    [bezierPath addCurveToPoint: CGPointMake(7, 62.94) controlPoint1: CGPointMake(7, 61.59) controlPoint2: CGPointMake(7, 62.23)];
    [bezierPath addCurveToPoint: CGPointMake(25.43, 92.54) controlPoint1: CGPointMake(12.94, 72.47) controlPoint2: CGPointMake(19.25, 82.61)];
    [bezierPath addLineToPoint: CGPointMake(24.25, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 64.83) controlPoint1: CGPointMake(18.47, 83.25) controlPoint2: CGPointMake(12.58, 73.79)];
    [bezierPath addCurveToPoint: CGPointMake(7, 66.72) controlPoint1: CGPointMake(7, 65.43) controlPoint2: CGPointMake(7, 66.06)];
    [bezierPath addCurveToPoint: CGPointMake(23.08, 92.54) controlPoint1: CGPointMake(12.22, 75.11) controlPoint2: CGPointMake(17.69, 83.89)];
    [bezierPath addLineToPoint: CGPointMake(21.9, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 68.61) controlPoint1: CGPointMake(16.91, 84.53) controlPoint2: CGPointMake(11.86, 76.41)];
    [bezierPath addCurveToPoint: CGPointMake(7, 70.5) controlPoint1: CGPointMake(7, 69.23) controlPoint2: CGPointMake(7, 69.86)];
    [bezierPath addCurveToPoint: CGPointMake(20.72, 92.54) controlPoint1: CGPointMake(11.49, 77.71) controlPoint2: CGPointMake(16.13, 85.16)];
    [bezierPath addLineToPoint: CGPointMake(19.54, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 72.4) controlPoint1: CGPointMake(15.35, 85.8) controlPoint2: CGPointMake(11.12, 79)];
    [bezierPath addCurveToPoint: CGPointMake(7, 74.29) controlPoint1: CGPointMake(7, 73.02) controlPoint2: CGPointMake(7, 73.65)];
    [bezierPath addCurveToPoint: CGPointMake(18.36, 92.54) controlPoint1: CGPointMake(10.74, 80.29) controlPoint2: CGPointMake(14.57, 86.44)];
    [bezierPath addLineToPoint: CGPointMake(17.19, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 76.18) controlPoint1: CGPointMake(13.78, 87.07) controlPoint2: CGPointMake(10.36, 81.58)];
    [bezierPath addCurveToPoint: CGPointMake(7, 78.07) controlPoint1: CGPointMake(7, 76.81) controlPoint2: CGPointMake(7, 77.44)];
    [bezierPath addCurveToPoint: CGPointMake(16.01, 92.54) controlPoint1: CGPointMake(9.98, 82.85) controlPoint2: CGPointMake(13, 87.71)];
    [bezierPath addLineToPoint: CGPointMake(14.83, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 79.96) controlPoint1: CGPointMake(12.22, 88.34) controlPoint2: CGPointMake(9.59, 84.13)];
    [bezierPath addCurveToPoint: CGPointMake(7, 81.85) controlPoint1: CGPointMake(7, 80.6) controlPoint2: CGPointMake(7, 81.24)];
    [bezierPath addCurveToPoint: CGPointMake(13.65, 92.54) controlPoint1: CGPointMake(9.21, 85.4) controlPoint2: CGPointMake(11.43, 88.97)];
    [bezierPath addLineToPoint: CGPointMake(12.47, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 83.75) controlPoint1: CGPointMake(10.65, 89.6) controlPoint2: CGPointMake(8.82, 86.67)];
    [bezierPath addCurveToPoint: CGPointMake(7, 85.64) controlPoint1: CGPointMake(7, 84.4) controlPoint2: CGPointMake(7, 85.03)];
    [bezierPath addCurveToPoint: CGPointMake(11.3, 92.54) controlPoint1: CGPointMake(8.43, 87.93) controlPoint2: CGPointMake(9.86, 90.24)];
    [bezierPath addLineToPoint: CGPointMake(10.12, 92.54)];
    [bezierPath addCurveToPoint: CGPointMake(7, 87.53) controlPoint1: CGPointMake(9.08, 90.87) controlPoint2: CGPointMake(8.04, 89.2)];
    [bezierPath addCurveToPoint: CGPointMake(7, 89.42) controlPoint1: CGPointMake(7, 88.22) controlPoint2: CGPointMake(7, 88.85)];
    [bezierPath addLineToPoint: CGPointMake(8.94, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(7.76, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(7, 91.31)];
    [bezierPath addCurveToPoint: CGPointMake(7, 92.54) controlPoint1: CGPointMake(7, 92.1) controlPoint2: CGPointMake(7, 92.54)];
    [bezierPath addLineToPoint: CGPointMake(93.48, 92.54)];
    [bezierPath closePath];
    [blueVc setStroke];
    bezierPath.lineWidth = expression5;
    bezierPath.lineCapStyle = kCGLineCapSquare;
    [bezierPath setLineDash: (CGFloat[]){expression3, expression4} count: 2 phase: 0];
    [bezierPath stroke];


    //// Bezier 7 Drawing
    UIBezierPath* bezier7Path = [UIBezierPath bezierPath];
    [bezier7Path moveToPoint: CGPointMake(88.96, 6.5)];
    [bezier7Path addCurveToPoint: CGPointMake(35.97, 92) controlPoint1: CGPointMake(88.96, 6.5) controlPoint2: CGPointMake(38.98, 87.2)];
    [bezier7Path addLineToPoint: CGPointMake(43.05, 92)];
    [bezier7Path addCurveToPoint: CGPointMake(92.99, 12.4) controlPoint1: CGPointMake(48.11, 83.94) controlPoint2: CGPointMake(92.99, 12.4)];
    [bezier7Path addLineToPoint: CGPointMake(93, 6.5)];
    [bezier7Path addLineToPoint: CGPointMake(88.96, 6.5)];
    [bezier7Path closePath];
    [bezier7Path moveToPoint: CGPointMake(93, 23.66)];
    [bezier7Path addCurveToPoint: CGPointMake(50.13, 92) controlPoint1: CGPointMake(93, 23.66) controlPoint2: CGPointMake(55.27, 83.81)];
    [bezier7Path addLineToPoint: CGPointMake(57.22, 92)];
    [bezier7Path addCurveToPoint: CGPointMake(92.99, 34.98) controlPoint1: CGPointMake(61.38, 85.36) controlPoint2: CGPointMake(92.99, 34.98)];
    [bezier7Path addLineToPoint: CGPointMake(93, 23.66)];
    [bezier7Path closePath];
    [bezier7Path moveToPoint: CGPointMake(93, 46.36)];
    [bezier7Path addCurveToPoint: CGPointMake(64.3, 92) controlPoint1: CGPointMake(91.12, 49.24) controlPoint2: CGPointMake(68.45, 85.38)];
    [bezier7Path addLineToPoint: CGPointMake(93, 92)];
    [bezier7Path addCurveToPoint: CGPointMake(93, 46.36) controlPoint1: CGPointMake(93, 92) controlPoint2: CGPointMake(93, 68.65)];
    [bezier7Path closePath];
    [bezier7Path moveToPoint: CGPointMake(7.5, 59.15)];
    [bezier7Path addCurveToPoint: CGPointMake(7.5, 92) controlPoint1: CGPointMake(7.5, 76.76) controlPoint2: CGPointMake(7.5, 92)];
    [bezier7Path addLineToPoint: CGPointMake(28.04, 92)];
    [bezier7Path addCurveToPoint: CGPointMake(7.5, 59.15) controlPoint1: CGPointMake(25.13, 87.32) controlPoint2: CGPointMake(14.85, 70.82)];
    [bezier7Path closePath];
    [bezier7Path moveToPoint: CGPointMake(106, -6)];
    [bezier7Path addCurveToPoint: CGPointMake(106, 105) controlPoint1: CGPointMake(106, -6) controlPoint2: CGPointMake(106, 105)];
    [bezier7Path addLineToPoint: CGPointMake(-5, 105)];
    [bezier7Path addLineToPoint: CGPointMake(-5, -6)];
    [bezier7Path addLineToPoint: CGPointMake(106, -6)];
    [bezier7Path addLineToPoint: CGPointMake(106, -6)];
    [bezier7Path closePath];
    [bezier7Path moveToPoint: CGPointMake(82.44, 6.5)];
    [bezier7Path addLineToPoint: CGPointMake(7.5, 6.5)];
    [bezier7Path addCurveToPoint: CGPointMake(7.5, 48.22) controlPoint1: CGPointMake(7.5, 6.5) controlPoint2: CGPointMake(7.5, 27.19)];
    [bezier7Path addCurveToPoint: CGPointMake(31.85, 87.27) controlPoint1: CGPointMake(11.03, 53.88) controlPoint2: CGPointMake(31.85, 87.27)];
    [bezier7Path addLineToPoint: CGPointMake(82.44, 6.5)];
    [bezier7Path closePath];
    [UIColor.whiteColor setFill];
    [bezier7Path fill];

    ////// Bezier 7 Inner Shadow
    CGContextSaveGState(context);
    CGContextClipToRect(context, bezier7Path.bounds);
    CGContextSetShadowWithColor(context, CGSizeZero, 0, NULL);

    CGContextSetAlpha(context, CGColorGetAlpha([shadow.shadowColor CGColor]));
    CGContextBeginTransparencyLayer(context, NULL);
    {
        UIColor* opaqueShadow = [shadow.shadowColor colorWithAlphaComponent: 1];
        CGContextSetShadowWithColor(context, CGSizeMake(shadow.shadowOffset.width * resizedShadowScale, shadow.shadowOffset.height * resizedShadowScale), shadow.shadowBlurRadius * resizedShadowScale, [opaqueShadow CGColor]);
        CGContextSetBlendMode(context, kCGBlendModeSourceOut);
        CGContextBeginTransparencyLayer(context, NULL);

        [opaqueShadow setFill];
        [bezier7Path fill];

        CGContextEndTransparencyLayer(context);
    }
    CGContextEndTransparencyLayer(context);
    CGContextRestoreGState(context);
    
    CGContextRestoreGState(context);

}

@end



CGRect PKvectorConsultingLogoResizingBehaviorApply(PKvectorConsultingLogoResizingBehavior behavior, CGRect rect, CGRect target)
{
    if (CGRectEqualToRect(rect, target) || CGRectEqualToRect(target, CGRectZero))
        return rect;

    CGSize scales = CGSizeZero;
    scales.width = ABS(target.size.width / rect.size.width);
    scales.height = ABS(target.size.height / rect.size.height);

    switch (behavior)
    {
        case PKvectorConsultingLogoResizingBehaviorAspectFit:
        {
            scales.width = MIN(scales.width, scales.height);
            scales.height = scales.width;
            break;
        }
        case PKvectorConsultingLogoResizingBehaviorAspectFill:
        {
            scales.width = MAX(scales.width, scales.height);
            scales.height = scales.width;
            break;
        }
        case PKvectorConsultingLogoResizingBehaviorStretch:
            break;
        case PKvectorConsultingLogoResizingBehaviorCenter:
        {
            scales.width = 1;
            scales.height = 1;
            break;
        }
    }

    CGRect result = CGRectStandardize(rect);
    result.size.width *= scales.width;
    result.size.height *= scales.height;
    result.origin.x = target.origin.x + (target.size.width - result.size.width) / 2;
    result.origin.y = target.origin.y + (target.size.height - result.size.height) / 2;
    return result;
}
