//
//  URLMacros.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 08.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#ifndef URLMacros_h
#define URLMacros_h


#define DefaultOrganization @"10" //id Default Commercial Organization
#define DefaultField @"Не указано"

#define kScreenWidth ([UIScreen mainScreen].bounds.size.width)
#define kScreenHeight ([UIScreen mainScreen].bounds.size.height)
//#define isiPhoneX if(kScreenHeight == 812.f){ return YES} else {return NO}

#define VisitUlyanovskVideo @"VisitUlyanovsk.mp4"

#define cvcMenuSizeiPad 500.f
#define cvcMenuMarginiPad 10.f
#define cvcFontSizeiPad 22.f

#define cvcMenuSizeiPhone5s 270.f
#define cvcMenuMarginiPhone5s 5.f
#define cvcFontSizeiPhone5s 12.f

#define cvcMenuSizeDefault 320.f
#define cvcMenuMarginDefault 5.f
#define cvcFontSizeiDefault 14.f

#define cvcTitleSize ((isiPhone5s) ? cvcTitle5s : cvcTitleDefault)
#define cvcTitleDefault 22.f
#define cvcTitle5s 20.f

#define cvcTitleOfsetLeft ((isiPhone5s) ? cvcTitleOfsetLeft5s : cvcTitleOfsetLeftDefault)
#define cvcTitleOfsetLeftDefault 20.f
#define cvcTitleOfsetLeft5s 10.f

#define zoomCellsInRowDefault 2.f
#define zoomCellsInRowRecomendedDefault 1.f
#define zoomCellsInRowiPad 3.f
#define zoomCellsInRowRecomendediPad 2.f

#define zoomCellsTipsDefault 1.f
#define zoomCellsTipsiPad 3.f

#define zoomDetailImageHeightDefault 300.f
#define zoomDetailImageHeightiPad 600.f

#define zoomCellTextAreaHeightiPad 64.f
#define zoomCellTextAreaHeightDefault 44.f

#define zoomCellMarginiPad 25.f
#define zoomCellMarginDefault 10.f

#define zoomCellSpacingiPad 25.f
#define zoomCellSpacingDefault 10.f


//#define marginTopNavigationHeight ((kScreenHeight == 812.f) ? 44.f: 22.f)
#define marginTopNavigationHeight (hasTopNotch? safeAreaInsetsTop : safeAreaInsetsTop + 2.f)
#define hasTopNotch (safeAreaInsetsTop > 20.0 ? YES : NO)
#define safeAreaInsetsTop [[[UIApplication sharedApplication] delegate] window].safeAreaInsets.top

#define isiPhone5s ((kScreenWidth == 320.f) ? YES: NO)
#define isiPad ((kScreenWidth >= 768.f) ? YES: NO)

#if TARGET_IPHONE_SIMULATOR
#define UUID @"00000000-0000-0000-0000-000000000000"
#else
#define UUID [[UIDevice currentDevice].identifierForVendor UUIDString]
#endif


#define cvcMenuSize ((isiPhone5s) ? cvcMenuSizeiPhone5s : (isiPad) ? cvcMenuSizeiPad : cvcMenuSizeDefault)
#define cvcMenuMargin ((isiPhone5s) ? cvcMenuMarginiPhone5s : (isiPad) ? cvcMenuMarginiPad : cvcMenuMarginDefault)
#define cvcFontSize ((isiPhone5s) ? cvcFontSizeiPhone5s : (isiPad) ? cvcFontSizeiPad : cvcFontSizeiDefault)
#define zoomCellsInRow ((isiPad) ? zoomCellsInRowiPad : zoomCellsInRowDefault)
#define zoomCellsInRowRecomended ((isiPad) ? zoomCellsInRowRecomendediPad : zoomCellsInRowRecomendedDefault)
#define zoomCellsTips ((isiPad) ? zoomCellsTipsiPad : zoomCellsTipsDefault)
#define zoomDetailImageHeight ((isiPad) ? zoomDetailImageHeightiPad : zoomDetailImageHeightDefault)
#define zoomCellTextAreaHeight ((isiPad) ? zoomCellTextAreaHeightiPad : zoomCellTextAreaHeightDefault)
#define zoomCellMargin ((isiPad) ? zoomCellMarginiPad : zoomCellMarginDefault)
#define zoomCellSpacing ((isiPad) ? zoomCellSpacingiPad : zoomCellSpacingDefault)


#define detailFZFontSizeTitle ((isiPad) ? 25.f : 20.f)
#define detailFZFontSizeDescription ((isiPad) ? 18.f : 16.f)
#define zoomTextFontTitle ((isiPhone5s) ? 12.f : (isiPad) ? 17.f : 13.f)
#define zoomTextFontTitle300 ((isiPhone5s) ? 16.f : (isiPad) ? 20.f : 17.f)

#define regularFZMargin ((isiPad) ? 25.f : 14.f)
#define regularFZMargin2 regularFZMargin

#define cellTextAreaHeightTips ((isiPad) ? 86.f : 66.f)

#define cellTextFontTitleTips ((isiPhone5s) ? 16.f : (isiPad) ? 19.f : 17.f)
//#define cellTextFontSubTitleTips ((isiPhone5s) ? 13.f : (isiPad) ? 16.f : 15.f)


#define zoomTopMargin ((isiPad) ? 86.f : 66.f)

#define cvcMenuStrokeWidth ((isiPhone5s) ? 1.5f : (isiPad) ? 3.f : 2.f)

//#define URL_main @"http://rtips.vc73.ru/"
#define URL_main @"http://visit-ulyanovsk.com/"
#define URL_imagePath images/

#define path_mapObjImagePathPreview @"images/mapobj/preview/"
#define path_mapObjImagePathPhotogall @"images/mapobj/photogall/"

#define path_geoRouteImagePathPreview @"images/georoute/preview/"
#define path_geoRouteImagePathPhotogall @"images/georoute/photogall/"

#define path_informImagePathPreview @"images/inform/preview/"
#define path_informImagePathPhotogall @"images/inform/photogall/"

#define path_newsImagePathPreview @"images/news/preview/"
#define path_newsImagePathPhotogall @"images/news/photogall/"

#define path_eventsImagePathPreview @"images/events/preview/"
#define path_eventsImagePathPhotogall @"images/events/photogall/"

#define path_to_logos @"images/logos/"

//URLS
#define URL_mapObjImagePathPreview(imageName) [[URL_main stringByAppendingPathComponent: path_mapObjImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define URL_mapObjImagePathPhotogall(imageName) [[URL_main stringByAppendingPathComponent: path_mapObjImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define URL_geoRouteImagePathPreview(imageName) [[URL_main stringByAppendingPathComponent: path_geoRouteImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define URL_geoRouteImagePathPhotogall(imageName) [[URL_main stringByAppendingPathComponent: path_geoRouteImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define URL_informImagePathPreview(imageName) [[URL_main stringByAppendingPathComponent: path_informImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define URL_informImagePathPhotogall(imageName) [[URL_main stringByAppendingPathComponent: path_informImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define URL_newsImagePathPreview(imageName) [[URL_main stringByAppendingPathComponent: path_newsImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define URL_newsImagePathPhotogall(imageName) [[URL_main stringByAppendingPathComponent: path_newsImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define URL_eventsImagePathPreview(imageName) [[URL_main stringByAppendingPathComponent: path_eventsImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define URL_eventsImagePathPhotogall(imageName) [[URL_main stringByAppendingPathComponent: path_eventsImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define URL_LogosPath(imageName) [[URL_main stringByAppendingPathComponent: path_to_logos] stringByAppendingPathComponent: (imageName)]

//LOCALS


//NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); objectAtIndex:0]; // Get documents folder
#define LOCAL_PATH [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]

#define LOCAL_mapObjImagePathPreview(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_mapObjImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define LOCAL_mapObjImagePathPhotogall(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_mapObjImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define LOCAL_geoRouteImagePathPreview(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_geoRouteImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define LOCAL_geoRouteImagePathPhotogall(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_geoRouteImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define LOCAL_informImagePathPreview(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_informImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define LOCAL_informImagePathPhotogall(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_informImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define LOCAL_newsImagePathPreview(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_newsImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define LOCAL_newsImagePathPhotogall(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_newsImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define LOCAL_eventsImagePathPreview(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_eventsImagePathPreview] stringByAppendingPathComponent: (imageName)]
#define LOCAL_eventsImagePathPhotogall(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_eventsImagePathPhotogall] stringByAppendingPathComponent: (imageName)]

#define LOCAL_LogosPath(imageName) [[LOCAL_PATH stringByAppendingPathComponent: path_to_logos] stringByAppendingPathComponent: (imageName)]

#define ApplicationServerToken @"i5gLFazI9LedzpBVh"
//#define mapStyleDefault @"mapbox://styles/khvesiukpeter/cjemm3m3bb9c92ss4qp6z2mbl"//mapbox://styles/mapbox/streets-v10

//#define mapStyleDefault @"mapbox://styles/khvesiukpeter/cjdiizycric952rlc0pdf82ot"

#define isLOCAL_RU [[[NSLocale currentLocale] languageCode] isEqualToString:@"ru"]

#define mapStyleDefault (isLOCAL_RU ? @"mapbox://styles/khvesiukpeter/cjhnd6h9b12cr2smmy5qqidbs" : @"mapbox://styles/khvesiukpeter/cjfz4o5q67wig2rohvnphnvpq")

#define PKMaxMenuItemsCount 8
    
#define ALERT_OFFLINE_localizedProgressString (isLOCAL_RU ? @"Загрузка изображений:" : @"Downloading images:")
#define ALERT_OFFLINE_localizedProgressMapString (isLOCAL_RU ? @"Загрузка карты:" : @"Downloading maps:")
#define ALERT_OFFLINE_Title (isLOCAL_RU ? @"Автономный режим" : @"Offline mode")
#define ALERT_OFFLINE_Message (isLOCAL_RU ? @"Вы хотите использовать автономный режим? Будет произведена загрузка около 200 мегабайт." : @"Do you want to use offline mode? It will be loaded about 200 megabytes.")
#define ALERT_OFFLINE_YesBtnText (isLOCAL_RU ? @"ДА" : @"YES")
#define ALERT_OFFLINE_NoBtnText (isLOCAL_RU ? @"НЕТ" : @"NO")



#define mapStyleV10 @"mapbox://styles/mapbox/streets-v10"
#define NULL_TO_NIL(obj) ({ __typeof__ (obj) __obj = (obj); __obj == [NSNull null] ? nil : obj; })
#define IS_NOT_NIL_OR_NULL(value)           (value != nil && value != Nil && value != NULL && value != (id)[NSNull null])

#define UPFirst(word) [NSString stringWithFormat:@"%@%@",[[(word) substringToIndex:1] uppercaseString],[[(word) substringFromIndex:1] lowercaseString]]

#define TRANSLATE(key) [[PKDataManager sharedManager] translateWithMyDictionary:(key)]
#define TRANSLATE_UP(key)  UPFirst(TRANSLATE(key))
#define PK_st(string1,string2)  [NSString stringWithFormat:@"%@%@",(string1),(string2)]

#define PK_st_TRANSLATE_UP(translate,string2)  [NSString stringWithFormat:@"%@%@",TRANSLATE_UP((translate)),(string2)]

//#define ne_MAP_BORDER CLLocationCoordinate2DMake(55.924585,50.625000) //Границы карты
//#define sw_MAP_BORDER CLLocationCoordinate2DMake(51.392350, 45.834960)

#define sw_MAP_BORDER CLLocationCoordinate2DMake(48.99072, 43.46500) //Границы карты
#define ne_MAP_BORDER CLLocationCoordinate2DMake(57.94333, 51.53521) //Границы карты

#define RGB(r, g, b)                        [UIColor colorWithRed:(r)/255.f green:(g)/255.f blue:(b)/255.f alpha:1.0]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]



//filter
#define PKFilterAdditionalArray [[NSArray alloc] initWithObjects:TRANSLATE_UP(@"now_open"),TRANSLATE_UP(@"available_for_disabled"),TRANSLATE(@"Free_WiFi"),TRANSLATE(@"Cards_accepted"), TRANSLATE_UP(@"kid-friendly"), nil]


//#define PKFilterAdditionalPredicateWorkingTime(today, hm, hm2) @"SUBQUERY(workingTime, $w, $w.day = %i AND $w.begin <= %i AND $w.end > %i).@count == 0", (today), (hm), (hm2)
//#define PKFilterAdditionalPredicateAvalibleForDisables @"avalibleForDisables = %i", 1
//#define PKFilterAdditionalPredicateWiFi @"wiFi = %i", 1
//#define PKFilterAdditionalPredicatePayCard @"payCard = %i", 1



#define dOWF1 NSDateFormatter* dayOfWeekFormat = [[NSDateFormatter alloc] init];
#define dOWF2 [dayOfWeekFormat setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
#define dOWFdayOfWeek dayOfWeekFormat.dateFormat = @"e";
#define defineDayOfWeekFormat dOWF1 dOWF2 dOWFdayOfWeek

#define defineTodayInt [[dayOfWeekFormat stringFromDate: [NSDate date]] integerValue]

#define NSLog(STRING, ...) printf("%s\n", [[NSString stringWithFormat:STRING, ##__VA_ARGS__] UTF8String]);

#define isDebug NO

#endif /* URLMacros_h */



