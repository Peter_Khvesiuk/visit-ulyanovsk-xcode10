//
//  PKDataManager.m
//  MTSUSHI
//
//  Created by Petr Khvesiuk on 25.11.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import "PKDataManager.h"

#import "PKMapObject+CoreDataClass.h"
#import "PKGeoRoute+CoreDataClass.h"
#import "PKCity+CoreDataClass.h"
#import "PKMapObj+CoreDataClass.h"
#import "PKObjImages+CoreDataClass.h"
#import "PKKeywords+CoreDataClass.h"
#import "PKTypeObj+CoreDataClass.h"
#import "PKTypeobjGroup+CoreDataClass.h"
#import "PKFilterObjects+CoreDataClass.h"
#import "PKTranslateDictionary+CoreDataClass.h"
#import "PKTimeObject+CoreDataClass.h"

#import "PKUser+CoreDataClass.h"
#import "PKUserDocs+CoreDataClass.h"
#import "PKUserOptions+CoreDataClass.h"
#import "PKInformation+CoreDataClass.h"

#import "PKDictionaryMapObject.h"
#import "PKDictionaryGeoRoute.h"
#import "PKDictionaryUserDocs.h"
#import "PKComercialOrganization+CoreDataClass.h"
#import "PKDictionaryCommercialOrg.h"
#import "PKDictionarySettings.h"
#import "PKDictionaryTips.h"
#import "PKDictionaryEvent.h"
#import "PKDictionaryBrand.h"
#import "PKInformTips+CoreDataClass.h"
#import "PKEvent+CoreDataClass.h"
#import "PKBrand+CoreDataClass.h"
#import "PKContact+CoreDataClass.h"
#import "PKLike+CoreDataClass.h"

@implementation PKDataManager

@synthesize mainPrivateManagedObjectContext = _mainPrivateManagedObjectContext;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


+(PKDataManager*) sharedManager{
    
    static PKDataManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PKDataManager alloc] init];
    });
    
    return manager;
}
-(NSArray<PKUserDocs*>*)getUserDocs{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:self.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"optionName = %@", @"Documentation"];
    [request setPredicate:predicate];
    [request setEntity: description ];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    
    NSMutableArray *userDocs = [[NSMutableArray alloc] init];
    
    for (PKUserOptions *userOption in resultArray) {
        PKUserDocs *userDoc = userOption.docs;
        [userDocs addObject:userDoc];
    }
    return userDocs;
}
-(void)settingsUpdateFromArray:(NSArray*) settingsArray{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:self.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort = %i OR assort = %i OR assort = %i OR assort = %i", 2, 3, 4, 5];
    [request setPredicate:predicate];
    [request setEntity: description ];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    
    for (id object in resultArray) {
        [self.managedObjectContext deleteObject:object];
    }
//    [self saveDefaultContext:NO];
    
    
    for(id seting in settingsArray){
        PKDictionarySettings * settingsDict = [[PKDictionarySettings alloc] initWithDictionary:seting];
//        PKDictionaryUserDocs* userDocsDict = [[PKDictionaryUserDocs alloc] initWithDictionary:doc];
        
        
        if([settingsDict.type isEqualToString:@"docs"]){
            PKUserOptions *userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions"
                                                                       inManagedObjectContext:self.managedObjectContext];
            PKUserDocs* userDocs = [self returnCoreDataUserDocs:settingsDict];
            userOptions.docs = userDocs;
            userOptions.optionName = @"Documentation";
            userOptions.assort = 2;
            [self.managedObjectContext insertObject:userOptions];
        } else if([settingsDict.type isEqualToString:@"img"]){
//            PKUserOptions *userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions"
//                                                                       inManagedObjectContext:self.managedObjectContext];
//            PKUserDocs* userDocs = [self returnCoreDataUserDocs:settingsDict];
//            userOptions.docs = userDocs;
//            userOptions.optionName = @"Image";
//            userOptions.assort = 3;
//            [self.managedObjectContext insertObject:userOptions];
        } else if([settingsDict.type isEqualToString:@"text"]){
            PKUserOptions *userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions"
                                                                       inManagedObjectContext:self.managedObjectContext];
            PKInformation* userInformation = [self returnCoreDataUserInformation:settingsDict];
            userOptions.information = userInformation;
            if([settingsDict.sort isEqualToString:@"2"]){
                userOptions.optionName = @"Text";
                userOptions.assort = 4;
            } else if([settingsDict.sort isEqualToString:@"5"]){
                userOptions.optionName = @"About";
                userOptions.assort = 5;
            }
            [self.managedObjectContext insertObject:userOptions];
            
        }
        
        
        
        
        //[product.managedObjectContext save:nil];
    }
    
    
//    [self.managedObjectContext save:nil];
    [self saveDefaultContext:NO];
    
}


- (void)updateCoreDataMapObject:(NSDictionary*) responseObject {
    
//    NSError* error = nil;
    
//    [self deleteAllObjects];
    NSManagedObjectContext * bgcontext = [self getContextForBGTask];
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKMapObject"
                inManagedObjectContext:bgcontext];
    [request setEntity:description];
    NSError* requestError = nil;
    NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
    if (requestError) {
        // . NSLog(@"%@", [requestError localizedDescription]);
    }
    for (id object in resultArray) {
        [bgcontext deleteObject:object];
    }//deleteAllObjects
//    [self saveContextForBGTask:bgcontext];
    
    //  user docs begin ============================================================
    [self settingsUpdateFromArray:[responseObject objectForKey:@"settings"]];
    
    
//    [self userDocsUpdateFromArray:[responseObject objectForKey:@"docs"]];


    //  user docs end ============================================================
    
    
    
    //  keywords begin ============================================================
    NSDictionary* keywordsDict = [responseObject objectForKey:@"keywords"];
    
    NSMutableArray * keywords = [NSMutableArray array];
//    NSLog(@"keeeys = %@", [responseObject objectForKey:@"keywords"]);
    for(NSString* key in  [responseObject objectForKey:@"keywords"]){
        
        if(NULL_TO_NIL([[responseObject objectForKey:@"keywords"] objectForKey:key])){
            [keywords addObject:[self addKeywordsWithValue:[[responseObject objectForKey:@"keywords"] objectForKey:key] inContext:bgcontext]];
        }
    }
    //  keywords end ============================================================
    
    //  Comercial organization begin ============================================================
    NSMutableDictionary * organizationDict = [[NSMutableDictionary alloc] init];
    
//    phone, site, id, title, email, adds, mes
    // . NSLog(@"\norgDictionary orgDictionary = %@",[responseObject objectForKey:@"org"]);

    for(id orgDict in  [responseObject objectForKey:@"org"]){
        PKDictionaryCommercialOrg *orgDictionary = [[PKDictionaryCommercialOrg alloc] initWithDictionary:orgDict];
        // . NSLog(@"\nddfdd name = %@", orgDictionary.nameOrg);
        // . NSLog(@"\nddfdd phone = %@", orgDictionary.phoneOrg);
        // . NSLog(@"\nddfdd site = %@", orgDictionary.siteOrg);
        // . NSLog(@"\nddfdd email = %@", orgDictionary.emailOrg);
        PKComercialOrganization *org = [self addOrganization:orgDictionary inContext:bgcontext];
       
        [organizationDict setObject:org forKey:[orgDict objectForKey:@"id"]];
    }
    
    //  Comercial organization end ============================================================
    //  cities begin ============================================================
    NSDictionary* citiesDict = [responseObject objectForKey:@"city"];
    
    NSMutableArray * cities = [NSMutableArray array];
    
    for(NSString* key in  [responseObject objectForKey:@"city"]){
        [cities addObject:[self addCityWithName:[[responseObject objectForKey:@"city"] objectForKey:key] inContext:bgcontext]];
        // . NSLog(@"cities = %@", [[responseObject objectForKey:@"city"] objectForKey:key]);
    }
    
    //  cities end ============================================================
    
    //  weekdays begin ============================================================
    //NSDictionary* weekdays = [[responseObject objectForKey:@"interface"] objectForKey:@"weekdays"];
//    #warning check weekdays / delete from server?????
    //    // . NSLog(@"weekdays = %@", weekdays);
    
    //  weekdays end ============================================================
    
    //  interfaceDictionary begin ============================================================
    NSDictionary* interfaceDictionary = [[responseObject objectForKey:@"interface"] objectForKey:@"dictionary"];
    // . NSLog(@"interfaceDictionary = %@", interfaceDictionary);
    //    // . NSLog(@"interfaceDictionary = %@", interfaceDictionary);
    for (NSString* key in interfaceDictionary){
        NSString* value = [interfaceDictionary objectForKey:key];
        // . NSLog(@"key = %@ value = %@", key, value);
        //PKTranslateDictionary* translateDictionary =
        [self addCoreDataTranslateDictionary:key andValue:value inContext:bgcontext];
    }
    //  interfaceDictionary end ============================================================
    //  iconForMarkers begin ============================================================
    NSDictionary *iconForMarkers = [[NSDictionary alloc] initWithDictionary:[responseObject objectForKey:@"iconForMarkers"]];
    // . NSLog(@"bff f = %@", iconForMarkers);
    // . NSLog(@"bff f = %@", [iconForMarkers objectForKey:@"restaurant"]);
    //  iconForMarkers end ============================================================
    //  favorites begin ============================================================
//    // . NSLog(@"%@", );
    
    
    
    NSDictionary *favorites = [[NSDictionary alloc] initWithDictionary:[responseObject objectForKey:@"favorites"]];
    // . NSLog(@"bff f = %@", favorites);
    // . NSLog(@"bff f = %@", [favorites objectForKey:@"restaurants"]);
    NSArray *favoriteRestaurants = [[NSArray alloc] initWithArray:[favorites objectForKey:@"restaurants"]];
    NSArray *favoriteHotels = [[NSArray alloc] initWithArray:[favorites objectForKey:@"hotels"]];
    NSArray *favoriteAtractions = [[NSArray alloc] initWithArray:[favorites objectForKey:@"atractions"]];
   
    
    //  favorites end ============================================================
    //  types begin ============================================================
    
    
    
    NSMutableDictionary * typesGroupMutable = [[NSMutableDictionary alloc] init];
    NSDictionary* typesGroupDict = [responseObject objectForKey:@"typeobjgroup"];
//    NSLog(@"typesGroupDict = %@", typesGroupDict);
    
    NSDictionary* typesGroupSortedDict = [responseObject objectForKey:@"typeobjGroupSorted"];
//    NSLog(@"typeobjGroupSorted = %@",  typesGroupSortedDict);
    
    for(NSString*key in  typesGroupDict){
//        NSLog(@"objectForKey = %@ = %@",key, [typesGroupSortedDict objectForKey:key]);
        int sort  = (int)[[typesGroupSortedDict objectForKey:key] integerValue];
        id typeObjGroupObject = [self addTypeGroup:(int)[key integerValue] addSort:sort withTitle:[typesGroupDict objectForKey:key] inContext:bgcontext];
        [typesGroupMutable setObject:typeObjGroupObject forKey:key];
    }
    
   
    
    //NSLog(@"bff typeobjgroup = %@", [responseObject objectForKey:@"typeobjgroup"]);
    
    
    
   
    
    NSMutableDictionary * typesMutable = [[NSMutableDictionary alloc] init];
//    NSMutableArray * typesMutable = [NSMutableArray array];
    
    NSDictionary* typesDict = [responseObject objectForKey:@"typeobj"];
    //NSLog(@"bff typesDict = %@", typesDict);
//    NSDictionary* typesDict = [responseObject objectForKey:@"typeobj"];
    
    for(id key in  typesDict){
        NSString *keyMarkerIcon = [[NSString alloc] init];
        keyMarkerIcon = @"";
        for (NSString* keyMarker in iconForMarkers) {
            NSArray *iconForMarker = iconForMarkers[keyMarker];
            for (NSString *iconForMarkerId in iconForMarker) {
                if([[NSString stringWithFormat:@"%@", iconForMarkerId] isEqualToString:[NSString stringWithFormat:@"%@", key]] ){
                    keyMarkerIcon = keyMarker;
                    break;
                }
            }
        }
        int typeFavorites = 0;
        for(id keyFovor in  favoriteRestaurants){
            if([[NSString stringWithFormat:@"%@", keyFovor] isEqualToString:[NSString stringWithFormat:@"%@", key]]){
                typeFavorites = 1;
                break;
            }
        }
        if(typeFavorites == 0){
            for(id keyFovor in  favoriteHotels){
                if([[NSString stringWithFormat:@"%@", keyFovor] isEqualToString:[NSString stringWithFormat:@"%@", key]]){
                    typeFavorites = 2;
                    break;
                }
            }
            if(typeFavorites == 0){
                for(id keyFovor in  favoriteAtractions){
                    if([[NSString stringWithFormat:@"%@", keyFovor] isEqualToString:[NSString stringWithFormat:@"%@", key]]){
                        typeFavorites = 3;
                        break;
                    }
                }
            }
        }
        PKTypeObj* typesObject = [self addTypeObjWithValue:[typesDict objectForKey:key] andCategory:1 andKey: (int)[key integerValue] andTypeFavorites: typeFavorites andKeyMarker: keyMarkerIcon inContext:bgcontext];
        [typesMutable setObject:typesObject forKey:key];
//        [typesMutable addObject:[self addTypeObjWithValue:[typesDict objectForKey:key] andCategory:1 andKey: (int)[key integerValue] andTypeFavorites: typeFavorites andKeyMarker: keyMarkerIcon inContext:bgcontext]];
        // . NSLog(@"typeobj1 = %@", [typesDict objectForKey:key]);
    }
    NSDictionary* types = [[NSDictionary alloc] initWithDictionary:typesMutable];
    
    
    NSDictionary* typesGroupRelationsDict = [responseObject objectForKey:@"typeobjGroupRelations"];
    
    for (NSString*key in typesGroupRelationsDict) {
        PKTypeobjGroup* typeGroupObject = [typesGroupMutable objectForKey:key];
        for (NSString*typesId in [typesGroupRelationsDict objectForKey:key]) {
            PKTypeObj* typeObject = [types objectForKey:typesId];
            
            [typeGroupObject addTypeObject:typeObject];
            //NSLog(@"\nffgh value = %@, title = %@, group = %@ type = %@", typeObject.typeObjValue, typeGroupObject.typeGroupTitle, key, typesId);
        }
        
    }
    //NSLog(@"bff typeobjGroupRelations = %@", [responseObject objectForKey:@"typeobjGroupRelations"]);
    
    
    
    
//    NSArray* types = [[NSArray alloc] initWithArray:typesMutable];
    //  types end ============================================================
    
    
    NSArray* userLikes = [responseObject objectForKey:@"userLikes"];
//    NSLog(@"userLikes = %@",userLikes);
    //  mapObject begin
    NSMutableDictionary * mapObjectsMutable = [[NSMutableDictionary alloc] init];
    
    for(id dict in [responseObject objectForKey:@"mapobj"]){
//        NSLog(@"objFromJSON %@", dict);
        // . NSLog(@"objFromJSON %@", dict);
        PKDictionaryMapObject* objFromJSON = [[PKDictionaryMapObject alloc] initWithDictionary:dict];
//        NSLog(@"PKDictionaryMapObject = %@ %@", objFromJSON.avalibleForDisables, objFromJSON.mapObjectTitle);
        PKMapObj*mapObject = [self returnCoreDataMapObject: objFromJSON inContext:bgcontext];
        [mapObjectsMutable setObject:mapObject forKey:objFromJSON.mapObjectId]; // addObject:mapObject];
        // . NSLog(@"city = %@", objFromJSON.mapObjectCity);
        // . NSLog(@"city2 = %@", [citiesDict objectForKey: objFromJSON.mapObjectCity]);
        mapObject.city = [self addCityWithName: [citiesDict objectForKey:objFromJSON.mapObjectCity] inContext:bgcontext]; //city
        // . NSLog(@"mapObject.city.cityName");
        
        
        
        //  userLikes begin ============================================================
        if([userLikes count] > 0){
            if([self compareObjectID:mapObject.idobj andArray:userLikes]){
                mapObject.mylike = YES;
            } else {
                mapObject.mylike = NO;
            }
        }
        
        //  userLikes end ============================================================
        
        
        
        //  types begin ===============================================================
        
        PKTypeObj* typeObject = [types objectForKey:(NSString*)objFromJSON.mapObjectType];
        
        //NSLog(@"\ntypeObjectValue = %@ %@",typeObject.typeObjValue, mapObject.title);
        if(typeObject!= nil){
            
            [typeObject addMapobjectObject:mapObject];
        }
    
//        for (PKTypeObj* typeobj in types) {
//            if([typeobj.typeObjValue isEqualToString:[typesDict objectForKey: objFromJSON.mapObjectType]]){
//                [typeobj addMapobjectObject:mapObject];
//                break;
//            }
//        }

        //  types end =================================================================
        //  working time begin ===============================================================
        
//        if([mapObject.title isEqualToString:@"Noblesse"]){
//            NSLog(@"dict mapObjectKeywords == %@", objFromJSON.mapObjectKeywords);
//            // . NSLog(@"dict wifi == %@ paycard == %@", [dict objectForKey:@"wifi"], [dict objectForKey:@"paycard"]);
            // . NSLog(@"Археология Симбирского края wtime = %@", [dict objectForKey:@"wtime"]);
//        }
        
        id wtime = [dict objectForKey:@"wtime"];
        NSMutableSet<PKTimeObject *> *workingTimeSet = [[NSMutableSet alloc] init];
        if([wtime isKindOfClass:[NSArray class]]){
            NSUInteger index = 1;
            for (NSDictionary *wtimeDictionary in wtime) {
                // . NSLog(@"wtimeDictionary %@ %lu = %@", mapObject.title, (unsigned long)index, wtimeDictionary);
                PKTimeObject *workingTime = [self addWorkingTime:wtimeDictionary forDay:index inContext:bgcontext];
                [workingTimeSet addObject:workingTime];
                index++;
            }
        } else if([wtime isKindOfClass:[NSDictionary class]]){
            for (NSString* key in wtime) {
                NSUInteger index = [key integerValue] + 1;
                PKTimeObject *workingTime = [self addWorkingTime:[wtime objectForKey:key] forDay:index inContext:bgcontext];
                [workingTimeSet addObject:workingTime];
            }
        }
        
        [mapObject addWorkingTime:workingTimeSet];
        
        
        //  working time end =================================================================
        //  keywords begin ============================================================
        // . NSLog(@"mapObject.city.cityName2");
        NSMutableArray * keywordsArray = [NSMutableArray array];
        for (id key in objFromJSON.mapObjectKeywords) {
           
//            if([mapObject.title isEqualToString:@"Noblesse"]){
//                NSLog(@"key = %@", key);
//            }
            if([keywordsDict objectForKey:key] != nil && [keywordsDict objectForKey:key] !=[NSNull null]){
                [keywordsArray addObject:(PKKeywords*)[self addKeywordsWithValue:[keywordsDict objectForKey:key] inContext:bgcontext]];
            }
            
            
        }
        [mapObject addKeywords:[NSSet setWithArray: keywordsArray]];
        //  keywords end ============================================================
        
        //  images begin ============================================================
        NSMutableArray * imagesArray = [NSMutableArray array];
        for (id image in objFromJSON.mapObjectImageGallery) {
            [imagesArray addObject:(PKObjImages*)[self addImageWithName: image inContext:bgcontext]];
            
            // . NSLog(@"find image = %@",image);
            // do something with object
        }
        [mapObject addImages:[NSSet setWithArray: imagesArray]];
        
        //  images end ==============================================================
        
    }
    
//    PKMapObj *ff = (PKMapObj*)[mapObjectsMutable objectForKey:393];
//    // . NSLog(@"\n mmm mapobjects = %@", ff.title);
    
    
    //  types-geo begin ============================================================
    
    NSMutableArray * typesMutableGeo = [NSMutableArray array];
    NSDictionary* typesGeoDict = [responseObject objectForKey:@"typegeo"];
    // . NSLog(@"bff typesDict = %@", typesGeoDict);
    for(NSString* key in  typesGeoDict){
        [typesMutableGeo addObject:[self addTypeObjWithValue:[typesGeoDict objectForKey:key] andCategory:2  andKey:(int)[key integerValue] andTypeFavorites: 0  andKeyMarker:@"" inContext:bgcontext]];
        // . NSLog(@"typeobj2 = %@", [typesGeoDict objectForKey:key]);
    }
    NSArray* typesGeo = [[NSArray alloc] initWithArray:typesMutableGeo];
    
    //  types-geo end ==============================================================
    //  Events begin ============================================================
    
    NSDictionary<NSString*, PKEvent *>* mutableEvents = [self addEventsFromDictionary:[responseObject objectForKey:@"events"] withMapObjects:mapObjectsMutable andWithOrgs:organizationDict inContext:bgcontext];
    
    //  Events end ============================================================
    //  inform TIPS begin ============================================================
    
    [self addInContext:bgcontext withDictionary:[responseObject objectForKey:@"news"] andIsTips:NO with:mapObjectsMutable with:organizationDict];

    //  inform TIPS end ============================================================
    //  inform TIPS begin ============================================================
    
    [self addInContext:bgcontext withDictionary:[responseObject objectForKey:@"inform"] andIsTips:YES with:mapObjectsMutable with:organizationDict];

    //  inform TIPS end ============================================================
    // . NSLog(@"\norgDictionary dict = %@", [responseObject objectForKey:@"georoute"]);
    
    
    NSArray* userLikesGeo = [responseObject objectForKey:@"userLikesGeo"];
    
    for(id dict in [responseObject objectForKey:@"georoute"]){
        
#warning KOSTYL empty json from server
        if(![[dict objectForKey:@"id"] isEqualToString:@"0"]){
            // . NSLog(@"georoute = %@", dict);
            PKDictionaryGeoRoute *geoRouteFromJSON = [[PKDictionaryGeoRoute alloc] initWithDictionary:dict];
            PKGeoRoute *geoRoute = [self returnCoreDataGeoRoute: geoRouteFromJSON inContext:bgcontext];
            
            
            //  userLikes begin ============================================================
            if([userLikesGeo count] > 0){
                if([self compareObjectID:geoRoute.idgeo andArray:userLikesGeo]){
                    geoRoute.mylike = YES;
                } else {
                    geoRoute.mylike = NO;
                }
            }
            
            //  userLikes end ============================================================
            
            // . NSLog(@"\ngeoRoute zZ id = %@", [dict objectForKey:@"id"]);
            // . NSLog(@"\ngeoRoute zZ title = %@", geoRoute.title);
            
            for (PKTypeObj* typeGeo in typesGeo) {
                // . NSLog(@"geoRouteType1 = %@", [typesGeoDict objectForKey: geoRouteFromJSON.geoRouteType]);
                if([typeGeo.typeObjValue isEqualToString:[typesGeoDict objectForKey: geoRouteFromJSON.geoRouteType]]){
                    // . NSLog(@"geoRouteType2 = %@", [typesGeoDict objectForKey: geoRouteFromJSON.geoRouteType]);
                    [typeGeo addGeorouteObject:geoRoute];
                    break;
                }
            }
            for (NSString *mapObjectId in geoRouteFromJSON.geoRouteMapObjects) {
                if([mapObjectsMutable valueForKey:mapObjectId]  != nil){
                    PKMapObj* mapObject = (PKMapObj*)[mapObjectsMutable valueForKey:mapObjectId];
                    [geoRoute addMapObjectObject:mapObject];
                }
//                for (PKMapObj* mapObject in mapObjects) {
//                    if(mapObject.idobj == [mapObjectId integerValue]){
//                        [geoRoute addMapObjectObject:mapObject];
//                        break;
//                    }
//                }
            }
            // . NSLog(@"\ngeoRouteFromJSON.orgId = %@", geoRouteFromJSON.orgId);
            if(![geoRouteFromJSON.orgId isEqualToString:@"0"]){
                PKComercialOrganization* geoOrg = [organizationDict objectForKey:geoRouteFromJSON.orgId];
                // . NSLog(@"\n nameOrg = %@", geoOrg.nameOrg);
                //            // . NSLog(@"\nddfdd ===== title = %@", geoRoute.title);
                //            // . NSLog(@"\nddfdd ===== org_id = %@", geoRouteFromJSON.orgId);
                //            if([geoRouteFromJSON.orgId isEqualToString:@"0"]){
                //                // . NSLog(@"\nddfdd ===== dict = %@", dict);
                //            }
                //            // . NSLog(@"\nddfdd ===== nameOrg = %@", geoOrg.nameOrg);
                //            // . NSLog(@"\nddfdd ===== phone = %@", geoOrg.phoneOrg);
                //            // . NSLog(@"\nddfdd ===== site = %@", geoOrg.siteOrg);
                //            // . NSLog(@"\nddfdd ===== email = %@", geoOrg.emailOrg);
                [geoOrg addGeoRoutesObject:geoRoute];
            }
        }
        
        
    }
    
    //  Brand begin ============================================================
    
    [self addBrandsFromDictionary:[responseObject objectForKey:@"brand"]
                   withMapObjects:mapObjectsMutable
                      andWithOrgs:organizationDict
                       withEvents:mutableEvents
                        inContext:bgcontext];
    
    //  Brand end ============================================================
    
    [self setFirstTimeJsonLoaded:YES inManagedContext:bgcontext];
    
//    [self saveDefaultContext:NO];
    
    [self saveContextForBGTask:bgcontext];
    //    if (![self.managedObjectContext save:&error]) {
    //        // . NSLog(@"%@", [error localizedDescription]);
    //    }
    
    


}
-(void)addBrandsFromDictionary:(NSDictionary*)brandDictionariesD
                withMapObjects:(NSDictionary*)mapObjects
                   andWithOrgs:(NSDictionary*)organizationDict
                    withEvents:(NSDictionary<NSString*, PKEvent*>*)mutableEvents
                     inContext:bgcontext{
    //-(void)addInContext:bgcontext withDictionary: (NSDictionary*)informDictionary andIsTips:(BOOL)tips  with:(NSDictionary*)mapObjectsMutable with:(NSDictionary*)organizationDict{
    for(id informnews in  brandDictionariesD){
        //        NSLog(@"inform = %@", informnews);
        PKDictionaryBrand *brandDictionaries = [[PKDictionaryBrand alloc] initWithDictionary:informnews];
        PKBrand *brandCoreData = [self insertBrand:brandDictionaries inContext:bgcontext];
        
        if(!([brandDictionaries.objectsIds count] == 0) || brandDictionaries.objectsIds != nil){
            NSMutableArray *brandCoreDataObjectsIds = [[NSMutableArray alloc] init];
            int i = 0;
            for (NSString *objectId in brandDictionaries.objectsIds) {
                if([mapObjects valueForKey:objectId] != nil){
                    PKMapObj* mapObject = (PKMapObj*)[mapObjects valueForKey:objectId];
                    if(i == 0){
                        [brandCoreData setFirstMapObject:mapObject];
                        i++;
                    }
                    [brandCoreDataObjectsIds addObject:mapObject];
                }
                
            }
            if(!([brandCoreDataObjectsIds count] == 0) || brandCoreDataObjectsIds != nil){
                [brandCoreData addMapObjects:[NSSet setWithArray: brandCoreDataObjectsIds]];
            }
        }
        
        if(!([brandDictionaries.orgIdArray count] == 0) || brandDictionaries.orgIdArray != nil){
            NSMutableArray<PKComercialOrganization*> * organizationsArray = [NSMutableArray array];
            for (NSString* organizationId in brandDictionaries.orgIdArray) {
                if([organizationDict valueForKey:organizationId] != nil){
                    PKComercialOrganization* eventOrg = [organizationDict objectForKey:organizationId];
                    
                    [organizationsArray addObject:(PKComercialOrganization*)eventOrg];
                }
            }
            if(!([organizationsArray count] == 0) || organizationsArray != nil){
                [brandCoreData addOrganizations:[NSSet setWithArray: organizationsArray]];
            }
        }
        
        if(!([brandDictionaries.eventsArray count] == 0) || brandDictionaries.eventsArray != nil){
            NSMutableArray<PKEvent*> * eventsArray = [NSMutableArray array];
            for (NSString* eventId in brandDictionaries.eventsArray) {
                if([mutableEvents valueForKey:eventId] != nil){
                    PKEvent* eventOrg = [mutableEvents objectForKey:eventId];
                    
                    [eventsArray addObject:(PKEvent*)eventOrg];
                }
            }
            if(!([eventsArray count] == 0) || eventsArray != nil){
                [brandCoreData addEvents:[NSSet setWithArray: eventsArray]];
            }
        }
        
        #warning GeoRoute and Events for PKBrand not set
    }
}
-(NSDictionary<NSString*, PKEvent *>*)addEventsFromDictionary:(NSDictionary*)eventDictionariesD withMapObjects:(NSDictionary*)mapObjects andWithOrgs:(NSDictionary*)organizationDict inContext:bgcontext{

    NSMutableDictionary *mutableEvents = [[NSMutableDictionary alloc] init];
    for(id informnews in  eventDictionariesD){
        //        NSLog(@"inform = %@", informnews);
        PKDictionaryEvent *eventDictionaries = [[PKDictionaryEvent alloc] initWithDictionary:informnews];
        PKEvent *eventCoreData = [self insertEvent:eventDictionaries inContext:bgcontext];
        NSMutableArray * imagesArray = [NSMutableArray array];
        for (NSString* imageName in eventDictionaries.imagesArray) {
            [imagesArray addObject:(PKObjImages*)[self addImageWithName: imageName inContext:bgcontext]];
            //            NSLog(@"inform image = %@",imageName);
            // do something with object
        }
        [eventCoreData addImages:[NSSet setWithArray: imagesArray]];
        //#warning DOPISAT CODE
        if(!([eventDictionaries.objectsIds count] == 0) || eventDictionaries.objectsIds != nil){
            NSMutableArray *eventCoreDataObjectsIds = [[NSMutableArray alloc] init];
            for (NSString *objectId in eventDictionaries.objectsIds) {
                if([mapObjects valueForKey:objectId] != nil){
                    PKMapObj* mapObject = (PKMapObj*)[mapObjects valueForKey:objectId];
                    [eventCoreDataObjectsIds addObject:mapObject];
                    //                    NSLog(@"\n eventCoreData.mapObject = %@", mapObject.title);
                }
            }
            if(!([eventCoreDataObjectsIds count] == 0) || eventCoreDataObjectsIds != nil){
                [eventCoreData addMapObjects:[NSSet setWithArray: eventCoreDataObjectsIds]];
            }
        }

        if(!([eventDictionaries.orgIdArray count] == 0) || eventDictionaries.orgIdArray != nil){
            NSMutableArray<PKComercialOrganization*> * organizationsArray = [NSMutableArray array];
            for (NSString* organizationId in eventDictionaries.orgIdArray) {
                if([organizationDict valueForKey:organizationId] != nil){
                    PKComercialOrganization* eventOrg = [organizationDict objectForKey:organizationId];
                // . NSLog(@"\n nameOrg = %@", informDictionary.title);

                    [organizationsArray addObject:(PKComercialOrganization*)eventOrg];
                }
                //            NSLog(@"inform image = %@",imageName);
                // do something with object
            }
            if(!([organizationsArray count] == 0) || organizationsArray != nil){
                [eventCoreData addOrganizations:[NSSet setWithArray: organizationsArray]];
            }
        }
        
        if(mutableEvents){
            [mutableEvents setObject:eventCoreData forKey:eventDictionaries.idevent];
        }
    }
    return mutableEvents;
}

-(void)addInContext:bgcontext withDictionary: (NSDictionary*)informDictionary andIsTips:(BOOL)tips  with:(NSDictionary*)mapObjectsMutable with:(NSDictionary*)organizationDict{
    for(id informnews in  informDictionary){
//        NSLog(@"inform = %@", informnews);
        PKDictionaryTips *informDictionary = [[PKDictionaryTips alloc] initWithDictionary:informnews];
        PKInformTips *informTips = [self addInformTips:informDictionary andIsTips:tips inContext:bgcontext];
        NSMutableArray * imagesArray = [NSMutableArray array];
        for (NSString* imageName in informDictionary.imagesArray) {
            [imagesArray addObject:(PKObjImages*)[self addImageWithName: imageName inContext:bgcontext]];
//            NSLog(@"inform image = %@",imageName);
            // do something with object
        }
        [informTips addImages:[NSSet setWithArray: imagesArray]];
        if(!([informDictionary.objectsIds count] == 0) || informDictionary.objectsIds != nil){
            NSMutableArray *informTipsObjectsIds = [[NSMutableArray alloc] init];
            for (NSString *objectId in informDictionary.objectsIds) {
                if([mapObjectsMutable valueForKey:objectId] != nil){
                    PKMapObj* mapObject = (PKMapObj*)[mapObjectsMutable valueForKey:objectId];
                    [informTipsObjectsIds addObject:mapObject];
//                    NSLog(@"\n informTips.mapObject = %@", mapObject.title);
                }
            }
            [informTips addMapObjects:[NSSet setWithArray: informTipsObjectsIds]];
//            NSLog(@"\n informTips.title %@ == %@",informTips.title, (PKMapObj*)[[informTips.mapObjects allObjects] firstObject].title);
        }
        if(![informDictionary.orgId isEqualToString:@"0"]){
            PKComercialOrganization* tipsOrg = [organizationDict objectForKey:informDictionary.orgId];
            // . NSLog(@"\n nameOrg = %@", informDictionary.title);
            informTips.organization = tipsOrg;
        }
    }
}


- (void) deleteAllObjects {
    
    NSArray* allObjects = [self allObjects];

    for (id object in allObjects) {
        [self.managedObjectContext deleteObject:object];
    }
    [self saveDefaultContext:NO];

//    [self.managedObjectContext save:nil];
}
- (NSArray*) allPKGeoRoute {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKGeoRoute"
                inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:description];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        // . NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return resultArray;
}
- (void)calculateDistanceForMapObjects:(void(^)(NSArray<PKMapObj*>* mapObjects)) success{

    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    NSManagedObjectContext * bgcontext = self.managedObjectContext;//[self getContextForBGTask];
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKMapObj"
                inManagedObjectContext:bgcontext];
    
    [request setEntity:description];
    
    //    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    //    [request setPredicate:predicate];
    
    NSError* requestError = nil;
    NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
    if (requestError) {
        NSLog(@"%@", [requestError localizedDescription]);
    } else {
        if(success){
//            NSLog(@"ZZ success");
            success(resultArray);
        }
        [bgcontext updatedObjects];
        //    [self saveContextForBGTask:bgcontext];
        
        [bgcontext save:&requestError];
        if (requestError) {
            NSLog(@"error saving before delete: %@",requestError);
        }
//        NSLog(@"ZZ save");
        [self saveDefaultContext:NO];
    }
}
- (NSArray*) allPKMapObj {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKMapObj"
                inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:description];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        // . NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return resultArray;
}


- (NSArray*) allObjects {
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKMapObject"
                inManagedObjectContext:self.managedObjectContext];
    
    [request setEntity:description];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        // . NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return resultArray;
}
//- (PKMapObj*) oneMapObjectsWithId: (NSString*) mapObjId{
//    
//    NSFetchRequest* request = [[NSFetchRequest alloc] init];
//    
//    NSEntityDescription* description =
//    [NSEntityDescription entityForName:@"PKMapObj"
//                inManagedObjectContext:self.managedObjectContext];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idobj == %@", mapObjId];
//    [request setPredicate:predicate];
//    [request setEntity:description];
//    
//    NSError* requestError = nil;
//    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
//    if (requestError) {
//        // . NSLog(@"%@", [requestError localizedDescription]);
//    }
//    // . NSLog(@"oneMapObjectsWithId = %@", resultArray);
//    return resultArray[0];
//}


#pragma mark - Update Objects For Liked


- (void) updateObjectsForLikedFromArray:(NSArray*) idObjArray {
    
    NSArray* allObjects = [self allPKMapObj];
    
    for (PKMapObj* object in allObjects) {
        if([self compareObjectID:object.idobj andArray:idObjArray]){
//            // . NSLog(@"compareObjectID %hd == %@", object.idobj, idObjArray);
            object.mylike = YES;
        } else {
            
            object.mylike = NO;
        }
        
   
    }
    [self.managedObjectContext updatedObjects];
    [self saveDefaultContext:NO];
//    [self.managedObjectContext save:nil];
}
- (void) updateObjectsForLikedGeoFromArray:(NSArray*) idObjArray {
    
    NSArray* allObjects = [self allPKGeoRoute];
    
    for (PKGeoRoute* object in allObjects) {
        if([self compareObjectID:object.idgeo andArray:idObjArray]){
            //            // . NSLog(@"compareObjectID %hd == %@", object.idobj, idObjArray);
            object.mylike = YES;
        } else {
            
            object.mylike = NO;
        }
        
        
    }
    [self.managedObjectContext updatedObjects];
    [self saveDefaultContext:NO];
    //    [self.managedObjectContext save:nil];
}

- (NSArray*) switchObjectLike:(NSManagedObjectID*) objectID{
    NSManagedObjectContext * bgcontext = [self getContextForBGTask];
    
    int idObject = 0;
    NSString* objectOrGeo = nil;

#warning do I need counter here???
    int countAfter = 0;
    if([[bgcontext objectWithID:objectID] isKindOfClass:[PKMapObj class]]){
        PKMapObj * mapObject = [bgcontext objectWithID:objectID];
        idObject = mapObject.idobj;
        if(mapObject.mylike){
            mapObject.mylike = 0;
            if(mapObject.likes>0){
                mapObject.likes = mapObject.likes - 1;
                countAfter = mapObject.likes;
            } else {
                countAfter = 0;
            }
        } else {
            mapObject.mylike = 1;
            mapObject.likes = mapObject.likes +1;
            countAfter = mapObject.likes;
        }
        
        objectOrGeo = @"idObj";
    } else if([[bgcontext objectWithID:objectID] isKindOfClass:[PKGeoRoute class]]){
        PKGeoRoute * geoRoute = [bgcontext objectWithID:objectID];
        idObject = geoRoute.idgeo;
        if(geoRoute.mylike){
            geoRoute.mylike = 0;
            if(geoRoute.likes>0){
                geoRoute.likes = geoRoute.likes - 1;
                countAfter = geoRoute.likes;
            } else {
                countAfter = 0;
            }
        } else {
            geoRoute.mylike = 1;
            geoRoute.likes = geoRoute.likes +1;
            countAfter = geoRoute.likes;
        }
        objectOrGeo = @"idGeo";
    }
    
    
    [bgcontext updatedObjects];
    [self saveContextForBGTask:bgcontext];
    
    NSArray * returnArray = [[NSArray alloc] initWithObjects:[NSNumber numberWithInteger:idObject], objectOrGeo, nil];
    return returnArray;
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(dataChanged:)
//     name:NSManagedObjectContextObjectsDidChangeNotification //NSManagedObjectContextDidSaveNotification
//     object:bgcontext];
    
}
- (NSString*)translateWithMyDictionary:(NSString*) key{
//    NSArray* typeObjects = [self allObjectWithEntityName: @"PKTranslateDictionary"];
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKTranslateDictionary" inManagedObjectContext:self.managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"key == %@", key];
    [request setPredicate:predicate];
    [request setEntity: description];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if([resultArray count] == 0){
        return UPFirst([key stringByReplacingOccurrencesOfString:@"_"
                                              withString:@" "]);
    }
    PKTranslateDictionary* dictionary = [resultArray lastObject];
    return dictionary.value;
}
- (PKMapObj*) oneMapObject:(NSManagedObjectID*) mapObjectID{
    return [self.managedObjectContext objectWithID:mapObjectID];
}
- (PKMapObj*) oneMapObjectWithId:(int) idObj{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKMapObj"
                inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idobj = %i", idObj];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    [request setEntity: description ];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        // . NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return (PKMapObj*) [resultArray firstObject];
}
- (PKGeoRoute*) oneGeoRouteWithId:(int) idRoute{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKGeoRoute"
                inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idgeo = %i", idRoute];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    [request setEntity: description ];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        // . NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return (PKGeoRoute*) [resultArray firstObject];
}
- (PKEvent*) oneEventWithId:(int)idevent{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKEvent"
                inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idevent = %i", idevent];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    [request setEntity: description ];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return (PKEvent*) [resultArray firstObject];
}
- (PKInformTips*) oneInformTipsWithId:(int) idtips andIsTip:(BOOL) isTip{
    
    NSFetchRequest* request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription* description =
    [NSEntityDescription entityForName:@"PKInformTips"
                inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"idtips = %i AND isTips = %i", idtips, isTip];
    [request setPredicate:predicate];
    [request setFetchLimit:1];
    [request setEntity: description ];
    
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if (requestError) {
        NSLog(@"%@", [requestError localizedDescription]);
    }
    // . NSLog(@"allObjects = %@", resultArray);
    return (PKInformTips*) [resultArray firstObject];
}

- (void)dataChanged:(NSNotification *)notification{
//    NSDictionary *info = notification.userInfo;
//    NSSet *insertedObjects = [info objectForKey:NSInsertedObjectsKey];
//    NSSet *deletedObjects = [info objectForKey:NSDeletedObjectsKey];
    /*
    NSSet *updatedObjects = [info objectForKey:NSUpdatedObjectsKey];
    // . NSLog(@"updatedObjects: %@", [updatedObjects description]);
//    [_managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
    
     for(NSManagedObject *obj in updatedObjects){
        
        NSDictionary *changes = [obj changedValues];
        // . NSLog(@"changes = %@", changes);
        // now process the changes as you need
        
    }
     */
    
}

//- (void) updateOneObjectForLiked:(NSString*) idObj {
//
//    PKMapObj* mapObject = [self oneMapObjectsWithId: idObj];
//    if(mapObject.mylike){
//        mapObject.mylike = NO;
//    } else {
//        mapObject.mylike = YES;
//    }
//
//    [self.managedObjectContext updatedObjects];
//    [self saveDefaultContext:NO];
////    [self.managedObjectContext save:nil];
//}
- (BOOL) compareObjectID:(int16_t) idObj andArray: (NSArray*) idObjArray{
    BOOL done = NO;
    for (id string in idObjArray) {
//        NSLog(@"count = %lu compareObjectID = %@ == %@", (unsigned long)[idObjArray count] , [NSString stringWithFormat:@"%@", string], [NSString stringWithFormat:@"%hd", idObj]);
        if([[NSString stringWithFormat:@"%@", string] isEqualToString: [NSString stringWithFormat:@"%hd", idObj]]){
            
            done = YES;
            break;
        }
            
        
    }
    return done;
}




//- (void)addToCoreDataMapObject:(NSArray*) dict toTheEntity:(NSString*) entityName {
//
//    NSArray* allObject1 = [self allObjectWithEntityName: entityName];
//    for(PKMapObj* object in allObject1){
//        // // . NSLog(@"objectinCoreData title = %@ duration = %@ where = %@ trener = %@", object.title, object.duration, object.where, object.trener);
//
//        [self.managedObjectContext deleteObject:object];// пометить на удаление
//    }
//
//    [self saveDefaultContext:NO];
////    [self.managedObjectContext save:nil]; // сохранить изменения на удаление
//
//
//
//
//
//
////    NSArray* courses = @[[self addCourseWithName:@"iOS"],
////                         [self addCourseWithName:@"Android"],
////                         [self addCourseWithName:@"PHP"],
////                         [self addCourseWithName:@"Javascript"],
////                         [self addCourseWithName:@"HTML"]];
////
////    PKCity* city = [self addCity];
//
////    [university addCourses:[NSSet setWithArray:courses]];
////
////    for (int i = 0; i < 100; i++) {
////
////        ASStudent* student = [self addRandomStudent];
////
////        if (arc4random_uniform(1000) < 500) {
////            ASCar* car = [self addRandomCar];
////            student.car = car;
////        }
////
////        student.university = university;
////
////        NSInteger number = arc4random_uniform(5) + 1;
////
////        while ([student.courses count] < number) {
////            ASCourse* course = [courses objectAtIndex:arc4random_uniform(5)];
////            if (![student.courses containsObject:course]) {
////                [student addCoursesObject:course];
////            }
////        }
////    }
////
////    if (![self.managedObjectContext save:&error]) {
////        // . NSLog(@"%@", [error localizedDescription]);
////    }
//
//
//
//
//
//
//
//    for(PKDictionaryMapObject* productFromJSON in dict){
//
//
//
//        PKMapObj*product = [self returnCoreDataProductMenu: productFromJSON toTheEntity:entityName];
//        [self.managedObjectContext insertObject:product];
//        //[product.managedObjectContext save:nil];
//    }
//
//    [self saveDefaultContext:NO];
////    [self.managedObjectContext save:nil];
//
//
//
//}
//- (PKCity*) addCity {
//
//    PKCity* city =
//    [NSEntityDescription insertNewObjectForEntityForName:@"PKCity"
//                                  inManagedObjectContext:self.managedObjectContext];
//    city.cityName = @"Ulyanovsk";
//
//    return city;
//}
//- (PKTypeObj*) addObjectWithName:(NSString*) name {
//
//    ASCourse* course =
//    [NSEntityDescription insertNewObjectForEntityForName:@"ASCourse"
//                                  inManagedObjectContext:self.managedObjectContext];
//
//    course.name = name;
//
//    return course;
//}
//
//
//- (PKTypeObj*) addCourseWithName:(NSString*) name {
//
//    ASCourse* course =
//    [NSEntityDescription insertNewObjectForEntityForName:@"ASCourse"
//                                  inManagedObjectContext:self.managedObjectContext];
//
//    course.name = name;
//
//    return course;
//}
#pragma mark - Add some objects

- (PKCity*) addCityWithName:(NSString*) name inContext:(NSManagedObjectContext*)context{
    
    PKCity* city =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKCity"
                                  inManagedObjectContext:context];
    
    city.cityName = name;
    
    return city;
}

- (PKTypeObj*) addTypeObjWithValue:(NSString*) value andCategory:(int) category andKey:(int) key andTypeFavorites:(int) typeFavorites  andKeyMarker:(NSString*) keyMarkerIcon inContext:(NSManagedObjectContext*)context {
    
    PKTypeObj* typeObj =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKTypeObj"
                                  inManagedObjectContext:context];
    typeObj.typeIcon = [keyMarkerIcon integerValue];
    typeObj.typeFavorites = typeFavorites;
    typeObj.typeObjValue = value;
    typeObj.typeCategory = category;
    typeObj.typeKey = key;
    return typeObj;
}
- (PKKeywords*) addKeywordsWithValue:(NSString*) value inContext:(NSManagedObjectContext*)context {
    
    PKKeywords* keyword =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKKeywords"
                                  inManagedObjectContext:context];
    
    keyword.keywordValue = value;
    
    return keyword;
}
- (PKTypeobjGroup*) addTypeGroup:(int) id_group addSort:(int) sort withTitle:(NSString*) value inContext:(NSManagedObjectContext*)context {
    
    PKTypeobjGroup* typeGroup =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKTypeobjGroup"
                                  inManagedObjectContext:context];
    
    typeGroup.typeGroupTitle = value;
    typeGroup.typeGroupId = id_group;
    typeGroup.sort = sort;
    
    return typeGroup;
}
-(PKLike*)insertLikeWithLikes:(NSInteger)likes andMyLike:(BOOL)myLike inContext:(NSManagedObjectContext*)context{
    PKLike* likeCoreData =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKLike"
                                      inManagedObjectContext:context];
    likeCoreData.like = likes;
    likeCoreData.isMyLike = myLike;
    
    return likeCoreData;
}
-(PKContact*)insertContactWithEmail:(NSString*)email andWithPhone:(NSString*)phone andWithSite:(NSString*)site inContext:(NSManagedObjectContext*)context{
    
    if(![email isEqualToString:@""] || ![phone isEqualToString:@""] || ![site isEqualToString:@""] ){
        PKContact* contactCoreData =
        [NSEntityDescription insertNewObjectForEntityForName:@"PKContact"
                                      inManagedObjectContext:context];
        contactCoreData.email = email;
        contactCoreData.siteurl = site;
        contactCoreData.phone = phone;
        
        return contactCoreData;
    }
    return nil;
}

-(PKBrand*)insertBrand:(PKDictionaryBrand*)brandDict inContext:(NSManagedObjectContext*)context {
    PKBrand* brandCoreData =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKBrand"
                                  inManagedObjectContext:context];

    brandCoreData.idBrand = [brandDict.idbrand integerValue];
    brandCoreData.title = brandDict.title;
    brandCoreData.subtitle = brandDict.subtitle;
    brandCoreData.descriptionss = brandDict.descript;
    brandCoreData.logoImage = brandDict.logoimg;
    brandCoreData.bgColor = brandDict.color;
    brandCoreData.sort = [brandDict.sort integerValue];
    
    PKContact* contact = [self insertContactWithEmail:brandDict.email andWithPhone:brandDict.phone andWithSite:brandDict.url inContext:context];
    if(contact){
        [brandCoreData setContact:contact];
    }
#warning LIKE FOR PKBrand not set
    PKLike* like = [self insertLikeWithLikes:[brandDict.likes integerValue] andMyLike:NO inContext:context];
    if(like){
        [brandCoreData setLike:like];
    }
    return brandCoreData;
}
-(PKEvent*)insertEvent:(PKDictionaryEvent*)eventDict inContext:(NSManagedObjectContext*)context {
    PKEvent* eventCoreData =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKEvent"
                                  inManagedObjectContext:context];
    NSDateFormatter *formatterdate = [[NSDateFormatter alloc] init];
    [formatterdate setDateFormat:@"yyyy-MM-dd HH:mm"];//:ss
    [formatterdate setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    eventCoreData.dateStart = [formatterdate dateFromString:eventDict.dateStart];
    eventCoreData.dateEnd = [formatterdate dateFromString:eventDict.dateEnd];
    
    eventCoreData.price = [eventDict.price integerValue];
    eventCoreData.ageFrom = [eventDict.ageFrom integerValue];
    eventCoreData.ageTo = [eventDict.ageTo integerValue];

    eventCoreData.title = eventDict.title;
    eventCoreData.sort = [eventDict.sort integerValue];
    //    informTipsCoreData.images = informDict.siteOrg;
    eventCoreData.previewImgName = eventDict.prev;
    eventCoreData.descriptionss = eventDict.descript;
    eventCoreData.eventPeriodicity = eventDict.eventPeriodicity;
    eventCoreData.idevent = [eventDict.idevent integerValue];
//    eventCoreData.followEvent = [eventDict.followEvent integerValue];
    
    return eventCoreData;
}
-(PKInformTips*)addInformTips:(PKDictionaryTips*)informDict andIsTips:(BOOL)isTips inContext:(NSManagedObjectContext*)context {
    PKInformTips* informTipsCoreData =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKInformTips"
                                  inManagedObjectContext:context];

    informTipsCoreData.isTips = isTips;
    if(!isTips){
        informTipsCoreData.isFree = [informDict.isFree integerValue];
        informTipsCoreData.ageString = informDict.ageString;
        informTipsCoreData.dateString = informDict.dateString;
    }
    informTipsCoreData.title = informDict.title;
    informTipsCoreData.sort = [informDict.sort integerValue];
//    informTipsCoreData.images = informDict.siteOrg;
    informTipsCoreData.previewImgName = informDict.prev;
    informTipsCoreData.descriptionss = informDict.descript;
    informTipsCoreData.symbolCode = informDict.symbolCode;
    informTipsCoreData.idtips = [informDict.idtips integerValue];

    return informTipsCoreData;
}
-(PKComercialOrganization*)addOrganization:(PKDictionaryCommercialOrg*)orgDict inContext:(NSManagedObjectContext*)context {
    PKComercialOrganization* orgCoreData =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKComercialOrganization"
                                  inManagedObjectContext:context];
    orgCoreData.idOrg = orgDict.idOrg;
    orgCoreData.nameOrg = orgDict.nameOrg;
    orgCoreData.siteOrg = orgDict.siteOrg;
    orgCoreData.phoneOrg = orgDict.phoneOrg;
    orgCoreData.emailOrg = orgDict.emailOrg;
    orgCoreData.adressOrg = orgDict.adressOrg;
    orgCoreData.commercialRequest = orgDict.commercialRequest;
    return orgCoreData;
}
-(PKTimeObject*)addWorkingTime:(NSDictionary*)workingTime forDay:(NSInteger)day inContext:(NSManagedObjectContext*)context {
    
    PKTimeObject* timeObject =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKTimeObject"
                                  inManagedObjectContext:context];
    timeObject.day = day;
//    NSLog(@"workingTime begin1 == %@", [workingTime objectForKey:@"begin"]);
//    NSLog(@"workingTime begin2 == %i", (int)[self timeIntervalFromString:[workingTime objectForKey:@"begin"]]);
//
    timeObject.begin = (int)[self timeIntervalFromString:[workingTime objectForKey:@"begin"]];
    timeObject.end = (int)[self timeIntervalFromString:[workingTime objectForKey:@"end"]];
    timeObject.breakBegin = (int)[self timeIntervalFromString:[workingTime objectForKey:@"break_begin"]];
    timeObject.breakEnd = (int)[self timeIntervalFromString:[workingTime objectForKey:@"break_end"]];
    
    return timeObject;
}
-(NSInteger)timeIntervalFromString:(NSString*)time{
    NSArray* hoursAndMinutes = [time componentsSeparatedByString:@":"];
    NSInteger hours = [hoursAndMinutes[0] integerValue];
    NSInteger minutes = [hoursAndMinutes[1] integerValue];
    return hours*3600+minutes*60;
}


- (PKObjImages*) addImageWithName:(NSString*) name inContext:(NSManagedObjectContext*)context {
    
    PKObjImages* image =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKObjImages"
                                  inManagedObjectContext:context];
    
    image.imageName = name;
    
    return image;
}










-(PKGeoRoute*)returnCoreDataGeoRoute:(PKDictionaryGeoRoute*) dict inContext:(NSManagedObjectContext*) bgcontext{
    PKGeoRoute*geoRoute = [NSEntityDescription insertNewObjectForEntityForName:@"PKGeoRoute"
                                                       inManagedObjectContext:bgcontext];
    //    // . NSLog(@"idobj = %ld", (long)[dict.mapObjectId integerValue]);
    geoRoute.idgeo = [dict.geoRouteId integerValue];
    
    geoRoute.descriptions = dict.geoRouteDescription;
    geoRoute.title = dict.geoRouteTitle;
    geoRoute.geoJSON = dict.geoRouteGeoJSON;
    // . NSLog(@"geoRoute.title dict.geoRouteTitle == %@", dict.geoRouteTitle);
    geoRoute.prevImageName = dict.geoRouteImagePreview;
    //    product.header_menu = dict.;
    geoRoute.likes = [dict.geoRouteLikes integerValue];
    return geoRoute;
}
-(void)addCoreDataTranslateDictionary:(NSString*)key andValue:(NSString*)value inContext:(NSManagedObjectContext*) bgcontext{
    PKTranslateDictionary* translateDictionary = [NSEntityDescription insertNewObjectForEntityForName:@"PKTranslateDictionary"
                                                       inManagedObjectContext:bgcontext];
    translateDictionary.key = key;
    translateDictionary.value = value;
//    return translateDictionary;
}
-(PKMapObj*)returnCoreDataMapObject:(PKDictionaryMapObject*) dict inContext:(NSManagedObjectContext*) bgcontext{
    PKMapObj*mapobject = [NSEntityDescription insertNewObjectForEntityForName:@"PKMapObj"
                                                       inManagedObjectContext:bgcontext];
//    // . NSLog(@"idobj = %ld", (long)[dict.mapObjectId integerValue]);
    mapobject.idobj = [dict.mapObjectId integerValue];
    
    mapobject.descr = dict.mapObjectDescription;
    mapobject.title = dict.mapObjectTitle;
    mapobject.subtitle = dict.mapObjectSubTitle;
    mapobject.longitude = [NSDecimalNumber decimalNumberWithString:dict.mapObjectLongtitude];
    mapobject.latitude = [NSDecimalNumber decimalNumberWithString:dict.mapObjectLatitude];
    mapobject.prevImageName = dict.mapObjectImagePreview;
    //    product.header_menu = dict.;
    mapobject.likes = [dict.mapObjectLikes integerValue];
    
    mapobject.photoTrue = dict.photoTrue;
    mapobject.freeContent = [dict.freeContent integerValue];
    mapobject.adress      = dict.adress;
    mapobject.email       = dict.email;
    mapobject.phone       = dict.phone;
    
    mapobject.recomended  = [dict.recomended integerValue];
    mapobject.wiFi  = [dict.wifi integerValue];
    mapobject.kidsfriendly = [dict.kidsfriendly integerValue];
    mapobject.payCard  = [dict.paycard integerValue];
    mapobject.siteurl     = dict.siteurl;
    mapobject.geoJSON     = dict.mapObjGeoJSON;
    mapobject.turIndust   = [dict.turIndust integerValue];
    mapobject.avalibleForDisables   = [dict.avalibleForDisables integerValue];
    
    // . NSLog(@"siteurl = %@ == %@",mapobject.siteurl, dict.siteurl);
    
    
    return mapobject;
}

-(PKUserDocs*)returnCoreDataUserDocs:(PKDictionarySettings*) dict{
    PKUserDocs *userDocs = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserDocs"
                                                       inManagedObjectContext:self.managedObjectContext];
    userDocs.title = dict.title;
    userDocs.url = dict.value;
    userDocs.type = dict.type;
    return userDocs;
}
-(PKInformation*)returnCoreDataUserInformation:(PKDictionarySettings*) dict{
    PKInformation *userInfos = [NSEntityDescription insertNewObjectForEntityForName:@"PKInformation" inManagedObjectContext:self.managedObjectContext];
    userInfos.name = dict.title;
    userInfos.value = [dict.value integerValue];
    userInfos.simbolCode = dict.simbolCode;
    return userInfos;
}


//-(PKMapObj*)returnCoreDataProductMenu:(PKDictionaryMapObject*) dict toTheEntity:(NSString*) entityName {
//
//    // // . NSLog(@"AddRandom");
//
//    PKMapObj*product = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:self.managedObjectContext];
//
//    //    NSDateFormatter *formatterdate = [[NSDateFormatter alloc] init];
//    //    [formatterdate setDateFormat:@"yyyy-MM-dd HH:mm"];
//    //
//    //    [formatterdate setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
//
////    @property (nullable, nonatomic, copy) NSString *title;
////    @property (nullable, nonatomic, copy) NSString *descr;
////    @property (nonatomic) int16_t likes;
////    @property (nonatomic) int16_t idobj;
////    @property (nullable, nonatomic, copy) NSDecimalNumber *latitude;
////    @property (nullable, nonatomic, copy) NSDecimalNumber *longitude;
////    @property (nullable, nonatomic, copy) NSString *prevImageName;
////    @property (nullable, nonatomic, retain) PKCity *city;
////    @property (nullable, nonatomic, retain) NSSet<PKTypeObj *> *type;
////    @property (nullable, nonatomic, retain) NSSet<PKKeywords *> *keywords;
////    @property (nullable, nonatomic, retain) NSSet<PKObjImages *> *images;
//
//
////    @property (nonatomic, strong) NSString *mapObjectId;
////    @property (nonatomic, strong) NSString *mapObjectTitle;
////    @property (nonatomic, strong) NSString *mapObjectDescription;
////    @property (nonatomic, strong) NSString *mapObjectLikes;
////    @property (nonatomic, strong) NSString *mapObjectType;
////    @property (nonatomic, strong) NSString *mapObjectCity;
////    @property (nonatomic, strong) NSString *mapObjectLatitude;
////    @property (nonatomic, strong) NSString *mapObjectLongtitude;
////    @property (nonatomic, strong) NSString *mapObjectImagePreview;
////    @property (nonatomic, strong) NSString *mapObjectImageGallery;
////    @property (nonatomic, strong) NSString *mapObjectKeywords;
//    // . NSLog(@"idobj = %ld", (long)[dict.mapObjectId integerValue]);
//    product.idobj = [dict.mapObjectId integerValue];
//    //    product.art_special = [dict.art integerValue];
//    product.descr = dict.mapObjectDescription;
//    product.title = dict.mapObjectTitle;
//    //    product.header_menu = dict.;
//    product.likes = [dict.mapObjectLikes integerValue];
////    product.prevImageName = dict.mapObjectImagePreview;
////    product.price = [dict.price floatValue];
////
////    product.price_Action = [dict.priceAction floatValue];
////    product.price_Month = [dict.priceMonth floatValue];
////    //    product.show = dict.;
////    //    product.slotMachine_use = dict.;
////    product.veight = [dict.veight floatValue];
//
//
//    //    NSNumberFormatter *isf = [[NSNumberFormatter alloc] init];
//    //    isf.numberStyle = NSNumberFormatterDecimalStyle;
//    //    product.isFree = [isf numberFromString:dict.isFree];
//
//    return product;
//}


#pragma mark - User

- (void)logOutUser{
   [self createDefaultUserAndLogout:YES];
//    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
//    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
//    NSEntityDescription * description =
//    [NSEntityDescription entityForName: @"PKUser" inManagedObjectContext:bgcontext];
//    [request setEntity: description ];
//    //[request setResultType:NSDictionaryResultType];
//    NSError* requestError = nil;
//    NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
//
//    for (PKUser*user in resultArray) {
//        user.firstName = firstName;
//        user.lastName = lastName;
//        user.userID = userID;
//        user.clientID = clientID;
//    }
//    [bgcontext updatedObjects];
//    [self saveContextForBGTask:bgcontext];
    
    
    
//    NSArray* allObject = [self allObjectWithEntityName: @"PKUser"];
//    for(PKMapObj* object in allObject){
//        [self.managedObjectContext deleteObject:object];// пометить на удаление
//    }
//    [self saveDefaultContext:NO];

}
-(PKUser*)getUserInContext:(NSManagedObjectContext*)bgcontext{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:bgcontext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i", 1];
    [request setPredicate:predicate];
    [request setEntity: description ];
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    if(resultArray == nil || [resultArray count] == 0){
        return nil;
    } else {
        PKUserOptions* userOptions = [resultArray firstObject];
        return userOptions.user;
    }
}

-(BOOL)isFirstTimeRun{
    // . NSLog(@"\nisFirstTimeRun");
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user == nil){
        // . NSLog(@"\nisFirstTimeRun = YES");
        [self createDefaultUserAndLogout:NO];
        return YES;
    }
    // . NSLog(@"\nisFirstTimeRun = NO");
    return NO;
}
- (BOOL)isFirstTimeJsonLoaded{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user == nil){
        return NO;
    } else {
        return user.firstTimeJsonLoaded;
    }
}
- (BOOL)isSkipVideo{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user == nil){
        return NO;
    } else {
        return user.skipVideo;
    }
}
- (void)setFirstTimeJsonLoaded:(BOOL)jsonDone inManagedContext:(NSManagedObjectContext*)bgcontext{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:bgcontext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i", 1];
    [request setPredicate:predicate];
    [request setEntity: description ];
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    if(resultArray == nil || [resultArray count] == 0){

    } else {
        PKUserOptions* userOptions = [resultArray firstObject];
        // . NSLog(@"userOptions.user.askToDownload = %i",userOptions.user.firstTimeJsonLoaded);
        userOptions.user.firstTimeJsonLoaded = jsonDone;
    }
}
- (void)setSkipVideo{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user != nil){
        user.skipVideo = YES;
        [bgcontext updatedObjects];
        [self saveContextForBGTask:bgcontext];
    }
}
- (void)setUseOfflineMode:(BOOL)offline{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user != nil){
        user.useOflineMode = offline;
        [bgcontext updatedObjects];
        [self saveContextForBGTask:bgcontext];
    }
}
- (void)setAskedToDownload:(BOOL)download{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user != nil){
        // . NSLog(@"user.askToDownload 1 = %i", user.askToDownload);
        user.askToDownload = download;
        // . NSLog(@"user.askToDownload 2 = %i", user.askToDownload);
        [bgcontext updatedObjects];
        [self saveContextForBGTask:bgcontext];
    }
}
-(BOOL)isUseOfflineMode{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user == nil){
        return NO;
    } else {
        return user.useOflineMode;
    }
    
    return NO;
}
-(BOOL)isAskedToDownload{
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    PKUser *user = [self getUserInContext:bgcontext];
    if(user == nil){
        return NO;
    } else {
        // . NSLog(@"user.askToDownload 3 = %i", user.askToDownload);
        return user.askToDownload;
    }

    return NO;
}
- (void)createDefaultUserAndLogout:(BOOL)logout{
    NSString *userID = @"null";
    NSString *firstName = @"Name";
    NSString *lastName = @"My";
    NSString *clientID = @"null";
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    if(logout){
        NSFetchRequest  * request = [[NSFetchRequest alloc] init];
        NSEntityDescription * description =
        [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:bgcontext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i",  1];
        [request setPredicate:predicate];
        [request setEntity: description ];
        NSError* requestError = nil;
        NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
        if(requestError){
            // . NSLog(@"%@",requestError.localizedDescription);
        }
        
        
        
        PKUserOptions*userOptions = [resultArray firstObject];
        PKUser* user = userOptions.user;
        
        
        PKUser* newUser = [NSEntityDescription insertNewObjectForEntityForName:@"PKUser" inManagedObjectContext:bgcontext];
        newUser.firstName = firstName;
        newUser.lastName = lastName;
        newUser.userID = userID;
        newUser.clientID = clientID;
        newUser.askToDownload = user.askToDownload;
        newUser.useOflineMode = user.useOflineMode;
        newUser.skipVideo = user.skipVideo;
        newUser.firstTimeJsonLoaded = user.firstTimeJsonLoaded;
        
        PKUserOptions* newUserOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions" inManagedObjectContext:bgcontext];
        newUserOptions.assort = 1;
        newUserOptions.optionName = @"User";
        newUserOptions.user = newUser;
        [bgcontext deleteObject:user];
        [bgcontext deleteObject:userOptions];
        [bgcontext insertObject:newUserOptions];
        
        [self saveContextForBGTask:bgcontext];
    } else {
        PKUserOptions*userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions" inManagedObjectContext:bgcontext];
        PKUser* user = [NSEntityDescription insertNewObjectForEntityForName:@"PKUser" inManagedObjectContext:bgcontext];
        user.firstName = firstName;
        user.lastName = lastName;
        user.userID = userID;
        user.clientID = clientID;
        userOptions.assort = 1;
        userOptions.optionName = @"User";
        userOptions.user = user;
        [bgcontext insertObject:userOptions];
        [self saveContextForBGTask:bgcontext];
    }
}
- (NSArray <NSDictionary <NSString*, NSArray<NSNumber*>*> *> *)eventsDateListFromBd{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    [calendar setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    NSDateComponents *dateComponents = [calendar components: NSCalendarUnitYear| NSCalendarUnitMonth| NSCalendarUnitDay fromDate:[NSDate date]];
    
    NSInteger currentMonth = dateComponents.month;
    NSInteger currentYear = dateComponents.year;
    
    NSMutableArray <NSDictionary <NSString*, NSArray<NSNumber*>*> *> *arrayOfMonth = [[NSMutableArray alloc] init];
    
    NSString *year = @"year";
    NSString *yearmw = @"year-month-weekday";
    NSString *yearww = @"year-week-weekday";
    
//    for (int month=1; month<=12; month++) {
//        arrayOfMounth add
//    }
    
    for (int month=0; month<12; month++) {
        
        NSMutableDictionary <NSString*, NSArray<NSNumber*>*> *dictionaryOfMonth = [[NSMutableDictionary alloc] init];
        
        dateComponents.year = currentYear;
        dateComponents.month = currentMonth + month;
        
        dateComponents.day = 1;
        dateComponents.hour = 0;
        dateComponents.minute = 0;
        dateComponents.second = 0;
        NSDate *firstDayOfMonthDate = [calendar dateFromComponents: dateComponents];
//        NSLog(@"ZXLog First day of month: %@", [firstDayOfMonthDate descriptionWithLocale:[NSLocale currentLocale]]);
        
        dateComponents.year = currentYear - 1;
        NSDate *firstDayOfMonthDateYearBefore = [calendar dateFromComponents: dateComponents];
        
        dateComponents.year = currentYear;
        dateComponents.month = dateComponents.month + 1;
        dateComponents.day = 0;
        dateComponents.hour = 23;
        dateComponents.minute = 59;
        dateComponents.second = 59;
        NSDate *lastDayOfMonthDate = [calendar dateFromComponents: dateComponents];
//        NSLog(@"ZXLog First day of month: %@", [lastDayOfMonthDate descriptionWithLocale:[NSLocale currentLocale]]);
        
        dateComponents.year = currentYear - 1;
        NSDate *lastDayOfMonthDateYearBefore = [calendar dateFromComponents: dateComponents];
        
        
        NSFetchRequest  * request = [[NSFetchRequest alloc] init];
        NSEntityDescription * description =
        [NSEntityDescription entityForName: @"PKEvent" inManagedObjectContext:self.managedObjectContext];
        //    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i", 1];
        //    [request setPredicate:predicate];
        NSSortDescriptor* sortAscending =
        [[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES];
        NSSortDescriptor* dateAscending =
        [[NSSortDescriptor alloc] initWithKey:@"dateStart" ascending:YES];
        [request setSortDescriptors:@[dateAscending, sortAscending]];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(dateStart >= %@ AND dateEnd < %@) OR ((dateStart >= %@ AND dateEnd < %@) AND (eventPeriodicity = %@ OR eventPeriodicity = %@ OR eventPeriodicity = %@))", firstDayOfMonthDate, lastDayOfMonthDate, firstDayOfMonthDateYearBefore, lastDayOfMonthDateYearBefore, year, yearmw, yearww];//
        //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(dateStart >= %@ AND dateStart <= %@ AND dateEnd < %@ AND (eventPeriodicity = %@ OR eventPeriodicity = %@ OR eventPeriodicity = %@)) OR ", firstDayOfMonthDate, lastDayOfMonthDate, currentDay, year, yearmw, yearww];//
        //        1. Событие должно начинаться в заданый период (месяц).
        //        2. Ежегодное событие может быть прошлогодним, догда сделаем его "прогнозируемым на следующий год"

        [request setPredicate:predicate];
        
        
        [request setEntity: description ];
        //[request setResultType:NSDictionaryResultType];
        NSError* requestError = nil;
        NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
        
        if(requestError){
            NSLog(@"%@",requestError.localizedDescription);
        }
        
        if(resultArray == nil || [resultArray count] == 0){
            //            return nil;
            //            NSLog(@"ZXHi sresultArray nil");
//            NSLog(@"ZXHi month = %i == nil", month);
        } else {
//            NSLog(@"ZXHi month = %i", month);
            
            NSMutableArray <NSNumber*> *eventIdArray = [[NSMutableArray alloc] init];

            for (PKEvent *event in resultArray) {
                
                
                NSComparisonResult checkTimeBegin = [event.dateStart compare:firstDayOfMonthDate];
                NSComparisonResult checkTimeEnd = [event.dateStart compare:lastDayOfMonthDate];
//                NSLog(@"ZXHi title = %@ dateStart = %@ dateEnd = %@,\n Ascending = %@, Same = %@, Descending = %@", event.title, event.dateStart, event.dateEnd, checkTimeEnd == NSOrderedAscending?@"YES":@"NO", checkTimeBegin == NSOrderedSame?@"YES":@"NO", checkTimeBegin == NSOrderedDescending?@"YES":@"NO");
                if((checkTimeBegin == NSOrderedDescending || checkTimeBegin == NSOrderedSame) && (checkTimeEnd == NSOrderedAscending || checkTimeEnd == NSOrderedSame)) {//настоящее и будущие время
//                    NSLog(@"ZXHi now настоящее и будущие время title = %@ dateStart = %@ dateEnd = %@", event.title, event.dateStart, event.dateEnd);
                     [eventIdArray addObject:[NSNumber numberWithInt:event.idevent]];
                } else { //"прогнозируемым на следующий год"
//                    NSLog(@"ZXHi fut прогнозируемым на следующий год title = %@ dateStart = %@ dateEnd = %@", event.title, event.dateStart, event.dateEnd);
                     [eventIdArray addObject:[NSNumber numberWithInt:event.idevent]];
                }
//                NSLog(@"ZXHi 1 title = %@ dateStart = %@ dateEnd = %@", event.title, event.dateStart, event.dateEnd);
//                [eventIdArray addObject:[NSNumber numberWithInt:event.idevent]];
            }
            
            
            if(eventIdArray.count > 0){
                NSLocale *locale = [NSLocale currentLocale];//[[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"];
                NSDateFormatter *dateFormatterMonth = [[NSDateFormatter alloc] init];
                [dateFormatterMonth setDateFormat:@"LLLL yyyy"];//MMMM yyyy  HH:mm:ssr MMMM EEEE
                [dateFormatterMonth setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
                [dateFormatterMonth setLocale:locale];
                NSString *key = [dateFormatterMonth stringFromDate:firstDayOfMonthDate];
                [dictionaryOfMonth setObject:eventIdArray forKey:key];
                [arrayOfMonth addObject:dictionaryOfMonth];
            }
        }
    }
    return arrayOfMonth;
}
- (NSArray*)informTipsOrNewsFromBd:(BOOL)isTips{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKInformTips" inManagedObjectContext:self.managedObjectContext];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i", 1];
//    [request setPredicate:predicate];
    NSSortDescriptor* sortAscending =
    [[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES];
    [request setSortDescriptors:@[sortAscending]];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"isTips == %i", isTips];
    [request setPredicate:predicate];
    
    
    [request setEntity: description ];
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
//    NSLog(@"resultArray = %@", resultArray);
    if(resultArray == nil || [resultArray count] == 0){
        return nil;
    } else {
        NSMutableArray *symbolCode = [[NSMutableArray alloc] init];
        for (PKInformTips *tips in resultArray) {
//            NSLog(@"\nsymbolCode jj = %@ %i", tips.symbolCode, tips.sort);
            [symbolCode addObject:tips.symbolCode];
        }
        NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:symbolCode];
        NSSet *uniqueStates = [orderedSet set];
        return [uniqueStates allObjects];
    }
    
    
    //    // . NSLog(@"isDefaultUserExist != nil return YES");
    return nil;
}
//- (BOOL)isDefaultUserExist{
//    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
//    NSEntityDescription * description =
//    [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:self.managedObjectContext];
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i", 1];
//    [request setPredicate:predicate];
//    [request setEntity: description ];
//    //[request setResultType:NSDictionaryResultType];
//    NSError* requestError = nil;
//    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
//    if(requestError){
//        // . NSLog(@"%@",requestError.localizedDescription);
//    }
//    if(resultArray == nil || [resultArray count] == 0){
////        // . NSLog(@"isDefaultUserExist == nil return NO");
//        return NO;
//    }
////    // . NSLog(@"isDefaultUserExist != nil return YES");
//    return YES;
//}

//- (void)setDefaultUserWithLogout:(BOOL)logout{
//    if(logout){
//        NSFetchRequest  * request = [[NSFetchRequest alloc] init];
//        NSEntityDescription * description =
//        [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:self.managedObjectContext];
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i",  1];
//        [request setPredicate:predicate];
//        [request setEntity: description ];
//        NSError* requestError = nil;
//        NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
//        if(requestError){
//            // . NSLog(@"%@",requestError.localizedDescription);
//        }
//
//        for (id object in resultArray) {
//            [self.managedObjectContext deleteObject:object];
//        }
//
//
//    }
//    NSString *userID = @"null";
//    NSString *firstName = @"Name";
//    NSString *lastName = @"My";
//    NSString *clientID = @"null";
//    PKUserOptions*userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions" inManagedObjectContext:self.managedObjectContext];
//    PKUser* user = [NSEntityDescription insertNewObjectForEntityForName:@"PKUser" inManagedObjectContext:self.managedObjectContext];
//    user.firstName = firstName;
//    user.lastName = lastName;
//    user.userID = userID;
//    user.clientID = clientID;
//    userOptions.assort = 1;
//    userOptions.optionName = @"User";
//    userOptions.user = user;
//
//
//    [self.managedObjectContext insertObject:userOptions];
//    [self saveDefaultContext:NO];
//
////        [self insertUserWithID: userID
////                  andFirstName: firstName
////                   andLastName: lastName
////                   andClientID: clientID];
////    }
//
//
//
//}




//- (void)insertUserWithID:(NSString*) userID
//            andFirstName:(NSString*) firstName
//             andLastName:(NSString*) lastName
//             andClientID:(NSString*) clientID{
//    PKUserOptions*userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions" inManagedObjectContext:self.managedObjectContext];
//    PKUser* user = [NSEntityDescription insertNewObjectForEntityForName:@"PKUser" inManagedObjectContext:self.managedObjectContext];
//    user.firstName = firstName;
//    user.lastName = lastName;
//    user.userID = userID;
//    user.clientID = clientID;
//    userOptions.assort = 1;
//    userOptions.optionName = @"User";
//    userOptions.user = user;
//
//
//    [self.managedObjectContext insertObject:userOptions];
//    [self saveDefaultContext:NO];
//}




- (void)updateCoreDataUserWithID:(NSString*) userID
                    andFirstName:(NSString*) firstName
                     andLastName:(NSString*) lastName
                     andClientID:(NSString*) clientID{
    // . NSLog(@"updateCoreDataUserWithID = %@, %@, %@, %@", userID, firstName, lastName, clientID);
    
//    [self logOutUser];
    NSManagedObjectContext *bgcontext = [self getContextForBGTask];
    
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKUserOptions" inManagedObjectContext:bgcontext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"assort == %i", 1];
    [request setPredicate:predicate];
    [request setEntity: description ];
    NSError* requestError = nil;
    NSArray* resultArray = [bgcontext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    } else if([resultArray count] == 1){
        PKUserOptions*userOptions = [resultArray firstObject];
        PKUser* user = userOptions.user;

        
        PKUser* newUser = [NSEntityDescription insertNewObjectForEntityForName:@"PKUser" inManagedObjectContext:bgcontext];
        newUser.firstName = firstName;
        newUser.lastName = lastName;
        newUser.userID = userID;
        newUser.clientID = clientID;
        newUser.askToDownload = user.askToDownload;
        newUser.useOflineMode = user.useOflineMode;
        newUser.skipVideo = user.skipVideo;
        newUser.firstTimeJsonLoaded = user.firstTimeJsonLoaded;
        
        PKUserOptions* newUserOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions" inManagedObjectContext:bgcontext];
        newUserOptions.assort = 1;
        newUserOptions.optionName = @"User";
        newUserOptions.user = newUser;
        [bgcontext deleteObject:user];
        [bgcontext deleteObject:userOptions];
        [bgcontext insertObject:newUserOptions];
    } else {
        for (id object in resultArray) {
            [bgcontext deleteObject:object];
        }
        if(clientID == NULL || clientID == nil){
            clientID = @"NULL";
        }
        
        
        PKUserOptions*userOptions = [NSEntityDescription insertNewObjectForEntityForName:@"PKUserOptions" inManagedObjectContext:bgcontext];
        PKUser* user = [NSEntityDescription insertNewObjectForEntityForName:@"PKUser" inManagedObjectContext:bgcontext];
        user.firstName = firstName;
        user.lastName = lastName;
        user.userID = userID;
        user.clientID = clientID;
        userOptions.assort = 1;
        userOptions.optionName = @"User";
        userOptions.user = user;
        [bgcontext insertObject:userOptions];
    }
    
    
    

//
    [self saveContextForBGTask:bgcontext];
}
- (NSArray*)getTypesOfObjectsArrayByDefault{ // Получение списка объектов по умолчанию
    
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKTypeObj" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description ];
    NSPredicate * predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"typeCategory = %i", 1];
    NSPredicate * predicate2;
    predicate2 = [NSPredicate predicateWithFormat:@"typeKey = %i", 3];
//    predicate2 = [NSPredicate predicateWithFormat:@"typeKey = %i OR typeKey = %i OR typeKey = %i OR typeKey = %i", 3,9,14,19];
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    [request setPredicate:predicate];
    
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* typeObjects = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    NSMutableArray* typeObjectsString = [NSMutableArray array];
    
    // . NSLog(@"PKFilterObjects == %lu", (unsigned long)[typeObjects count]);
    for (PKTypeObj* obj in typeObjects) {
        [typeObjectsString addObject:(NSString*) obj.typeObjValue];
    }
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self"
                                                                ascending: YES];
    return [[[NSOrderedSet orderedSetWithArray:typeObjectsString] array] sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
}
- (NSArray*)getTypesOfObjectsArrayGrouped{ // Получение списка возможных типов объектов
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKTypeobjGroup" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description ];
    
    
    NSSortDescriptor* groupSort =
    [[NSSortDescriptor alloc] initWithKey:@"sort" ascending:YES];
    [request setSortDescriptors:@[groupSort]];
    
    
    NSPredicate * predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"ANY type.typeCategory = %i", 1];
    
    NSPredicate * predicate2;
    
    
    
    
//    predicate2 = [NSPredicate predicateWithFormat: @"ANY type.typeKey != %@ AND ANY type.typeKey != %@", @22, @27];
    predicate2 = [NSPredicate predicateWithFormat: @"typeGroupId != %@", @11]; //исключение служебных
    
    
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    
    [request setPredicate:predicate];
    
    
    
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* typeObjects = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    NSMutableArray *typeAaR = [[NSMutableArray alloc] init];
    
    
    
    
    
    NSLog(@"PKFilterObjects == %lu", (unsigned long)[typeObjects count]);
    for (PKTypeobjGroup* objGroup in typeObjects) {
        NSMutableDictionary *typeObjectsGroup = [[NSMutableDictionary alloc] init];
        NSLog(@"objGroup.sort == %lu", (unsigned long)objGroup.sort);
        
        NSMutableArray *typeObjectsString = [NSMutableArray array];
        for (PKTypeObj *obj in [objGroup.type allObjects]) {
            [typeObjectsString addObject:(NSString*) obj.typeObjValue];
        }
        
        NSLog(@"typeObjectsString = %@",typeObjectsString);
        [typeObjectsGroup setObject:typeObjectsString forKey:objGroup.typeGroupTitle];
        [typeAaR addObject:typeObjectsGroup];
    }
    
    for(NSArray *subArray in typeAaR) {
//        NSLog(@"Array in typeAaR: %@",subArray);
        for(NSArray *subArray2 in subArray) {
            NSLog(@"Array in typeAaR: %@",subArray2);
            
        }
    }
//    NSLog(@"typeObjectsGroup = %@",typeObjectsGroup);
//    for(NSArray *subArray in typeObjectsGroup) {
//        NSLog(@"Array in typeObjectsGroup: %@",subArray);
//
//    }
    return typeAaR;
    
}
/*
- (NSArray*)getTypesOfObjectsArray{ // Получение списка возможных типов объектов //////////////////// Старая версия
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKTypeObj" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description ];
    NSPredicate * predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"typeCategory = %i", 1];
    
    NSPredicate * predicate2;
    
    
    
    
    predicate2 = [NSPredicate predicateWithFormat: @"typeKey != %@ AND typeKey != %@", @22, @27];
    
    
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2]];
    
    [request setPredicate:predicate];
//    [request setPredicate:predicate1];
    
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* typeObjects = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    NSMutableArray* typeObjectsString = [NSMutableArray array];
    
    // . NSLog(@"PKFilterObjects == %lu", (unsigned long)[typeObjects count]);
    for (PKTypeObj* obj in typeObjects) {
        [typeObjectsString addObject:(NSString*) obj.typeObjValue];
    }
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self"
                                                                ascending: YES];
    return [[[NSOrderedSet orderedSetWithArray:typeObjectsString] array] sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
    
}
 */
- (NSArray*)getContentRecomendedArrayWithPredicate:(int)predicate{ //Получение типов, рекомендуемых
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description = [NSEntityDescription entityForName: @"PKTypeObj" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description];
    NSPredicate * predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"typeFavorites = %i", predicate];
    [request setPredicate:predicate1];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    
    NSMutableArray* typeObjectsString = [NSMutableArray array];
    
    for (PKTypeObj* obj in resultArray) {
        [typeObjectsString addObject:(NSString*) obj.typeObjValue];
    }
    
    
    return typeObjectsString;
}



- (NSString*)getClientID{
    PKUser* user = [self getCoreDataFacebookUser];
    if(IS_NOT_NIL_OR_NULL(user.clientID)){
        return user.clientID;
    }
    return @"0";
}
- (PKUser*)getCoreDataFacebookUser{
    
    
    NSArray* allObject = [self allObjectWithEntityName: @"PKUser"];
    for(PKUser* user in allObject){
        if(IS_NOT_NIL_OR_NULL(user.clientID)){
            
            return user;
        }
    }
    return nil;
}


#pragma mark - Filter Object Set and Get

-(NSArray*) allFilterObjectsForFilterCategory:(int)category {
    // . NSLog(@"allObjectInEntityName %@",entityName);
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKFilterObjects" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description ];

    NSPredicate * predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"filterCategory = %i", category];
    [request setPredicate:predicate1];
    
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    return resultArray;
}

- (NSArray*) getFilterTypeObjectsArrayForFilterCategory:(int)category { // Получение списка типов объектов по существующему фильтру
//    NSLog(@"Получение списка типов объектов по существующему фильтру");
    NSArray* typeObjects = [self allFilterObjectsForFilterCategory: category];
    if([typeObjects count] == 0 && category == 1){
//        NSLog(@"typeObjects count == 0");
        [self setFilterTypeObjectsArray:nil andFilterCategory:2];
        return [self getTypesOfObjectsArrayByDefault];
    }
    NSMutableArray* typeObjectsString = [NSMutableArray array];
    
    for (PKFilterObjects* obj in typeObjects) {
        [typeObjectsString addObject:(NSString*) obj.nameType];
    }
    NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self"
                                                                ascending: YES];
    return [[[NSOrderedSet orderedSetWithArray:typeObjectsString] array] sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
}
- (void)setFilterTypeObjectsArray:(NSArray*)filterTypeObjectsArray andFilterCategory:(int)category {
    NSArray* allObject = [self allFilterObjectsForFilterCategory: category];
    for(PKFilterObjects* object in allObject){
        [self.managedObjectContext deleteObject:object];// пометить на удаление
    }
    
    for(NSString* nameType in filterTypeObjectsArray){
        [self addFilterTypeObjectsWithName:nameType andFilterCategory:category];
    }
    [self saveDefaultContext:NO];
    //    [self.managedObjectContext save:nil];
}
- (void) addFilterTypeObjectsWithName:(NSString*) nameType andFilterCategory:(int)category{
    
    PKFilterObjects* filterTypeObject =
    [NSEntityDescription insertNewObjectForEntityForName:@"PKFilterObjects"
                                  inManagedObjectContext:self.managedObjectContext];
    filterTypeObject.nameType = nameType;
    filterTypeObject.filterCategory = category;
}

#pragma mark - Core Data stack


- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Visit_Ulyanovsk" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Visit_Ulyanovsk.sqlite"];
    NSError *error = nil;
    // NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error]) {
        // . NSLog(@"error = %@", error);
        // Report any error we got.
        
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]; //Удалить старую базу
        
        [_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]; //Создать базу заново
        
    }
    return _persistentStoreCoordinator;
}


- (NSManagedObjectContext *)managedObjectContext {
    
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        // . NSLog(@"get managedObjectContext");
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    
    _mainPrivateManagedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_mainPrivateManagedObjectContext setPersistentStoreCoordinator:coordinator];
    
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setParentContext:_mainPrivateManagedObjectContext];
    // . NSLog(@"get return managedObjectContext");
    return _managedObjectContext;
}

- (NSManagedObjectContext *)getContextForBGTask {
    NSManagedObjectContext *context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    
    [context setParentContext:self.managedObjectContext];
    return context;
}
#pragma mark - All Object In Entity Name and Delete Objects

-(NSArray*) allObjectWithEntityName: (NSString*) entityName {
    // . NSLog(@"allObjectInEntityName %@",entityName);
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description =
    [NSEntityDescription entityForName: entityName inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description ];
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    return resultArray;
}
- (void) deleteAllObjectsWithEntityName:(NSString*) entityName {
    
    NSArray* allObjects = [self allObjectWithEntityName:entityName];
    
    for (id object in allObjects) {
        [self.managedObjectContext deleteObject:object];
    }
    [self saveDefaultContext:NO];
//    [self.managedObjectContext save:nil];
}
- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.khvesiuk.CoreData" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
#pragma mark - Core Data Saving support

- (void)saveContext {
    [self saveDefaultContext:NO];
//    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
//    if (managedObjectContext != nil) {
//        NSError *error = nil;
//        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
//            // Replace this implementation with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            // . NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
//    }
}
- (void)saveContextForBGTask:(NSManagedObjectContext *)bgTaskContext {
    if (bgTaskContext.hasChanges) {
        
        [bgTaskContext performBlockAndWait:^{
            NSError *error = nil;
            // . NSLog(@"bgcontext context = %@", bgTaskContext);
            [bgTaskContext save:&error];
        }];
        // Save default context
        [self saveDefaultContext:YES];
    }
}
- (void)saveDefaultContext:(BOOL)wait {
    if (_managedObjectContext.hasChanges) {
        [_managedObjectContext performBlockAndWait:^{
            // . NSLog(@"managed context = %@", _managedObjectContext);
            NSError *error = nil;
            [self->_managedObjectContext save:&error];
        }];
    }
    
    // А после сохранения _defaultManagedObjectContext необходимо сохранить его родителя, то есть _daddyManagedObjectContext
    void (^saveMainPrivateManagedObjectContext) (void) = ^{
        NSError *error = nil;
        [self->_mainPrivateManagedObjectContext save:&error];
    };
    if ([_mainPrivateManagedObjectContext hasChanges]) {
        if (wait){
            // . NSLog(@"main context = %@", _mainPrivateManagedObjectContext);
            [_mainPrivateManagedObjectContext performBlockAndWait:saveMainPrivateManagedObjectContext];
        } else {
            [_mainPrivateManagedObjectContext performBlock:saveMainPrivateManagedObjectContext];
        }
    }
}
#pragma mark - Working with MapBox
-(NSInteger)hoursAndMinutes{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *currDate = [NSDate date];
    NSString *dateString = [formatter stringFromDate:currDate];
    NSInteger hoursAndMinutes = [self timeIntervalFromString:(NSString*)dateString];
    return hoursAndMinutes;
}
- (NSArray*) getMapObjectsWithTypesLike:(NSArray<NSString *> *) typesArray andAdditional:(NSArray<NSString *> *)selAdditional orNameLike:(NSString*) name{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKMapObj" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description];
    
    
    NSSortDescriptor* recomendedDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"recomended" ascending:YES];
    
    //    NSSortDescriptor* titleDescriptor =
    //    [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
    
    [request setSortDescriptors:@[recomendedDescriptor]];//, titleDescriptor]];
    
    
    NSPredicate * predicate1;
    
    
    
    if(name != nil){
        predicate1 = [NSPredicate predicateWithFormat:@"types.typeObjValue = %@", name];
    } else {
        if(typesArray == nil){
            typesArray = [self getTypesOfObjectsArrayGrouped];
        }
        
        predicate1 = [NSPredicate predicateWithFormat:@"types.typeObjValue IN %@", typesArray];
        //                      @"ANY types.typeObjValue like 'парк'"];
        //                      @"ANY types.typeObjValue in %@", [typesArray typeObjValue]];
    }
    
    
    NSPredicate * predicate2;
    
    
    
//    predicate2 = [NSPredicate predicateWithFormat: @"types.typeKey != %@ AND types.typeKey != %@", @22, @27];
    predicate2 = [NSPredicate predicateWithFormat: @"types.typegroup.typeGroupId != %@", @11]; //исключение служебных
    
    NSPredicate * predicate3;
    predicate3 = [NSPredicate predicateWithFormat:@"wiFi >= %i", 0]; //пустой предикат
    if([selAdditional count] > 0 && selAdditional!=nil){
        
        
        NSMutableArray<NSPredicate *> *predicateArray = [[NSMutableArray alloc] init];
        for (NSString *selAdditionalString in selAdditional) {
            NSLog(@"selectionsAdditional = %@", selAdditionalString);
            
            NSArray *itemsArray = PKFilterAdditionalArray;
            NSInteger anIndexA = 1000;//[itemsArray indexOfObject:(NSString*) [NSString stringWithFormat:@"%@", selAdditional]];
            NSInteger index = 0;
            for (NSString*string in itemsArray) {
                
                if([selAdditionalString isEqualToString: string]){
                    anIndexA = index;
                }
                NSLog(@"string compare = %@",[selAdditionalString isEqualToString: string]? @"YES":@"NO");
                NSLog(@"itemsArray = (%@) (%@)", string, selAdditionalString);
                index++;
            }
            if(anIndexA == 1000){
//            if(NSNotFound == anIndexA) {
//                NSLog(@"not found");
            } else if([selAdditionalString isEqualToString: [itemsArray objectAtIndex: anIndexA]]){
                NSLog(@"selAdditional isEqualToString anIndexA = %ld", (long)anIndexA);
                defineDayOfWeekFormat
                NSInteger todayInt = defineTodayInt;
                NSInteger hoursAndMinutes = [self hoursAndMinutes];
                switch (anIndexA) {
                    case 0:
                        [predicateArray addObject: [NSPredicate predicateWithFormat: @"SUBQUERY(workingTime, $w, $w.day = %i AND $w.begin <= %i AND $w.end > %i).@count > 0 OR workingTime.@count == 0", todayInt, hoursAndMinutes, hoursAndMinutes]];
                        break;
                    case 1:
                        [predicateArray addObject: [NSPredicate predicateWithFormat: @"avalibleForDisables = %i", 1]];
                        break;
                    case 2:
                        [predicateArray addObject: [NSPredicate predicateWithFormat: @"wiFi = %i", 1]];
                        break;
                    case 3:
                        [predicateArray addObject: [NSPredicate predicateWithFormat: @"payCard = %i", 1]];
                        break;
                    case 4:
                        [predicateArray addObject: [NSPredicate predicateWithFormat: @"kidsfriendly = %i", 1]];
                        break;
                        
                    default:
                        break;
                }
                
                
                
            }
            
        }
       
        predicate3 = [NSCompoundPredicate andPredicateWithSubpredicates:predicateArray];
    }
        
    NSPredicate *predicate = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicate1, predicate2, predicate3]];
    
    [request setPredicate:predicate];
    
    
    
    //[request setResultType:NSDictionaryResultType];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    
    
    return resultArray;
}

- (NSSet<PKMapObj*>*)getMapObjectsFifa2018{
    NSFetchRequest  * request = [[NSFetchRequest alloc] init];
    NSEntityDescription * description =
    [NSEntityDescription entityForName: @"PKMapObj" inManagedObjectContext:self.managedObjectContext];
    [request setEntity: description];
    NSSortDescriptor* recomendedDescriptor =
    [[NSSortDescriptor alloc] initWithKey:@"recomended" ascending:YES];
    [request setSortDescriptors:@[recomendedDescriptor]];//, titleDescriptor]];
    NSPredicate * predicate1;
    predicate1 = [NSPredicate predicateWithFormat:@"types.typeIcon = %i OR types.typeIcon = %i", 10, 9];
    [request setPredicate:predicate1];
    NSError* requestError = nil;
    NSArray* resultArray = [self.managedObjectContext executeFetchRequest:request error:&requestError];
    if(requestError){
        // . NSLog(@"%@",requestError.localizedDescription);
    }
    return [NSSet setWithArray: resultArray];
}
@end


