//
//  PKDataManager.h
//  MTSUSHI
//
//  Created by Petr Khvesiuk on 25.11.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class PKEvent, PKUser, PKMapObj, PKGeoRoute, PKInformTips, PKUserDocs;

@interface PKDataManager : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *mainPrivateManagedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL*)applicationDocumentsDirectory;



+ (PKDataManager*)sharedManager;

- (BOOL) isFirstTimeRun;
- (BOOL) isAskedToDownload;
- (BOOL) isSkipVideo;
- (void) setAskedToDownload:(BOOL)download;
- (BOOL) isUseOfflineMode;
- (void) setSkipVideo;
- (void) setUseOfflineMode:(BOOL)offline;
- (BOOL) isFirstTimeJsonLoaded;
//- (void)setFirstTimeJsonLoaded:(BOOL)jsonDone;
- (NSString*) translateWithMyDictionary:(NSString*) key;

//- (void)addToCoreDataMapObject:(NSArray*) dict toTheEntity:(NSString*) entityName;
- (void) updateCoreDataMapObject:(NSDictionary*) responseObject;

- (NSString*) getClientID;
- (void) updateCoreDataUserWithID:(NSString*) userID
                    andFirstName:(NSString*) firstName
                     andLastName:(NSString*) lastName
                     andClientID:(NSString*) clientID;
- (void) logOutUser;
- (void) updateObjectsForLikedFromArray:(NSArray*) idObjArray;
- (void) updateObjectsForLikedGeoFromArray:(NSArray*) idObjArray;
- (NSArray*) switchObjectLike:(NSManagedObjectID*) objectID;
- (NSArray*) getTypesOfObjectsArrayGrouped;
- (NSArray*) getMapObjectsWithTypesLike:(NSArray<NSString *> *) typesArray andAdditional:(NSArray<NSString *> *)selAdditional orNameLike:(NSString*) name;

- (NSArray*) getContentRecomendedArrayWithPredicate:(int)predicate;

- (NSArray*) getFilterTypeObjectsArrayForFilterCategory:(int)category;
- (void) setFilterTypeObjectsArray:(NSArray*)filterTypeObjectsArray andFilterCategory:(int)category;

- (PKMapObj*) oneMapObject:(NSManagedObjectID*) mapObjectID;
- (PKMapObj*) oneMapObjectWithId:(int) idObj;
- (PKInformTips*) oneInformTipsWithId:(int) idtips andIsTip:(BOOL) isTip;
- (PKGeoRoute*) oneGeoRouteWithId:(int) idRoute;
- (NSArray*) informTipsOrNewsFromBd:(BOOL)isTips;
- (PKEvent*) oneEventWithId:(int)idevent;
- (NSArray<PKUserDocs*>*) getUserDocs;
- (NSSet<PKMapObj*>*) getMapObjectsFifa2018;
- (void)calculateDistanceForMapObjects:(void(^)(NSArray<PKMapObj*>* mapObjects)) success;
- (NSArray <NSDictionary <NSString*, NSArray<NSNumber*>*> *> *)eventsDateListFromBd;

@end
