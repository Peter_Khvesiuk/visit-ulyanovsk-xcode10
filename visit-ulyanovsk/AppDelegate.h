//
//  AppDelegate.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 19.12.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
//#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate>

@property (strong, nonatomic) UIWindow *window;

- (void)registerUserNotification;
//@property (readonly, strong) NSPersistentContainer *persistentContainer;

//- (void)saveContext;


@end

