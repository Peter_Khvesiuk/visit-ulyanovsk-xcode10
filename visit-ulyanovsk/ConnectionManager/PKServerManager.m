//
//  PKServerManager.m
//  MTSUSHI
//
//  Created by Petr Khvesiuk on 25.11.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import "PKServerManager.h"
//#import <AFNetworking/AFNetworking.h>
//#import "PKDictionaryMapObject.h"
#import "PKDataManager.h"
#import "NSString+MD5.h"
#import <CommonCrypto/CommonDigest.h>
#import <SSZipArchive/ZipArchive.h>

@interface PKServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager* requestOperationManager;
//@property (strong, nonatomic) PKAccessToken* accessToken;

@end




@implementation PKServerManager



+ (PKServerManager*) sharedManager {
    
    static PKServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[PKServerManager alloc] init];
    });
    
    
    
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        //NSURL* url = [NSURL URLWithString:@"http://api.app.fitness-forma.ru/"];
        //@"http://mt-sushi.ru/test_php/test.php"];
        
        
        NSURL* url = [NSURL URLWithString:URL_main];//http://test.mt-sushi.ru/php/update.php https://api.vk.com/method/"];
        
        
        self.requestOperationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:url];
    }
    return self;
}
//- (CGFloat) getSizeMainZipDataFromServer{
//    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
//
//    NSURL *URL2 = [NSURL URLWithString:URL_main@"app/mapobj.zip"];
//
//
//    NSMutableURLRequest *requestsize = [NSMutableURLRequest requestWithURL:URL2];
//    requestsize.HTTPMethod = @"HEAD";
//    NSURLSessionDataTask *dataTask  = [manager dataTaskWithRequest:(NSURLRequest *)requestsize
//                                                    uploadProgress:nil
//                                                  downloadProgress:nil
//                                                 completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
//                                                     if (error) {
//                                                         // . NSLog(@"Error: %@", error);
//                                                     } else {
//                                                         // . NSLog(@"%@", response );
//                                                         NSDictionary *responseHeaders = ((NSHTTPURLResponse *)response).allHeaderFields;
//                                                         NSString *actualContentLength = responseHeaders[@"Content-Length"];
//                                                         // . NSLog(@"actualContentLength = %@", actualContentLength );
//
//                                                     }
//                                                 }];
//    [dataTask resume];
//
//
//    return 0.f;
//}




- (void) getMainJSONDataFromServerOnSuccess:(void(^)(BOOL isComplete)) success
                                 inProgress:(void(^)(CGFloat)) progress
                                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {

    // . NSLog(@"detectlanguageCode %@", [self detectlanguageCode]);
    NSString* clientID = [[PKDataManager sharedManager] getClientID];
    
    NSString * iostoken = [NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] dataForKey:@"PushToken"]];
    //Format token as per need:
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@" " withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@">" withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSLog(@"getMainJSONDataFromServerOnSuccess iostoken = %@", iostoken);
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     iostoken,                        @"iostoken",
     [self detectlanguageCode],       @"lang",
     [self basicToken],               @"token",
     [self userHash:clientID],          @"userHash", //clientID, @"clientID",
     //     @(count),      @"count",
     nil];
    [self.requestOperationManager
     POST:@"app/index"
     parameters:params
     progress:^(NSProgress * _Nonnull downloadProgress) {
//         if(downloadProgress.fractionCompleted == 1.0) {}
         if(progress) {
             progress(downloadProgress.fractionCompleted);
         }
     }
     success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
         //NSLog(@"JSON: %@", responseObject);
//         NSLog(@"JSON brand: %@", responseObject[@"brand"]);
         
         /////////CORE DATA UPDATE
         
         
         [[PKDataManager sharedManager] updateCoreDataMapObject: responseObject];
//         [[PKDataManager sharedManager] setFirstTimeJsonLoaded:YES];
         if (success) {
             success(YES);
         }
//         NSMutableArray * objects = [NSMutableArray array];
//         for(id mapobj in [responseObject objectForKey:@"mapobj"]){
//             [objects addObject:[[PKDictionaryMapObject alloc] initWithDictionary:mapobj]];
//         }
//         [[PKDataManager sharedManager] addToCoreDataMapObject:objects toTheEntity:@"PKMapObj"];
         
         
         /////////CORE DATA END UPDATE
//         [refresh endRefreshing];
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         NSLog(@"Error: %@", error);
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)operation.response;
//         // . NSLog(@"status code: %li", (long)httpResponse.statusCode);
         if(error.code==-1009){
             // . NSLog(@"Error 1009 detected %@", error);
         }
         if (failure) {
             failure(error, httpResponse.statusCode);
             
         }
//         [refresh endRefreshing];
     }];
}



//- (void) sendFirstTimeRunonSuccess:(void(^)(PKServerResponse* answer)) success
//                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
//    //    // . NSLog(@"type = %@, string = %@, fromName = %@, fromPhone = %@", type, string1, fromName, fromPhone );
//    NSString * token;
//    token = [[PKPushNotificationsInfo sharedManager] getToken];
//    if(token ==nil || token ==NULL){
//        token = @"empty";
//
//    }
//    NSDictionary* params =
//    [NSDictionary dictionaryWithObjectsAndKeys:
//     @"firstLaunch",   @"type",
//     token, @"apnstoken", nil];
//
//    // . NSLog(@" [[PKPushNotificationsInfo sharedManager] getToken] = %@",token);
//    //     userID,        @"user_ids",
//    //     @"photo_50",   @"fields",
//    //     @"nom",        @"name_case", nil];
//    //
//
//    [self.requestOperationManager
//     POST:@"http://api.app.fitness-forma.ru/connect.php"
//     parameters:params
//     progress:nil
//     success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
//         // . NSLog(@"JSON: %@", responseObject);
//         NSMutableArray * days = [NSMutableArray array];
//         for(id dayArr in [responseObject objectForKey:@"raspisanie"]){
//             [days addObject:[[PKDictionaryCalendar alloc] initWithDictionary:dayArr]];
//         }
//         //// . NSLog(@"days = %@", days);
//         [[PKDataManager sharedManager] addToCoreDataCalendar:days toTheEntity:@"PKCoreWeek"];
//
//         ///////////////
//
//
//
//         NSMutableArray * user = [NSMutableArray array];
//         for(id dayArr in [responseObject objectForKey:@"user"]){
//             [user addObject:[[PKUser alloc] initWithServerResponse:dayArr]];
//         }
//         //// . NSLog(@"days = %@", days);
//         [[PKDataManager sharedManager] addToCoreDataUser:user toTheEntity:@"PKCoreUser"];
//
//         ///////////////
//
//
//
//         NSMutableArray * workhours = [NSMutableArray array];
//         for(id dayArr in [responseObject objectForKey:@"workhours"]){
//             [workhours addObject:[[PKWorkHours alloc] initWithServerResponse:dayArr]];
//         }
//         //// . NSLog(@"days = %@", days);
//         [[PKDataManager sharedManager] addToCoreDataWorkHours:workhours toTheEntity:@"PKCoreWorkHours"];
//
//         ///////////////
//
//         NSMutableArray * workhoursWeek = [NSMutableArray array];
//         for(id dayArr in [responseObject objectForKey:@"workhoursWeek"]){
//             [workhoursWeek addObject:[[PKServerResponse alloc] initWithServerResponse:dayArr]];
//         }
//         //// . NSLog(@"days = %@", days);
//         [[PKDataManager sharedManager] addToCoreDataWorkMain:workhoursWeek toTheEntity:@"PKCoreHours"];
//
//         ///////////////
//         //
//         //         NSMutableArray * days = [NSMutableArray array];
//         //         for(id dayArr in [responseObject objectForKey:@"raspisanie"]){
//         //             [days addObject:[[PKDictionaryCalendar alloc] initWithDictionary:dayArr]];
//         //         }
//         //         // . NSLog(@"days = %@", days);
//         //         [[PKDataManager sharedManager] addToCoreDataCalendar:days toTheEntity:@"PKCoreWeek"];
//         //
//
//
//
//
//
//         NSArray* dictsArray = [responseObject objectForKey:@"response"];
//         //// . NSLog(@"dictsArray: %@", dictsArray);
//         if ([dictsArray count] > 0) {
//             PKServerResponse * serverResponse = [[PKServerResponse alloc] initWithServerResponse:[dictsArray firstObject]];
//             if (success) {
//                 success(serverResponse);
//             }
//         } else {
//             //             if (failure) {
//             //                 failure(nil, operation.response.statusCode);
//             //             }
//         }
//     } failure:^(NSURLSessionTask *operation, NSError *error) {
//         // . NSLog(@"Error: %@", error);
//         //        if (failure) {
//         //            failure(error, operation.response.statusCode);
//         //        }
//     }];
//
//
//
//
//}


#pragma mark - Send Email to register


- (void) sendtoServerEmail:(NSString*) eMail
                 onSuccess:(void(^)(NSDictionary* posts)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    
    NSString * iostoken = [NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] dataForKey:@"PushToken"]];
    //Format token as per need:
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@" " withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@">" withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     eMail,             @"emfaail",
     iostoken,              @"iostoken",
     [self authHash],   @"hash",
     [self basicToken], @"token",
     nil];
    [self.requestOperationManager
     POST:@"app/registration"
     parameters:params
     progress:nil
     success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
         NSLog(@"JSON sendtoServerEmail: %@", responseObject);
         if(success){
             success(responseObject);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         if(failure){
             failure(error,1);
         }
         NSLog(@"Error: %@", error);
     }];
}
- (void) sendtoServerEmail:(NSString*) eMail
               andPassword:(NSString*) password
                 onSuccess:(void(^)(NSDictionary* posts)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{

//    NSString *passwordMD5 = [[NSString stringWithFormat:@"%@phalaenopsis", password] MD5String];
    
    NSString * iostoken = [NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] dataForKey:@"PushToken"]];
    //Format token as per need:
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@" " withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@">" withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     eMail,                     @"email",
     iostoken,                      @"iostoken",
     [self authHash],           @"hash",
     [self passWord:password],  @"password",
     [self basicToken],         @"token",
     nil];
    [self.requestOperationManager
     POST:@"app/registration"
     parameters:params
     progress:nil
     success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
         NSLog(@"JSON sendtoServerEmail andPassword: %@", responseObject);
         NSArray* userLikes = [responseObject objectForKey:@"userLikes"];
         if([userLikes count] > 0){
             [[PKDataManager sharedManager] updateObjectsForLikedFromArray: userLikes];
         }
         
         NSArray* userLikesGeo = [responseObject objectForKey:@"userLikesGeo"];
         if([userLikesGeo count] > 0){
             
             
             [[PKDataManager sharedManager] updateObjectsForLikedGeoFromArray: userLikesGeo];
         }
         if(success){
             success(responseObject);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         if(failure){
             failure(error,1);
         }
         NSLog(@"Error: %@", error);
     }];
}

#pragma mark - Send Facebook UserID to Server for Register

- (void) sendFacebookUserID:(NSString*) userID
                  firstName:(NSString*) firstName
                   lastName:(NSString*) lastName
                  onSuccess:(void(^)(NSString* clientID)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    
        
//        NSString* clientID = [[PKDataManager sharedManager] getClientID];
        if(firstName == nil){
            firstName = @"";
        }
        if(lastName == nil){
            lastName = @"";
        }
    
        NSString * iostoken = [NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] dataForKey:@"PushToken"]];
        //Format token as per need:
        iostoken = [iostoken stringByReplacingOccurrencesOfString:@" " withString:@""];
        iostoken = [iostoken stringByReplacingOccurrencesOfString:@">" withString:@""];
        iostoken = [iostoken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    // . NSLog(@"authToken %@", [self authHash]);
        NSDictionary* params =
        [NSDictionary dictionaryWithObjectsAndKeys:
         [self basicToken],  @"token",
         iostoken,           @"iostoken",
         userID,             @"userID",
         firstName,          @"first",
         lastName,           @"second",
         [self authHash],    @"hash",
         nil];
        [self.requestOperationManager
         POST:@"app/registration"
         parameters:params
         progress:nil
         success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
             NSLog(@"JSON: %@", responseObject);
             
             NSArray* userLikes = [responseObject objectForKey:@"userLikes"];
             if([userLikes count] > 0){
                 [[PKDataManager sharedManager] updateObjectsForLikedFromArray: userLikes];
             }
             
             NSArray* userLikesGeo = [responseObject objectForKey:@"userLikesGeo"];
             if([userLikesGeo count] > 0){
                 
                 
                 [[PKDataManager sharedManager] updateObjectsForLikedGeoFromArray: userLikesGeo];
             }
             
            
             
            NSString* clientID = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"clientId"]];
            
            if (success) {
                success(clientID);
            }
             
             
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             NSLog(@"Error: %@", error);
             
//             if (failure) {
//                 failure(error, operation.response);
//             }
             //         [refresh endRefreshing];
         }];
}


#pragma mark - Send Like via Facebook or Email

- (void) sendLiketoServerFor:(NSString *) objectOrGeo
                       andId:(NSString*) mapObjectID
                andOnSuccess:(void(^)(NSArray* posts)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    if(mapObjectID != nil){
        NSString* clientID = [[PKDataManager sharedManager] getClientID];
        NSLog(@"app/set-like \nobjectOrGeo = %@\nidObj = %@, \nuserHash = %@, \ntoken = %@", objectOrGeo, mapObjectID, [self userHash:clientID], [self basicToken]);
        NSDictionary* params =
        [NSDictionary dictionaryWithObjectsAndKeys:
           mapObjectID,                       objectOrGeo,//@"idObj",
           [self userHash:clientID],          @"userHash", //clientID, @"clientID",
           [self basicToken],                 @"token",
            nil];
        [self.requestOperationManager
         POST:@"app/set-like"
         parameters:params
         progress:nil
         success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
             NSLog(@"JSON: %@", responseObject);
             NSString* error = [responseObject objectForKey:@"error"];
             if(error != 0){
//                 NSAlert
             }
             /////////CORE DATA UPDATE
//             NSArray* userLike = [[NSArray alloc] initWithObjects: mapObjectID, nil];
//             [[PKDataManager sharedManager] updateObjectsForLikedFromArray: userLike];
             /////////CORE DATA END UPDATE
         } failure:^(NSURLSessionTask *operation, NSError *error) {
             if(failure){
                 failure(error,1);
             }
             
         }];
    }
}
//-(void)alertMessageWithServer{
//    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Synchronization"
//                                                                   message:@"Synchronization failed. Please try again later."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//
//    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//
//    [self presentViewController:alert animated:YES completion:nil];
//}
#pragma mark - Tokens and passwords




-(NSString *)basicToken{
    NSDate *currentDate = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    dateFormatter.dateFormat = @"yy-MM dd";
    NSString *dateStr = [dateFormatter stringFromDate:currentDate];
    
    return [self createSHA512:dateStr];
}

-(NSString *)authHash{
    
    //date("Y").".".date("m")."-".date("d")
    NSDate *currentDate = [NSDate date];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:3600*4]];
    dateFormatter.dateFormat = @"yyyy.MM-dd";
    NSString *dateStr = [dateFormatter stringFromDate:currentDate];
    return [self createSHA512:dateStr];
}
-(NSString *)userHash:(NSString*)clientId{
//    NSLog(@"clientId = %@ \nuserHashString = %@", clientId, [NSString stringWithFormat:@"%@phalaenopsis", clientId]);
//    $anketa->id)."phalaenopsis"
    return [self createSHA512:[NSString stringWithFormat:@"%@phalaenopsis", clientId]];
}

-(NSString *)passWord:(NSString*)pass{
    //    pass."phalaenopsis"
    return [self createSHA512:[NSString stringWithFormat:@"%@phalaenopsis", pass]];
}

-(NSString *)createSHA512:(NSString *)string {
    NSData *data = [string dataUsingEncoding:NSUTF8StringEncoding];//NSUTF16LittleEndianStringEncoding
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    CC_SHA512(data.bytes, data.length, digest);
    NSMutableString* output = [NSMutableString  stringWithCapacity:CC_SHA512_DIGEST_LENGTH];
    
    for(int i = 0; i < CC_SHA512_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    return output;
}

-(NSString*)detectlanguageCode{
    NSString *locale = [[NSLocale currentLocale] languageCode];
    if(![locale isEqualToString:@"ru"]){
        return @"en";
    }
#warning KOSTYL with multilanguage, beacause if not RU or EN => CRASH
//    // . NSLog(@"[NSLocale currentLocale] = %@", [NSLocale currentLocale]);
//    // . NSLog(@"[NSLocale currentLocale] = %@", [[NSLocale currentLocale] languageCode]);
//    // . NSLog(@"[NSLocale currentLocale] = %@", [[NSLocale currentLocale] localeIdentifier]);
//    // . NSLog(@"[NSLocale currentLocale] = %@", [NSLocale autoupdatingCurrentLocale]);
//    // . NSLog(@"[NSLocale currentLocale] = %@", [[NSLocale systemLocale] languageCode]);
    return locale;
}

#pragma mark - Handshake
- (void)handshakeWithServer
{
    
    NSString * iostoken = [NSString stringWithFormat:@"%@",  [[NSUserDefaults standardUserDefaults] dataForKey:@"PushToken"]];
    //Format token as per need:
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@" " withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@">" withString:@""];
    iostoken = [iostoken stringByReplacingOccurrencesOfString:@"<" withString:@""];
//    id tokenParam = [NSNull null];
//    NSData* token = [[NSUserDefaults standardUserDefaults] dataForKey:@"PushToken"];
//
//    if (token != nil)
//        tokenParam = [token base64EncodedStringWithOptions:0];
//    else
//        tokenParam = @"iOS Simulator";
    
    NSString* clientID = [[PKDataManager sharedManager] getClientID];
    
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
             iostoken,                                                              @"iostoken",
             [self authHash],                                                       @"hash",
             [self basicToken],                                                     @"token",
             [self detectlanguageCode],                                             @"lang",
             [self basicToken],                                                     @"token",
             [self userHash:clientID],                                              @"userHash",
             [UIDevice currentDevice].model,                                        @"model",
             [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"], @"v",
             nil];
    
//    NSLog(@"Handshake request: %@", params);
    
    [self.requestOperationManager
     POST:@"app/handshake"
     parameters:params
     progress:nil
     success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
         NSLog(@"JSON handshake: %@", responseObject);
         
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         NSLog(@"Error: %@", error);
     }];
}
#pragma mark - CommercialRequest

- (void) sendCommercialRequestToServerForOrganization:(NSString*) orgId
                                             withName:(NSString*) name
                                           andMessage:(NSString*) message
                                             andPhone:(NSString*) phone
                                             andEmail:(NSString*) email
                                         onSuccess:(void(^)(NSDictionary* posts)) success
                                            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{

    if(orgId == nil){
        orgId = DefaultOrganization;
    }
    if(name == nil){
        name = DefaultField;
    }
    if(phone == nil){
        phone = DefaultField;
    }
    if(email == nil){
        email = DefaultField;
    }
    if(message == nil){
        message = DefaultField;
    }
    
    NSString* clientID = [[PKDataManager sharedManager] getClientID];
    if(clientID == (id)[NSNull null] || [clientID isEqualToString:@"0"] || [clientID isEqualToString:@"null"]){
        clientID = @"1";//Вася Пупкин
    }
//    NSLog(@"app/commercial orgId = %@, userHash = %@, token = %@, clientID = %@", orgId, [self userHash:clientID], [self basicToken], clientID);
    
    NSDictionary* params =
    [NSDictionary dictionaryWithObjectsAndKeys:
     orgId,                             @"org_id",
     name,                              @"name",
     message,                           @"message",
     phone,                             @"phone",
     email,                             @"email",
     [self userHash:clientID],          @"userHash",
     [self basicToken],                 @"token",
     [self detectlanguageCode],         @"lang",
     clientID,                          @"anketa_id",
     nil];
     
    [self.requestOperationManager
     POST:@"app/commercial"
     parameters:params
     progress:nil
     success:^(NSURLSessionTask *task, NSDictionary* responseObject) {
          NSLog(@"JSON commercial: %@", responseObject);
         if(success){
             success(responseObject);
         }
     } failure:^(NSURLSessionTask *operation, NSError *error) {
         if(failure){
             failure(error,1);
         }
          NSLog(@"Error: %@", error);
     }];
    
    
}



#pragma mark - ZIP DOWNLOADING AND MANAGE
/*zip file*/
- (void) getZipDataWithName:(NSString*)zipname fromServerOnSuccess:(void(^)(BOOL isComplete)) success
                                inProgress:(void(^)(CGFloat)) progress
                                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    
    
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL2 = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@", URL_main,  @"app/", zipname]];//[NSURL URLWithString:URL_main@"app/mapobj.zip"];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:URL2];
    
    //    __weak typeof(self) vc = self;
    NSURLSessionDownloadTask *downloadTask =
    [manager downloadTaskWithRequest:request
                            progress:^(NSProgress * _Nonnull downloadProgress) {
                                if(downloadProgress.fractionCompleted == 1.0) {}
                                if(progress) {
                                    progress(downloadProgress.fractionCompleted);
                                }
                            } destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
                             //        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
                             
                             NSError *error;
                             NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                             NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
                             NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:@"/temp"];
                             
                             if (![[NSFileManager defaultManager] fileExistsAtPath:dataPath])
                                 [[NSFileManager defaultManager] createDirectoryAtPath:dataPath withIntermediateDirectories:NO attributes:nil error:&error]; //Create folder
                             
                             // . NSLog(@" paths1 = %@", [self applicationDocumentsDirectory]);
                             NSURL *documentsDirectoryURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"temp"];
                             
                             return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
                             
                         } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
                             //-(void)unzip:(NSString*)file inProgress:(void(^)(CGFloat)) progress onComplete:(void(^)(BOOL isComplete)) success
                                success(YES);
//                                #warning CHECK COMPLETE DOWNLOAD
                             // . NSLog(@"File downloaded to: %@", filePath);
                         }];
    
    [downloadTask resume];
}
-(NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)removeZipFile:(NSString *)filename
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    //    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSURL *documentsPath = [self applicationDocumentsDirectory];
    NSURL *filePath = [documentsPath URLByAppendingPathComponent:filename];
    NSError *error;
    BOOL success = [fileManager removeItemAtURL:filePath error:&error];
    if (success) {
        // . NSLog(@"removeZipFile success");
        //        UIAlertView *removedSuccessFullyAlert = [[UIAlertView alloc] initWithTitle:@"Congratulations:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        //        [removedSuccessFullyAlert show];
    } else {
        // . NSLog(@"removeZipFile Could not delete file -:%@ ",[error localizedDescription]);
    }
}
-(void)unzip:(NSString*)file inProgress:(void(^)(CGFloat)) progress onComplete:(void(^)(BOOL isComplete)) success{
    
    //    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
    //                                                         NSUserDomainMask, YES);
    //    NSString *documentsDirectory = [paths objectAtIndex:0];
    //    NSURL *paths = [self applicationDocumentsDirectory];
    //    NSString *documentsDirectory = paths.absoluteString;
    //    NSString *filePath = [[[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"temp"] URLByAppendingPathComponent:file].absoluteString;
    
    
    //    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *filePath = [[documentsDirectory stringByAppendingPathComponent:@"/temp"] stringByAppendingPathComponent:file];
    //if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])// . NSLog(@"%@ - fileExists", file);
    
    
    // . NSLog(@"unzip documentsDirectory = %@, filePath = %@", documentsDirectory, filePath);
    //    [SSZipArchive unzipFileAtPath:(NSString *)filePath toDestination:(NSString *)documentsDirectory];
    [SSZipArchive unzipFileAtPath:(NSString *)filePath
                    toDestination:(NSString *)documentsDirectory
                  progressHandler:^(NSString * _Nonnull entry, unz_file_info zipInfo, long entryNumber, long total) {
                      
                      if(progress) {
                          
                          CGFloat progressComleted =  (CGFloat)entryNumber / (CGFloat)total;
                          // . NSLog(@"unzip progressHandler entry = %@, entryNumber = %ld, total = %ld, progressComleted = %f", entry, entryNumber, total, progressComleted);
                          progress(progressComleted);
                      }
                  }
                completionHandler:^(NSString * _Nonnull path, BOOL succeeded, NSError * _Nullable error) {
                    // . NSLog(@"unzip completionHandler path = %@, succeeded = %@, error = %@", path, succeeded?@"YES":@"NO", error);
                    if(succeeded){
//                        [self clearTmpDirectory];
                        if(success){
                            success(succeeded);
                        }
                    }
                    
                }];
}

+ (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
- (void)deleteVideo
{
    NSString *file=[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:VisitUlyanovskVideo];
    if ([[NSFileManager defaultManager] fileExistsAtPath:file]){
        NSError* error = nil;
//        NSLog(@"%@ - fileExists", file);
        [[NSFileManager defaultManager] removeItemAtPath:file error:&error];
//            NSLog(@"remove file %@",file);
        if (error) {
            NSLog(@"error 2 = %@", [error description]);
        }
        if ([[NSFileManager defaultManager] fileExistsAtPath:file]){
             NSLog(@"%@ - fileExists!!!!!!!!", file);
        }
    }
}
- (void)clearTmpDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"/temp/"];
    
    
    //NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath error:NULL];
    NSError* error = nil;
    NSArray* tmpDirectory =  [[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath
                                                                                 error:&error];
    if (error) {
        // . NSLog(@"error 1 = %@", [error localizedDescription]);
    }
    
    
    for (NSString *file in tmpDirectory) {
        NSString *filePathFILE = [filePath stringByAppendingPathComponent:file];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePathFILE]){
            NSError* error = nil;
            // . NSLog(@"%@ - fileExists", filePathFILE);
            [[NSFileManager defaultManager] removeItemAtPath:filePathFILE error:&error];
            // . NSLog(@"remove file %@",file);
            if (error) {
                // . NSLog(@"error 2 = %@", [error localizedDescription]);
            }
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePathFILE]){
                // . NSLog(@"%@ - fileExists!!!!!!!!", filePathFILE);
            }
        }
    }
}
- (void)clearImagesDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0]; // Get documents folder
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"/images/"];
    
    
    //NSArray* tmpDirectory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath error:NULL];
    NSError* error = nil;
    NSArray* tmpDirectory =  [[NSFileManager defaultManager] contentsOfDirectoryAtPath:filePath
                                                                                 error:&error];
    if (error) {
        // . NSLog(@"error 1 = %@", [error localizedDescription]);
    }
    
    
    for (NSString *file in tmpDirectory) {
        NSString *filePathFILE = [filePath stringByAppendingPathComponent:file];
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePathFILE]){
            NSError* error = nil;
            // . NSLog(@"%@ - fileExists", filePathFILE);
            [[NSFileManager defaultManager] removeItemAtPath:filePathFILE error:&error];
            // . NSLog(@"remove file %@",file);
            if (error) {
                // . NSLog(@"error 2 = %@", [error localizedDescription]);
            }
            if ([[NSFileManager defaultManager] fileExistsAtPath:filePathFILE]){
                // . NSLog(@"%@ - fileExists!!!!!!!!", filePathFILE);
            }
        }
    }
}

/*zip file*/


@end
