//
//  PKServerManager.h
//  MTSUSHI
//
//  Created by Petr Khvesiuk on 25.11.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.


#import <AFNetworking/AFNetworking.h>

@interface PKServerManager : AFHTTPSessionManager

//@property (strong, nonatomic, readonly) PKUser* currentUser;

+ (PKServerManager*) sharedManager;

- (void) getMainJSONDataFromServerOnSuccess:(void(^)(BOOL isComplete)) success
                                 inProgress:(void(^)(CGFloat)) progress
                                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) sendLiketoServerFor:(NSString *) objectOrGeo
                       andId:(NSString*) mapObjectID
                andOnSuccess:(void(^)(NSArray* posts)) success
                   onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) getZipDataWithName:(NSString*)zipname
        fromServerOnSuccess:(void(^)(BOOL isComplete)) success
                 inProgress:(void(^)(CGFloat)) progress
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
- (void) unzip:(NSString*)file inProgress:(void(^)(CGFloat)) progress onComplete:(void(^)(BOOL isComplete)) success;
- (void) clearTmpDirectory;
- (void) deleteVideo;
- (void) clearImagesDirectory;
- (void) sendFacebookUserID:(NSString*) userID
                  firstName:(NSString*) firstName
                   lastName:(NSString*) lastName
                  onSuccess:(void(^)(NSString* clientID)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


- (void) sendtoServerEmail:(NSString*) eMail
                 onSuccess:(void(^)(NSDictionary* posts)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) sendtoServerEmail:(NSString*) eMail
               andPassword:(NSString*) password
                 onSuccess:(void(^)(NSDictionary* posts)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void) sendCommercialRequestToServerForOrganization:(NSString*) orgId
                                             withName:(NSString*) name
                                           andMessage:(NSString*) message
                                             andPhone:(NSString*) phone
                                             andEmail:(NSString*) email
                                            onSuccess:(void(^)(NSDictionary* posts)) success
                                            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

- (void)handshakeWithServer;
       //  refresh:(UIRefreshControl *) refresh


//- (void) metrikaCall:(NSString*) number
//           onSuccess:(void(^)(PKServerResponse* answer)) success
//           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
//
//- (void) sendFirstTimeRunonSuccess:(void(^)(PKServerResponse* answer)) success
//                         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//- (void) registrAPNS:(void(^)(NSArray* posts)) success
//           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
//
//
//
//- (void) getFriendsWithOffset:(NSInteger) offset
//                        count:(NSInteger) count
//                      refresh:(UIRefreshControl *) refresh
//                    onSuccess:(void(^)(NSArray* friends)) success
//                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
//
//- (void) sendRequest:(NSString*) fromName withPhoneNumber:(NSString*) fromPhone type:(NSString*) type withAdditionalStr1:(NSString*) string1 andwithAdditionalStr2:(NSString*) string2 andwithAdditionalStr3:(NSString*) string3 andEmail:(NSString*) email
//           onSuccess:(void(^)(PKServerResponse* answer)) success
//           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
//
//
//
////- (void) sendPhoneNumber:(NSString*) phoneNumber onSuccess:(void(^)(PKAuthorizeSMS* sms)) success
////               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
////- (void) sendFreezeCard:(NSString*) phoneNumber onSuccess:(void(^)(PKServerResponse* answer)) success
////               onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
////- (void) sendCode1:(NSString*) code1 withSMSCode2: (NSString*) smsCode2
////            onSuccess:(void(^)(PKAccessToken* token)) success
////            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
//
//
//
//- (void) getActionsSpaOnSuccess:(void(^)(NSArray* posts)) success
//                      onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//
////- (void) authorizeUser:(void(^)(PKUser* user)) completion;
//
//
//
//- (void) getGroupWall:(NSString*) groupID
//           withOffset:(NSInteger) offset
//                count:(NSInteger) count
//            onSuccess:(void(^)(NSArray* posts)) success
//            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//- (void) getSPAUslugi:(NSString*) groupID
//           withOffset:(NSInteger) offset
//                count:(NSInteger) count
//            onSuccess:(void(^)(NSArray* posts)) success
//            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//- (void) getTrener:(NSString*) getTrener
//         onSuccess:(void(^)(NSArray* posts)) success
//         onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;
//
//- (void) getTimeOfWork:(NSString*) getTrener
//             onSuccess:(void(^)(NSArray* posts)) success
//             onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


@end

