//
//  PKDictionaryMapObject.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 20.12.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKDictionaryMapObject : NSObject

@property (nonatomic) int photoTrue;
@property (nonatomic, strong) NSString *mapObjectId;
@property (nonatomic, strong) NSString *mapObjectTitle;
@property (nonatomic, strong) NSString *mapObjectSubTitle;
@property (nonatomic, strong) NSString *mapObjectDescription;
@property (nonatomic, strong) NSString *mapObjectLikes;
@property (nonatomic, strong) NSArray *mapObjectType;
@property (nonatomic, strong) NSString *mapObjectCity;
@property (nonatomic, strong) NSString *mapObjectLatitude;
@property (nonatomic, strong) NSString *mapObjectLongtitude;
@property (nonatomic, strong) NSString *mapObjectImagePreview;
@property (nonatomic, strong) NSArray *mapObjectImageGallery;
@property (nonatomic, strong) NSArray *mapObjectKeywords;
@property (nonatomic, strong) NSString *freeContent;
@property (nonatomic, strong) NSString *wifi;
@property (nonatomic, strong) NSString *kidsfriendly;
@property (nonatomic, strong) NSString *turIndust;
@property (nonatomic, strong) NSString *avalibleForDisables;
@property (nonatomic, strong) NSString *paycard;
@property (nonatomic, strong) NSString *recomended;
@property (nonatomic, strong) NSString *adress;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *siteurl;
@property (nonatomic, strong) NSString *mapObjGeoJSON;

-(id)initWithDictionary: (NSDictionary *) dic;


@end

//{
//    id: "1",
//likes: "6",
//title: "Баку",
//descr: "Азербайджанская, грузинская кухни",
//type: "ресторан",
//city: "Ульяновск",
//lat: "54.310000",
//    long: "48.380000",
//prev: "79.jpg",
//fgall: "24nov.jpg,test.jpg,24nov.jpg,test.jpg",
//kwords: "2, 2, 17, 17"
//},
