//
//  PKDictionaryBrand.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 26/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryBrand.h"

@implementation PKDictionaryBrand
-(id)initWithDictionary: (NSDictionary *) dic{
    

    
    self= [super init];
    if (self){
        
        self.idbrand        = [NSString stringWithFormat:@"%@", dic[@"id"]];
        self.title          = dic[@"title"];
        self.subtitle       = dic[@"subt"];
        self.descript       = dic[@"descr"];
        self.logoimg        = dic[@"img"];
        self.color          = dic[@"color"];
        self.followBrand    = [NSString stringWithFormat:@"%@", dic[@"bfollow"]];
        self.url            = dic[@"url"];
        self.email          = dic[@"email"];
        self.phone          = dic[@"phone"];
        self.sort           = dic[@"sort"];
        self.likes          = dic[@"likes"];
        self.orgIdArray     = dic[@"orgs"];
        self.eventsArray    = dic[@"events"];
        self.objectsIds     = dic[@"mapobjs"];
        self.geoArray       = dic[@"georts"];
        
        
        self.phone = [self checkIntegerForNull:self.phone];
        self.likes = [self checkIntegerForNull:self.likes];
        
        self.title = [self checkStringForNull:self.title];
        self.subtitle = [self checkStringForNull:self.subtitle];
        self.descript = [self checkStringForNull:self.descript];
        
        if (self.logoimg == (id)[NSNull null] || self.logoimg.length == 0 ) {
            self.logoimg = @"no_image.jpg";
        }
        
        
    }
    return self;
    
}
-(NSString*)checkIntegerForNull:(NSString*)string{
    if (string == (id)[NSNull null] || string.length == 0 ) {
        return @"0";
    }
    return string;
}
-(NSString*)checkStringForNull:(NSString*)string{
    if (string == (id)[NSNull null] || string.length == 0 ) {
        return @"";
    }
    return string;
}


@end
