//
//  PKDictionaryMapObject.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 20.12.2017.
//  Copyright © 2017 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryMapObject.h"

@implementation PKDictionaryMapObject
-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.mapObjectId            = [NSString stringWithFormat:@"%@", dic[@"id"]];
        self.mapObjectTitle         = dic[@"title"];
        self.mapObjectSubTitle      = dic[@"subt"];
        self.mapObjectDescription   = dic[@"descr"];
        self.mapObjectLikes         = dic[@"likes"];
        self.mapObjectType          = dic[@"type"];
        self.mapObjectCity          = dic[@"city"];
        self.mapObjectLatitude      = dic[@"lat"];
        self.mapObjectLongtitude    = dic[@"long"];
        self.mapObjectKeywords      = dic[@"kwords"];
        self.mapObjectImageGallery  = dic[@"img"];
        self.mapObjectImagePreview  = dic[@"prev"];
        
        self.freeContent            = dic[@"free_content"];
        self.wifi                   = dic[@"wifi"];
        self.kidsfriendly           = dic[@"kidsfriendly"];
        self.paycard                = dic[@"paycard"];
        self.recomended             = dic[@"recomend"];
        self.adress                 = dic[@"adds"];
        self.email                  = dic[@"email"];
        self.phone                  = dic[@"phone"];
        self.mapObjGeoJSON          = dic[@"geojson"];
        self.siteurl                = dic[@"url"];
        self.turIndust              = dic[@"turIndust"];
        self.avalibleForDisables    = dic[@"avalibleForDisables"];
        if (self.mapObjGeoJSON == (id)[NSNull null] || self.mapObjGeoJSON.length == 0 ) {
            self.mapObjGeoJSON = @"";
        }
        
        self.adress = [self checkStringForNull:self.adress];
        self.email = [self checkStringForNull:self.email];
        self.siteurl = [self checkStringForNull:self.siteurl];
        self.mapObjectTitle = [self checkStringForNull:self.mapObjectTitle];
        self.mapObjectDescription = [self checkStringForNull:self.mapObjectDescription];
        if (self.mapObjectImagePreview == (id)[NSNull null] || self.mapObjectImagePreview.length == 0 ) {
            self.mapObjectImagePreview = @"no_image.jpg";
            self.photoTrue = 0;
        } else {
            self.photoTrue = 1;
        }
        
        self.phone = [self checkIntegerForNull:self.phone];
        self.mapObjectLikes = [self checkIntegerForNull:self.mapObjectLikes];

        
        
    }
    return self;

}
-(NSString*)checkIntegerForNull:(NSString*)string{
    if (string == (id)[NSNull null] || string.length == 0 ) {
        return @"0";
    }
    return string;
}
-(NSString*)checkStringForNull:(NSString*)string{
    if (string == (id)[NSNull null] || string.length == 0 ) {
        return @"";
    }
    return string;
}

@end



//{
//    id: "1",
//likes: "6",
//title: "Баку",
//descr: "Азербайджанская, грузинская кухни",
//type: "ресторан",
//city: "Ульяновск",
//lat: "54.310000",
//    long: "48.380000",
//prev: "79.jpg",
//fgall: "24nov.jpg,test.jpg,24nov.jpg,test.jpg",
//kwords: "2, 2, 17, 17"
//},

