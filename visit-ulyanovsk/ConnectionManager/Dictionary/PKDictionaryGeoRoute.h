//
//  PKDictionaryGeoRoute.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKDictionaryGeoRoute : NSObject

@property (nonatomic, strong) NSString *geoRouteId;
@property (nonatomic, strong) NSString *geoRouteImagePreview;
@property (nonatomic, strong) NSString *geoRouteTitle;
@property (nonatomic, strong) NSString *geoRouteDescription;
@property (nonatomic, strong) NSString *geoRouteLikes;
@property (nonatomic, strong) NSArray *geoRouteType;
@property (nonatomic, strong) NSArray *geoRouteImageGallery;
@property (nonatomic, strong) NSArray *geoRouteMapObjects;
@property (nonatomic, strong) NSString *geoRouteGeoJSON;
@property (nonatomic, strong) NSString *orgId;


-(id)initWithDictionary: (NSDictionary *) dic;

@end


