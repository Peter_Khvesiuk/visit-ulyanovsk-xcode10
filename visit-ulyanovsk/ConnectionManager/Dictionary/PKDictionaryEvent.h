//
//  PKDictionaryEvent.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 13/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKDictionaryEvent : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *prev;
@property (nonatomic, strong) NSArray *orgIdArray;
@property (nonatomic, strong) NSString *idevent;
@property (nonatomic, strong) NSString *descript;
@property (nonatomic, strong) NSArray *imagesArray;
@property (nonatomic, strong) NSString *dateStart;
@property (nonatomic, strong) NSString *dateEnd;
@property (nonatomic, strong) NSString *followEvent;
@property (nonatomic, strong) NSString *sort;
@property (nonatomic, strong) NSArray *objectsIds;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) NSString *ageFrom;
@property (nonatomic, strong) NSString *ageTo;
@property (nonatomic, strong) NSString *eventPeriodicity;



-(id)initWithDictionary: (NSDictionary *) dic;
@end

NS_ASSUME_NONNULL_END
