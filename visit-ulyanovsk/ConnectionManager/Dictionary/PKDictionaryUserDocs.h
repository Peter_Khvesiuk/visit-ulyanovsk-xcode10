//
//  PKDictionaryUserDocs.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 08.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKDictionaryUserDocs : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *type;

-(id)initWithDictionary: (NSDictionary *) dic;

@end
