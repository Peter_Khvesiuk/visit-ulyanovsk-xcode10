//
//  PKDictionarySettings.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 15.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionarySettings.h"

@implementation PKDictionarySettings

-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.simbolCode = dic[@"simb"];
        self.title = dic[@"title"];
        self.type = dic[@"type"];
        self.activityFrom = dic[@"act_from"];
        self.activityTill = dic[@"act_till"];
        self.sort = dic[@"sort"];
        self.value = dic[@"value"];
        if (self.value == (id)[NSNull null] || self.value.length == 0 ) {
            self.value = @"";
        }
        if (self.simbolCode == (id)[NSNull null] || self.simbolCode.length == 0 ) {
            self.simbolCode = @"";
        }
        if (self.title == (id)[NSNull null] || self.title.length == 0 ) {
            self.title = @"";
        }
        if (self.type == (id)[NSNull null] || self.type.length == 0 ) {
            self.type = @"";
        }
        if (self.activityFrom == (id)[NSNull null] || self.activityFrom.length == 0 ) {
            self.activityFrom = @"0";
        }
        if (self.activityTill == (id)[NSNull null] || self.activityTill.length == 0 ) {
            self.activityTill = @"0";
        }
        if (self.sort == (id)[NSNull null] || self.sort.length == 0 ) {
            self.sort = @"0";
        }
        
    }
    return self;
    
}
@end
