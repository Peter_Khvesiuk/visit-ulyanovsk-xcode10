//
//  PKDictionarySettings.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 15.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKDictionarySettings : NSObject

@property (nonatomic, strong) NSString *simbolCode;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *activityFrom;
@property (nonatomic, strong) NSString *activityTill;
@property (nonatomic, strong) NSString *sort;
@property (nonatomic, strong) NSString *value;

-(id)initWithDictionary: (NSDictionary *) dic;

@end
