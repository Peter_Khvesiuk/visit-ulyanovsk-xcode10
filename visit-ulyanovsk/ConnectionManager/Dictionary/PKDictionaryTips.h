//
//  PKDictionaryTips.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKDictionaryTips : NSObject
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *prev;
@property (nonatomic, strong) NSString *orgId;
@property (nonatomic, strong) NSString *idtips;
@property (nonatomic, strong) NSString *descript;
@property (nonatomic, strong) NSArray *imagesArray;
@property (nonatomic, strong) NSString *datetips;
@property (nonatomic, strong) NSString *symbolCode;
@property (nonatomic, strong) NSString *sort;
@property (nonatomic, strong) NSArray *objectsIds;
@property (nonatomic, strong) NSString *isFree;
@property (nonatomic, strong) NSString *ageString;
@property (nonatomic, strong) NSString *dateString;



-(id)initWithDictionary: (NSDictionary *) dic;
@end
