//
//  PKDictionaryCommercialOrg.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 14.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryCommercialOrg.h"

@implementation PKDictionaryCommercialOrg
-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.nameOrg            = dic[@"title"];
        self.siteOrg            = dic[@"site"];
        self.phoneOrg           = dic[@"phone"];
        self.emailOrg           = dic[@"email"];
        self.adressOrg          = dic[@"adds"];
        self.commercialRequest  = dic[@"mes"];
        self.idOrg              = dic[@"id"];
        
        
        if (self.phoneOrg == (id)[NSNull null] || self.phoneOrg.length == 0 ) {
            self.phoneOrg = @"0";
        }
        if (self.emailOrg == (id)[NSNull null] || self.emailOrg.length == 0 ) {
            self.emailOrg = @"";
        }
        if (self.adressOrg == (id)[NSNull null] || self.adressOrg.length == 0 ) {
            self.adressOrg = @"";
        }
        // . NSLog(@"self.siteOrg 1 = >%@< %@",self.siteOrg, self.nameOrg);
        if (self.siteOrg == (id)[NSNull null] || self.siteOrg.length == 0 ) {
            self.siteOrg = @"";
            // . NSLog(@"self.siteOrg 2 = >%@< %@",self.siteOrg, self.nameOrg);
        }
        
    }
    return self;
    
}
@end
