//
//  PKDictionaryGeoRoute.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.04.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryGeoRoute.h"

@implementation PKDictionaryGeoRoute
-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.geoRouteId             = dic[@"id"];
        self.geoRouteTitle          = dic[@"title"];
        self.geoRouteDescription    = dic[@"descr"];
        self.geoRouteLikes          = dic[@"likes"];
        self.geoRouteImagePreview   = dic[@"prev"];
        self.geoRouteImageGallery   = dic[@"img"];
        self.geoRouteType           = dic[@"type"];
        self.geoRouteMapObjects     = dic[@"mapobj"];
        self.geoRouteGeoJSON        = dic[@"geo"];
        self.orgId                  = dic[@"org_id"];
        
        if (self.orgId == (id)[NSNull null] || self.orgId.length == 0 ) {
            self.orgId = @"0";
        }
        if (self.geoRouteImagePreview == (id)[NSNull null] || self.geoRouteImagePreview.length == 0 ) {
            self.geoRouteImagePreview = @"no_image.jpg";
        }
        if (self.geoRouteTitle == (id)[NSNull null] || self.geoRouteTitle.length == 0 ) {
            self.geoRouteTitle = @"";
        }
        if (self.geoRouteDescription == (id)[NSNull null] || self.geoRouteDescription.length == 0 ) {
            self.geoRouteDescription = @"";
        }
        self.geoRouteLikes = [self checkIntegerForNull:self.geoRouteLikes];
    }
    return self;
    
}
-(NSString*)checkIntegerForNull:(NSString*)string{
    if (string == (id)[NSNull null] || string.length == 0 ) {
        return @"0";
    }
    return string;
}
-(NSString*)checkStringForNull:(NSString*)string{
    if (string == (id)[NSNull null] || string.length == 0 ) {
        return @"";
    }
    return string;
}

@end
//{
//georoute: [
//    {
//        id: "1",
//    prev: "lEywhE4s1518151237.jpg",
//    geo: "8WJ0krnv1518088587.geojson",
//    title: "заголовок",
//    descr: "описание",
//    type: 1,
//    img: [
//          "PXKpV6tA1518151523.jpg",
//          "jDkl6Ynk1518151523.jpg"
//          ],mapobj: [
//                     "22",
//                     "23"
//                     ]
//    }
//           ]
//}
