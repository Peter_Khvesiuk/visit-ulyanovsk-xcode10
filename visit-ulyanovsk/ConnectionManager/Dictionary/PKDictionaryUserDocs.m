//
//  PKDictionaryUserDocs.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 08.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryUserDocs.h"

@implementation PKDictionaryUserDocs
-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.url      = dic[@"url"];
        self.title    = dic[@"title"];
        self.type     = dic[@"url"];
    }
    return self;
    
}
@end
