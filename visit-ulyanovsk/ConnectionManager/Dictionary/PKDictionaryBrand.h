//
//  PKDictionaryBrand.h
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 26/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PKDictionaryBrand : NSObject
@property (nonatomic, strong) NSString *idbrand;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *subtitle;
@property (nonatomic, strong) NSString *descript;
@property (nonatomic, strong) NSString *logoimg;
@property (nonatomic, strong) NSString *color;
@property (nonatomic, strong) NSString *followBrand;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *sort;
@property (nonatomic, strong) NSString *likes;
@property (nonatomic, strong) NSArray *orgIdArray;
@property (nonatomic, strong) NSArray *eventsArray;
@property (nonatomic, strong) NSArray *geoArray;
@property (nonatomic, strong) NSArray *objectsIds;

-(id)initWithDictionary: (NSDictionary *) dic;
@end

NS_ASSUME_NONNULL_END
