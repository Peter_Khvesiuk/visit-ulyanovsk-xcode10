//
//  PKDictionaryCommercialOrg.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 14.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PKDictionaryCommercialOrg : NSObject
@property (nonatomic, strong) NSString *nameOrg;
@property (nonatomic, strong) NSString *siteOrg;
@property (nonatomic, strong) NSString *phoneOrg;
@property (nonatomic, strong) NSString *emailOrg;
@property (nonatomic, strong) NSString *adressOrg;
@property (nonatomic, strong) NSString *commercialRequest;
@property (nonatomic, strong) NSString *idOrg;

-(id)initWithDictionary: (NSDictionary *) dic;
@end
