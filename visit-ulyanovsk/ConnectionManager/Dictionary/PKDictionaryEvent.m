//
//  PKDictionaryEvent.m
//  visit-ulyanovsk
//
//  Created by Petr Khvesiuk on 13/09/2019.
//  Copyright © 2019 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryEvent.h"

@implementation PKDictionaryEvent

-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.title          = dic[@"title"];
        self.prev           = dic[@"prev"];
        self.orgIdArray     = dic[@"orgs"];
        self.idevent        = [NSString stringWithFormat:@"%@", dic[@"id"]];
        self.descript       = dic[@"descr"];
        self.imagesArray    = dic[@"img"];
        self.dateStart      = dic[@"dStart"];
        self.dateEnd        = dic[@"dEnd"];
        self.sort           = dic[@"sort"];
        self.objectsIds     = dic[@"mapobj"];
        self.followEvent    = dic[@"eFollow"];
        self.price          = dic[@"price"];
        self.ageFrom        = dic[@"ageFrom"];
        self.ageTo          = dic[@"ageTo"];
        self.eventPeriodicity = dic[@"ePeriod"];
        
        if (self.price == (id)[NSNull null] || self.price.length == 0 ) {
            self.price = @"0";
        }
        if (self.ageFrom == (id)[NSNull null] || self.ageFrom.length == 0 ) {
            self.ageFrom = @"0";
        }
        if (self.ageTo == (id)[NSNull null] || self.ageTo.length == 0 ) {
            self.ageTo = @"0";
        }
        if (self.title == (id)[NSNull null] || self.title.length == 0 ) {
            self.title = @"";
        }
        if (self.descript == (id)[NSNull null] || self.descript.length == 0 ) {
            self.descript = @"";
        }
        if (self.prev == (id)[NSNull null] || self.prev.length == 0 ) {
            self.prev = @"no_image.jpg";
        }
        
        
    }
    return self;
    
}
@end
