//
//  PKDictionaryTips.m
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 17.05.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import "PKDictionaryTips.h"

@implementation PKDictionaryTips

-(id)initWithDictionary: (NSDictionary *) dic{
    
    self= [super init];
    if (self){
        
        self.title          = dic[@"title"];
        self.prev           = dic[@"prev"];
        self.orgId          = [NSString stringWithFormat:@"%@", dic[@"org_id"]];
        self.idtips         = [NSString stringWithFormat:@"%@", dic[@"id"]];
        self.descript       = dic[@"descr"];
        self.imagesArray    = dic[@"img"];
        self.datetips       = dic[@"date"];
        self.symbolCode     = dic[@"symbolCode"];
        self.sort           = dic[@"sort"];
        self.objectsIds     = dic[@"mapobj"];
        
        self.isFree         = dic[@"isFree"];
        self.ageString      = dic[@"ageString"];
        self.dateString     = dic[@"dateString"];
        if (self.isFree == (id)[NSNull null] || self.isFree.length == 0 ) {
            self.isFree = @"0";
        }
        if (self.ageString == (id)[NSNull null] || self.ageString.length == 0 ) {
            self.ageString = @"";
        }
        if (self.dateString == (id)[NSNull null] || self.dateString.length == 0 ) {
            self.dateString = @"";
        }
//        if (self.objectId == (id)[NSNull null] || self.objectId.length == 0 ) {
//            self.objectId = @"";
//        }
        if (self.title == (id)[NSNull null] || self.title.length == 0 ) {
            self.title = @"";
        }
        if (self.orgId == (id)[NSNull null] || self.orgId.length == 0 ) {
            self.orgId = @"0";
        }
        if (self.prev == (id)[NSNull null] || self.prev.length == 0 ) {
            self.prev = @"no_image.jpg";
        }
        
        
    }
    return self;
    
}
@end
