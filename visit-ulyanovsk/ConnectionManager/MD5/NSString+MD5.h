//
//  NSString+MD5.h
//  Visit-Ulyanovsk
//
//  Created by Petr Khvesiuk on 05.02.2018.
//  Copyright © 2018 Petr Khvesiuk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5String;

@end
